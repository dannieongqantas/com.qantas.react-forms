/**
 *  Storybook for react-forms-element
 */

import React from 'react';
import { Provider } from 'react-redux';
import { storiesOf, addDecorator, action } from 'kadira-storybook';

/* Qantas.com Global Styles */

import "!style!css!sass!../use/fonts/qcom.css";
import "!style!css?-url!../node_modules/com.qantas.global-ui/dist/designs/qantas/global/static.css";

/* Components */

import ArrowIcon from '../src/Icons/ArrowIcon';
import BorderTriangle from '../src/Icons/BorderTriangle';
import ButtonLink from '../src/Button/ButtonLink';
import Checkbox from '../src/Checkbox/Checkbox';
import CheckboxIcon from '../src/Icons/CheckboxIcon';
import ClassicRewardsIcon from '../src/Icons/ClassicRewardsIcon';
import ClearButton from '../src/Icons/ClearButton';
import DatePicker from '../src/DatePicker/DatePicker';
import DropdownList from '../src/DropdownList/DropdownList';
import ExclamationIcon from '../src/Icons/ExclamationIcon';
import Form from '../src/Form/Form';
import FormInfoMessage from '../src/FormInfoMessage/FormInfoMessage';
import InfoIcon from '../src/Icons/InfoIcon';
import Input from '../src/Input/Input';
import InputArea from '../src/Input/InputArea';
import LoadingIndicator from '../src/LoadingIndicator/LoadingIndicator';
import MinusIcon from '../src/Icons/MinusIcon';
import NumberPicker from '../src/NumberPicker/NumberPicker';
import PeopleSelector from '../src/PeopleSelector/PeopleSelector';
import PlusIcon from '../src/Icons/PlusIcon';
import Radiobutton from '../src/Radiobutton/Radiobutton';
import RadiobuttonIcon from '../src/Icons/RadiobuttonIcon';
import Radiogroup from '../src/Radiobutton/Radiogroup';
import Select from '../src/Select/Select';
import SubmitButton from '../src/Button/Button';
import Tooltip from '../src/Tooltip/Tooltip';
import TypeaheadWithRedux from '../use/components/TypeaheadWithRedux';
import { currentDate, plusDays } from '../src/utils/date';
import i18nData from '../test/mock/i18n';

import "!style!css!sass!../use/Components/ArrowIcon.scss";
import "!style!css!sass!../use/Components/BorderTriangle.scss";
import "!style!css!sass!../use/Components/Button.scss";
import "!style!css!sass!../use/Components/Checkbox.scss";
import "!style!css!sass!../use/Components/CheckboxIcon.scss";
import "!style!css!sass!../use/Components/ClearButton.scss";
import "!style!css!sass!../use/Components/DatePicker.scss";
import "!style!css!sass!../use/Components/DropdownList.scss";
import "!style!css!sass!../use/Components/InfoIcon.scss";
import "!style!css!sass!../use/Components/InputArea.scss";
import "!style!css!sass!../use/Components/FormValidationMessage.scss";
import "!style!css!sass!../use/Components/LoadingIndicator.scss";
import "!style!css!sass!../use/Components/NumberPicker.scss";
import "!style!css!sass!../use/Components/PeopleSelector.scss";
import "!style!css!sass!../use/Components/Radiobutton.scss";
import "!style!css!sass!../use/Components/Select.scss";
import "!style!css!sass!../use/Components/Tooltip.scss";
import "!style!css!sass!../use/Components/Typeahead.scss";

/* Mocks */

const i18n = { ...i18nData.shared, ...i18nData.car, ...i18nData.flight  };
const peopleSelectorValue = { adults: 2, youths: 2, children: 1, infants: 1 };
const greyBgd = { backgroundColor: 'lightgray', padding: '10px' };

/* Redux */

// Typeahead

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { TYPEAHEAD_ID } from '../use/ActionTypes/locationActionTypes';
import locationReducer from '../use/Reducer/locationReducer';

const store = createStore(locationReducer({ id: TYPEAHEAD_ID }), applyMiddleware(thunk));

/* Stories */

storiesOf('ButtonLink', module)
  .add('default - required props', () => (
    <ButtonLink text="Submit" url="http://www.qantas.com" />
  ))
  .add('optional props', () => (
    <ButtonLink text="Submit" url="http://www.qantas.com" className="my-custom-class" onClick={action('click')} ariaLabel="My Custom Label" />
  ));

storiesOf('Checkbox', module)
  .add('default - required props', () => (
    <Checkbox formLabel="Check it" checked={true} onChange={action('change')} />
  ));

storiesOf('DatePicker', module)
  .add('default - required props', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" />
  ))
  .add('readonly prop', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" readOnly/>
  ))
  .add('minimum date', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} minDate={plusDays(currentDate(), -7)} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" />
  ))
  .add('maximum date', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} daysLimit={7} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" />
  ))
  .add('select date - reverse arrow direction', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} selectedDateArrowDirection="left" ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" />
  ))
  .add('on change event', () => (
    <DatePicker i18n={i18n} id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} onChange={action('change')} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" />
  )).add('chinese format', () => (
    <DatePicker i18n={i18n} languageCodeForDateFormat="zh-cn" id="check-in" minMonthMessage="Minimum month" maxMonthMessage="Maximum month" formLabel="Select a date" initialValue={currentDate()} ariaLabel="DatePicker" ariaToggleNextMonth="Next month" ariaTogglePreviousMonth="Previous month" format="YYYY年 MMM D日 (ddd)" />
  ));

storiesOf('Form', module)
  .add('default - required props', () => (
    <Form className="test-form" onSubmit={action('submit')} onKeyDown={action('keydown')}>
      <Input formLabel="Enter a message" />
      <SubmitButton text="Submit" />
    </Form>
  ));

storiesOf('FormInfoMessage', module)
  .add('default - required props', () => (
    <FormInfoMessage content="Example message" isVisible={true} />
  ))
  .add('validation message', () => (
    <FormInfoMessage className="form-validation-message" content=" " isVisible={true}>
      <div className="form-validation-message__container">
        <div className="form-validation-message__text">Required field</div>
      </div>
    </FormInfoMessage>
  ));

storiesOf('Icons', module)
  .add('arrow icon - right', () => (
    <ArrowIcon direction="right" onClick={action('click')} />
  ))
  .add('arrow icon - left', () => (
    <ArrowIcon direction="left" onClick={action('click')} />
  ))
  .add('arrow icon - up', () => (
    <ArrowIcon direction="up" onClick={action('click')} />
  ))
  .add('arrow icon - down', () => (
    <ArrowIcon direction="down" onClick={action('click')} />
  ))
  .add('arrow icon - resize', () => (
    <ArrowIcon direction="right" onClick={action('click')} size="48px" />
  ))
  .add('arrow icon - with circle', () => (
    <div style={greyBgd}><ArrowIcon direction="right" onClick={action('click')} circle="true" /></div>
  ))
  .add('border triangle - required props', () => (
    <BorderTriangle />
  ))
  .add('border triangle - colours', () => (
    <BorderTriangle innerColor="lightblue" outerColor="blue" />
  ))
  .add('border triangle - resize', () => (
    <BorderTriangle innerSize="18px" outerSize="22px" />
  ))
  .add('checkbox icon - required props', () => (
    <CheckboxIcon checked={true} />
  ))
  .add('classic rewards icon - required props', () => (
    <ClassicRewardsIcon />
  ))
  .add('clear button - required props', () => (
    <ClearButton />
  ))
  .add('exclamation icon - required props', () => (
    <ExclamationIcon />
  ))
  .add('info icon - required props', () => (
    <InfoIcon />
  ))
  .add('minus icon - required props', () => (
    <MinusIcon />
  ))
  .add('plus icon - required props', () => (
    <PlusIcon />
  ))
  .add('radio button icon - required props', () => (
    <div style={greyBgd}><RadiobuttonIcon /></div>
  ));

storiesOf('Input', module)
  .add('default - required props', () => (
    <Input formLabel="Enter a message" />
  ))
  .add('inline label', () => (
    <Input formLabel="Enter a message" isLabelInline={true} />
  ))
  .add('inline oversized label', () => (
    <Input formLabel="Now this is a story all about how My life got flipped-turned upside down And I'd like to take a minute, Just sit right there, I'll tell you how I became the prince of a town called Bel-Air" isLabelInline={true} />
  ))
  .add('event handling', () => (
    <Input formLabel="Enter a message" onChange={action('change')} onBlur={action('blur')} />
  ));

storiesOf('InputArea', module)
  .add('default - required props', () => (
    <InputArea formLabel="Enter a message" ariaLabel="Enter a message" onChange={action('change')} />
  ))
  .add('default - value', () => (
    <InputArea formLabel="Enter a message" ariaLabel="Enter a message" onChange={action('change')} value="Qantas" />
  ))
  .add('inline label', () => (
    <InputArea formLabel="Enter a message" ariaLabel="Enter a message" onChange={action('change')} isLabelInline={true} value="Qantas" />
  ))
  .add('event handling', () => (
    <InputArea formLabel="Enter a message" ariaLabel="Enter a message" onChange={action('change')} onBlur={action('blur')} onFocus={action('focus')} onKeyDown={action('keydown')} />
  ));

storiesOf('LoadingIndicator', module)
  .add('default - required props', () => (
    <LoadingIndicator srText="Loading..." />
  ));

storiesOf('NumberPicker', module)
  .add('default - required props', () => (
    <NumberPicker i18n={i18n} label="Select a number" ariaLabel="Select a number" onChange={action('change')} />
  ))
  .add('initial value', () => (
    <NumberPicker i18n={i18n} label="Select a number" ariaLabel="Select a number" onChange={action('change')} initialValue={2} />
  ))
  .add('minimum value', () => (
    <NumberPicker i18n={i18n} label="Select a number" ariaLabel="Select a number" onChange={action('change')} initialValue={4} rangeStart={2} />
  ))
  .add('maximum value', () => (
    <NumberPicker i18n={i18n} label="Select a number" ariaLabel="Select a number" onChange={action('change')} initialValue={4} rangeStart={2} rangeEnd={6} />
  ));

storiesOf('PeopleSelector', module)
  .add('default - required props', () => (
    <PeopleSelector value={peopleSelectorValue} copy={i18n.passengers} onChange={action('change')} />
  ))

storiesOf('Radiobutton', module)
  .add('default - required props', () => (
    <Radiogroup id="fruits" selectedItem="fruits[0]">
      <Radiobutton formLabel="Apple" id="fruits[0]" />
      <Radiobutton formLabel="(Disabled) Orange" id="fruits[1]" disabled={true} />
      <Radiobutton formLabel="Banana" id="fruits[2]" />
    </Radiogroup>
  ));

storiesOf('Select', module)
  .add('default - required props', () => (
    <Select selectedItem={1} items={[{ text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00' }, { text: '01:00', ariaLabel: 'one hundred', backendCode: '01:00' }]}
    keyDownHandler={action('keydown')} blurInput={action('blur input')} handleHoverItem={action('hover')} focusInput={action('focus')} selectItem={action('select')}
    navigatedItem={0} ariaNavigatedItem={0} blur={action('blur')} formLabel="Select an item" ariaLabel="Select an item" />
  ))
  .add('readonly prop', () => (
    <Select selectedItem={1} items={[{ text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00' }, { text: '01:00', ariaLabel: 'one hundred', backendCode: '01:00' }]}
    keyDownHandler={action('keydown')} blurInput={action('blur input')} handleHoverItem={action('hover')} focusInput={action('focus')} selectItem={action('select')}
    navigatedItem={0} ariaNavigatedItem={0} blur={action('blur')} formLabel="Select an item" ariaLabel="Select an item" readOnly />
  ))
  .add('opened', () => (
    <Select selectedItem={1} items={[{ text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00' }, { text: '01:00', ariaLabel: 'one hundred', backendCode: '01:00' }]}
    keyDownHandler={action('keydown')} blurInput={action('blur input')} handleHoverItem={action('hover')} focusInput={action('focus')} selectItem={action('select')}
    navigatedItem={0} ariaNavigatedItem={0} blur={action('blur')} formLabel="Select an item" ariaLabel="Select an item" opened={true} />
  ))
  .add('disabled prop', () => (
    <Select selectedItem={1} items={[{ text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00' }, { text: '01:00', ariaLabel: 'one hundred', backendCode: '01:00' }]}
    keyDownHandler={action('keydown')} blurInput={action('blur input')} handleHoverItem={action('hover')} focusInput={action('focus')} selectItem={action('select')}
    navigatedItem={0} ariaNavigatedItem={0} blur={action('blur')} formLabel="Select an item" ariaLabel="Select an item" opened={true} disabled/>
  ));

storiesOf('SubmitButton', module)
  .add('default - required props', () => (
    <SubmitButton text="Submit" />
  ))
  .add('optional props', () => (
    <SubmitButton text="Submit" type="button" className="my-custom-class" onClick={action('click')} onBlur={action('blur')} ariaLabel="My Custom Label" />
  ));

storiesOf('Tooltip', module)
  .add('default - required props', () => (
    <Tooltip className="tooltip-center-offset">
      <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
    </Tooltip>
  ))
  .add('left align', () => (
    <Tooltip className="tooltip-left-offset" align="left">
      <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
    </Tooltip>
  ))
  .add('right align', () => (
    <Tooltip className="tooltip-right-offset" align="right">
      <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
    </Tooltip>
  ));

storiesOf('Typeahead', module)
  .addDecorator((getStory) => (<Provider store={store}>{ getStory() }</Provider>))
  .add('default - with redux', () => (
    <TypeaheadWithRedux />
  ));

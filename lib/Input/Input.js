'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

var _FormValidationMessage = require('../FormValidation/FormValidationMessage');

var _FormValidationMessage2 = _interopRequireDefault(_FormValidationMessage);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _calculateSize = require('calculate-size');

var _calculateSize2 = _interopRequireDefault(_calculateSize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* @todo Code including handling isLabelInline duplicated across Input and InputArea. Investigate if we can merge these components or at least have InputArea use Input. */

var Input = function (_Component) {
  _inherits(Input, _Component);

  function Input(props) {
    _classCallCheck(this, Input);

    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this.id = props.id || 'input-' + (0, _idGenerator2.default)();
    _this.state = {
      inlineTextIdent: null
    };
    return _this;
  }

  _createClass(Input, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setInlineTextIdentIfNeeded();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.setInlineTextIdentIfNeeded(); // in case it didn't get set in componentDidMount
    }
  }, {
    key: 'setInlineTextIdentIfNeeded',
    value: function setInlineTextIdentIfNeeded() {
      if (this.props.isLabelInline && !this.state.inlineTextIdent) {
        var formLabel = this.props.formLabel;

        var _window$getComputedSt = window.getComputedStyle(this.labelRefs),
            fontFamily = _window$getComputedSt.fontFamily,
            fontSize = _window$getComputedSt.fontSize,
            fontWeight = _window$getComputedSt.fontWeight,
            paddingRight = _window$getComputedSt.paddingRight;

        var calcWidth = (0, _calculateSize2.default)(formLabel, { font: fontFamily, fontSize: fontSize, fontWeight: fontWeight }).width;
        var inlineTextIdent = parseInt(calcWidth, 10) + parseInt(paddingRight, 10);
        this.setState({ inlineTextIdent: inlineTextIdent + 'px' });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          formLabel = _props.formLabel,
          isLabelInline = _props.isLabelInline,
          containerClassName = _props.containerClassName,
          _onChange = _props.onChange,
          _onBlur = _props.onBlur,
          error = _props.error,
          otherInputProps = _objectWithoutProperties(_props, ['formLabel', 'isLabelInline', 'containerClassName', 'onChange', 'onBlur', 'error']);

      var containerClassNames = (0, _classnames2.default)({
        'widget-form__group--label-inline': isLabelInline
      }, containerClassName);

      var inputClassNames = (0, _classnames2.default)({
        'qfa1-input--validation-error': !!error
      }, 'qfa1-input');

      var inputStyles = {
        textIndent: isLabelInline ? this.state.inlineTextIdent : null
      };

      /* ^ Note: At the moment, we expect `isLabelInline` to be set to `false` when input is focused
          (collapsing/expanding container on homepage).
          If in future, the <input> needs to be focused while isLabelInline === true, we just need to:
          - add `isFocused` to state that reflects whether focused or not
          - expand `inputStyles` `paddingLeft` condition to `(isLabelInline && !isFocused)`
          - add a `display` style to the label like `display: isFocused ? 'none' : ''`
      */

      return _react2.default.createElement(
        'div',
        { className: containerClassNames },
        _react2.default.createElement(
          'label',
          { className: 'widget-form__label', htmlFor: this.id, ref: function ref(node) {
              _this2.labelRefs = node;
            } },
          formLabel
        ),
        _react2.default.createElement('input', _extends({}, otherInputProps, {
          id: this.id,
          className: inputClassNames,
          onChange: function onChange(e) {
            return _onChange(e.target.value);
          },
          onBlur: function onBlur(e) {
            return _onBlur(e.target.value);
          },
          style: inputStyles })),
        _react2.default.createElement(_FormValidationMessage2.default, { hide: !error, message: error })
      );
    }
  }]);

  return Input;
}(_react.Component);

Input.propTypes = {
  formLabel: _propTypes2.default.string.isRequired,
  containerClassName: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  onBlur: _propTypes2.default.func,
  error: _propTypes2.default.string,
  id: _propTypes2.default.string,
  isLabelInline: _propTypes2.default.bool
};
Input.defaultProps = {
  formLabel: '',
  containerClassName: '',
  isLabelInline: false,
  onChange: function onChange() {},
  onBlur: function onBlur() {},
  error: '',
  id: ''
};
exports.default = Input;
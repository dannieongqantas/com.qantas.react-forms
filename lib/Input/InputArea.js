'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InputArea = exports.DEBOUNCE_TIME = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _FormValidationMessage = require('../FormValidation/FormValidationMessage');

var _FormValidationMessage2 = _interopRequireDefault(_FormValidationMessage);

var _WindowResize = require('../ReusableComponents/WindowResize');

var _WindowResize2 = _interopRequireDefault(_WindowResize);

var _breakpoints = require('../grid/breakpoints');

var _breakpoints2 = _interopRequireDefault(_breakpoints);

var _DOM = require('../utils/DOM');

var _userAgent = require('../utils/userAgent');

var _calculateSize = require('calculate-size');

var _calculateSize2 = _interopRequireDefault(_calculateSize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* @todo Code including handling isLabelInline duplicated across Input and InputArea. Investigate if we can merge these components or at least have InputArea use Input. */

// Delay only for touch device
// Adding a debounce time causes an issue for the NVDA screen reader,
// typing 'Sydney' then 'Sydney P' in the typeahead causes it to think there are 20 results
var DEBOUNCE_TIME = exports.DEBOUNCE_TIME = (0, _userAgent.isTouchDevice)() ? 250 : 0;

var InputArea = exports.InputArea = function (_Component) {
  _inherits(InputArea, _Component);

  function InputArea(props) {
    _classCallCheck(this, InputArea);

    var _this = _possibleConstructorReturn(this, (InputArea.__proto__ || Object.getPrototypeOf(InputArea)).call(this, props));

    _this.focus = function () {
      _this.inputElement.focus();
    };

    _this.blur = function () {
      _this.inputElement.blur();
    };

    _this.handleChange = function (event) {
      var value = event.target.value;

      _this.setState({ value: value });
      _this.debouncedOnChange(_extends({}, event));
    };

    _this.debouncedOnChange = function (event) {
      _this.props.onChange(event);
    };

    _this.debouncedOnChange = (0, _debounce2.default)(_this.debouncedOnChange, DEBOUNCE_TIME);

    _this.state = {
      value: props.value,
      inlineLabelWidth: null
    };
    return _this;
  }

  _createClass(InputArea, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setInlineLabelWidthIfNeeded();
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(props) {
      this.setState({ value: props.value });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.focused) {
        this.focus();
      }

      this.inputElement.setAttribute('value', this.props.value);

      this.setInlineLabelWidthIfNeeded(); // in case it didn't get set in componentDidMount
    }
  }, {
    key: 'setInlineLabelWidthIfNeeded',
    value: function setInlineLabelWidthIfNeeded() {
      if (this.props.isLabelInline && !this.state.inlineLabelWidth) {
        var formLabel = this.props.formLabel;

        var el = this.labelElement;

        var _window$getComputedSt = window.getComputedStyle(el),
            fontFamily = _window$getComputedSt.fontFamily,
            fontSize = _window$getComputedSt.fontSize,
            fontWeight = _window$getComputedSt.fontWeight,
            paddingLeft = _window$getComputedSt.paddingLeft,
            paddingRight = _window$getComputedSt.paddingRight;

        var calcWidth = (0, _calculateSize2.default)(formLabel, { font: fontFamily, fontSize: fontSize, fontWeight: fontWeight }).width;
        var inlineLabelWidth = parseInt(calcWidth, 10) + parseInt(paddingLeft, 10) + parseInt(paddingRight, 10);
        this.setState({ inlineLabelWidth: inlineLabelWidth + 'px' });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          popup = _props.popup,
          formLabel = _props.formLabel,
          ariaLabel = _props.ariaLabel,
          activeDescendant = _props.activeDescendant,
          validationMessage = _props.validationMessage,
          windowWidth = _props.windowWidth,
          noEntry = _props.noEntry,
          dropdown = _props.dropdown,
          isLabelInline = _props.isLabelInline,
          props = _objectWithoutProperties(_props, ['children', 'popup', 'formLabel', 'ariaLabel', 'activeDescendant', 'validationMessage', 'windowWidth', 'noEntry', 'dropdown', 'isLabelInline']);

      var validationError = validationMessage && !popup;

      var inputProps = _extends({}, props, {
        autoComplete: 'off',
        'aria-required': this.props.required,
        value: this.state.value
      });

      // don't pass invalid props to the input element
      delete inputProps.focused;

      if (!props.readOnly && !noEntry) {
        inputProps.onChange = this.handleChange;
      } else if (noEntry) {
        // Jaws cant handle readOnly and keyboard actions. Without readOnly, it will bring up keyboard on touch devices.
        if ((0, _userAgent.isTouchDevice)()) {
          inputProps.readOnly = true;
        } else {
          inputProps.onChange = function () {};
        }
      }

      if (typeof popup !== 'undefined') {
        inputProps['aria-haspopup'] = popup;
        inputProps['aria-expanded'] = popup;
      }

      inputProps['aria-invalid'] = !!validationError;
      inputProps.className = (0, _classnames2.default)('qfa1-input', props.className, {
        'qfa1-input--validation-error': validationError
      });

      inputProps.onFocus = function (event) {
        if (windowWidth < _breakpoints2.default.medium) {
          (0, _DOM.scrollToTop)(_this2.labelElement);
        }
        if (props.selectOnFocus) {
          var input = event.target;
          setTimeout(function () {
            // required for Edge and iOS
            input.setSelectionRange(0, input.value.length);
          }, 0);
        }
        props.onFocus(event);
      };

      inputProps.onMouseUp = function (event) {
        if (props.selectOnFocus) {
          event.preventDefault();
        }
      };

      // only add when it exists
      if (activeDescendant) inputProps['aria-activedescendant'] = activeDescendant;

      // Aria Label
      inputProps['aria-label'] = (0, _userAgent.isIE)() ? this.props.value + ', ' : '';
      inputProps['aria-label'] += formLabel + ', ';
      inputProps['aria-label'] += validationError ? validationMessage : ariaLabel;

      var classes = (0, _classnames2.default)({
        'widget-form__group': !dropdown,
        'widget-form__group--label-inline': isLabelInline
      });

      var inputStyles = {
        paddingLeft: isLabelInline ? this.state.inlineLabelWidth : ''
      };
      /* ^ Note: At the moment, we expect `isLabelInline` to be set to `false` when input is focused
          (collapsing/expanding container on homepage).
          If in future, the <input> needs to be focused while isLabelInline === true, we just need to:
          - add `isFocused` to state that reflects whether focused or not
          - expand `inputStyles` `paddingLeft` condition to `(isLabelInline && !isFocused)`
          - add a `display` style to the label like `display: isFocused ? 'none' : ''`
      */

      return _react2.default.createElement(
        'div',
        { className: classes },
        _react2.default.createElement(
          'label',
          { ref: function ref(el) {
              _this2.labelElement = el;
            },
            className: 'widget-form__label',
            htmlFor: inputProps.id },
          formLabel
        ),
        _react2.default.createElement('input', _extends({
          ref: function ref(el) {
            _this2.inputElement = el;
          },
          style: inputStyles
        }, inputProps)),
        children,
        _react2.default.createElement(_FormValidationMessage2.default, { hide: popup, message: validationMessage })
      );
    }
  }]);

  return InputArea;
}(_react.Component);

InputArea.propTypes = {
  activeDescendant: _propTypes2.default.string,
  ariaLabel: _propTypes2.default.string.isRequired,
  autoComplete: _propTypes2.default.oneOf(['off', 'on']),
  children: _propTypes2.default.node,
  className: _propTypes2.default.string,
  dropdown: _propTypes2.default.bool,
  focused: _propTypes2.default.bool,
  focusMe: _propTypes2.default.func,
  formLabel: _propTypes2.default.string.isRequired,
  id: _propTypes2.default.string,
  isLabelInline: _propTypes2.default.bool,
  noEntry: _propTypes2.default.bool,
  onBlur: _propTypes2.default.func,
  onChange: _propTypes2.default.func.isRequired,
  onFocus: _propTypes2.default.func,
  onKeyDown: _propTypes2.default.func,
  popup: _propTypes2.default.bool,
  required: _propTypes2.default.bool,
  type: _propTypes2.default.oneOf(['text']),
  validationMessage: _propTypes2.default.string,
  value: _propTypes2.default.string,
  windowWidth: _propTypes2.default.number,
  selectOnFocus: _propTypes2.default.bool
};
InputArea.defaultProps = {
  activeDescendant: '',
  dropdown: false,
  isLabelInline: false,
  noEntry: false,
  onChange: function onChange() {},
  onFocus: function onFocus() {},
  required: false,
  type: 'text',
  validationMessage: '',
  selectOnFocus: false
};
exports.default = (0, _WindowResize2.default)(InputArea, _breakpoints2.default.medium);
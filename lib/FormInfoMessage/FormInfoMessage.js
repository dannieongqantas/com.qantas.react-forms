'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = FormInfoMessage;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function FormInfoMessage(_ref) {
  var isVisible = _ref.isVisible,
      content = _ref.content,
      className = _ref.className,
      children = _ref.children;

  var showMessage = isVisible && content;
  var messageClass = (0, _classnames2.default)('qfa1-form-msg__container', className);
  return showMessage ? _react2.default.createElement(
    'div',
    { role: 'alert', tabIndex: '0', className: messageClass },
    children,
    content
  ) : null;
}

FormInfoMessage.propTypes = {
  className: _propTypes2.default.string,
  content: _propTypes2.default.string.isRequired,
  isVisible: _propTypes2.default.bool.isRequired,
  children: _propTypes2.default.node
};
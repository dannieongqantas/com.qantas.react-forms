'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InfoIcon = function InfoIcon(_ref) {
  var className = _ref.className,
      width = _ref.width,
      title = _ref.title,
      height = _ref.height,
      fillClass = _ref.fillClass;
  return _react2.default.createElement(
    _Svg2.default,
    {
      backgroundColor: 'transparent',
      className: className,
      width: width,
      height: height,
      'aria-label': title,
      viewBox: '0 0 15 20' },
    _react2.default.createElement(
      'title',
      null,
      title
    ),
    _react2.default.createElement(
      'g',
      { stroke: 'none', strokeWidth: '1', fill: 'none' },
      _react2.default.createElement(
        'g',
        { transform: 'translate(-969.000000, -184.000000)', className: fillClass },
        _react2.default.createElement(
          'g',
          { transform: 'translate(515.000000, 126.000000)' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(0.000000, 57.000000)' },
            _react2.default.createElement('path', { d: 'M462,1 C457.581667,1 454,4.582 454,9 C454,13.4186667 457.581667,17 462,17 C466.418,17 470,13.4186667 470,9 C470,4.582 466.418,1 462,1 L462,1 Z M462,15.6666667 C458.318193,15.6666667 455.333333,12.6821277 455.333333,9.00032044 C455.333333,5.31851318 458.318193,2.33333333 462,2.33333333 C465.681807,2.33333333 468.666667,5.31851318 468.666667,9.00032044 C468.666667,12.6821277 465.681807,15.6666667 462,15.6666667 L462,15.6666667 Z M463.830361,10.8597581 C463.631709,10.8597581 462.85212,11.9427256 462.42717,11.9427256 C462.313849,11.9427256 462.257531,11.853763 462.257531,11.7644792 C462.257531,11.5608607 462.413175,11.2419441 462.498165,11.0508511 L463.518389,8.56696285 C464.028671,7.33112087 463.376739,7 462.767472,7 C461.945558,7 461.208636,7.36933947 460.642034,7.85333476 C460.457377,8.01937694 460,8.66202762 460,8.90579167 C460,8.98704635 460.086355,9.08211112 460.187388,9.08211112 C460.446454,9.08211112 460.953324,7.84080934 461.4776,7.84080934 C461.591262,7.84080934 461.718918,7.95578632 461.619592,8.18445563 L460.627699,10.426828 C460.528373,10.6436142 460.046762,11.3864683 460.046762,11.997966 C460.046762,12.4819612 460.400717,13 460.910999,13 C462.342521,13 464,11.4205117 464,11.0508511 C464,10.9361953 463.901015,10.8597581 463.830361,10.8597581 L463.830361,10.8597581 Z M463.086124,5 C462.503349,5 462,5.48620102 462,6.08074957 C462,6.6350937 462.357895,7 462.900797,7 C463.496651,7 464,6.5407155 464,5.91925043 C464,5.36456559 463.615949,5 463.086124,5 L463.086124,5 Z', id: 'tooltip' })
          )
        )
      )
    )
  );
};

InfoIcon.propTypes = {
  className: _propTypes2.default.string,
  width: _propTypes2.default.string,
  title: _propTypes2.default.string,
  height: _propTypes2.default.string,
  fillClass: _propTypes2.default.string
};

InfoIcon.defaultProps = {
  className: 'svg__tooltip-info',
  title: 'Information icon',
  fillClass: 'svg__tooltip-info__fill',
  width: '20px',
  height: '20px'
};

exports.default = InfoIcon;
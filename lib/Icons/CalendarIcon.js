'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CalendarIcon = function (_Component) {
  _inherits(CalendarIcon, _Component);

  function CalendarIcon() {
    _classCallCheck(this, CalendarIcon);

    return _possibleConstructorReturn(this, (CalendarIcon.__proto__ || Object.getPrototypeOf(CalendarIcon)).apply(this, arguments));
  }

  _createClass(CalendarIcon, [{
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      return _react2.default.createElement(
        _Svg2.default,
        { className: props.className,
          onClick: props.onClick,
          width: props.width,
          height: props.height,
          viewBox: '0 0 30 32' },
        _react2.default.createElement(
          'title',
          null,
          'CALENDAR'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1612.000000, -1745.000000)', className: props.fillClass },
            _react2.default.createElement(
              'g',
              { transform: 'translate(425.000000, 1408.000000)' },
              _react2.default.createElement(
                'g',
                { transform: 'translate(860.000000, 262.000000)' },
                _react2.default.createElement(
                  'g',
                  { transform: 'translate(327.000000, 75.000000)' },
                  _react2.default.createElement('path', { d: 'M10,12 L14,12 L14,16 L10,16 L10,12 Z' }),
                  _react2.default.createElement('path', { d: 'M16,12 L20,12 L20,16 L16,16 L16,12 Z' }),
                  _react2.default.createElement('path', { d: 'M22,12 L26,12 L26,16 L22,16 L22,12 Z' }),
                  _react2.default.createElement('path', { d: 'M4,18 L8,18 L8,22 L4,22 L4,18 Z' }),
                  _react2.default.createElement('path', { d: 'M10,18 L14,18 L14,22 L10,22 L10,18 Z' }),
                  _react2.default.createElement('path', { d: 'M16,18 L20,18 L20,22 L16,22 L16,18 Z' }),
                  _react2.default.createElement('path', { d: 'M22,18 L26,18 L26,22 L22,22 L22,18 Z' }),
                  _react2.default.createElement('path', { d: 'M4,24 L8,24 L8,28 L4,28 L4,24 Z' }),
                  _react2.default.createElement('path', { d: 'M10,24 L14,24 L14,28 L10,28 L10,24 Z' }),
                  _react2.default.createElement('path', { d: 'M16,24 L20,24 L20,28 L16,28 L16,24 Z' }),
                  _react2.default.createElement('path', { d: 'M26,4 L26,1 C26,0.45 25.552,0 25,0 L21,0 C20.448,0 20,0.45 20,1 L20,4 L10,4 L10,1 C10,0.45 9.552,0 9,0 L5,0 C4.448,0 4,0.45 4,1 L4,4 L0,4 L0,32 L30,32 L30,4 L26,4 L26,4 Z M22,2 L24,2 L24,6 L22,6 L22,2 Z M6,2 L8,2 L8,6 L6,6 L6,2 Z M2,10 L28,10 L28,30 L2,30 L2,10 Z' })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return CalendarIcon;
}(_react.Component);

CalendarIcon.propTypes = {
  className: _propTypes2.default.string,
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  fillClass: _propTypes2.default.string,
  onClick: _propTypes2.default.func
};
CalendarIcon.defaultProps = {
  className: '',
  fillClass: '',
  width: '15px',
  height: '16px',
  onClick: function onClick() {}
};
exports.default = CalendarIcon;
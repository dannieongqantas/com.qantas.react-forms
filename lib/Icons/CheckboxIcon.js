'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CheckboxIcon = function (_Component) {
  _inherits(CheckboxIcon, _Component);

  function CheckboxIcon(props) {
    _classCallCheck(this, CheckboxIcon);

    var _this = _possibleConstructorReturn(this, (CheckboxIcon.__proto__ || Object.getPrototypeOf(CheckboxIcon)).call(this, props));

    _this.renderBox = _this.renderBox.bind(_this);
    _this.renderTick = _this.renderTick.bind(_this);
    return _this;
  }

  _createClass(CheckboxIcon, [{
    key: 'renderBox',
    value: function renderBox() {
      var _props = this.props,
          size = _props.size,
          focused = _props.focused;

      var boxClass = (0, _classnames2.default)('checkbox-icon__box-border', {
        'checkbox-icon__box-border--focused': focused
      });

      return _react2.default.createElement(
        _Svg2.default,
        { className: 'checkbox-icon__box',
          width: size + 'px',
          height: size + 'px',
          viewBox: '0 0 ' + size + ' ' + size },
        _react2.default.createElement(
          'title',
          null,
          'Checkbox'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1286.000000, -1544.000000)', className: boxClass, strokeWidth: '4', fill: '#FFFFFF' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(485.000000, 1478.000000)' },
              _react2.default.createElement('rect', { x: '801', y: '66', width: size, height: size })
            )
          )
        )
      );
    }
  }, {
    key: 'renderTick',
    value: function renderTick() {
      var _props2 = this.props,
          size = _props2.size,
          checked = _props2.checked;

      if (!checked) return null;

      return _react2.default.createElement(
        _Svg2.default,
        { className: 'checkbox-icon__tick', width: size + 'px', height: size + 'px', viewBox: '0 0 ' + size * 2 + ' ' + size * 2, version: '1.1', backgroundColor: '' },
        _react2.default.createElement(
          'title',
          null,
          'selected'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1297.000000, -1557.000000)', fill: '#4A4A4A' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(497.000000, 1490.000000)' },
              _react2.default.createElement('path', { d: 'M823.010797,78.5423199 L823.010797,98.365835 L819.170797,98.365835 L819.170797,74.365835 L823.010797,74.365835 L823.010797,74.7023199 L830.10527,74.7023199 L830.10527,78.5423199 L823.010797,78.5423199 Z', transform: 'translate(824.638034, 86.365835) rotate(-135.000000) translate(-824.638034, -86.365835) ' })
            )
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'checkbox-icon', role: 'presentation', onMouseDown: function onMouseDown(e) {
            return e.preventDefault();
          } /* Prevent highlighting when clicking fast */ },
        this.renderBox(),
        this.renderTick()
      );
    }
  }]);

  return CheckboxIcon;
}(_react.Component);

CheckboxIcon.propTypes = {
  size: _propTypes2.default.string,
  checked: _propTypes2.default.bool,
  focused: _propTypes2.default.bool
};
CheckboxIcon.defaultProps = {
  size: '24',
  checked: false,
  focused: false
};
exports.default = CheckboxIcon;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MinusIcon = function (_Component) {
  _inherits(MinusIcon, _Component);

  function MinusIcon() {
    _classCallCheck(this, MinusIcon);

    return _possibleConstructorReturn(this, (MinusIcon.__proto__ || Object.getPrototypeOf(MinusIcon)).apply(this, arguments));
  }

  _createClass(MinusIcon, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _Svg2.default,
        {
          className: this.props.className,
          width: '30px',
          height: '30px',
          viewBox: '0 0 30 6',
          onClick: this.props.onClick,
          incremental: true,
          disabled: this.props.disabled },
        _react2.default.createElement(
          'title',
          null,
          'minus'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-32.000000, -1967.000000)', fill: this.props.color },
            _react2.default.createElement(
              'g',
              { transform: 'translate(15.000000, 1872.000000)' },
              _react2.default.createElement(
                'g',
                { transform: 'translate(0.000000, 50.000000)' },
                _react2.default.createElement('rect', { x: '27', y: '48', width: '10', height: '2' })
              )
            )
          )
        )
      );
    }
  }]);

  return MinusIcon;
}(_react.Component);

MinusIcon.propTypes = {
  className: _propTypes2.default.string,
  color: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
};
MinusIcon.defaultProps = {
  color: '#404040',
  className: '',
  onClick: function onClick() {}
};
exports.default = MinusIcon;
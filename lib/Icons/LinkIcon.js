'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LinkIcon = function (_Component) {
  _inherits(LinkIcon, _Component);

  function LinkIcon() {
    _classCallCheck(this, LinkIcon);

    return _possibleConstructorReturn(this, (LinkIcon.__proto__ || Object.getPrototypeOf(LinkIcon)).apply(this, arguments));
  }

  _createClass(LinkIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          fillClass = _props.fillClass,
          className = _props.className,
          onClick = _props.onClick,
          width = _props.width,
          height = _props.height,
          title = _props.title;

      return _react2.default.createElement(
        _Svg2.default,
        { backgroundColor: 'transparent',
          className: className,
          onClick: onClick,
          width: width,
          height: height,
          'aria-hidden': 'true',
          viewBox: '0 0 15 20' },
        _react2.default.createElement(
          'title',
          null,
          title
        ),
        _react2.default.createElement(
          'g',
          { transform: 'translate(5, 8.000000)', stroke: 'none', strokeWidth: '1', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { className: fillClass },
            _react2.default.createElement('polygon', { points: '0 3 0 3.99939605 9.06000042 3.99939605 6.78459383 6.28999996 7.5 7 11 3.5 7.5 0 6.78459383 0.707000017 9.06276842 3' })
          )
        )
      );
    }
  }]);

  return LinkIcon;
}(_react.Component);

LinkIcon.propTypes = {
  title: _propTypes2.default.string,
  className: _propTypes2.default.string,
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  fillClass: _propTypes2.default.string,
  onClick: _propTypes2.default.func
};
LinkIcon.defaultProps = {
  title: '',
  className: 'svg__link-icon',
  fillClass: 'svg__link-icon__fill',
  width: '20px',
  height: '20px',
  onClick: function onClick() {}
};
exports.default = LinkIcon;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ArrowIcon = function (_Component) {
  _inherits(ArrowIcon, _Component);

  function ArrowIcon(props) {
    _classCallCheck(this, ArrowIcon);

    var _this = _possibleConstructorReturn(this, (ArrowIcon.__proto__ || Object.getPrototypeOf(ArrowIcon)).call(this, props));

    _this.renderLeft = _this.renderLeft.bind(_this);
    _this.renderRight = _this.renderRight.bind(_this);
    _this.renderUp = _this.renderUp.bind(_this);
    _this.renderDown = _this.renderDown.bind(_this);
    return _this;
  }

  _createClass(ArrowIcon, [{
    key: 'renderRight',
    value: function renderRight() {
      var circle = this.props.circle ? _react2.default.createElement('circle', { fill: '#FFFFFF', cx: '24', cy: '24', r: '24' }) : null;
      return _react2.default.createElement(
        'g',
        { transform: 'translate(-562, -240)' },
        _react2.default.createElement(
          'g',
          { transform: 'translate(-5, 176)' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(35, 64)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(532, 0)' },
              circle,
              _react2.default.createElement('path', { d: 'M34,19.8026316 L31.2456897,17 L25,23.3552632 L18.7543103,17 L16,19.8026316 L25,29 L34,19.8026316 Z', transform: 'translate(25, 23) rotate(-90) translate(-25, -23)' })
            )
          )
        )
      );
    }
  }, {
    key: 'renderLeft',
    value: function renderLeft() {
      var circle = this.props.circle ? _react2.default.createElement('circle', { fill: '#FFFFFF', cx: '24', cy: '24', r: '24' }) : null;
      return _react2.default.createElement(
        'g',
        { transform: 'translate(-30, -240)' },
        _react2.default.createElement(
          'g',
          { transform: 'translate(-5, 176)' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(35, 64)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(24, 24) scale(-1, 1) translate(-24, -24) ' },
              circle,
              _react2.default.createElement('path', { d: 'M34,19.8026316 L31.2456897,17 L25,23.3552632 L18.7543103,17 L16,19.8026316 L25,29 L34,19.8026316 Z', transform: 'translate(25, 23) rotate(-90) translate(-25, -23)' })
            )
          )
        )
      );
    }
  }, {
    key: 'renderUp',
    value: function renderUp() {
      return _react2.default.createElement(
        'g',
        { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
        _react2.default.createElement(
          'g',
          { transform: 'translate(-1172, -1755)' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(425, 1408)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(532, 281)' },
              _react2.default.createElement('path', { d: 'M248,87.8026316 L245.24569,85 L239,91.3552632 L232.75431,85 L230,87.8026316 L239,97 L248,87.8026316 Z', transform: 'translate(239, 91) scale(1, -1) translate(-239, -91) ' })
            )
          )
        )
      );
    }
  }, {
    key: 'renderDown',
    value: function renderDown() {
      return _react2.default.createElement(
        'g',
        { transform: 'translate(-1972, -1755)' },
        _react2.default.createElement(
          'g',
          { transform: 'translate(485, 1478)' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(1275, 210)' },
            _react2.default.createElement('path', { d: 'M245,88.8026316 L242.24569,86 L236,92.3552632 L229.75431,86 L227,88.8026316 L236,98 L245,88.8026316 Z' })
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      if (props.hide) return null;
      var svgBody = {
        left: this.renderLeft,
        right: this.renderRight,
        down: this.renderDown,
        up: this.renderUp
      }[props.direction]();

      var className = (0, _classnames2.default)(props.className, 'qfa1-arrow-icon', {
        'qfa1-arrow-icon--disabled': props.disabled
      });

      return _react2.default.createElement(
        _Svg2.default,
        { width: props.size,
          height: props.size,
          viewBox: '0 0 48 48',
          tabbable: props.tabbable,
          role: 'button',
          disabled: props.disabled,
          'aria-label': props['aria-label'],
          className: className,
          type: 'button',
          onClick: props.disabled ? function () {} : props.onClick,
          onMouseDown: function onMouseDown(e) {
            return e.preventDefault();
          } /* Prevent highlighting when clicking fast */
          , backgroundColor: this.props.circle ? '' : '#FFFFFF' },
        _react2.default.createElement(
          'title',
          null,
          'Arrow ',
          props.direction
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          svgBody
        )
      );
    }
  }]);

  return ArrowIcon;
}(_react.Component);

ArrowIcon.propTypes = {
  tabbable: _propTypes2.default.bool,
  onClick: _propTypes2.default.func,
  className: _propTypes2.default.string,
  direction: _propTypes2.default.oneOf(['left', 'right', 'down', 'up']).isRequired,
  hide: _propTypes2.default.bool,
  size: _propTypes2.default.string,
  circle: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  'aria-label': _propTypes2.default.string
};
ArrowIcon.defaultProps = {
  tabbable: false,
  className: '',
  onClick: function onClick() {},
  hide: false,
  size: '24px',
  circle: false
};
exports.default = ArrowIcon;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var INCREMENT_INTERVAL = 250;

var Svg = function (_Component) {
  _inherits(Svg, _Component);

  function Svg(props) {
    _classCallCheck(this, Svg);

    var _this = _possibleConstructorReturn(this, (Svg.__proto__ || Object.getPrototypeOf(Svg)).call(this, props));

    _this.handleKeyDown = _this.handleKeyDown.bind(_this);
    _this.incrementalStart = _this.incrementalStart.bind(_this);
    _this.incrementalEnd = _this.incrementalEnd.bind(_this);

    _this.interval = null;
    return _this;
  }

  _createClass(Svg, [{
    key: 'handleKeyDown',
    value: function handleKeyDown(e) {
      if (this.props.disabled) return;

      if ([_KeyCodes.RETURN_KEY_CODE, _KeyCodes.SPACE_KEY_CODE].indexOf(e.keyCode) > -1) {
        e.preventDefault();
        e.stopPropagation();
        this.props.onClick();
      }
    }
  }, {
    key: 'incrementalStart',
    value: function incrementalStart() {
      this.props.onClick();
      this.interval = setInterval(this.props.onClick, INCREMENT_INTERVAL);
    }
  }, {
    key: 'incrementalEnd',
    value: function incrementalEnd(e) {
      e.preventDefault();
      clearInterval(this.interval);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          tabbable = _props.tabbable,
          backgroundColor = _props.backgroundColor,
          className = _props.className,
          role = _props.role,
          onClick = _props.onClick,
          incremental = _props.incremental,
          props = _objectWithoutProperties(_props, ['tabbable', 'backgroundColor', 'className', 'role', 'onClick', 'incremental']);

      var a11yProps = {
        'aria-hidden': true,
        role: 'presentation',
        tabIndex: '-1'
      };

      var style = { backgroundColor: backgroundColor };

      var containerDivProps = {
        tabIndex: tabbable ? '0' : '-1',
        'aria-label': props['aria-label']
      };

      var incrementalProps = {};
      if (incremental) {
        incrementalProps.onTouchStart = this.incrementalStart;
        incrementalProps.onTouchEnd = this.incrementalEnd;
      }

      return _react2.default.createElement(
        'div',
        _extends({ className: className, onKeyDown: this.handleKeyDown }, containerDivProps, { role: role, onClick: onClick }),
        _react2.default.createElement(
          'svg',
          _extends({}, props, { version: '1.1' }, a11yProps, incrementalProps, { style: style, focusable: 'false' }),
          this.props.children
        )
      );
    }
  }]);

  return Svg;
}(_react.Component);

Svg.propTypes = {
  tabbable: _propTypes2.default.bool,
  backgroundColor: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  onClick: _propTypes2.default.func,
  children: _propTypes2.default.oneOfType([_propTypes2.default.element, _propTypes2.default.arrayOf(_propTypes2.default.element)]),
  className: _propTypes2.default.string,
  role: _propTypes2.default.string,
  incremental: _propTypes2.default.bool
};
Svg.defaultProps = {
  tabbable: false,
  disabled: false,
  incremental: false,
  // Background color for IE10, so transparent area can be clicked
  backgroundColor: '#fff'
};
exports.default = Svg;
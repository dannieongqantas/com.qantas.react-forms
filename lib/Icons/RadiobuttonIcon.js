'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RadiobuttonIcon = function (_Component) {
  _inherits(RadiobuttonIcon, _Component);

  function RadiobuttonIcon(props) {
    _classCallCheck(this, RadiobuttonIcon);

    var _this = _possibleConstructorReturn(this, (RadiobuttonIcon.__proto__ || Object.getPrototypeOf(RadiobuttonIcon)).call(this, props));

    _this.renderBox = _this.renderBox.bind(_this);
    _this.renderTick = _this.renderTick.bind(_this);
    return _this;
  }

  _createClass(RadiobuttonIcon, [{
    key: 'renderBox',
    value: function renderBox() {
      var _props = this.props,
          size = _props.size,
          focused = _props.focused,
          disabled = _props.disabled;

      var boxClass = (0, _classnames2.default)('radiobutton-icon__box-border', {
        'radiobutton-icon__box-border--focused': focused,
        'radiobutton-icon__box-border--disabled': disabled
      });

      return _react2.default.createElement(
        _Svg2.default,
        { backgroundColor: '',
          className: 'radiobutton-icon__box',
          width: size + 'px',
          height: size + 'px',
          viewBox: '0 0 ' + size + ' ' + size },
        _react2.default.createElement(
          'title',
          null,
          'Radiobutton'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '2', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1286.000000, -1543.000000)', className: boxClass },
            _react2.default.createElement(
              'g',
              { transform: 'translate(485.000000, 1478.000000)' },
              _react2.default.createElement('circle', { cx: '813', cy: '77', r: '10', fill: '#ffffff', width: '24', height: '24' })
            )
          )
        )
      );
    }
  }, {
    key: 'renderTick',
    value: function renderTick() {
      var _props2 = this.props,
          size = _props2.size,
          checked = _props2.checked;

      if (!checked) return null;

      return _react2.default.createElement(
        _Svg2.default,
        { className: 'radiobutton-icon__check', backgroundColor: '', width: size + 'px', fill: 'none', height: size + 'px', viewBox: '0 0 ' + size * 2 + ' ' + size * 2, version: '1.1' },
        _react2.default.createElement(
          'title',
          null,
          'selected'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1297.000000, -1555.000000)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(497.000000, 1490.000000)' },
              _react2.default.createElement('circle', { cx: '824', cy: '89', r: '8', fill: '#4A4A4A', width: size, height: size })
            )
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        {
          className: 'radiobutton-icon',
          role: 'presentation',
          onMouseDown: function onMouseDown(e) {
            return e.preventDefault();
          },
          onClick: this.props.onClick
        },
        this.renderBox(),
        this.renderTick()
      );
    }
  }]);

  return RadiobuttonIcon;
}(_react.Component);

RadiobuttonIcon.propTypes = {
  size: _propTypes2.default.string,
  checked: _propTypes2.default.bool,
  focused: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
};
RadiobuttonIcon.defaultProps = {
  size: '24',
  checked: false,
  focused: false,
  disabled: false
};
exports.default = RadiobuttonIcon;
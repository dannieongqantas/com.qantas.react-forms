'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ExclamationIcon = function (_Component) {
  _inherits(ExclamationIcon, _Component);

  function ExclamationIcon() {
    _classCallCheck(this, ExclamationIcon);

    return _possibleConstructorReturn(this, (ExclamationIcon.__proto__ || Object.getPrototypeOf(ExclamationIcon)).apply(this, arguments));
  }

  _createClass(ExclamationIcon, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Todo: After react upgrade check if this will work
      this.refs.text.setAttribute('font-weight', '600'); // For some reason font-weight doesnt work as a prop
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.refs.text.setAttribute('font-weight', '600');
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _Svg2.default,
        { className: this.props.className, width: '20px', height: '20px', viewBox: '0 0 20 20', backgroundColor: '' },
        _react2.default.createElement(
          'title',
          null,
          'error icon'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1.5', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-30.000000, -432.000000)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(15.000000, 98.000000)' },
              _react2.default.createElement(
                'g',
                { transform: 'translate(0.000000, 318.000000)' },
                _react2.default.createElement(
                  'g',
                  null,
                  _react2.default.createElement(
                    'g',
                    { transform: 'translate(16.000000, 17.000000)' },
                    _react2.default.createElement('path', { d: 'M16.601377,8.89555594 C17.1328743,8.40101081 17.1328743,7.59898919 16.601377,7.10444406 C16.4622516,6.974939 3.04502015,-0.825426978 2.96573664,-0.86244336 L2.95573489,-0.868351342 L2.95506062,-0.868037644 C2.77474822,-0.949912862 2.57505042,-1 2.36130521,-1 C1.68736726,-1 1.13176459,-0.543150924 1.02303773,0.0553851516 L1,15.7336006 C1,16.4329383 1.6094323,17 2.36102427,17 C2.57342092,17 2.77177017,16.9508017 2.95067783,16.8702335 L16.601377,8.89555594 Z', stroke: '#323232', transform: 'translate(9.000000, 8.000000) rotate(-90.000000) translate(-9.000000, -8.000000) ' }),
                    _react2.default.createElement(
                      'text',
                      { ref: 'text', fontFamily: 'Arial Black, Gadget, sans-serif', fontSize: '11', fill: '#323232' },
                      _react2.default.createElement(
                        'tspan',
                        { x: '7.2', y: '13' },
                        '!'
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ExclamationIcon;
}(_react.Component);

ExclamationIcon.propTypes = {
  className: _propTypes2.default.string
};
ExclamationIcon.defaultProps = {
  className: ''
};
exports.default = ExclamationIcon;
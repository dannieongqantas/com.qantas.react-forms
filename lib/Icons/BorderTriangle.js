'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BorderTriangle = function (_Component) {
  _inherits(BorderTriangle, _Component);

  function BorderTriangle() {
    _classCallCheck(this, BorderTriangle);

    return _possibleConstructorReturn(this, (BorderTriangle.__proto__ || Object.getPrototypeOf(BorderTriangle)).apply(this, arguments));
  }

  _createClass(BorderTriangle, [{
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      var innerStyles = {
        borderBottomColor: props.innerColor,
        left: props.positionFromLeft,
        borderBottomWidth: props.innerSize,
        borderRightWidth: props.innerSize,
        borderLeftWidth: props.innerSize,
        marginLeft: '-' + props.innerSize,
        zIndex: props.zIndex + 1
      };

      var outerStyles = {
        borderBottomColor: props.outerColor,
        left: props.positionFromLeft,
        borderBottomWidth: props.outerSize,
        borderRightWidth: props.outerSize,
        borderLeftWidth: props.outerSize,
        marginLeft: '-' + props.outerSize,
        zIndex: props.zIndex
      };

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('span', { className: 'qfa1-border-triangle__outer', style: outerStyles }),
        _react2.default.createElement('span', { className: 'qfa1-border-triangle__inner', style: innerStyles })
      );
    }
  }]);

  return BorderTriangle;
}(_react.Component);

BorderTriangle.propTypes = {
  innerColor: _propTypes2.default.string,
  outerColor: _propTypes2.default.string,
  positionFromLeft: _propTypes2.default.string,
  innerSize: _propTypes2.default.string,
  outerSize: _propTypes2.default.string,
  zIndex: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
};
BorderTriangle.defaultProps = {
  innerColor: '#f4f5f6',
  outerColor: '#dadada',
  positionFromLeft: '50%',
  innerSize: '9px',
  outerSize: '11px',
  zIndex: 300
};
exports.default = BorderTriangle;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PlusIcon = function (_Component) {
  _inherits(PlusIcon, _Component);

  function PlusIcon() {
    _classCallCheck(this, PlusIcon);

    return _possibleConstructorReturn(this, (PlusIcon.__proto__ || Object.getPrototypeOf(PlusIcon)).apply(this, arguments));
  }

  _createClass(PlusIcon, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _Svg2.default,
        {
          className: this.props.className,
          width: '30px',
          height: '30px',
          viewBox: '0 0 30 30',
          onClick: this.props.onClick,
          incremental: true,
          disabled: this.props.disabled },
        _react2.default.createElement(
          'title',
          null,
          'plus'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-278.000000, -1963.000000)', fill: this.props.color },
            _react2.default.createElement(
              'g',
              { transform: 'translate(15.000000, 1872.000000)' },
              _react2.default.createElement(
                'g',
                { transform: 'translate(10.000000, 60.000000)' },
                _react2.default.createElement('path', {
                  d: 'M269,45 L269,41 L267,41 L267,45 L263,45 L263,47 L267,47 L267,51 L269,51 L269,47 L273,47 L273,45 L269,45 Z' })
              )
            )
          )
        )
      );
    }
  }]);

  return PlusIcon;
}(_react.Component);

PlusIcon.propTypes = {
  className: _propTypes2.default.string,
  color: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
};
PlusIcon.defaultProps = {
  color: '#323232',
  className: '',
  onClick: function onClick() {}
};
exports.default = PlusIcon;
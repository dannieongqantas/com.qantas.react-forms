'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CalendarStarIcon = function (_Component) {
  _inherits(CalendarStarIcon, _Component);

  function CalendarStarIcon() {
    _classCallCheck(this, CalendarStarIcon);

    return _possibleConstructorReturn(this, (CalendarStarIcon.__proto__ || Object.getPrototypeOf(CalendarStarIcon)).apply(this, arguments));
  }

  _createClass(CalendarStarIcon, [{
    key: 'render',
    value: function render() {
      var props = this.props;
      return _react2.default.createElement(
        _Svg2.default,
        {
          className: props.className,
          onClick: props.onClick,
          width: props.width,
          height: props.height,
          viewBox: '0 0 306 294',
          backgroundColor: 'transparent'
        },
        _react2.default.createElement(
          'title',
          null,
          'Star'
        ),
        _react2.default.createElement(
          'g',
          { id: 'Symbols', stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { id: 'star', fill: '#323232' },
            _react2.default.createElement(
              'g',
              { id: 'rate-star-button' },
              _react2.default.createElement('polygon', {
                id: 'Path',
                points: '153 224.775 247.35 293.625 211.65 181.425 306 115.125 191.25 115.125 153 0.375 114.75 115.125 0 115.125 94.35 181.425 58.65 293.625'
              })
            )
          )
        )
      );
    }
  }]);

  return CalendarStarIcon;
}(_react.Component);

CalendarStarIcon.propTypes = {
  className: _propTypes2.default.string,
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  fillClass: _propTypes2.default.string,
  onClick: _propTypes2.default.func
};
CalendarStarIcon.defaultProps = {
  className: '',
  fillClass: '',
  width: '13px',
  height: '12px',
  onClick: function onClick() {}
};
exports.default = CalendarStarIcon;
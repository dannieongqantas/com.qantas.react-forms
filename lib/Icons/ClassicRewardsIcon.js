'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ClassicRewardsIcon = function (_Component) {
  _inherits(ClassicRewardsIcon, _Component);

  function ClassicRewardsIcon() {
    _classCallCheck(this, ClassicRewardsIcon);

    return _possibleConstructorReturn(this, (ClassicRewardsIcon.__proto__ || Object.getPrototypeOf(ClassicRewardsIcon)).apply(this, arguments));
  }

  _createClass(ClassicRewardsIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          fillClass = _props.fillClass,
          className = _props.className,
          onClick = _props.onClick,
          width = _props.width,
          height = _props.height,
          title = _props.title;

      return _react2.default.createElement(
        _Svg2.default,
        { backgroundColor: 'transparent',
          className: className,
          onClick: onClick,
          width: width,
          height: height,
          'aria-label': title,
          viewBox: '0 0 15 20' },
        _react2.default.createElement(
          'title',
          null,
          'Classic Rewards'
        ),
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', className: fillClass, fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-406.000000, -921.000000)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(126.000000, 504.000000)' },
              _react2.default.createElement(
                'g',
                { transform: 'translate(0.000000, 56.000000)' },
                _react2.default.createElement(
                  'g',
                  { transform: 'translate(29.000000, 198.000000)' },
                  _react2.default.createElement('path', { d: 'M256.725433,171.893457 L256.725433,171.893457 C258.930695,171.893457 260.718413,170.105738 260.718413,167.900476 C260.718413,165.695214 258.930695,163.907496 256.725433,163.907496 C254.52017,163.907496 252.732452,165.695214 252.732452,167.900476 C252.732452,170.105738 254.52017,171.893457 256.725433,171.893457 L256.725433,171.893457 L256.725433,171.893457 Z M253.220439,171.325342 C252.357065,170.441896 251.824957,169.233326 251.824957,167.900476 C251.824957,165.194018 254.018975,163 256.725433,163 C259.431891,163 261.625909,165.194018 261.625909,167.900476 C261.625909,170.304947 259.894193,172.304954 257.609919,172.721341 L255.324674,179 L253.961565,176.017112 L251,177.425948 C251,177.425948 252.094055,174.420057 253.22044,171.325339 L253.220439,171.325342 Z M258.22495,170.033094 C258.202844,170.033094 258.180737,170.027702 258.160518,170.016649 L256.742217,169.243225 L255.323915,170.016649 C255.278624,170.041451 255.223359,170.037946 255.181303,170.008023 C255.139248,169.978099 255.11795,169.926879 255.126577,169.875929 L255.384842,168.326923 L254.355556,167.297668 C254.31997,167.262083 254.30703,167.209246 254.322396,167.161261 C254.337763,167.113275 254.37874,167.077691 254.428614,167.069334 L255.978474,166.811077 L256.621711,165.524642 C256.644626,165.479083 256.691265,165.450238 256.742217,165.450238 C256.793169,165.450238 256.840077,165.479083 256.862723,165.524642 L257.505959,166.811077 L259.05582,167.069334 C259.105693,167.077691 259.146671,167.113006 259.162037,167.161261 C259.177404,167.209515 259.164733,167.262083 259.128878,167.297668 L258.099591,168.326923 L258.357857,169.875929 C258.366214,169.926879 258.344916,169.978099 258.30313,170.008023 C258.279946,170.024467 258.252448,170.033094 258.22495,170.033094 L258.22495,170.033094 Z M260.411742,172.388862 L262.218941,177.425948 L259.257376,176.017112 L257.894268,179 L256.968378,176.518283 C256.968378,176.518283 257.579072,174.800331 258.018574,173.563965 C258.912164,173.360784 259.727425,172.951546 260.411742,172.388862 L260.411742,172.388862 Z', id: 'Classic-Flight-Rewards' })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ClassicRewardsIcon;
}(_react.Component);

ClassicRewardsIcon.propTypes = {
  title: _propTypes2.default.string,
  className: _propTypes2.default.string,
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  fillClass: _propTypes2.default.string,
  onClick: _propTypes2.default.func
};
ClassicRewardsIcon.defaultProps = {
  title: 'Classic Rewards',
  className: 'svg__classic-rewards',
  fillClass: 'svg__classic-rewards__fill',
  width: '20px',
  height: '20px',
  onClick: function onClick() {}
};
exports.default = ClassicRewardsIcon;
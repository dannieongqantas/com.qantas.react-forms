'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Svg = require('./Svg');

var _Svg2 = _interopRequireDefault(_Svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ClearButton = function (_Component) {
  _inherits(ClearButton, _Component);

  function ClearButton() {
    _classCallCheck(this, ClearButton);

    return _possibleConstructorReturn(this, (ClearButton.__proto__ || Object.getPrototypeOf(ClearButton)).apply(this, arguments));
  }

  _createClass(ClearButton, [{
    key: 'render',
    value: function render() {
      var props = this.props;

      if (props.hide) return null;

      return _react2.default.createElement(
        _Svg2.default,
        { onClick: props.onClick,
          tabbable: props.tabbable,
          className: props.className, role: 'button', 'aria-label': props.title,
          width: props.size,
          height: props.size,
          viewBox: '0 0 40 40' },
        _react2.default.createElement(
          'g',
          { stroke: 'none', strokeWidth: '1', fill: 'none', fillRule: 'evenodd' },
          _react2.default.createElement(
            'g',
            { transform: 'translate(-1150.000000, -1548.000000)' },
            _react2.default.createElement(
              'g',
              { transform: 'translate(485.000000, 1478.000000)' },
              _react2.default.createElement(
                'g',
                null,
                _react2.default.createElement(
                  'g',
                  { className: props.svgClass, transform: 'translate(665.000000, 70.000000)' },
                  _react2.default.createElement('circle', { className: props.circleClass, id: 'Oval-23', cx: '20', cy: '20', r: '20' }),
                  _react2.default.createElement('path', { d: 'M20.1548387,17.4915676 L25.5517241,12 L28,14.4912281 L22.6094421,20 L28,25.5087719 L25.5517241,28 L20.1548387,22.5084324 L20,22.6666667 L19.8451613,22.5084324 L14.4482759,28 L12,25.5087719 L17.3905579,20 L12,14.4912281 L14.4482759,12 L19.8451613,17.4915676 L20,17.3333333 L20.1548387,17.4915676 Z', className: this.props.crossClass })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ClearButton;
}(_react.Component);

ClearButton.propTypes = {
  size: _propTypes2.default.string,
  className: _propTypes2.default.string,
  svgClass: _propTypes2.default.string,
  circleClass: _propTypes2.default.string,
  crossClass: _propTypes2.default.string,
  title: _propTypes2.default.string,
  onClick: _propTypes2.default.func,
  hide: _propTypes2.default.bool,
  focusable: _propTypes2.default.bool,
  tabbable: _propTypes2.default.bool
};
ClearButton.defaultProps = {
  size: '20px',
  className: '',
  svgClass: 'svg__clear-button',
  circleClass: 'svg__clear-button-circle',
  crossClass: 'svg__clear-button-cross',
  hide: true,
  focusable: false,
  title: '',
  tabbable: true
};
exports.default = ClearButton;
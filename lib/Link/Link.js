'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _LinkIcon = require('../Icons/LinkIcon');

var _LinkIcon2 = _interopRequireDefault(_LinkIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Link = function (_Component) {
  _inherits(Link, _Component);

  function Link() {
    _classCallCheck(this, Link);

    return _possibleConstructorReturn(this, (Link.__proto__ || Object.getPrototypeOf(Link)).apply(this, arguments));
  }

  _createClass(Link, [{
    key: 'renderArrow',
    value: function renderArrow() {
      return this.props.withArrow ? _react2.default.createElement(_LinkIcon2.default, null) : null;
    }
  }, {
    key: 'renderScreenReaderMessage',
    value: function renderScreenReaderMessage() {
      return this.props.isExternal ? _react2.default.createElement(
        'span',
        { className: 'widget-form__element--aria-hidden' },
        this.props.ariaMessage
      ) : null;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          href = _props.href,
          className = _props.className,
          title = _props.title,
          children = _props.children,
          otherProps = _objectWithoutProperties(_props, ['href', 'className', 'title', 'children']);

      return _react2.default.createElement(
        'a',
        _extends({ href: href, className: className, title: title }, otherProps),
        children,
        this.renderArrow(),
        this.renderScreenReaderMessage()
      );
    }
  }]);

  return Link;
}(_react.Component);

Link.propTypes = {
  href: _propTypes2.default.string.isRequired,
  className: _propTypes2.default.string,
  ariaMessage: _propTypes2.default.string,
  title: _propTypes2.default.string,
  isExternal: _propTypes2.default.bool,
  children: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string]),
  withArrow: _propTypes2.default.bool
};
Link.defaultProps = {
  className: 'qfa1-link',
  withArrow: false,
  isExternal: false,
  ariaMessage: 'Opens external site'
};
exports.default = Link;
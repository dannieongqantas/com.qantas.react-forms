'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoadingIndicator = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoadingIndicator = exports.LoadingIndicator = function LoadingIndicator(_ref) {
  var srText = _ref.srText;
  return _react2.default.createElement(
    'div',
    { className: 'qfa1-loading-indicator' },
    _react2.default.createElement(
      'div',
      { className: 'sk-three-bounce loader__icon' },
      _react2.default.createElement(
        'span',
        { className: 'show-for-sr' },
        srText
      ),
      _react2.default.createElement('div', { className: 'sk-child sk-bounce1' }),
      ' ',
      ' ',
      _react2.default.createElement('div', { className: 'sk-child sk-bounce2' }),
      ' ',
      ' ',
      _react2.default.createElement('div', { className: 'sk-child sk-bounce3' }),
      ' ',
      ' '
    )
  );
};

LoadingIndicator.propTypes = {
  srText: _propTypes2.default.string.isRequired
};

exports.default = LoadingIndicator;
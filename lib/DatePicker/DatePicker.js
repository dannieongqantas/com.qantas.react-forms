'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.calculateMaxDate = calculateMaxDate;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DatePickerControlled = require('./DatePickerControlled');

var _DatePickerControlled2 = _interopRequireDefault(_DatePickerControlled);

var _date = require('../utils/date');

var _screenReading = require('../utils/screenReading');

var _breakpoints = require('../grid/breakpoints');

var _breakpoints2 = _interopRequireDefault(_breakpoints);

var _WindowResize = require('../ReusableComponents/WindowResize');

var _ActionTypes = require('../Constants/ActionTypes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function calculateMaxDate(date, daysLimit) {
  return (0, _date.plusDays)(date, daysLimit - 1);
}

function initializeState(initialDate) {
  var state = { isOpened: false, screenRead: '', width: (0, _WindowResize.getWindowWidth)() };
  var selectedDate = initialDate || (0, _date.currentDate)();
  state.dateNavigated = selectedDate;
  state.calendarStartDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
  state.rangeEndDate = selectedDate;
  return state;
}

var DatePicker = function (_Component) {
  _inherits(DatePicker, _Component);

  function DatePicker(props) {
    _classCallCheck(this, DatePicker);

    var _this = _possibleConstructorReturn(this, (DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).call(this, props));

    _initialiseProps.call(_this);

    var value = props.value,
        initialValue = props.initialValue;

    var state = initializeState(value || initialValue);
    if (!value) {
      state.value = initialValue || (0, _date.currentDate)();
    }
    _this.state = state;
    return _this;
  }

  _createClass(DatePicker, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var props = _objectWithoutProperties(this.props, []);

      return _react2.default.createElement(_DatePickerControlled2.default, _extends({
        ref: function ref(datePickerControlled) {
          _this2.datePickerControlled = datePickerControlled;
        }
      }, props, this.state, {
        maxDate: this.maxDate(),
        selectedDate: this.selectedDate(),
        openCalendar: function openCalendar() {
          return _this2.send(_ActionTypes.CALENDAR_OPEN);
        },
        closeCalendar: function closeCalendar() {
          return _this2.send(_ActionTypes.CALENDAR_CLOSE);
        },
        changeCalendarWidth: function changeCalendarWidth(width) {
          return _this2.send(_ActionTypes.CALENDAR_WIDTH_CHANGE, { width: width });
        },
        toggleCalendarMonth: function toggleCalendarMonth(toggleAmount) {
          return _this2.send(_ActionTypes.CALENDAR_VIEWING_MONTH_TOGGLE, { toggleAmount: toggleAmount });
        },
        navigateToDate: function navigateToDate(dateNavigated, minMessage, maxMessage) {
          return _this2.send(_ActionTypes.CALENDAR_NAVIGATE_DAY, { dateNavigated: dateNavigated, minMessage: minMessage, maxMessage: maxMessage });
        },
        selectDate: function selectDate(value) {
          return _this2.onChange(value);
        }
      }));
    }
  }]);

  return DatePicker;
}(_react.Component);

DatePicker.propTypes = {
  i18n: _propTypes2.default.object.isRequired,
  formLabel: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.instanceOf(Date),
  initialValue: _propTypes2.default.instanceOf(Date),
  minDate: _propTypes2.default.instanceOf(Date),
  daysLimit: _propTypes2.default.number,
  disabled: _propTypes2.default.bool,
  isRangeEnabled: _propTypes2.default.bool,
  hasEventRange: _propTypes2.default.bool,
  rangeStartDate: _propTypes2.default.instanceOf(Date),
  eventRangeStartDate: _propTypes2.default.instanceOf(Date),
  eventRangeEndDate: _propTypes2.default.instanceOf(Date),
  selectedDateArrowDirection: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  ariaLabel: _propTypes2.default.string.isRequired,
  ariaToggleNextMonth: _propTypes2.default.string.isRequired,
  ariaTogglePreviousMonth: _propTypes2.default.string.isRequired,
  pickerType: _propTypes2.default.string,
  scrollCalendarInViewOnOpen: _propTypes2.default.oneOf(['smooth', 'instant']),
  onCalendarClose: _propTypes2.default.func
};
DatePicker.defaultProps = {
  daysLimit: 100,
  isRangeEnabled: false,
  languageCodeForDateFormat: 'en',
  selectedDateArrowDirection: 'right',
  minDate: (0, _date.currentDate)(),
  rangeStartDate: (0, _date.currentDate)(),
  pickerType: '',
  hasEventRange: false,
  eventRangeStartDate: (0, _date.currentDate)(),
  eventRangeEndDate: (0, _date.currentDate)()
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.onChange = function (value) {
    if (!_this3.props.value) {
      _this3.send(_ActionTypes.CALENDAR_SELECT_ITEM, { value: value });
    } else {
      _this3.send(_ActionTypes.CALENDAR_CLOSE);
    }
    _this3.props.onChange(value);
  };

  this.focus = function () {
    if (_this3.datePickerControlled) {
      _this3.datePickerControlled.focus();
    }
  };

  this.selectedDate = function () {
    return _this3.props.value || _this3.state.value;
  };

  this.maxDate = function () {
    return calculateMaxDate(_this3.props.minDate, _this3.props.daysLimit);
  };

  this.update = function (action, data) {
    var state = _this3.state;
    var props = _this3.props;
    switch (action) {
      case _ActionTypes.CALENDAR_SELECT_ITEM:
        return _extends({}, state, {
          isOpened: false,
          screenRead: '',
          value: data.value,
          dateNavigated: data.value
        });
      case _ActionTypes.CALENDAR_CLOSE:
        return _extends({}, state, {
          isOpened: false,
          screenRead: ''
        });
      case _ActionTypes.CALENDAR_OPEN:
        {
          if (!props.disabled) {
            var currentSelectedDate = _this3.selectedDate();
            var resetCalendarStartDate = new Date(currentSelectedDate.getFullYear(), currentSelectedDate.getMonth(), 1);

            if (props.isRangeEnabled && props.rangeStartDate.getMonth() !== currentSelectedDate.getMonth() && state.width >= _breakpoints2.default.medium) resetCalendarStartDate = (0, _date.plusMonths)(resetCalendarStartDate, -1);

            return _extends({}, state, {
              isOpened: true,
              calendarStartDate: resetCalendarStartDate,
              rangeEndDate: currentSelectedDate,
              dateNavigated: currentSelectedDate
            });
          }
          return state;
        }
      case _ActionTypes.CALENDAR_VIEWING_MONTH_TOGGLE:
        {
          var newCalendarStartDate = (0, _date.plusMonths)(state.calendarStartDate, data.toggleAmount);
          return _extends({}, state, {
            calendarStartDate: newCalendarStartDate
          });
        }
      case _ActionTypes.CALENDAR_WIDTH_CHANGE:
        return _extends({}, state, { width: data.width });
      case _ActionTypes.CALENDAR_NAVIGATE_DAY:
        {
          var dateNavigated = data.dateNavigated;

          if (!state.isOpened) {
            return state;
          } else if (props.minDate.getTime() > dateNavigated.getTime()) {
            var screenReaderText = (0, _screenReading.reread)(state.screenRead, data.minMessage);
            return _extends({}, state, { screenRead: screenReaderText });
          } else if (_this3.maxDate().getTime() < dateNavigated.getTime()) {
            var _screenReaderText = (0, _screenReading.reread)(state.screenRead, data.maxMessage);
            return _extends({}, state, { screenRead: _screenReaderText });
          }

          var calendarStartDate = state.calendarStartDate;
          var calendarEndDate = (0, _date.plusDays)(calendarStartDate, (0, _date.getDaysInMonth)(calendarStartDate.getFullYear(), calendarStartDate.getMonth()));

          if (state.width >= _breakpoints2.default.medium) calendarEndDate = (0, _date.plusMonths)(calendarEndDate, 1);

          if (dateNavigated.getTime() >= calendarEndDate.getTime()) calendarStartDate = (0, _date.plusMonths)(calendarStartDate, 1);

          if (dateNavigated.getTime() < calendarStartDate.getTime()) calendarStartDate = (0, _date.plusMonths)(calendarStartDate, -1);

          return _extends({}, state, {
            dateNavigated: dateNavigated,
            calendarStartDate: calendarStartDate,
            rangeEndDate: dateNavigated,
            screenRead: (0, _date.transformDateToLongWords)(dateNavigated, props.i18n)
          });
        }
      default:
        return state;
    }
  };

  this.send = function (action, data) {
    var state = _this3.update(action, data);
    if (_this3.state !== state) {
      _this3.setState(state);
    }
  };
};

exports.default = DatePicker;
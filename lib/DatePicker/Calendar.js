'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CalendarRow = require('./CalendarRow');

var _CalendarRow2 = _interopRequireDefault(_CalendarRow);

var _date = require('../utils/date');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DAYS_IN_WEEK = 7;

var Calendar = function (_Component) {
  _inherits(Calendar, _Component);

  function Calendar() {
    _classCallCheck(this, Calendar);

    return _possibleConstructorReturn(this, (Calendar.__proto__ || Object.getPrototypeOf(Calendar)).apply(this, arguments));
  }

  _createClass(Calendar, [{
    key: 'createCalendarRows',
    value: function createCalendarRows(props) {
      var daysInMonth = (0, _date.getDaysInMonth)(props.year, props.month);
      var calendarRowStartDate = new Date(props.year, props.month, 1);
      var numberOfDaysInFirstRow = DAYS_IN_WEEK - (0, _date.getDayBeginFromMonday)(calendarRowStartDate) + 1;
      var numberOfWeeks = Calendar.calculateNumberOfRows(daysInMonth, numberOfDaysInFirstRow);

      var rows = [];
      // first calendar row
      rows.push(_react2.default.createElement(_CalendarRow2.default, _extends({}, props, { key: 0, startDate: calendarRowStartDate, maxDay: daysInMonth })));

      // second row onwards so start with i = 1
      calendarRowStartDate = (0, _date.plusDays)(calendarRowStartDate, numberOfDaysInFirstRow);
      for (var i = 1; i < numberOfWeeks; i++) {
        rows.push(_react2.default.createElement(_CalendarRow2.default, _extends({}, props, { key: i, startDate: calendarRowStartDate, maxDay: daysInMonth })));
        calendarRowStartDate = (0, _date.plusDays)(calendarRowStartDate, DAYS_IN_WEEK);
      }
      return rows;
    }
  }, {
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      var rows = this.createCalendarRows(props);

      return _react2.default.createElement(
        'div',
        { className: 'date-picker__calendar' },
        _react2.default.createElement(
          'table',
          { className: 'date-picker__calendar-table' },
          _react2.default.createElement(
            'tbody',
            null,
            _react2.default.createElement(
              'tr',
              { className: 'date-picker__calendar-heading' },
              _react2.default.createElement(
                'th',
                { colSpan: '7' },
                props.i18n.months[props.month + 1].long + ' ' + props.year
              )
            ),
            _react2.default.createElement(
              'tr',
              { className: 'date-picker__calendar-weekday-headings' },
              [1, 2, 3, 4, 5, 6, 7].map(function (day) {
                return _react2.default.createElement(
                  'th',
                  { key: day },
                  _react2.default.createElement(
                    'div',
                    null,
                    props.i18n.weekdays[day].short
                  )
                );
              })
            ),
            _react2.default.createElement(
              'tr',
              { 'aria-hidden': 'true' },
              _react2.default.createElement('td', { className: 'date-picker__calendar-heading-padding' })
            ),
            rows
          )
        )
      );
    }
  }], [{
    key: 'calculateNumberOfRows',
    value: function calculateNumberOfRows(daysInMonth, numberOfDaysInFirstRow) {
      // always adding one for first row
      return Math.ceil((daysInMonth - numberOfDaysInFirstRow) / DAYS_IN_WEEK) + 1;
    }
  }]);

  return Calendar;
}(_react.Component);

Calendar.propTypes = {
  i18n: _propTypes2.default.object.isRequired,
  year: _propTypes2.default.number.isRequired,
  month: _propTypes2.default.number.isRequired,
  id: _propTypes2.default.string.isRequired,
  // below ones get passed to calendar row
  selectedDate: _propTypes2.default.instanceOf(Date).isRequired,
  minDate: _propTypes2.default.instanceOf(Date).isRequired,
  maxDate: _propTypes2.default.instanceOf(Date).isRequired,
  isRangeEnabled: _propTypes2.default.bool.isRequired,
  selectedDateArrowDirection: _propTypes2.default.string.isRequired,
  descendantId: _propTypes2.default.string.isRequired,
  selectDate: _propTypes2.default.func.isRequired,
  navigateToDate: _propTypes2.default.func.isRequired
};
exports.default = Calendar;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatePickerControlled = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CalendarIcon = require('../Icons/CalendarIcon');

var _CalendarIcon2 = _interopRequireDefault(_CalendarIcon);

var _CalendarDropdown = require('./CalendarDropdown');

var _CalendarDropdown2 = _interopRequireDefault(_CalendarDropdown);

var _ComponentBlur = require('../ReusableComponents/ComponentBlur');

var _ComponentBlur2 = _interopRequireDefault(_ComponentBlur);

var _WindowResize = require('../ReusableComponents/WindowResize');

var _WindowResize2 = _interopRequireDefault(_WindowResize);

var _DateFormatter = require('../ReusableComponents/DateFormatter');

var _DateFormatter2 = _interopRequireDefault(_DateFormatter);

var _date = require('../utils/date');

var _BorderTriangle = require('../Icons/BorderTriangle');

var _BorderTriangle2 = _interopRequireDefault(_BorderTriangle);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _InputArea = require('../Input/InputArea');

var _InputArea2 = _interopRequireDefault(_InputArea);

var _breakpoints = require('../grid/breakpoints');

var _breakpoints2 = _interopRequireDefault(_breakpoints);

var _userAgent = require('../utils/userAgent');

var _stringBind = require('../utils/stringBind');

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var COMPONENT_REF = 'DatePickerControlled';

var Triangle = function Triangle(_ref) {
  var opened = _ref.opened;

  var triangleClass = (0, _classnames2.default)('date-picker__triangle-container-father', {
    'widget-form__element--hidden': !opened
  });
  return _react2.default.createElement(
    'div',
    { className: triangleClass },
    _react2.default.createElement(
      'div',
      { className: 'date-picker__triangle-container' },
      _react2.default.createElement(_BorderTriangle2.default, null)
    )
  );
};

Triangle.propTypes = {
  opened: _propTypes2.default.bool.isRequired
};

var DatePickerControlled = exports.DatePickerControlled = function (_Component) {
  _inherits(DatePickerControlled, _Component);

  _createClass(DatePickerControlled, null, [{
    key: 'toggleMonthDiff',
    value: function toggleMonthDiff(dateNavigated, direction, minDate) {
      var newDate = (0, _date.firstDateInMonth)((0, _date.plusMonths)(dateNavigated, direction));

      if (minDate && dateNavigated.getMonth() !== minDate.getMonth()) {
        newDate = (0, _date.maxDate)(newDate, minDate);
      }

      return direction * ((0, _date.dateDiff)(newDate, dateNavigated) || direction);
    }
  }]);

  function DatePickerControlled(props) {
    _classCallCheck(this, DatePickerControlled);

    var _this = _possibleConstructorReturn(this, (DatePickerControlled.__proto__ || Object.getPrototypeOf(DatePickerControlled)).call(this, props));

    _initialiseProps.call(_this);

    _this.isIE = (0, _userAgent.isIE)() || (0, _userAgent.isEdge)();
    _this.descendant = 0;
    _this.id = props.id || (0, _idGenerator2.default)();
    return _this;
  }

  _createClass(DatePickerControlled, [{
    key: 'componentWillUpdate',
    value: function componentWillUpdate(newProps) {
      if (this.isIE) {
        if (this.refs.screen1) this.refs.screen1.innerHTML = newProps.screenRead;
        if (this.refs.screen2) this.refs.screen2.innerHTML = newProps.screenRead;
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var _props = this.props,
          onCalendarClose = _props.onCalendarClose,
          isOpened = _props.isOpened;


      if (!!onCalendarClose && prevProps.isOpened === true && isOpened === false) {
        onCalendarClose();
      }
    }
  }, {
    key: 'navigateToDate',
    value: function navigateToDate(days, minMessage, maxMessage) {
      var dateNavigated = (0, _date.plusDays)(this.props.dateNavigated, days);
      this.props.navigateToDate(dateNavigated, minMessage, maxMessage);
    }
  }, {
    key: 'selectDate',
    value: function selectDate(evt) {
      evt.preventDefault();
      if (!this.props.isOpened) {
        this.handleInputFocus();
      } else {
        this.props.selectDate(this.props.dateNavigated);
      }
    }
  }, {
    key: 'renderIEScreenReader',
    value: function renderIEScreenReader(inputId) {
      if (!this.isIE) return null;

      return _react2.default.createElement(
        'div',
        { role: 'application', className: 'widget-form__element--aria-hidden' },
        _react2.default.createElement('div', { role: 'option', ref: 'screen1', id: inputId + 0 }),
        _react2.default.createElement('div', { role: 'option', ref: 'screen2', id: inputId + 1 })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var props = _objectWithoutProperties(this.props, []);

      var inputId = 'datepicker-input-' + this.id;

      // Toggles between active descendants as a hack for aria-live on IE
      var ariaPropsForIE = {
        activeDescendant: props.screenRead && this.isIE ? inputId + this.descendant++ % 2 : '' // Toggles between inputId0 or inputId1
      };

      var ariaLabel = (0, _stringBind.stringBind)(props.ariaLabel, {
        dateSelected: (0, _date.transformDateToLongWords)(props.selectedDate, props.i18n),
        maxDays: props.daysLimit
      });

      var isDisabledClass = props.disabled ? props.className + ' date-picker__disabled' : props.className;
      var calendarIconClass = props.disabled ? 'date-picker__calendar-icon-fill date-picker__calendar-icon__disabled' : 'date-picker__calendar-icon-fill';
      var classname = (0, _classnames2.default)('widget-form__group-container', isDisabledClass);

      return _react2.default.createElement(
        'div',
        { className: classname, onKeyDown: this.keyDownHandler },
        _react2.default.createElement(
          'div',
          { ref: function ref(el) {
              _this2[COMPONENT_REF] = el;
            } },
          _react2.default.createElement(
            _InputArea2.default,
            _extends({ disabled: props.disabled, className: 'date-picker__input',
              id: inputId,
              ref: 'input',
              onFocus: this.handleInputFocus,
              onClick: this.handleInputClick,
              value: props.dateFormatter(props.selectedDate, props.dateFormat),
              noEntry: true,
              onBlur: props.blur,
              formLabel: props.formLabel,
              ariaLabel: ariaLabel,
              popup: props.isOpened }, ariaPropsForIE, {
              readOnly: props.readOnly }),
            _react2.default.createElement(_CalendarIcon2.default, { className: 'date-picker__calendar-icon', fillClass: calendarIconClass,
              onClick: this.handleIconClick }),
            this.renderIEScreenReader(inputId),
            _react2.default.createElement(_ScreenReader2.default, { ariaAtomic: true, ariaRole: 'live', ariaRelevant: 'text', ariaLabel: props.screenRead }),
            _react2.default.createElement(Triangle, { opened: props.isOpened })
          ),
          _react2.default.createElement(_CalendarDropdown2.default, _extends({ isMobile: props.width < _breakpoints2.default.medium }, props, {
            descendantId: inputId
          }))
        )
      );
    }
  }]);

  return DatePickerControlled;
}(_react.Component);

DatePickerControlled.defaultProps = {
  className: '',
  disabled: false,
  dateFormat: 'ddd D MMM YYYY',
  readOnly: false
};
DatePickerControlled.propTypes = {
  i18n: _propTypes2.default.object.isRequired,
  dateFormat: _propTypes2.default.string,
  languageCodeForDateFormat: _propTypes2.default.string.isRequired,
  formLabel: _propTypes2.default.string.isRequired,
  selectedDate: _propTypes2.default.instanceOf(Date),
  dateNavigated: _propTypes2.default.instanceOf(Date),
  isOpened: _propTypes2.default.bool.isRequired,
  width: _propTypes2.default.number.isRequired,
  minDate: _propTypes2.default.instanceOf(Date).isRequired,
  maxDate: _propTypes2.default.instanceOf(Date).isRequired,
  isRangeEnabled: _propTypes2.default.bool.isRequired,
  disabled: _propTypes2.default.bool,
  rangeStartDate: _propTypes2.default.instanceOf(Date).isRequired,
  rangeEndDate: _propTypes2.default.instanceOf(Date).isRequired,
  selectedDateArrowDirection: _propTypes2.default.string.isRequired,
  selectDate: _propTypes2.default.func.isRequired,
  pickerType: _propTypes2.default.string,
  navigateToDate: _propTypes2.default.func.isRequired,
  closeCalendar: _propTypes2.default.func.isRequired,
  openCalendar: _propTypes2.default.func.isRequired,
  changeCalendarWidth: _propTypes2.default.func.isRequired,
  toggleCalendarMonth: _propTypes2.default.func.isRequired,
  blur: _propTypes2.default.func.isRequired,
  id: _propTypes2.default.string,
  ariaLabel: _propTypes2.default.string.isRequired,
  screenRead: _propTypes2.default.string,
  className: _propTypes2.default.string,
  ariaToggleNextMonth: _propTypes2.default.string.isRequired,
  ariaTogglePreviousMonth: _propTypes2.default.string.isRequired,
  dateFormatter: _propTypes2.default.func.isRequired,
  onCalendarClose: _propTypes2.default.func
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.focus = function () {
    if (_this3.refs.input) {
      _this3.refs.input.focus();
    }
  };

  this.handleIconClick = function () {
    if (_this3.props.isOpened) {
      _this3.refs.input.blur();
      _this3.closeCalendar();
    } else {
      _this3.refs.input.focus();
    }
  };

  this.handleInputClick = function () {
    _this3.refs.input.focus();
    _this3.handleInputFocus();
  };

  this.handleInputFocus = function () {
    _this3.props.openCalendar();
  };

  this.closeCalendar = function () {
    _this3.props.closeCalendar();
  };

  this.keyDownHandler = function (e) {
    var _props2 = _this3.props,
        dateNavigated = _props2.dateNavigated,
        minDate = _props2.minDate,
        props = _objectWithoutProperties(_props2, ['dateNavigated', 'minDate']);

    var dateDifference = void 0;
    switch (e.keyCode) {
      case _KeyCodes.P_KEY_CODE:
        e.preventDefault();
        dateDifference = DatePickerControlled.toggleMonthDiff(dateNavigated, -1, minDate);
        _this3.navigateToDate(dateDifference, props.minMonthMessage, props.maxMonthMessage); // make sure its not 0 so screen reader reads it out
        break;
      case _KeyCodes.N_KEY_CODE:
        e.preventDefault();
        dateDifference = DatePickerControlled.toggleMonthDiff(dateNavigated, 1);
        _this3.navigateToDate(dateDifference || 1, props.minMonthMessage, props.maxMonthMessage); // make sure its not 0 so screen reader reads it out
        break;
      case _KeyCodes.SPACE_KEY_CODE:
        if (_this3.props.pickerType !== 'hasChild') {
          _this3.selectDate(e);
        }
        break;
      case _KeyCodes.RETURN_KEY_CODE:
        _this3.selectDate(e);
        break;
      case _KeyCodes.ESCAPE_KEY_CODE:
        e.preventDefault();
        _this3.closeCalendar();
        break;
      case _KeyCodes.ARROW_LEFT_KEY_CODE:
        e.preventDefault();
        _this3.navigateToDate(-1, props.minDateMessage, props.maxDateMessage);
        break;
      case _KeyCodes.ARROW_RIGHT_KEY_CODE:
        e.preventDefault();
        _this3.navigateToDate(1, props.minDateMessage, props.maxDateMessage);
        break;
      case _KeyCodes.ARROW_UP_KEY_CODE:
        e.preventDefault();
        _this3.navigateToDate(-7, props.minWeekMessage, props.maxWeekMessage);
        break;
      case _KeyCodes.ARROW_DOWN_KEY_CODE:
        e.preventDefault();
        _this3.navigateToDate(7, props.minWeekMessage, props.maxWeekMessage);
        break;
      default:
        break;
    }
  };
};

var ComponentWithBlur = (0, _ComponentBlur2.default)(DatePickerControlled, COMPONENT_REF, function (props) {
  return props.closeCalendar;
});

var ComponentWithDateFormatter = (0, _DateFormatter2.default)(ComponentWithBlur, function (props) {
  return props.languageCodeForDateFormat;
});

exports.default = (0, _WindowResize2.default)(ComponentWithDateFormatter, _breakpoints2.default.medium, function (props, windowWidth) {
  return props.changeCalendarWidth(windowWidth);
});
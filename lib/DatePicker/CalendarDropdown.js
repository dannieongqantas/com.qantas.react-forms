'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _smoothscrollPolyfill = require('smoothscroll-polyfill');

var _smoothscrollPolyfill2 = _interopRequireDefault(_smoothscrollPolyfill);

var _Calendar = require('./Calendar');

var _Calendar2 = _interopRequireDefault(_Calendar);

var _ArrowIcon = require('../Icons/ArrowIcon');

var _ArrowIcon2 = _interopRequireDefault(_ArrowIcon);

var _CalendarStarIcon = require('../Icons/CalendarStarIcon');

var _CalendarStarIcon2 = _interopRequireDefault(_CalendarStarIcon);

var _date = require('../utils/date');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CalendarDropdown = function (_Component) {
  _inherits(CalendarDropdown, _Component);

  function CalendarDropdown() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, CalendarDropdown);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = CalendarDropdown.__proto__ || Object.getPrototypeOf(CalendarDropdown)).call.apply(_ref, [this].concat(args))), _this), _this.handleCalendarToggle = function (toggleAmount) {
      _this.props.toggleCalendarMonth(toggleAmount);
    }, _this.resetToLastNavigatedDate = function () {
      _this.props.navigateToDate(_this.props.selectedDate);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(CalendarDropdown, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (!!this.props.scrollCalendarInViewOnOpen) {
        _smoothscrollPolyfill2.default.polyfill();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var _props = this.props,
          scrollCalendarInViewOnOpen = _props.scrollCalendarInViewOnOpen,
          isOpened = _props.isOpened;


      var isScrollable = !!scrollCalendarInViewOnOpen && !!this.wrapperElement;
      var didOpen = prevProps.isOpened === false && isOpened === true;

      if (isScrollable && didOpen) {
        var elemPosition = this.wrapperElement.getBoundingClientRect().bottom;
        var distanceToScroll = elemPosition - window.innerHeight;
        var options = {
          behavior: scrollCalendarInViewOnOpen,
          top: distanceToScroll,
          left: 0
        };

        window.scrollBy(options);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var props = _objectWithoutProperties(this.props, []);

      var legendText = '= Offer period';

      if (!props.isOpened) return null;

      var calendarOneStartDate = props.calendarStartDate;

      var calendars = [_react2.default.createElement(_Calendar2.default, _extends({}, props, { month: calendarOneStartDate.getMonth(), year: calendarOneStartDate.getFullYear(), key: '0' }))];

      var hideLeft = calendarOneStartDate.getTime() <= (0, _date.firstDateInMonth)(props.minDate).getTime();

      var hideRight = calendarOneStartDate.getTime() >= (0, _date.firstDateInMonth)(props.maxDate).getTime();

      // first calendar row

      if (!props.isMobile) {
        var calendarTwoStartDate = (0, _date.plusMonths)(calendarOneStartDate, 1);
        calendars.push(_react2.default.createElement(_Calendar2.default, _extends({}, props, { month: calendarTwoStartDate.getMonth(),
          year: calendarTwoStartDate.getFullYear(),
          key: '1' })));
        hideRight = hideRight || calendarTwoStartDate.getTime() >= (0, _date.firstDateInMonth)(props.maxDate).getTime();
      }

      var leftArrowClass = (0, _classnames2.default)('date-picker__arrow date-picker__arrow-left', {
        'date-picker__arrow--hidden': hideLeft
      });

      var leftArrowOnClick = function leftArrowOnClick() {
        return _this2.handleCalendarToggle(-1);
      };
      var rightArrowOnClick = function rightArrowOnClick() {
        return _this2.handleCalendarToggle(1);
      };
      return _react2.default.createElement(
        'div',
        {
          className: 'date-picker__dropdown',
          onMouseLeave: this.resetToLastNavigatedDate,
          ref: function ref(wrapperElement) {
            _this2.wrapperElement = wrapperElement;
          } },
        _react2.default.createElement(
          'div',
          { className: 'date-picker__dropdown-wrapper' },
          props.children,
          _react2.default.createElement(_ArrowIcon2.default, {
            className: leftArrowClass,
            disabled: hideLeft,
            'aria-label': hideLeft ? props.minMonthMessage : props.ariaTogglePreviousMonth,
            circle: true,
            direction: 'left',
            onClick: leftArrowOnClick
          }),
          _react2.default.createElement(_ArrowIcon2.default, {
            className: 'date-picker__arrow date-picker__arrow-right',
            disabled: hideRight,
            'aria-label': hideLeft ? props.maxMonthMessage : props.ariaToggleNextMonth,
            circle: true,
            direction: 'right',
            onClick: rightArrowOnClick
          }),
          _react2.default.createElement(
            'div',
            { className: 'date-picker__calendar-container' },
            calendars
          ),
          props.hasEventRange && _react2.default.createElement(
            'div',
            { className: 'date-picker__legend' },
            _react2.default.createElement(
              _CalendarStarIcon2.default,
              { className: 'date-picker__legend-star' },
              _CalendarStarIcon2.default,
              ' />'
            ),
            _react2.default.createElement(
              'span',
              { className: 'date-picker__legend-text' },
              legendText
            )
          )
        )
      );
    }
  }]);

  return CalendarDropdown;
}(_react.Component);

CalendarDropdown.propTypes = {
  i18n: _propTypes2.default.object.isRequired,
  isMobile: _propTypes2.default.bool.isRequired,
  id: _propTypes2.default.string.isRequired,
  selectedDate: _propTypes2.default.instanceOf(Date).isRequired,
  isOpened: _propTypes2.default.bool.isRequired,
  calendarStartDate: _propTypes2.default.instanceOf(Date).isRequired,
  minDate: _propTypes2.default.instanceOf(Date).isRequired,
  maxDate: _propTypes2.default.instanceOf(Date).isRequired,
  isRangeEnabled: _propTypes2.default.bool.isRequired,
  hasEventRange: _propTypes2.default.bool,
  rangeStartDate: _propTypes2.default.instanceOf(Date).isRequired,
  rangeEndDate: _propTypes2.default.instanceOf(Date).isRequired,
  eventRangeStartDate: _propTypes2.default.instanceOf(Date),
  eventRangeEndDate: _propTypes2.default.instanceOf(Date),
  selectedDateArrowDirection: _propTypes2.default.string.isRequired,
  descendantId: _propTypes2.default.string.isRequired,
  selectDate: _propTypes2.default.func.isRequired,
  navigateToDate: _propTypes2.default.func.isRequired,
  toggleCalendarMonth: _propTypes2.default.func.isRequired,
  ariaToggleNextMonth: _propTypes2.default.string.isRequired,
  ariaTogglePreviousMonth: _propTypes2.default.string.isRequired,
  maxMonthMessage: _propTypes2.default.string.isRequired,
  minMonthMessage: _propTypes2.default.string.isRequired,
  scrollCalendarInViewOnOpen: _propTypes2.default.oneOf(['smooth', 'instant'])
};
exports.default = CalendarDropdown;
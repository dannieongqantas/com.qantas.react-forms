'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _date = require('../utils/date');

var _CalendarStarIcon = require('../Icons/CalendarStarIcon');

var _CalendarStarIcon2 = _interopRequireDefault(_CalendarStarIcon);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DAYS_IN_WEEK = 7;

var CalendarRow = function (_Component) {
  _inherits(CalendarRow, _Component);

  _createClass(CalendarRow, null, [{
    key: 'appliedClasses',
    value: function appliedClasses(isSelected, isDisabled, isHovered, isInRange) {
      return (0, _classnames2.default)('date-picker__calendar-weekdays-items', {
        'date-picker__calendar-weekdays-items--selected': isSelected,
        'date-picker__calendar-weekdays-items--disabled': isDisabled,
        'date-picker__calendar-weekdays-items--enabled': !isDisabled,
        'date-picker__calendar-weekdays-items--hovered': isHovered,
        'date-picker__calendar-weekdays-items--in-range': isInRange
      });
    }
  }, {
    key: 'isDisabled',
    value: function isDisabled(date, minDate, maxDate) {
      return minDate.getTime() > date.getTime() || maxDate.getTime() < date.getTime();
    }
  }]);

  function CalendarRow(props) {
    _classCallCheck(this, CalendarRow);

    var _this = _possibleConstructorReturn(this, (CalendarRow.__proto__ || Object.getPrototypeOf(CalendarRow)).call(this, props));

    _this.clickHandler = _this.clickHandler.bind(_this);
    _this.handleHover = _this.handleHover.bind(_this);
    return _this;
  }

  _createClass(CalendarRow, [{
    key: 'clickHandler',
    value: function clickHandler(date) {
      this.props.selectDate(date);
    }
  }, {
    key: 'handleHover',
    value: function handleHover(date) {
      this.props.navigateToDate(date);
    }
  }, {
    key: 'validDaysForRow',
    value: function validDaysForRow(props) {
      var diff = props.maxDay - props.startDate.getDate();
      return diff >= DAYS_IN_WEEK ? DAYS_IN_WEEK : diff + 1;
    }
  }, {
    key: 'createEmptyElement',
    value: function createEmptyElement(key) {
      return _react2.default.createElement('td', { key: key });
    }
  }, {
    key: 'createDayElement',
    value: function createDayElement(date, props, key) {
      var _this2 = this;

      var isDisabled = CalendarRow.isDisabled(date, props.minDate, props.maxDate);
      var isSelected = props.selectedDate && props.selectedDate.getTime() === date.getTime();
      var isHovered = date.getTime() === props.dateNavigated.getTime();
      var isInRange = props.isRangeEnabled && props.rangeStartDate.getTime() <= date.getTime() && date.getTime() < props.rangeEndDate.getTime();
      var hasEventRange = !isDisabled && props.hasEventRange && props.eventRangeStartDate.getTime() <= date.getTime() && date.getTime() <= props.eventRangeEndDate.getTime();
      var className = CalendarRow.appliedClasses(isSelected, isDisabled, isHovered, isInRange);

      var newProps = {
        className: className,
        key: key
      };

      if (!isDisabled) {
        newProps.onClick = function (e) {
          e.currentTarget.blur();
          _this2.clickHandler(date);
        };

        newProps.onMouseOver = function () {
          return _this2.handleHover(date);
        };
        newProps.onTouchStart = function () {
          return _this2.handleHover(date);
        };
        newProps['aria-label'] = (0, _date.transformDateToLongWords)(date, props.i18n);
        newProps.role = 'option'; // Need role option for iOS voiceover
        newProps.tabIndex = -1;
      }

      var triangle = isSelected ? _react2.default.createElement('div', {
        className: 'date-picker__calendar-weekdays-items-triangle-pointing-' + props.selectedDateArrowDirection }) : null;

      var star = hasEventRange && _react2.default.createElement(_CalendarStarIcon2.default, { className: 'date-picker__calendar-star' });

      var rangeStartTriangle = props.isRangeEnabled && props.rangeStartDate.getTime() === date.getTime() ? _react2.default.createElement('div', {
        className: 'date-picker__calendar-weekdays-items-triangle-pointing-right' }) : null;

      return _react2.default.createElement(
        'td',
        newProps,
        triangle,
        rangeStartTriangle,
        _react2.default.createElement(
          'div',
          { className: 'date-picker__calendar-weekdays-items-content' },
          _react2.default.createElement(
            'span',
            { className: 'date-picker__calendar-weekdays-items-text' },
            date.getDate()
          )
        ),
        star
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      var startDay = (0, _date.getDayBeginFromMonday)(props.startDate) - 1;
      var daysInRow = [];

      for (var i = 0; i < startDay; i++) {
        daysInRow.push(this.createEmptyElement(i));
      }var validDays = this.validDaysForRow(props);
      var date = props.startDate;
      for (var _i = startDay; _i < validDays; _i++) {
        daysInRow.push(this.createDayElement(date, props, _i));
        date = (0, _date.plusDays)(date, 1);
      }

      // if there are any columns still left
      for (var _i2 = validDays; _i2 < DAYS_IN_WEEK; _i2++) {
        daysInRow.push(this.createEmptyElement(_i2));
      }return _react2.default.createElement(
        'tr',
        null,
        daysInRow
      );
    }
  }]);

  return CalendarRow;
}(_react.Component);

CalendarRow.propTypes = {
  startDate: _propTypes2.default.instanceOf(Date).isRequired,
  maxDay: _propTypes2.default.number.isRequired,
  id: _propTypes2.default.string.isRequired,
  selectedDate: _propTypes2.default.instanceOf(Date).isRequired,
  dateNavigated: _propTypes2.default.instanceOf(Date).isRequired,
  minDate: _propTypes2.default.instanceOf(Date).isRequired,
  maxDate: _propTypes2.default.instanceOf(Date).isRequired,
  isRangeEnabled: _propTypes2.default.bool.isRequired,
  hasEventRange: _propTypes2.default.bool.isRequired,
  rangeStartDate: _propTypes2.default.instanceOf(Date).isRequired,
  rangeEndDate: _propTypes2.default.instanceOf(Date).isRequired,
  eventRangeStartDate: _propTypes2.default.instanceOf(Date),
  eventRangeEndDate: _propTypes2.default.instanceOf(Date),
  selectedDateArrowDirection: _propTypes2.default.string.isRequired,
  descendantId: _propTypes2.default.string.isRequired,
  selectDate: _propTypes2.default.func.isRequired,
  navigateToDate: _propTypes2.default.func.isRequired
};
exports.default = CalendarRow;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Dropdown = require('../Dropdown/Dropdown');

var _Dropdown2 = _interopRequireDefault(_Dropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DropdownList = function (_Component) {
  _inherits(DropdownList, _Component);

  function DropdownList(props) {
    _classCallCheck(this, DropdownList);

    var _this = _possibleConstructorReturn(this, (DropdownList.__proto__ || Object.getPrototypeOf(DropdownList)).call(this, props));

    _this.scrollWithinView = _this.scrollWithinView.bind(_this);
    _this.createNotFound = _this.createNotFound.bind(_this);
    _this.createItem = _this.createItem.bind(_this);
    return _this;
  }

  _createClass(DropdownList, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.scrollWithinView();
    }
  }, {
    key: 'createItem',
    value: function createItem(item, itemsCount) {
      var _props = this.props,
          selectedItem = _props.selectedItem,
          itemIdPrefix = _props.itemIdPrefix,
          props = _objectWithoutProperties(_props, ['selectedItem', 'itemIdPrefix']);

      var text = item.text,
          ariaLabel = item.ariaLabel;

      var selected = itemsCount === selectedItem;

      return _react2.default.createElement(
        'li',
        { key: itemsCount,
          role: 'option',
          'aria-label': ariaLabel,
          id: itemIdPrefix + itemsCount,
          className: (0, _classnames2.default)('qfa1-dropdown-list__list-item', { 'qfa1-dropdown-list__list-item--selected': selected }),
          onMouseOver: function onMouseOver() {
            return props.handleHoverItem(itemsCount);
          },
          onTouchStart: function onTouchStart() {
            return props.handleHoverItem(itemsCount);
          },
          'aria-selected': selected,
          onClick: props.clickAction,
          tabIndex: '-1' },
        _react2.default.createElement(
          'div',
          null,
          text
        )
      );
    }
  }, {
    key: 'createNotFound',
    value: function createNotFound(itemsCount) {
      var _this2 = this;

      var _props2 = this.props,
          itemIdPrefix = _props2.itemIdPrefix,
          selectedItem = _props2.selectedItem,
          value = _props2.value,
          noResultsText = _props2.noResultsText;

      var selected = itemsCount === selectedItem;

      var noResults = noResultsText.split(/(\{value})/g);
      noResults = noResults.map(function (item, index) {
        return index % 2 === 0 ? _react2.default.createElement(
          'span',
          { key: index },
          item
        ) : _react2.default.createElement(
          'strong',
          { key: index },
          '\'' + value + '\''
        );
      });

      return _react2.default.createElement(
        'li',
        {
          className: (0, _classnames2.default)('qfa1-dropdown-list__list-item', { 'qfa1-dropdown-list__list-item--selected': selected }),
          'aria-selected': selected,
          id: itemIdPrefix + itemsCount,
          onMouseOver: function onMouseOver() {
            return _this2.props.handleHoverItem(itemsCount);
          },
          onTouchStart: function onTouchStart() {
            return _this2.props.handleHoverItem(itemsCount);
          },
          role: 'option',
          onClick: this.props.clickAction },
        _react2.default.createElement(
          'div',
          null,
          noResults
        )
      );
    }
  }, {
    key: 'scrollWithinView',
    value: function scrollWithinView() {
      if (this.refs.list && this.props.selectedItem !== -1) {
        var listHeight = this.refs.list.clientHeight;
        var listScrollHeight = this.refs.list.scrollHeight;
        if (listHeight < listScrollHeight) {
          var childNodes = this.refs.list.childNodes;

          var sumHeight = 0;
          for (var i = 0; i < this.props.selectedItem; i++) {
            sumHeight += childNodes[i].offsetHeight;
          }var selectedItemHeight = childNodes[this.props.selectedItem].offsetHeight;
          var minHeight = this.refs.list.scrollTop - selectedItemHeight;
          var elmHeight = listHeight + selectedItemHeight - 1; // plus border
          var maxHeight = minHeight + elmHeight;
          if (!(minHeight < sumHeight && sumHeight < maxHeight)) this.refs.list.scrollTop = sumHeight;
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props3 = this.props,
          value = _props3.value,
          opened = _props3.opened,
          items = _props3.items,
          formatter = _props3.formatter,
          children = _props3.children,
          disabled = _props3.disabled,
          props = _objectWithoutProperties(_props3, ['value', 'opened', 'items', 'formatter', 'children', 'disabled']);

      var formattedItems = items.map(function (item) {
        return formatter(item, value);
      });
      var list = formattedItems.length ? formattedItems.map(function (item, index) {
        return _this3.createItem(item, index);
      }) : this.createNotFound(0);

      return _react2.default.createElement(
        _Dropdown2.default,
        { opened: opened, disabled: disabled },
        _react2.default.createElement(
          'ul',
          { className: 'qfa1-dropdown-list__list', role: 'listbox', id: props.listId, ref: 'list', tabIndex: '-1' },
          list
        ),
        children
      );
    }
  }]);

  return DropdownList;
}(_react.Component);

DropdownList.propTypes = {
  items: _propTypes2.default.array,
  handleHoverItem: _propTypes2.default.func.isRequired,
  clickAction: _propTypes2.default.func.isRequired,
  listId: _propTypes2.default.string.isRequired,
  noResultsText: _propTypes2.default.string.isRequired,
  itemIdPrefix: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  opened: _propTypes2.default.bool.isRequired,
  value: _propTypes2.default.string,
  selectedItem: _propTypes2.default.number,
  formatter: _propTypes2.default.func,
  children: _propTypes2.default.node,
  disabled: _propTypes2.default.bool
};
DropdownList.defaultProps = {
  items: [],
  value: '',
  clickClose: false,
  opened: false,
  selectedItem: -1,
  noResultsText: '',
  handleHoverItem: function handleHoverItem() {},
  formatter: function formatter(item) {
    return item;
  },
  disabled: false
};
exports.default = DropdownList;
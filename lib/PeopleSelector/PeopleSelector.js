'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _PeopleSelectorControlled = require('./PeopleSelectorControlled');

var _PeopleSelectorControlled2 = _interopRequireDefault(_PeopleSelectorControlled);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var OPEN = 'OPEN';
var SELECTION_CHANGE = 'SELECTION_CHANGE';
var BLUR = 'BLUR';
var INPUT_BLUR = 'INPUT_BLUR';
var FOCUS = 'FOCUS';
var CLOSE_AND_FOCUS = 'CLOSE_AND_FOCUS';

var PeopleSelector = function (_Component) {
  _inherits(PeopleSelector, _Component);

  function PeopleSelector(props) {
    _classCallCheck(this, PeopleSelector);

    var _this = _possibleConstructorReturn(this, (PeopleSelector.__proto__ || Object.getPrototypeOf(PeopleSelector)).call(this, props));

    var state = {
      opened: false,
      focused: false
    };
    if (!props.value) {
      state.value = props.initialValue || { adults: 1, children: 0, infants: 0 };
    }
    _this.state = state;
    return _this;
  }

  _createClass(PeopleSelector, [{
    key: 'send',
    value: function send(action, data) {
      var state = this.update(action, data);
      if (this.state !== state) {
        this.setState(state);
      }
    }
  }, {
    key: 'update',
    value: function update(action, data) {
      var state = this.state;
      switch (action) {
        case OPEN:
          {
            return _extends({}, state, { opened: true, focused: true });
          }
        case BLUR:
          {
            return _extends({}, state, { opened: false, focused: false });
          }
        case INPUT_BLUR:
          {
            return _extends({}, state, { focused: false });
          }
        case FOCUS:
          {
            if (!state.focused) {
              return _extends({}, state, { opened: true, focused: true });
            }
            return state;
          }
        case CLOSE_AND_FOCUS:
          {
            return _extends({}, state, { opened: false, focused: true });
          }
        case SELECTION_CHANGE:
          {
            var props = this.props;
            props.onChange(data);
            if (!props.value) {
              return _extends({}, state, { value: data });
            }
            return state;
          }
        default:
          return state;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var value = this.props.value || this.state.value;
      return _react2.default.createElement(_PeopleSelectorControlled2.default, _extends({}, this.props, this.state, {
        value: value,
        blur: function blur() {
          return _this2.send(BLUR);
        },
        onFocus: function onFocus() {
          return _this2.send(FOCUS);
        },
        inputBlur: function inputBlur() {
          return _this2.send(INPUT_BLUR);
        },
        closeAndFocus: function closeAndFocus() {
          return _this2.send(CLOSE_AND_FOCUS);
        },
        open: function open() {
          return _this2.send(OPEN);
        },
        onChange: function onChange(newValue) {
          return _this2.send(SELECTION_CHANGE, newValue);
        }
      }));
    }
  }]);

  return PeopleSelector;
}(_react.Component);

PeopleSelector.propTypes = {
  initialValue: _propTypes2.default.object,
  copy: _propTypes2.default.shape({
    adults: _propTypes2.default.object.isRequired,
    youths: _propTypes2.default.shape({
      ariaLabel: _propTypes2.default.string,
      ariaMaxLimitMessage: _propTypes2.default.string,
      ariaMinLimitMessage: _propTypes2.default.string,
      label: _propTypes2.default.string,
      plural: _propTypes2.default.string,
      singular: _propTypes2.default.string
    }),
    children: _propTypes2.default.object.isRequired,
    infants: _propTypes2.default.object.isRequired,
    buttonText: _propTypes2.default.string.isRequired,
    formLabel: _propTypes2.default.string.isRequired,
    ariaLabel: _propTypes2.default.string.isRequired,
    ariaSelectButton: _propTypes2.default.string.isRequired,
    information: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string])
  }).isRequired,
  value: _propTypes2.default.object,
  range: _propTypes2.default.object,
  validationMessage: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired
};
PeopleSelector.defaultProps = {
  range: {
    adults: {
      rangeStart: 1,
      rangeEnd: 9
    },
    youths: {
      rangeStart: 0,
      rangeEnd: 9
    },
    children: {
      rangeStart: 0,
      rangeEnd: 9
    },
    infants: {
      rangeStart: 0,
      rangeEnd: 9
    }
  },
  copy: {
    youths: {}
  }
};
exports.default = PeopleSelector;
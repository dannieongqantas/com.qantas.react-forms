'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PeopleSelectorControlled = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.createSelectionText = createSelectionText;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ComponentBlur = require('../ReusableComponents/ComponentBlur');

var _ComponentBlur2 = _interopRequireDefault(_ComponentBlur);

var _ArrowIcon = require('../Icons/ArrowIcon');

var _ArrowIcon2 = _interopRequireDefault(_ArrowIcon);

var _InputArea = require('../Input/InputArea');

var _InputArea2 = _interopRequireDefault(_InputArea);

var _Dropdown = require('../Dropdown/Dropdown');

var _Dropdown2 = _interopRequireDefault(_Dropdown);

var _Button = require('../Button/Button');

var _Button2 = _interopRequireDefault(_Button);

var _NumberPicker = require('../NumberPicker/NumberPicker');

var _NumberPicker2 = _interopRequireDefault(_NumberPicker);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _stringBind = require('../utils/stringBind');

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* @note
 * It would be good if we could pass in a cleaner object into this component, without splitting up value, copy etc.
*/

function createSelectionText(people, copy) {
  return Object.keys(people).reduce(function (previousText, key) {
    var text = '';
    if (people[key]) {
      if (previousText) text += ', ';
      text += people[key] > 1 ? people[key] + ' ' + copy[key].plural : people[key] + ' ' + copy[key].singular;
    }
    return previousText + text;
  }, '');
}

var PeopleSelectorControlled = exports.PeopleSelectorControlled = function (_Component) {
  _inherits(PeopleSelectorControlled, _Component);

  function PeopleSelectorControlled(props) {
    _classCallCheck(this, PeopleSelectorControlled);

    var _this = _possibleConstructorReturn(this, (PeopleSelectorControlled.__proto__ || Object.getPrototypeOf(PeopleSelectorControlled)).call(this, props));

    _initialiseProps.call(_this);

    _this.id = props.id || 'people-selector-input-' + (0, _idGenerator2.default)();
    return _this;
  }

  _createClass(PeopleSelectorControlled, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          opened = _props.opened,
          focused = _props.focused,
          range = _props.range,
          value = _props.value,
          copy = _props.copy,
          className = _props.className,
          blur = _props.blur,
          screenRead = _props.screenRead,
          validationMessage = _props.validationMessage;


      var combinedValue = createSelectionText(value, copy);

      var classname = (0, _classnames2.default)('widget-form__group-container', className);

      return _react2.default.createElement(
        'div',
        { className: classname, onKeyDown: this.keyDownHandler },
        _react2.default.createElement(
          'div',
          { ref: function ref(el) {
              _this2[_this2.constructor.componentRef] = el;
            }, onBlur: blur },
          _react2.default.createElement(
            _InputArea2.default,
            {
              ref: 'input',
              className: 'qfa1-people-selector__input',
              value: combinedValue,
              id: this.id,
              onBlur: this.props.inputBlur,
              onFocus: this.props.onFocus,
              onClick: this.props.open,
              popup: opened,
              formLabel: copy.formLabel,
              ariaLabel: copy.ariaLabel,
              focused: focused,
              validationMessage: validationMessage,
              noEntry: true },
            _react2.default.createElement(_ArrowIcon2.default, {
              className: 'qfa1-arrow-icon__dropdown',
              direction: opened ? 'up' : 'down',
              onClick: this.toggleDropdown })
          ),
          _react2.default.createElement(_ScreenReader2.default, { text: screenRead }),
          _react2.default.createElement(
            _Dropdown2.default,
            { opened: opened },
            _react2.default.createElement(
              'div',
              { className: 'qfa1-dropdown' },
              Object.keys(value).map(function (key) {
                return _react2.default.createElement(_NumberPicker2.default, {
                  key: key,
                  rangeStart: range[key].rangeStart,
                  rangeEnd: range[key].rangeEnd,
                  value: value[key],
                  label: copy[key].label,
                  ariaLabel: copy[key].ariaLabel,
                  ariaMinLimitMessage: copy[key].ariaMinLimitMessage,
                  ariaMaxLimitMessage: copy[key].ariaMaxLimitMessage,
                  information: copy[key].information,
                  onChange: function onChange(val) {
                    _this2.numberPickerChange(key, val);
                  }
                });
              }),
              _react2.default.createElement(_Button2.default, {
                type: 'button',
                text: copy.buttonText,
                className: 'qfa1-people-selector__button',
                ariaLabel: (0, _stringBind.stringBind)(copy.ariaSelectButton, { value: combinedValue }),
                onClick: this.props.closeAndFocus })
            )
          )
        )
      );
    }
  }]);

  return PeopleSelectorControlled;
}(_react.Component);

PeopleSelectorControlled.propTypes = {
  value: _propTypes2.default.object.isRequired,
  range: _propTypes2.default.object,
  blur: _propTypes2.default.func.isRequired,
  copy: _propTypes2.default.shape({
    adults: _propTypes2.default.object.isRequired,
    youths: _propTypes2.default.shape({
      ariaLabel: _propTypes2.default.string,
      ariaMaxLimitMessage: _propTypes2.default.string,
      ariaMinLimitMessage: _propTypes2.default.string,
      label: _propTypes2.default.string,
      plural: _propTypes2.default.string,
      singular: _propTypes2.default.string
    }),
    children: _propTypes2.default.object.isRequired,
    infants: _propTypes2.default.object.isRequired,
    buttonText: _propTypes2.default.string.isRequired,
    formLabel: _propTypes2.default.string.isRequired,
    ariaLabel: _propTypes2.default.string.isRequired,
    ariaSelectButton: _propTypes2.default.string.isRequired,
    information: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string])
  }).isRequired,
  opened: _propTypes2.default.bool.isRequired,
  focused: _propTypes2.default.bool.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  validationMessage: _propTypes2.default.string,
  open: _propTypes2.default.func.isRequired,
  onFocus: _propTypes2.default.func.isRequired,
  id: _propTypes2.default.string,
  inputBlur: _propTypes2.default.func.isRequired,
  closeAndFocus: _propTypes2.default.func.isRequired,
  className: _propTypes2.default.string,
  screenRead: _propTypes2.default.string
};
PeopleSelectorControlled.defaultProps = {
  className: '',
  screenRead: ''
};
PeopleSelectorControlled.componentRef = 'peopleSelectorControlled';

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.numberPickerChange = function (key, value) {
    var props = _this3.props;
    props.onChange(_extends({}, props.value, _defineProperty({}, key, value)));
  };

  this.keyDownHandler = function (e) {
    switch (e.keyCode) {
      case _KeyCodes.SPACE_KEY_CODE:
      case _KeyCodes.RETURN_KEY_CODE:
        e.preventDefault();
        _this3.toggleDropdown();
        break;
      case _KeyCodes.ESCAPE_KEY_CODE:
        e.preventDefault();
        if (_this3.props.opened) {
          _this3.props.closeAndFocus();
        }
        break;
      default:
        break;
    }
  };

  this.toggleDropdown = function () {
    if (_this3.props.opened) {
      _this3.props.closeAndFocus();
    } else {
      _this3.props.open();
    }
  };
};

exports.default = (0, _ComponentBlur2.default)(PeopleSelectorControlled, PeopleSelectorControlled.componentRef, function (props) {
  return props.opened ? props.blur() : null;
});
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NumberPickerControlled = require('./NumberPickerControlled');

var _NumberPickerControlled2 = _interopRequireDefault(_NumberPickerControlled);

var _screenReading = require('../utils/screenReading');

var _stringBind = require('../utils/stringBind');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumberPicker = function (_Component) {
  _inherits(NumberPicker, _Component);

  function NumberPicker(props) {
    _classCallCheck(this, NumberPicker);

    var _this = _possibleConstructorReturn(this, (NumberPicker.__proto__ || Object.getPrototypeOf(NumberPicker)).call(this, props));

    _this.state = {
      screenRead: ''
    };
    if (!props.hasOwnProperty('value')) {
      _this.state.value = props.initialValue || 0;
    }
    _this.onChange = _this.onChange.bind(_this);
    return _this;
  }

  _createClass(NumberPicker, [{
    key: 'onChange',
    value: function onChange(value) {
      var _props = this.props,
          rangeStart = _props.rangeStart,
          rangeEnd = _props.rangeEnd,
          ariaMinLimitMessage = _props.ariaMinLimitMessage,
          ariaMaxLimitMessage = _props.ariaMaxLimitMessage;

      if (isNaN(value)) {
        return;
      }

      if (value < rangeStart) {
        this.setState({
          screenRead: (0, _screenReading.reread)(this.state.screenRead, (0, _stringBind.stringBind)(ariaMinLimitMessage, { minValue: rangeStart }))
        });
        return;
      }

      if (value > rangeEnd) {
        this.setState({
          screenRead: (0, _screenReading.reread)(this.state.screenRead, (0, _stringBind.stringBind)(ariaMaxLimitMessage, { maxValue: rangeEnd }))
        });
        return;
      }

      if (!this.props.hasOwnProperty('value')) {
        this.setState({
          value: value
        });
      }
      this.props.onChange(value);
    }
  }, {
    key: 'render',
    value: function render() {
      var value = this.props.hasOwnProperty('value') ? this.props.value : this.state.value;
      return _react2.default.createElement(_NumberPickerControlled2.default, {
        rangeStart: this.props.rangeStart,
        rangeEnd: this.props.rangeEnd,
        ariaLabel: this.props.ariaLabel,
        value: value,
        label: this.props.label,
        screenRead: this.state.screenRead,
        onChange: this.onChange,
        information: this.props.information
      });
    }
  }]);

  return NumberPicker;
}(_react.Component);

NumberPicker.propTypes = {
  label: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.number,
  initialValue: _propTypes2.default.number,
  ariaLabel: _propTypes2.default.string,
  rangeStart: _propTypes2.default.number,
  rangeEnd: _propTypes2.default.number,
  ariaMinLimitMessage: _propTypes2.default.string,
  ariaMaxLimitMessage: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired,
  information: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string])
};
exports.default = NumberPicker;
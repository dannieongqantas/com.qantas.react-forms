'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _PlusIcon = require('../Icons/PlusIcon');

var _PlusIcon2 = _interopRequireDefault(_PlusIcon);

var _MinusIcon = require('../Icons/MinusIcon');

var _MinusIcon2 = _interopRequireDefault(_MinusIcon);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _Tooltip = require('../Tooltip/Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _KeyCodes = require('../Constants/KeyCodes');

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DEFAULT_RANGE_END = 10;

var NumberPickerControlled = function (_Component) {
  _inherits(NumberPickerControlled, _Component);

  function NumberPickerControlled(props) {
    _classCallCheck(this, NumberPickerControlled);

    var _this = _possibleConstructorReturn(this, (NumberPickerControlled.__proto__ || Object.getPrototypeOf(NumberPickerControlled)).call(this, props));

    _this.handleKeyDown = _this.handleKeyDown.bind(_this);
    _this.id = props.id || 'number-picker-input-' + (0, _idGenerator2.default)();
    return _this;
  }

  _createClass(NumberPickerControlled, [{
    key: 'handleKeyDown',
    value: function handleKeyDown(event) {
      var keyCode = event.keyCode;


      switch (keyCode) {
        case _KeyCodes.ARROW_UP_KEY_CODE:
          event.preventDefault();
          this.props.onChange(this.clampValueByRange(1));
          break;
        case _KeyCodes.ARROW_DOWN_KEY_CODE:
          event.preventDefault();
          this.props.onChange(this.clampValueByRange(-1));
          break;
        default:
          break;
      }

      if (_KeyCodes.ZERO_KEY_CODE <= keyCode && keyCode < _KeyCodes.ZERO_KEY_CODE + DEFAULT_RANGE_END) {
        event.preventDefault();
        this.props.onChange(keyCode - _KeyCodes.ZERO_KEY_CODE);
      }
    }
  }, {
    key: 'clampValueByRange',
    value: function clampValueByRange(increment) {
      var newValue = this.props.value + increment;
      if (newValue < this.props.rangeStart) {
        newValue = this.props.rangeStart;
      }
      if (newValue > this.props.rangeEnd) {
        newValue = this.props.rangeEnd;
      }
      return newValue;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          screenRead = _props.screenRead,
          label = _props.label,
          value = _props.value,
          ariaLabel = _props.ariaLabel,
          rangeStart = _props.rangeStart,
          rangeEnd = _props.rangeEnd,
          information = _props.information;

      var plusIconClasses = (0, _classnames2.default)('qfa1-numberpicker__plus-icon', {
        'qfa1-numberpicker__plus-icon--disabled': value >= rangeEnd
      });

      var minusIconClasses = (0, _classnames2.default)('qfa1-numberpicker__minus-icon', {
        'qfa1-numberpicker__minus-icon--disabled': value <= rangeStart
      });

      return _react2.default.createElement(
        'div',
        { className: 'qfa1-numberpicker__group widget-form__group' },
        _react2.default.createElement(
          'label',
          { className: 'widget-form__label', htmlFor: this.id },
          label
        ),
        _react2.default.createElement(
          _Tooltip2.default,
          { align: 'right' },
          information
        ),
        _react2.default.createElement('input', { className: 'qfa1-input qfa1-numberpicker__input', type: 'number', value: value, 'aria-label': ariaLabel, id: this.id,
          'aria-valuemin': rangeStart, 'aria-valuemax': rangeEnd, 'aria-valuenow': value, role: 'spinbutton',
          onKeyDown: this.handleKeyDown, onChange: function onChange() {} }),
        _react2.default.createElement(_MinusIcon2.default, { className: minusIconClasses, disabled: value <= rangeStart, onClick: function onClick() {
            return _this2.props.onChange(_this2.clampValueByRange(-1));
          } }),
        _react2.default.createElement(_PlusIcon2.default, { className: plusIconClasses, disabled: value >= rangeEnd, onClick: function onClick() {
            return _this2.props.onChange(_this2.clampValueByRange(1));
          } }),
        _react2.default.createElement(_ScreenReader2.default, { text: screenRead })
      );
    }
  }]);

  return NumberPickerControlled;
}(_react.Component);

NumberPickerControlled.propTypes = {
  label: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.number.isRequired,
  rangeStart: _propTypes2.default.number.isRequired,
  rangeEnd: _propTypes2.default.number.isRequired,
  ariaLabel: _propTypes2.default.string.isRequired,
  screenRead: _propTypes2.default.string,
  information: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string]),
  id: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired
};
NumberPickerControlled.defaultProps = {
  rangeStart: 0,
  rangeEnd: DEFAULT_RANGE_END
};
exports.default = NumberPickerControlled;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _InfoIcon = require('../Icons/InfoIcon');

var _InfoIcon2 = _interopRequireDefault(_InfoIcon);

var _BorderTriangle = require('../Icons/BorderTriangle');

var _BorderTriangle2 = _interopRequireDefault(_BorderTriangle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tooltip = function (_Component) {
  _inherits(Tooltip, _Component);

  function Tooltip(props) {
    _classCallCheck(this, Tooltip);

    var _this = _possibleConstructorReturn(this, (Tooltip.__proto__ || Object.getPrototypeOf(Tooltip)).call(this, props));

    _this.getTooltipPosLeft = function (width) {
      var tooltipWidthOffset = 20;

      switch (_this.props.align) {
        case 'center':
          return '-' + width / 2 + 'px';
        case 'right':
          return '-' + (width - tooltipWidthOffset) + 'px';
        case 'left':
        default:
          return '0px';
      }
    };

    _this.showTooltip = function () {
      var _this$refs$tooltipCon = _this.refs.tooltipContent.getBoundingClientRect(),
          width = _this$refs$tooltipCon.width,
          height = _this$refs$tooltipCon.height;

      var arrowOffset = 45;

      _this.setState({
        posTop: -height - arrowOffset + 'px',
        posLeft: _this.getTooltipPosLeft(width),
        isVisible: true
      });
    };

    _this.hideToolTip = function () {
      _this.setState({
        isVisible: false
      });
    };

    _this.state = {
      isVisible: false,
      posTop: 0,
      posLeft: 0
    };
    return _this;
  }

  _createClass(Tooltip, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          className = _props.className,
          align = _props.align,
          title = _props.title;
      var _state = this.state,
          isVisible = _state.isVisible,
          posTop = _state.posTop,
          posLeft = _state.posLeft;

      var tooltipClasses = (0, _classnames2.default)(className, 'qfa1-tool-tip');
      var contentClass = (0, _classnames2.default)('qfa1-tool-tip__content', { 'qfa1-tool-tip__hidden': !isVisible });
      var arrowClass = (0, _classnames2.default)('qfa1-tool-tip__arrow', 'qfa1-tool-tip__arrow-' + align);

      return children ? _react2.default.createElement(
        'div',
        { className: tooltipClasses, onClick: this.showTooltip, onFocus: this.showTooltip, onBlur: this.hideToolTip },
        _react2.default.createElement(
          'div',
          { tabIndex: '0', className: 'qfa1-tool-tip__icon' },
          _react2.default.createElement(_InfoIcon2.default, { title: title })
        ),
        _react2.default.createElement(
          'div',
          { style: { marginTop: posTop, marginLeft: posLeft }, ref: 'tooltipContent', className: contentClass },
          children,
          _react2.default.createElement(
            'div',
            { className: arrowClass },
            _react2.default.createElement(_BorderTriangle2.default, { innerColor: '#bff4f2', outerColor: '#bff4f2' })
          )
        )
      ) : null;
    }
  }]);

  return Tooltip;
}(_react.Component);

Tooltip.propTypes = {
  className: _propTypes2.default.string,
  title: _propTypes2.default.string,
  align: _propTypes2.default.string,
  children: _propTypes2.default.node
};
Tooltip.defaultProps = {
  align: 'center',
  title: 'Information Icon'
};
exports.default = Tooltip;
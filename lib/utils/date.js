'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.retrieveLocaleDateFormatter = retrieveLocaleDateFormatter;
exports.getDayBeginFromMonday = getDayBeginFromMonday;
exports.dateDiff = dateDiff;
exports.maxDate = maxDate;
exports.transformDateToLongWords = transformDateToLongWords;
exports.getDaysInMonth = getDaysInMonth;
exports.plusDays = plusDays;
exports.plusMonths = plusMonths;
exports.currentDateTime = currentDateTime;
exports.currentDate = currentDate;
exports.currentTime = currentTime;
exports.firstDateInMonth = firstDateInMonth;
exports.isDateEqual = isDateEqual;
exports.dateFormatter = dateFormatter;
exports.createDateTime = createDateTime;
exports.datePart = datePart;
exports.formatFlight = formatFlight;
exports.serialize = serialize;
exports.deserialize = deserialize;
exports.compare = compare;

var _format = require('date-fns/format');

var _format2 = _interopRequireDefault(_format);

var _dateLocale = require('../i18n/shared/dateLocale');

var _dateLocale2 = _interopRequireDefault(_dateLocale);

var _transformLanguageCode = require('../i18n/transformLanguageCode');

var _transformLanguageCode2 = _interopRequireDefault(_transformLanguageCode);

var _getDataForLanguageCode = require('../i18n/getDataForLanguageCode');

var _getDataForLanguageCode2 = _interopRequireDefault(_getDataForLanguageCode);

var _stringBind = require('./stringBind');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO - SME - REPLACE CAPITALIZE FUNCTION

// ripped straight from http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript/196991#196991
function capitalize(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

function retrieveLocaleDateFormatter(languageCode) {
  if (!languageCode && process.env.NODE_ENV !== 'production') {
    console.error('Make sure languageCode is passed to retrieveLocaleDateFormatter(). Defaulting to use \'en\' for languageCode'); //eslint-disable-line
  }
  var lang = (0, _transformLanguageCode2.default)(languageCode || 'en');
  var retrieveLocaleFunctions = (0, _getDataForLanguageCode2.default)(_dateLocale2.default, lang);
  return retrieveLocaleFunctions().then(function (locale) {
    return function (date, format) {
      return (0, _format2.default)(date, format, { locale: locale });
    };
  });
}

// getDayBeginFromMonday returns 1-7 for monday-sunday
// the getDay method returns 0-6 for sunday-saturday
function getDayBeginFromMonday(date) {
  return (date.getDay() + 6) % 7 + 1;
}

var MS_PER_DAY = 1000 * 60 * 60 * 24;

function dateDiff(a, b) {
  // Discard the time and time-zone information.
  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor(Math.abs(utc2 - utc1) / MS_PER_DAY);
}

function maxDate(a, b) {
  return a.getTime() > b.getTime() ? a : b;
}

function transformDateToLongWords(date, i18n) {
  var defaultVal = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  if (!(date instanceof Date)) return defaultVal;

  var weekDay = capitalize(i18n.weekdays[getDayBeginFromMonday(date)].long);
  var dayOfMonth = i18n.dayOfMonth[date.getDate()];
  var month = capitalize(i18n.months[date.getMonth() + 1].long);
  var year = date.getFullYear();

  return (0, _stringBind.stringBind)(i18n.aria.dateLong, {
    weekDay: weekDay, month: month, year: year, dayOfMonth: dayOfMonth
  });
}

// get days in months
function getDaysInMonth(year, month) {
  var adjustedMonth = month + 1;
  var adjustedYear = year;
  if (month === 12) {
    adjustedYear = year + 1;
    adjustedMonth = 0;
  }
  return new Date(adjustedYear, adjustedMonth, 0).getDate();
}

function plusDays(date, days) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() + days);
}

function plusMonths(date, months) {
  return new Date(date.getFullYear(), date.getMonth() + months, date.getDate());
}

function currentDateTime() {
  return new Date();
}

function currentDate() {
  var today = new Date();
  return new Date(today.getFullYear(), today.getMonth(), today.getDate());
}

function currentTime() {
  var todayTime = new Date();
  return {
    hours: todayTime.getHours(),
    minutes: todayTime.getMinutes(),
    seconds: todayTime.getSeconds()
  };
}

function firstDateInMonth(date) {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}

function isDateEqual(date1, date2) {
  return date1.getTime() === date2.getTime();
}

/*
 * return mm/dd/yyyy
 */
function dateFormatter(date) {
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var year = date.getFullYear();
  return month + '/' + day + '/' + year;
}

function createDateTime(date, timeString) {
  var datePartVar = new Date(date);
  var timeParts = timeString.split(':');
  return new Date(datePartVar.getFullYear(), datePartVar.getMonth(), datePartVar.getDate(), timeParts[0], timeParts[1]);
}

function datePart(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

function prependZero(value) {
  return value.length === 1 ? '0' + value : value;
}

function formatFlight(date) {
  var month = prependZero((date.getMonth() + 1).toString());
  var day = prependZero(date.getDate().toString());
  var year = date.getFullYear();
  var hour = ('0' + date.getHours()).substr(-2);
  return '' + year + month + day + hour + '00';
}

function serialize(date) {
  return date.getTime() + '-1-0-0';
}

function deserialize(value) {
  var serialisedDate = value.split('-');
  return new Date(parseInt(serialisedDate[0], 10));
}

function compare(date) {
  return {
    isGreaterThan: function isGreaterThan(compareDate) {
      var equality = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (equality) return date.getTime() >= compareDate.getTime();
      return date.getTime() > compareDate.getTime();
    }
  };
}
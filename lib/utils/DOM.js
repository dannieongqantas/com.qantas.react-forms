'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.scrollToTop = scrollToTop;
/*
 * All independent dom events
 */

var FRAME = 1000 / 60;

var canUseDOM = exports.canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

var SCROLL_DURATION = 150;

function scrollTick(ticks, height, perTick) {
  var _window = window,
      pageXOffset = _window.pageXOffset;

  window.scrollTo(pageXOffset, height);
  if (ticks > 0) setTimeout(function () {
    return scrollTick(ticks - 1, height + perTick, perTick);
  }, FRAME);
}

function scrollToTop(elm) {
  var ms = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : SCROLL_DURATION;

  setTimeout(function () {
    var _window2 = window,
        pageYOffset = _window2.pageYOffset;

    if (elm) {
      var elmTop = elm.getBoundingClientRect().top;
      var ticks = Math.ceil(ms / FRAME);
      var perTick = elmTop / ticks;

      scrollTick(ticks - 1, pageYOffset + perTick, perTick);
    }
  }, 0);
}
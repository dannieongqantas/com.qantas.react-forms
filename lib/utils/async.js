'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaAsync = ariaAsync;

var _userAgent = require('./userAgent');

// This is for osx safari whilst using voiceover
// When there are dom changes, it stops reading the labels. e.g
// When blurring from calendar to timepicker, calendar is removed from dom and timepicker is prefilled then it wont read timepicker label
function ariaAsync(cb) {
  return (0, _userAgent.isSafari)() ? setTimeout(cb, 0) : cb();
}
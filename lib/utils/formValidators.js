"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isBlank = isBlank;
exports.isExactLength = isExactLength;
exports.isAlphaCharsOnly = isAlphaCharsOnly;
exports.isAlphanumeric = isAlphanumeric;
exports.isEmail = isEmail;
function isBlank(value) {
  return value.length === 0;
}

function isExactLength(length) {
  return function (value) {
    return value.length === length;
  };
}

function isAlphaCharsOnly(value) {
  return (/^[A-Z]+$/i.test(value)
  );
}

function isAlphanumeric(value) {
  return (/^[A-Z0-9]+$/i.test(value)
  );
}

function isEmail(value) {
  return (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i.test(value)
  );
}
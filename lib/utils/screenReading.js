'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reread = reread;
// Causes a reread if the text is the same by appending or removing a full stop
function reread(currentText, newText) {
  if (newText === currentText) {
    if (currentText[currentText.length - 1] === '.') {
      return currentText.substring(0, currentText.length - 1);
    }
    return currentText + '.';
  }
  return newText;
}
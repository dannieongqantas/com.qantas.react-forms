'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = deprecationNotice;
function deprecationNotice(message) {
  if (process.ENV !== 'production') console.warn('Deprecation notice - ' + message); //eslint-disable-line
}
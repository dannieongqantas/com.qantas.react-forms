'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Store = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _date = require('./date');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LAST_UPDATED_DATE_KEY = 'lastUpdatedDate';
var VERSION_KEY = 'versionKey';
var DAYS_SAVED_SEARCH_VALID_FOR = 30;

var Store = exports.Store = function () {
  function Store(localStore) {
    var appKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var version = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'DEFAULT';
    var expiryLength = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : DAYS_SAVED_SEARCH_VALID_FOR;

    _classCallCheck(this, Store);

    if (!appKey) throw new Error('localStore must contain an appKey');

    this.appKey = appKey;
    this.localStore = localStore;
    this.currentVersion = version;
    this.expiryLength = expiryLength;
    this.checkDataReset();
  }

  _createClass(Store, [{
    key: 'set',
    value: function set(key, value) {
      var storage = this.getStorage();
      storage[LAST_UPDATED_DATE_KEY] = (0, _date.currentDate)();
      storage[key] = value;
      this.saveStorage(storage);
    }
  }, {
    key: 'get',
    value: function get(key) {
      return this.getStorage()[key];
    }
  }, {
    key: 'checkDataReset',
    value: function checkDataReset() {
      if (this.isVersionChanged() || this.isExpired()) {
        this.localStore.removeItem(this.appKey);
        this.set(VERSION_KEY, this.currentVersion);
      }
    }
  }, {
    key: 'isVersionChanged',
    value: function isVersionChanged() {
      var storage = this.getStorage();
      var storedVersion = storage[VERSION_KEY];
      return this.currentVersion !== storedVersion;
    }
  }, {
    key: 'isExpired',
    value: function isExpired() {
      var storage = this.getStorage();
      var lastUpdatedDateString = storage[LAST_UPDATED_DATE_KEY];
      return (0, _date.plusDays)(new Date(lastUpdatedDateString), this.expiryLength).getTime() < (0, _date.currentDate)().getTime();
    }
  }, {
    key: 'saveStorage',
    value: function saveStorage(storage) {
      try {
        // For safari private browsing mode
        this.localStore.setItem(this.appKey, JSON.stringify(storage));
      } catch (e) {} //eslint-disable-line
    }
  }, {
    key: 'getStorage',
    value: function getStorage() {
      var appData = this.localStore.getItem(this.appKey);
      return JSON.parse(appData || '{}');
    }
  }]);

  return Store;
}();

var initializeStore = function initializeStore(appKey, version, expiryLength) {
  return new Store(window.localStorage, appKey, version, expiryLength);
};
exports.default = initializeStore;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cookie = exports.Cookie = function () {
  function Cookie(document) {
    _classCallCheck(this, Cookie);

    this.document = document;
  }

  _createClass(Cookie, [{
    key: 'get',
    value: function get(key) {
      var cookies = this.document.cookie.split(';');
      var matchingCookie = cookies.find(function (cookie) {
        return cookie.trim().indexOf(key + '=') === 0;
      });
      if (matchingCookie) return matchingCookie.split('=')[1];
      return '';
    }
  }, {
    key: 'set',
    value: function set(key, value, expiryMins) {
      var cookie = key + '=' + value + ';';

      if (expiryMins) {
        var date = new Date();
        var cookieExpiry = new Date(date.getTime() + 1000 * 60 * expiryMins).toUTCString();
        cookie += ' expires=' + cookieExpiry + ';';
      }
      this.document.cookie = cookie;
    }
  }]);

  return Cookie;
}();

var cookie = new Cookie(document);
exports.default = cookie;
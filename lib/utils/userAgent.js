'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isTouchDevice = isTouchDevice;
exports.isSafari = isSafari;
exports.isIE = isIE;
exports.isEdge = isEdge;
exports.isFirefox = isFirefox;

var _canUseDOM = require('./canUseDOM');

var _canUseDOM2 = _interopRequireDefault(_canUseDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isTouchDevice() {
  try {
    document.createEvent('TouchEvent');
    return true;
  } catch (e) {
    return false;
  }
}

// Try to avoid using browser detect unless neccessary
var ua = _canUseDOM2.default ? navigator.userAgent : 'nodejs';
function isSafari() {
  return ua.indexOf('Safari') !== -1 && ua.indexOf('Chrome') === -1;
}

// Returns ie10 or less
function isIE() {
  return ua.indexOf('MSIE') !== -1;
}

function isEdge() {
  return ua.indexOf('Edge') !== -1 || ua.indexOf('Trident') !== -1;
}

function isFirefox() {
  return ua.indexOf('Firefox') !== -1;
}
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalyticsInternal = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _date = require('./date');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function currentPagePathWithOrigin() {
  return window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname;
}

var url = currentPagePathWithOrigin();

var AnalyticsInternal = exports.AnalyticsInternal = function () {
  function AnalyticsInternal(dataLayer, module, categories) {
    _classCallCheck(this, AnalyticsInternal);

    this.dataLayer = dataLayer;
    this.module = module;
    this.categories = categories;
  }

  _createClass(AnalyticsInternal, [{
    key: 'setEvent',
    value: function setEvent(event) {
      if (this.dataLayer && this.dataLayer.setEvent) {
        var sendEvent = _extends({
          timestamp: (0, _date.currentDateTime)(),
          module: this.module,
          categories: this.categories,
          points: 100
        }, event);

        sendEvent.attributes = sendEvent.attributes || {};
        sendEvent.attributes.url = url;

        this.dataLayer.setEvent(sendEvent);
      }
    }
  }, {
    key: 'setListing',
    value: function setListing(items) {
      if (this.dataLayer && this.dataLayer.setListing) {
        this.dataLayer.setListing({
          url: this.url,
          categories: this.categories,
          name: this.module,
          items: items
        });
      }
    }
  }]);

  return AnalyticsInternal;
}();

exports.default = AnalyticsInternal.bind(null, window.dataLayer || {});
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createNonHighlightedWord = createNonHighlightedWord;
exports.default = highlightWords;

var _escapeStringRegexp = require('escape-string-regexp');

var _escapeStringRegexp2 = _interopRequireDefault(_escapeStringRegexp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createNonHighlightedWord(text) {
  return {
    highlighted: false,
    text: text
  };
}

function highlightWords(words, target) {
  if (!target) return [{ highlighted: false, text: words }];

  var targetRegex = new RegExp((0, _escapeStringRegexp2.default)(target), 'gi');
  var matches = words.match(targetRegex) || [];
  var splitWords = words.split(targetRegex);
  var highlightedWords = [];
  splitWords.forEach(function (text, index) {
    if (text) highlightedWords.push(createNonHighlightedWord(text));
    matches[index] ? highlightedWords.push({ highlighted: true, text: matches[index] }) : null;
  });
  return highlightedWords;
}
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stringBind = stringBind;
function stringBind(str, items) {
  if (typeof str !== 'string') return str;
  var result = str;
  Object.keys(items).forEach(function (key) {
    var value = items[key];
    result = result.split('{' + key + '}').join(value);
  });
  return result;
}
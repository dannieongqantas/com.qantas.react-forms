'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.appendScript = appendScript;
exports.appendStyles = appendStyles;
function appendScript(scripts, async) {
  if (scripts && Array.isArray(scripts)) {
    scripts.forEach(function (src) {
      var script = document.createElement('script');
      script.src = src;
      script.async = typeof async === 'undefined' ? true : async;
      document.body.appendChild(script);
    });
  }
}

function appendStyles(styles) {
  if (styles && Array.isArray(styles)) {
    styles.forEach(function (href) {
      var link = document.createElement('link');
      link.href = href;
      link.rel = 'stylesheet';
      link.type = 'text/css';
      document.head.appendChild(link);
    });
  }
}
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ButtonLink = function (_Component) {
  _inherits(ButtonLink, _Component);

  function ButtonLink(props) {
    _classCallCheck(this, ButtonLink);

    var _this = _possibleConstructorReturn(this, (ButtonLink.__proto__ || Object.getPrototypeOf(ButtonLink)).call(this, props));

    _this.handleKeyDown = _this.handleKeyDown.bind(_this);
    _this.buttonClick = _this.buttonClick.bind(_this);
    return _this;
  }

  _createClass(ButtonLink, [{
    key: 'handleKeyDown',
    value: function handleKeyDown(e) {
      if (e.keyCode === _KeyCodes.RETURN_KEY_CODE) this.buttonClick();
    }
  }, {
    key: 'buttonClick',
    value: function buttonClick() {
      this.refs.link.click();
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          url = _props.url,
          prefetch = _props.prefetch,
          props = _objectWithoutProperties(_props, ['url', 'prefetch']);

      var prefetchLink = prefetch ? _react2.default.createElement('link', { rel: 'prefetch', href: url }) : null;

      var classnames = (0, _classnames2.default)('widget-form__group-container', props.className);

      return _react2.default.createElement(
        'div',
        { className: classnames },
        _react2.default.createElement(
          'div',
          { className: 'widget-form__group' },
          prefetchLink,
          _react2.default.createElement('a', { className: 'widget-form__element--hidden',
            ref: 'link',
            onClick: props.onClick,
            href: url }),
          _react2.default.createElement(
            'button',
            { className: 'qfa1-submit-button__button',
              type: props.type,
              onClick: this.buttonClick,
              onKeyDown: this.handleKeyDown, 'aria-label': props.ariaLabel },
            props.text
          )
        )
      );
    }
  }]);

  return ButtonLink;
}(_react.Component);

ButtonLink.propTypes = {
  text: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  type: _propTypes2.default.string,
  className: _propTypes2.default.string,
  ariaLabel: _propTypes2.default.string,
  onClick: _propTypes2.default.func,
  prefetch: _propTypes2.default.bool
};
ButtonLink.defaultProps = {
  type: 'submit',
  className: '',
  onClick: function onClick() {},
  prefetch: false
};
exports.default = ButtonLink;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _KeyCodes = require('../Constants/KeyCodes');

var _ControlledTypeahead = require('./ControlledTypeahead');

var _ControlledTypeahead2 = _interopRequireDefault(_ControlledTypeahead);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Typeahead = function (_Component) {
  _inherits(Typeahead, _Component);

  function Typeahead(props) {
    _classCallCheck(this, Typeahead);

    var _this = _possibleConstructorReturn(this, (Typeahead.__proto__ || Object.getPrototypeOf(Typeahead)).call(this, props));

    _this.focus = function () {
      _this.typeahead.focus();
    };

    _this.state = { opened: false, focused: false, navigatedItem: -1 };
    _this.isOpen = _this.isOpen.bind(_this);
    _this.handleHoverItem = _this.handleHoverItem.bind(_this);
    _this.keyDownHandler = _this.keyDownHandler.bind(_this);
    _this.navigateList = _this.navigateList.bind(_this);
    _this.clearField = _this.clearField.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    _this.handleFocus = _this.handleFocus.bind(_this);
    _this.blurInput = _this.blurInput.bind(_this);
    _this.selectItem = _this.selectItem.bind(_this);
    return _this;
  }

  _createClass(Typeahead, [{
    key: 'selectItem',
    value: function selectItem(event) {
      var selectedValue = this.props.items && this.state.navigatedItem >= 0 ? this.props.items[this.state.navigatedItem] : this.props.items[0];
      if (!!selectedValue) {
        this.props.onSelectItem(selectedValue, this.state.navigatedItem, event);
        this.props.clearValidation();
        this.setState({ opened: false });
      }
    }
  }, {
    key: 'clearField',
    value: function clearField() {
      this.props.onClearField();
      this.props.clearValidation();
    }
  }, {
    key: 'handleHoverItem',
    value: function handleHoverItem(item) {
      this.setState({ navigatedItem: item });
    }
  }, {
    key: 'handleFocus',
    value: function handleFocus() {
      this.setState({ navigatedItem: -1, opened: true, focused: true });
      this.props.onFocus();
    }
  }, {
    key: 'handleChange',
    value: function handleChange(e) {
      var value = e.target.value;
      if (value !== this.props.value) {
        this.props.onChange(value);
        this.setState({ navigatedItem: -1, opened: this.state.focused });
      }
    }
  }, {
    key: 'keyDownHandler',
    value: function keyDownHandler(e) {
      switch (e.keyCode) {
        case _KeyCodes.RETURN_KEY_CODE:
          e.preventDefault();
          return this.selectItem();
        case _KeyCodes.TAB_KEY_CODE:
          if (this.state.navigatedItem > -1 || !!this.props.value) {
            this.selectItem(e);
          }
          return true;
        case _KeyCodes.ARROW_UP_KEY_CODE:
          e.preventDefault();
          return this.navigateList(-1);
        case _KeyCodes.ARROW_DOWN_KEY_CODE:
          e.preventDefault();
          return this.navigateList(1);
        default:
          return null;
      }
    }
  }, {
    key: 'navigateList',
    value: function navigateList(direction) {
      var newNavigatedItem = this.state.navigatedItem + direction;
      var UPPER_BOUND = Math.max(this.props.items.length, 1);

      if (newNavigatedItem >= UPPER_BOUND) newNavigatedItem = -1;else if (newNavigatedItem < -1) newNavigatedItem = UPPER_BOUND - 1;

      this.setState({ navigatedItem: newNavigatedItem });
    }
  }, {
    key: 'blurInput',
    value: function blurInput() {
      if (this.state.focused) {
        this.setState({ opened: false, focused: false });
        this.props.onBlur();
      }
    }
  }, {
    key: 'isOpen',
    value: function isOpen() {
      var opened = this.state.opened;
      var _props = this.props,
          items = _props.items,
          value = _props.value,
          minLength = _props.minLength;
      // If it's opened and has items and value is greater than the minimum value then it should be opened.

      return !!(opened && items && value.length >= minLength);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var opened = this.isOpen();
      return _react2.default.createElement(_ControlledTypeahead2.default, _extends({}, this.props, {
        ref: function ref(typeahead) {
          _this2.typeahead = typeahead;
        },
        navigatedItem: this.state.navigatedItem,
        opened: opened,
        selectItem: this.selectItem,
        blurInput: this.blurInput,
        handleChange: this.handleChange,
        handleHoverItem: this.handleHoverItem,
        focusHandler: this.handleFocus,
        keyDownHandler: this.keyDownHandler,
        clearField: this.clearField }));
    }
  }]);

  return Typeahead;
}(_react.Component);

Typeahead.propTypes = {
  onChange: _propTypes2.default.func.isRequired,
  onFocus: _propTypes2.default.func,
  onBlur: _propTypes2.default.func,
  onSelectItem: _propTypes2.default.func.isRequired,
  clearValidation: _propTypes2.default.func,
  onClearField: _propTypes2.default.func,
  minLength: _propTypes2.default.number,
  value: _propTypes2.default.string,
  items: _propTypes2.default.array,
  isLabelInline: _propTypes2.default.bool,
  clearButtonTabbable: _propTypes2.default.bool,
  selectOnFocus: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool
};
Typeahead.defaultProps = {
  value: '',
  minLength: 3,
  onBlur: function onBlur() {},
  onFocus: function onFocus() {},
  clearValidation: function clearValidation() {},
  onSelectItem: function onSelectItem() {},
  onClearField: function onClearField() {},
  clearButtonTabbable: true,
  selectOnFocus: false,
  disabled: false
};
exports.default = Typeahead;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ControlledTypeahead = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _DropdownList = require('../DropdownList/DropdownList');

var _DropdownList2 = _interopRequireDefault(_DropdownList);

var _ComponentBlur = require('../ReusableComponents/ComponentBlur');

var _ComponentBlur2 = _interopRequireDefault(_ComponentBlur);

var _ClearButton = require('../Icons/ClearButton');

var _ClearButton2 = _interopRequireDefault(_ClearButton);

var _InputArea = require('../Input/InputArea');

var _InputArea2 = _interopRequireDefault(_InputArea);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var COMPONENT_REF = 'Typeahead';

var ControlledTypeahead = exports.ControlledTypeahead = function (_Component) {
  _inherits(ControlledTypeahead, _Component);

  function ControlledTypeahead(props) {
    _classCallCheck(this, ControlledTypeahead);

    var _this = _possibleConstructorReturn(this, (ControlledTypeahead.__proto__ || Object.getPrototypeOf(ControlledTypeahead)).call(this, props));

    _this.focus = function () {
      _this.refs.input.focus();
    };

    _this.id = props.id || (0, _idGenerator2.default)();
    return _this;
  }

  _createClass(ControlledTypeahead, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          blur = _props.blur,
          children = _props.children,
          isLabelInline = _props.isLabelInline,
          items = _props.items,
          navigatedItem = _props.navigatedItem,
          noResultsText = _props.noResultsText,
          opened = _props.opened,
          value = _props.value,
          props = _objectWithoutProperties(_props, ['blur', 'children', 'isLabelInline', 'items', 'navigatedItem', 'noResultsText', 'opened', 'value']);

      var inputId = 'typeahead-input-' + this.id;
      var descendantIdPrefix = 'typeahead-list-item-' + this.id + '-';
      var activeDescendant = navigatedItem === -1 ? '' : descendantIdPrefix + navigatedItem;

      var listId = descendantIdPrefix + 'list';

      var classname = (0, _classnames2.default)('widget-form__group-container', props.className, {
        'qfa1-typeahead--disabled': props.disabled
      });

      return _react2.default.createElement(
        'div',
        { className: classname },
        _react2.default.createElement(
          'div',
          { ref: function ref(el) {
              _this2[COMPONENT_REF] = el;
            }, onBlur: blur },
          _react2.default.createElement(
            _InputArea2.default,
            { ref: 'input',
              id: inputId,
              isLabelInline: isLabelInline,
              'aria-owns': listId,
              'aria-autocomplete': 'list',
              role: 'combobox',
              popup: opened,
              placeholder: props.placeholder,
              formLabel: props.formLabel,
              ariaLabel: props.ariaLabel || props.formLabel,
              activeDescendant: activeDescendant,
              value: value,
              selectOnFocus: props.selectOnFocus,
              onFocus: props.focusHandler,
              onKeyDown: props.keyDownHandler,
              onChange: props.handleChange,
              validationMessage: props.validationMessage,
              disabled: props.disabled },
            _react2.default.createElement(_ClearButton2.default, { tabbable: props.clearButtonTabbable, className: 'qfa1-typeahead__close-button', onClick: function onClick() {
                _this2.refs.input.focus();props.clearField();
              }, hide: !value, focusable: true, title: props.ariaClearButton })
          ),
          _react2.default.createElement(_ScreenReader2.default, { text: props.screenRead }),
          _react2.default.createElement(
            _DropdownList2.default,
            {
              formatter: props.formatter,
              noResultsText: noResultsText,
              itemIdPrefix: descendantIdPrefix,
              selectedItem: navigatedItem,
              items: items,
              listId: listId,
              value: value,
              opened: opened,
              handleHoverItem: props.handleHoverItem,
              clickAction: props.selectItem
            },
            children
          )
        )
      );
    }
  }]);

  return ControlledTypeahead;
}(_react.Component);

ControlledTypeahead.defaultProps = {
  value: '',
  items: [],
  ariaSuggestions: '',
  className: '',
  ariaClearButton: 'clear button',
  opened: false,
  placeholder: '',
  clearButtonTabbable: true,
  selectOnFocus: false,
  disabled: false
};
ControlledTypeahead.propTypes = {
  handleHoverItem: _propTypes2.default.func.isRequired,
  clearField: _propTypes2.default.func.isRequired,
  placeholder: _propTypes2.default.string,
  id: _propTypes2.default.string,
  isLabelInline: _propTypes2.default.bool,
  selectItem: _propTypes2.default.func.isRequired,
  handleChange: _propTypes2.default.func.isRequired,
  focusHandler: _propTypes2.default.func.isRequired,
  blurInput: _propTypes2.default.func.isRequired,
  keyDownHandler: _propTypes2.default.func.isRequired,
  items: _propTypes2.default.array,
  opened: _propTypes2.default.bool,
  formLabel: _propTypes2.default.string.isRequired,
  ariaLabel: _propTypes2.default.string,
  noResultsText: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string,
  navigatedItem: _propTypes2.default.number,
  validationMessage: _propTypes2.default.string,
  ariaSuggestions: _propTypes2.default.string,
  screenRead: _propTypes2.default.string,
  className: _propTypes2.default.string,
  ariaClearButton: _propTypes2.default.string,
  blur: _propTypes2.default.func.isRequired,
  children: _propTypes2.default.node,
  clearButtonTabbable: _propTypes2.default.bool,
  selectOnFocus: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool
};
exports.default = (0, _ComponentBlur2.default)(ControlledTypeahead, COMPONENT_REF, function (props) {
  return props.blurInput;
});
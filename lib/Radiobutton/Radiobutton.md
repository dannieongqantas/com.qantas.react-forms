## Checkbox

### Import to your project
```js
import Radiobutton from 'com.qantas.react-forms/Radiobutton';
import Radiogroup from 'com.qantas.react-forms/Radiogroup';
```
```scss
@import '~com.qantas.react-forms/scss/Radiobutton';
```

### Example Usage (Without flux/Redux)
```js
export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItem: null
        }
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(e) {
        this.setState({checked: !this.state.checked})
    }

    render() {
        return <form>
            <Radiogroup group="groupName" onChange={this.handleChange}>
              <Radiobutton checked={true} formLabel="Radio Button 1" />
              <Radiobutton checked={false} formLabel="Radio Button 2" />
            </Radiogroup>
        </form>
    }
}
```

Radiogroup

| Prop          | PropType      | Description  | Example |
| ------------- |:-------------:| ------------:| -------:| 
| group | string | Radio button group name | ```'triptype'``` |
| onSelect | func.isRequired | When user interacts with the radiobutton | ```() => {}``` |

Radiobutton 

| Prop          | PropType      | Description  | Example |
| ------------- |:-------------:| ------------:| -------:| 
| formLabel | string.isRequired | The form label | ```'Flexible Dates'``` |
| checked | string.isRequired | The value of the checkbox | ```false``` |
| ariaLabel | string | The aria label, defaults to the formLabel | ```'Flexible Dates'``` |
| screenRead | string | The text that will be prompted by the screenReader | ```'Alert, you have selected'``` |
| className | string | Additional class | ```'radiobutton__class'``` | 


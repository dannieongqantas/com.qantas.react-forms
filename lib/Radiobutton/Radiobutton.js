'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Tooltip = require('../Tooltip/Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _RadiobuttonIcon = require('../Icons/RadiobuttonIcon');

var _RadiobuttonIcon2 = _interopRequireDefault(_RadiobuttonIcon);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _async = require('../utils/async');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Radiobutton = function (_Component) {
  _inherits(Radiobutton, _Component);

  function Radiobutton(props) {
    _classCallCheck(this, Radiobutton);

    var _this = _possibleConstructorReturn(this, (Radiobutton.__proto__ || Object.getPrototypeOf(Radiobutton)).call(this, props));

    _this.handleToggle = function () {
      _this.props.selectItem(_this);
      _this.setState({ focused: true });
    };

    _this.handleFocus = function () {
      _this.setState({ focused: true });
    };

    _this.handleBlur = function () {
      (0, _async.ariaAsync)(function () {
        return _this.setState({ focused: false });
      });
    };

    _this.handleIconClick = function () {
      if (!!_this.props.handleIconClick) {
        _this.input.focus();
        _this.handleToggle();
      }
    };

    _this.state = {
      focused: false
    };
    return _this;
  }

  _createClass(Radiobutton, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          ariaLabel = _props.ariaLabel,
          checked = _props.checked,
          children = _props.children,
          className = _props.className,
          formLabel = _props.formLabel,
          group = _props.group,
          id = _props.id,
          screenRead = _props.screenRead,
          information = _props.information,
          disabled = _props.disabled;


      var labelClass = (0, _classnames2.default)('qfa1-radiobutton__label', {
        'qfa1-radiobutton__label--focused': this.state.focused,
        'qfa1-radiobutton__label__hasTooltip': information
      });

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('qfa1-radiobutton__container', className) },
        _react2.default.createElement('input', {
          onBlur: this.handleBlur,
          onFocus: this.handleFocus,
          onChange: this.handleToggle,
          id: id,
          type: 'radio',
          'aria-label': ariaLabel || formLabel,
          className: 'qfa1-radiobutton__radio',
          role: 'radio',
          'aria-checked': checked,
          checked: checked,
          name: group,
          disabled: disabled,
          ref: function ref(input) {
            _this2.input = input;
          }
        }),
        _react2.default.createElement(
          'label',
          { htmlFor: id, className: labelClass },
          _react2.default.createElement(_RadiobuttonIcon2.default, {
            checked: checked,
            focused: this.state.focused,
            onClick: this.handleIconClick,
            disabled: disabled
          }),
          children || _react2.default.createElement(
            'span',
            { className: 'qfa1-radiobutton__label-text' },
            formLabel
          )
        ),
        _react2.default.createElement(_ScreenReader2.default, { text: screenRead }),
        _react2.default.createElement(
          _Tooltip2.default,
          { align: 'right' },
          information
        )
      );
    }
  }]);

  return Radiobutton;
}(_react.Component);

Radiobutton.propTypes = {
  ariaLabel: _propTypes2.default.string,
  checked: _propTypes2.default.bool,
  children: _propTypes2.default.oneOfType([_propTypes2.default.element, _propTypes2.default.arrayOf(_propTypes2.default.element)]),
  className: _propTypes2.default.string,
  formLabel: _propTypes2.default.string,
  group: _propTypes2.default.string,
  id: _propTypes2.default.string.isRequired,
  screenRead: _propTypes2.default.string,
  selectItem: _propTypes2.default.func,
  information: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.string]),
  disabled: _propTypes2.default.bool,
  handleIconClick: _propTypes2.default.bool
};
Radiobutton.defaultProps = {
  formLabel: '',
  selectItem: function selectItem() {},
  information: '',
  disabled: false
};
exports.default = Radiobutton;
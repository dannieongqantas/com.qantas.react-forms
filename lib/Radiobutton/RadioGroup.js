'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 This is a helper to improve readibility
 You can set the checked radio button in one of 2 ways:
   1. Set the selectedItem to an ID of a RadioButton child when rendering the <RadioGroup />
     <RadioGroup selectedItem="ABC"><RadioButton id="ABC" /></RadioGroup>
   2. Omit the selectedItem prop and use a checked prop on the RadioButton child
     <RadioGroup><RadioButton id="ABC" checked /></RadioGroup>
 TODO: All decide on one method in a major version of this?
*/
function isRadioButtonChecked(radioGroup, radioButton) {
  var selectedItem = radioGroup.props.selectedItem;
  var _radioButton$props = radioButton.props,
      id = _radioButton$props.id,
      checked = _radioButton$props.checked;

  var isChecked = id === selectedItem;
  return selectedItem == null && checked ? checked === 'true' : isChecked;
}

var Radiogroup = function (_Component) {
  _inherits(Radiogroup, _Component);

  function Radiogroup(props) {
    _classCallCheck(this, Radiogroup);

    var _this = _possibleConstructorReturn(this, (Radiogroup.__proto__ || Object.getPrototypeOf(Radiogroup)).call(this, props));

    _this.selectItem = _this.selectItem.bind(_this);
    return _this;
  }

  _createClass(Radiogroup, [{
    key: 'selectItem',
    value: function selectItem(item) {
      this.setState({
        selectedItem: item.props.id
      });
    }
  }, {
    key: 'groupRadioButtonChildren',
    value: function groupRadioButtonChildren() {
      var _this2 = this;

      var _props = this.props,
          radioGroupId = _props.id,
          onChange = _props.onChange,
          children = _props.children;

      // TODO In the future, we should use React.Children.map. I have not
      // changed this yet as it breaks unit tests.

      return children.map(function (radioButton) {
        var _radioButton$props2 = radioButton.props,
            radioButtonId = _radioButton$props2.id,
            selectItem = _radioButton$props2.selectItem;


        return _react2.default.cloneElement(radioButton, {
          key: radioButtonId,
          group: radioGroupId,
          checked: isRadioButtonChecked(_this2, radioButton),
          selectItem: selectItem || _this2.selectItem,
          onChange: onChange
        });
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { role: 'radiogroup', className: this.props.className + ' qfa1-radiobutton' },
        _react2.default.createElement(
          'fieldset',
          { className: 'qfa1-radiobutton__radiogroup' },
          _react2.default.createElement(
            'legend',
            { className: 'qfa1-radiobutton__legend' },
            this.props.legend
          ),
          _react2.default.createElement(
            'div',
            { className: 'qfa1-radiobutton__radiogroup__wrapper' },
            this.groupRadioButtonChildren()
          )
        )
      );
    }
  }]);

  return Radiogroup;
}(_react.Component);

Radiogroup.propTypes = {
  id: _propTypes2.default.string.isRequired,
  selectedItem: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  legend: _propTypes2.default.string,
  selectItem: _propTypes2.default.func,
  children: _propTypes2.default.arrayOf(_propTypes2.default.element).isRequired, // Should always be [<Radiobutton />...] elements
  className: _propTypes2.default.string
};
Radiogroup.defaultProps = {
  className: '',
  legend: ''
};
exports.default = Radiogroup;
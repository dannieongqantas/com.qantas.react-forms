'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formChildren = undefined;

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formChildren = exports.formChildren = _propTypes2.default.oneOfType([
// A single element
_propTypes2.default.element,

// A single boolean. Allows conditional syntax:
// <Form>{bool && <element />}</Form>
_propTypes2.default.bool,

// A combination of the above
_propTypes2.default.arrayOf(_propTypes2.default.oneOfType([_propTypes2.default.element, _propTypes2.default.bool]))]).isRequired;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('../Button/Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Collapsible = function (_Component) {
  _inherits(Collapsible, _Component);

  function Collapsible(props) {
    _classCallCheck(this, Collapsible);

    var _this = _possibleConstructorReturn(this, (Collapsible.__proto__ || Object.getPrototypeOf(Collapsible)).call(this, props));

    _this.state = {
      isExpanded: false
    };
    return _this;
  }

  _createClass(Collapsible, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      document.addEventListener('formModalExpanded', function () {
        return _this2.setExpandSate(true);
      });
      document.addEventListener('formModalCollapsed', function () {
        return _this2.setExpandSate(false);
      });
    }
  }, {
    key: 'setExpandSate',
    value: function setExpandSate() {
      var isExpanded = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      this.props.onToggle(isExpanded);
      this.setState({ isExpanded: isExpanded });
    }
  }, {
    key: 'render',
    value: function render() {
      var props = this.props;
      var isExpanded = this.state.isExpanded;


      var containerClasses = (0, _classnames2.default)('qfa1-collapsible__container', {
        'qfa1-collapsible__container--expanded': isExpanded,
        'qfa1-collapsible__container--collapsed': !isExpanded
      }, props.className);

      return _react2.default.createElement(
        'div',
        { className: containerClasses },
        _react2.default.createElement(
          'div',
          { className: 'qfa1-collapsible__content-container' },
          props.children
        ),
        !isExpanded && _react2.default.createElement(
          'div',
          { className: 'qfa1-collapsible__button-container' },
          _react2.default.createElement(
            'div',
            { className: 'widget-form__group-container' },
            _react2.default.createElement(_Button2.default, { type: 'button', className: 'qfa1-collapsible__button' })
          )
        )
      );
    }
  }]);

  return Collapsible;
}(_react.Component);

Collapsible.propTypes = {
  children: _propTypes2.default.array,
  className: _propTypes2.default.string,
  onToggle: _propTypes2.default.func
};
Collapsible.defaultProps = {
  className: '',
  onToggle: function onToggle() {}
};
exports.default = Collapsible;
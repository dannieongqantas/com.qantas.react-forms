'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ServerErrorResponse = function ServerErrorResponse(_ref) {
  var children = _ref.children,
      header = _ref.header;
  return _react2.default.createElement(
    'div',
    { className: 'server-error-card card' },
    _react2.default.createElement(
      'h3',
      { className: 'server-error-card__header card__heading' },
      header
    ),
    _react2.default.createElement(
      'div',
      { className: 'server-error-card__message card__description' },
      children
    )
  );
};

ServerErrorResponse.propTypes = {
  header: _propTypes2.default.string,
  children: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.arrayOf(_propTypes2.default.node)])
};

exports.default = ServerErrorResponse;
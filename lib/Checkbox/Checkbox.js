'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _KeyCodes = require('../Constants/KeyCodes');

var _CheckboxIcon = require('../Icons/CheckboxIcon');

var _CheckboxIcon2 = _interopRequireDefault(_CheckboxIcon);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _async = require('../utils/async');

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Checkbox = function (_Component) {
  _inherits(Checkbox, _Component);

  function Checkbox(props) {
    _classCallCheck(this, Checkbox);

    var _this = _possibleConstructorReturn(this, (Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).call(this, props));

    _this.handleBlur = _this.handleBlur.bind(_this);
    _this.handleFocus = _this.handleFocus.bind(_this);
    _this.handleKeyDown = _this.handleKeyDown.bind(_this);

    _this.id = props.id || 'checkbox-' + (0, _idGenerator2.default)();

    _this.state = {
      focused: false
    };
    return _this;
  }

  _createClass(Checkbox, [{
    key: 'handleFocus',
    value: function handleFocus() {
      this.setState({ focused: true });
    }
  }, {
    key: 'handleBlur',
    value: function handleBlur() {
      var _this2 = this;

      (0, _async.ariaAsync)(function () {
        return _this2.setState({ focused: false });
      });
    }
  }, {
    key: 'handleKeyDown',
    value: function handleKeyDown(e) {
      if (e.keyCode === _KeyCodes.SPACE_KEY_CODE) this.props.onChange();
    }
  }, {
    key: 'render',
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      var containerClass = (0, _classnames2.default)('qfa1-checkbox__container', {
        'qfa1-checkbox__container--focused': this.state.focused
      });

      return _react2.default.createElement(
        'div',
        { className: props.className },
        _react2.default.createElement(
          'div',
          { className: 'widget-form__group' },
          _react2.default.createElement(
            'div',
            { className: containerClass },
            _react2.default.createElement('input', { checked: props.checked,
              id: this.id,
              onChange: props.onChange,
              type: 'checkbox',
              'aria-label': props.ariaLabel || props.formLabel,
              className: 'qfa1-checkbox__checkbox',
              role: 'checkbox',
              'aria-checked': props.checked,
              onFocus: this.handleFocus,
              onBlur: this.handleBlur }),
            _react2.default.createElement(
              'label',
              { onClick: this.handleClick, htmlFor: this.id, className: 'qfa1-checkbox__label' },
              _react2.default.createElement(_CheckboxIcon2.default, { checked: props.checked, focused: this.state.focused }),
              _react2.default.createElement(
                'span',
                { className: 'qfa1-checkbox__label-text' },
                props.formLabel
              )
            ),
            _react2.default.createElement(_ScreenReader2.default, { text: props.screenRead })
          )
        )
      );
    }
  }]);

  return Checkbox;
}(_react.Component);

Checkbox.propTypes = {
  formLabel: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.element]).isRequired,
  checked: _propTypes2.default.bool.isRequired,
  ariaLabel: _propTypes2.default.string,
  screenRead: _propTypes2.default.string,
  className: _propTypes2.default.string,
  id: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired
};
Checkbox.defaultProps = {
  className: 'widget-form__group-container',
  centered: false,
  ariaLabel: ''
};
exports.default = Checkbox;
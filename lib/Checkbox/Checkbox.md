## Checkbox

### Import to your project
```js
import Checkbox from 'com.qantas.react-forms/Checkbox';
```
```scss
@import '~com.qantas.react-forms/scss/Checkbox';
```

### Example Usage (Without flux/Redux)
```js
export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(e) {
        this.setState({checked: !this.state.checked})
    }

    render() {
        return <form>
            <Checkbox 
                formLabel="Flexible Dates"
                checked={this.state.checked}
                onChange={this.handleChange} />
        </form>
    }
}
```

| Prop          | PropType      | Description  | Example |
| ------------- |:-------------:| ------------:| -------:| 
| formLabel | string.isRequired | The form label | ```'Flexible Dates'``` |
| checked | string.isRequired | The value of the checkbox | ```false``` |
| ariaLabel | string | The aria label, defaults to the formLabel | ```'Flexible Dates'``` |
| screenRead | string | The text that will be prompted by the screenReader | ```'Alert, you have selected'``` |
| className | string | Additional class | ```'checkbox__class'``` |
| onChange | func.isRequired | When user interacts with the checkbox | ```() => {this.setState({checked: !this.state.checked})}``` |


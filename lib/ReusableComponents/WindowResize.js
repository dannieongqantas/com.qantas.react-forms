'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.getWindowWidth = getWindowWidth;
exports.default = WindowResize;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _throttle = require('lodash/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

var _canUseDOM = require('../utils/canUseDOM');

var _canUseDOM2 = _interopRequireDefault(_canUseDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var THROTTLE_RATE = 1000 / 60;
var DESKTOP_WIDTH = 1500;

/*
 * getWindowWidth
 *
 * A cross-browser solution
 * using clientWidth and clientHeight for IE8 and earlier
 * see: https://www.w3schools.com/jsref/prop_win_innerheight.asp
 * browser compatibility: https://developer.mozilla.org/en-US/docs/Web/API/Element/clientWidth
 */
function getWindowWidth() {
  if (!_canUseDOM2.default) return DESKTOP_WIDTH;
  return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

/*
 * WindowResize
 *
 * @param {React Component} Component
 * @param {Array, Number} breakpoints - the breakpoints which will cause a rerender
 * @param {Function(props, windowWidth)} payload - the payload function, when this is defined and both is not then it will not pass the windowWidth property
 * @param {Boolean} both - sends a dispatch and also updates the windowWidth prop
 *
 * @return {<ReactComponent windowWidth={windowWidth} />} - returns a React Component with the windowWidth prop
 */

function WindowResize(MyComponent) {
  var breakpoints = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var payload = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var both = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

  var breakpointsArray = breakpoints instanceof Array ? breakpoints : [breakpoints];

  if (breakpointsArray.length && typeof breakpointsArray[0] !== 'number') throw new Error('WindowResize breakpoints should only accept a number or an array of numbers');

  breakpointsArray.length > 1 && breakpointsArray.reduce(function (previousVal, currentVal) {
    if (typeof currentVal !== 'number') throw new Error('WindowResize breakpoints should only accept a number or an array of numbers');

    if (currentVal < previousVal) throw new Error('WindowResize breakpoints should be in ascending order');

    return currentVal;
  });

  var WindowResizeComponent = function (_Component) {
    _inherits(WindowResizeComponent, _Component);

    _createClass(WindowResizeComponent, null, [{
      key: 'computeBreakpoint',
      value: function computeBreakpoint(width, breakpointsVal) {
        if (breakpointsVal.length === 0) return null;

        if (width < breakpointsVal[0]) return 0;

        for (var i = 0; i < breakpointsVal.length - 1; i++) {
          if (breakpointsVal[i] < width && width < breakpointsVal[i + 1]) return i + 1;
        }if (width > breakpointsVal[breakpointsVal.length - 1]) return breakpointsVal.length;

        return 0;
      }
    }]);

    function WindowResizeComponent(props) {
      _classCallCheck(this, WindowResizeComponent);

      var _this = _possibleConstructorReturn(this, (WindowResizeComponent.__proto__ || Object.getPrototypeOf(WindowResizeComponent)).call(this, props));

      _this.focus = function () {
        if (_this.resizeComponent && _this.resizeComponent) {
          _this.resizeComponent.focus();
        }
      };

      _this.onResize = _this.onResize.bind(_this);
      var windowWidth = getWindowWidth();
      var currentBreakpoint = WindowResizeComponent.computeBreakpoint(windowWidth, breakpoints);
      _this.state = {
        windowWidth: windowWidth,
        currentBreakpoint: currentBreakpoint
      };
      return _this;
    }

    _createClass(WindowResizeComponent, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.onResize = (0, _throttle2.default)(this.onResize, THROTTLE_RATE);

        if (this.resizeComponent.focus) this.focus = this.resizeComponent.focus.bind(this.resizeComponent);

        if (this.resizeComponent.blur) this.blur = this.resizeComponent.blur.bind(this.resizeComponent);

        window.addEventListener('resize', this.onResize);
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
      }
    }, {
      key: 'onResize',
      value: function onResize() {
        var windowWidth = getWindowWidth();
        var currentBreakpoint = WindowResizeComponent.computeBreakpoint(windowWidth, breakpointsArray);
        if (breakpointsArray.length === 0 || currentBreakpoint !== this.state.currentBreakpoint) {
          this.setState({ windowWidth: windowWidth, currentBreakpoint: currentBreakpoint });
          if (payload && typeof payload === 'function') {
            payload(this.props, windowWidth);
          }
        }
      }
    }, {
      key: 'render',
      value: function render() {
        var _this2 = this;

        // Only send payload if both is true or if no payload is defined
        var widthProp = !both && payload !== null ? {} : { windowWidth: this.state.windowWidth };
        return _react2.default.createElement(MyComponent, _extends({}, this.props, {
          ref: function ref(el) {
            _this2.resizeComponent = el;
          }
        }, widthProp));
      }
    }]);

    return WindowResizeComponent;
  }(_react.Component);

  return WindowResizeComponent;
}
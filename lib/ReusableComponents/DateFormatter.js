'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.default = dateFormatterWrapper;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _date = require('../utils/date');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function dateFormatterWrapper(MyComponent, getLanguageCodeFunc) {
  var _class, _temp;

  return _temp = _class = function (_Component) {
    _inherits(DateFormatter, _Component);

    function DateFormatter(props) {
      _classCallCheck(this, DateFormatter);

      var _this = _possibleConstructorReturn(this, (DateFormatter.__proto__ || Object.getPrototypeOf(DateFormatter)).call(this, props));

      _this.focus = function () {
        if (_this.myComponent && _this.myComponent.focus) {
          _this.myComponent.focus();
        }
      };

      _this.state = {
        dateFormatter: function dateFormatter() {}
      };

      var languageCode = getLanguageCodeFunc ? getLanguageCodeFunc(props) : props.languageCode;

      (0, _date.retrieveLocaleDateFormatter)(languageCode).then(function (dateFormatterFunc) {
        _this.setState({ dateFormatter: dateFormatterFunc });
      });
      return _this;
    }

    _createClass(DateFormatter, [{
      key: 'render',
      value: function render() {
        var _this2 = this;

        return _react2.default.createElement(MyComponent, _extends({}, this.props, { ref: function ref(myComponent) {
            _this2.myComponent = myComponent;
          }, dateFormatter: this.state.dateFormatter }));
      }
    }]);

    return DateFormatter;
  }(_react.Component), _class.propTypes = {
    languageCode: _propTypes2.default.string
  }, _temp;
}
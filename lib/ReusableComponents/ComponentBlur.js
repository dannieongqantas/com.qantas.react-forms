'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.default = componentBlur;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*
 * ComponentBlur, handles blurring of components
 * @param {React Component} Component - The component being enhanced
 * @param {String} ref - The element reference being blurred
 * @param {func} payload - The payload function which creates an object to be dispatched
 *
 * @return {<ReactComponent blur={onBlur}>} - returns a component with a blur prop which can also be attached to input field blurs
 */

var COMPONENT_PREFIX = 'blurred';

function componentBlur(MyComponent, childComponentRef) {
  var payload = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

  if (typeof payload !== 'function') throw new Error('ComponentBlur accepts (Component, actionType, payloadFunc)');

  var blurWrapperRef = COMPONENT_PREFIX + '_' + childComponentRef;

  return function (_Component) {
    _inherits(ComponentBlur, _Component);

    function ComponentBlur(props, context) {
      _classCallCheck(this, ComponentBlur);

      var _this = _possibleConstructorReturn(this, (ComponentBlur.__proto__ || Object.getPrototypeOf(ComponentBlur)).call(this, props, context));

      _this.leaveFieldByKeyDown = function (e) {
        if (e.type === 'keydown' && e.keyCode !== _KeyCodes.TAB_KEY_CODE) return;
        _this.checkIfBlurred(e.target);
      };

      _this.onBlur = function () {
        if (!_this.clickedOutside) {
          setTimeout(function () {
            return _this.checkIfBlurred(document.activeElement);
          }, 0); // Hack to get voiceover working for pop overs on safari
        }
      };

      _this.focus = function () {
        var childComponent = _this[blurWrapperRef];

        if (childComponent && childComponent.focus) {
          childComponent.focus();
        }
      };

      _this.leaveField = function (e) {
        _this.clickedOutside = true;
        _this.checkIfBlurred(e.target);
        setTimeout(function () {
          _this.clickedOutside = false;
        }, 0); // to cancel clickedOutside if blurred
      };

      _this.checkIfBlurred = function (target) {
        var insideElm = _this[blurWrapperRef] && _this[blurWrapperRef][childComponentRef] || null;

        if (!insideElm || !insideElm.contains(target)) {
          var payloadData = payload(_this.props);
          if (typeof payloadData === 'function') payloadData();
        }
      };

      _this.clickedOutside = false;
      return _this;
    }

    _createClass(ComponentBlur, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        document.getElementsByTagName('body')[0].addEventListener('mousedown', this.leaveField, false);
        document.getElementsByTagName('body')[0].addEventListener('touchend', this.leaveField, false);
        document.getElementsByTagName('body')[0].addEventListener('keydown', this.leaveFieldByKeyDown, true);
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        document.getElementsByTagName('body')[0].removeEventListener('mousedown', this.leaveField, false);
        document.getElementsByTagName('body')[0].addEventListener('touchend', this.leaveField, false);
        document.getElementsByTagName('body')[0].removeEventListener('keydown', this.leaveFieldByKeyDown, true);
      }
    }, {
      key: 'render',
      value: function render() {
        var _this2 = this;

        return _react2.default.createElement(MyComponent, _extends({ ref: function ref(el) {
            _this2[blurWrapperRef] = el;
          } }, this.props, { blur: this.onBlur }));
      }
    }]);

    return ComponentBlur;
  }(_react.Component);
}
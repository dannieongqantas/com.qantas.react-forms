"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var RETURN_KEY_CODE = exports.RETURN_KEY_CODE = 13;
var ARROW_LEFT_KEY_CODE = exports.ARROW_LEFT_KEY_CODE = 37;
var ARROW_UP_KEY_CODE = exports.ARROW_UP_KEY_CODE = 38;
var ARROW_RIGHT_KEY_CODE = exports.ARROW_RIGHT_KEY_CODE = 39;
var ARROW_DOWN_KEY_CODE = exports.ARROW_DOWN_KEY_CODE = 40;
var ESCAPE_KEY_CODE = exports.ESCAPE_KEY_CODE = 27;
var SPACE_KEY_CODE = exports.SPACE_KEY_CODE = 32;
var TAB_KEY_CODE = exports.TAB_KEY_CODE = 9;
var ZERO_KEY_CODE = exports.ZERO_KEY_CODE = 48;
var N_KEY_CODE = exports.N_KEY_CODE = 78;
var P_KEY_CODE = exports.P_KEY_CODE = 80;
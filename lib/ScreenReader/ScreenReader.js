'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _userAgent = require('../utils/userAgent');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScreenReader = function (_Component) {
  _inherits(ScreenReader, _Component);

  function ScreenReader(props) {
    _classCallCheck(this, ScreenReader);

    var _this = _possibleConstructorReturn(this, (ScreenReader.__proto__ || Object.getPrototypeOf(ScreenReader)).call(this, props));

    _this.elementSwitch = 'div';
    _this.isIE = (0, _userAgent.isIE)() || (0, _userAgent.isEdge)();
    return _this;
  }

  _createClass(ScreenReader, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.elementSwitch = this.elementSwitch === 'div' ? 'span' : 'div';
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return this.props.text !== nextProps.text || this.props.ariaLabel !== nextProps.ariaLabel;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.elementSwitch = this.elementSwitch === 'div' ? 'span' : 'div';
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          text = _props.text,
          ariaRole = _props.ariaRole,
          ariaLive = _props.ariaLive,
          ariaAtomic = _props.ariaAtomic,
          ariaRelevant = _props.ariaRelevant,
          ariaLabel = _props.ariaLabel;


      var props = {
        className: 'widget-form__element--aria-hidden',
        role: ariaRole,
        'aria-atomic': ariaAtomic,
        'aria-relevant': ariaRelevant,
        'aria-label': ariaLabel,
        'aria-live': ariaLive
      };

      // This forces a full rerender of this element to work in IE
      var textElm = this.elementSwitch === 'div' ? _react2.default.createElement(
        'div',
        props,
        text
      ) : _react2.default.createElement(
        'span',
        props,
        text
      );

      return _react2.default.createElement(
        'span',
        props,
        this.isIE ? textElm : text
      );
    }
  }]);

  return ScreenReader;
}(_react.Component);

ScreenReader.propTypes = {
  ariaLive: _propTypes2.default.oneOf(['polite', 'assertive']),
  ariaRelevant: _propTypes2.default.oneOf(['additions', 'removals', 'text']),
  ariaAtomic: _propTypes2.default.bool,
  ariaLabel: _propTypes2.default.string,
  ariaRole: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.bool]),
  text: _propTypes2.default.string
};
ScreenReader.defaultProps = {
  ariaLive: 'polite',
  ariaRole: 'status'
};
exports.default = ScreenReader;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getRegionCode;
function getRegionCode(languageCode) {
  if (process.env.NODE_ENV !== 'production') {
    console.error('Mappings need to be updated and tested. Remove this once updated'); //eslint-disable-line
  }
  var splitLanguageCode = languageCode.toLowerCase().split('-');

  var regionFromLanguageCode = splitLanguageCode[1];
  switch (regionFromLanguageCode) {
    case 'jp':
    case 'tw':
    case 'cn':
    case 'id':
      return 'as';
    case 'ar':
    case 'cl':
    case 'mx':
      return 'am';
    case 'de':
    case 'fr':
      return 'eu';
    // TODO: Need to add other mappings
    // case '':
    //   return 'sp';
    // case '':
    //   return 'af';
    default:
      return 'au';
  }
}
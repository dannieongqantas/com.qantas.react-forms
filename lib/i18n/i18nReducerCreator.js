'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getDataForLanguageCode = require('./getDataForLanguageCode');

var _getDataForLanguageCode2 = _interopRequireDefault(_getDataForLanguageCode);

var _transformLanguageCode = require('./transformLanguageCode');

var _transformLanguageCode2 = _interopRequireDefault(_transformLanguageCode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
  Example use:

	YourAppReducer.js
	---

	import i18nReducerCreator from 'com.qantas.widget-provider-engine/lib/i18n/i18nReducerCreator';
	import config from 'somewhere';
  	import allLanguagesData from '../i18n/';
  	// ^^ where index.js exports all language files keyed by lowercase language code

	export default combineReducers({
	  some: otherReducer,
	  i18n: i18nReducerCreator(allLanguagesData, config.languageCode)
	});

  Note: If you find you need custom behaviour, don't use this -- create your own version of i18nReducer in your widget.
  (e.g. Manage Booking widget does this to support using `variant` to change some labels)
*/

var i18nReducerCreator = function i18nReducerCreator(allLanguagesData, languageCode) {
  var transformedLanguageCode = (0, _transformLanguageCode2.default)(languageCode);
  var initialState = (0, _getDataForLanguageCode2.default)(allLanguagesData, transformedLanguageCode);

  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    return state;
  };
};

exports.default = i18nReducerCreator;
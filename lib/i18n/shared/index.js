'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _en = require('./en');

var _en2 = _interopRequireDefault(_en);

var _fr = require('./fr');

var _fr2 = _interopRequireDefault(_fr);

var _es = require('./es');

var _es2 = _interopRequireDefault(_es);

var _de = require('./de');

var _de2 = _interopRequireDefault(_de);

var _ja = require('./ja');

var _ja2 = _interopRequireDefault(_ja);

var _id = require('./id');

var _id2 = _interopRequireDefault(_id);

var _zhHans = require('./zh-hans');

var _zhHans2 = _interopRequireDefault(_zhHans);

var _zhHant = require('./zh-hant');

var _zhHant2 = _interopRequireDefault(_zhHant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  en: _en2.default,
  fr: _fr2.default,
  es: _es2.default,
  de: _de2.default,
  ja: _ja2.default,
  id: _id2.default,
  'zh-hans': _zhHans2.default,
  'zh-hant': _zhHant2.default
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  en: function en() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/en');
    });
  },
  de: function de() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/de');
    });
  },
  es: function es() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/es');
    });
  },
  fr: function fr() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/fr');
    });
  },
  ja: function ja() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/ja');
    });
  },
  id: function id() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/id');
    });
  },
  'zh-hans': function zhHans() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/zh_cn');
    });
  },
  'zh-hant': function zhHant() {
    return Promise.resolve().then(function () {
      return require('date-fns/locale/zh_tw');
    });
  }
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (languageCode) {
  if (!languageCode) return languageCode;

  var lowercaseLanguageCode = languageCode.toLowerCase();
  switch (lowercaseLanguageCode) {
    case 'zh-cn':
    case 'zh-sg':
      return 'zh-hans';
    case 'zh-tw':
    case 'zh-hk':
      return 'zh-hant';
    default:
      return lowercaseLanguageCode;
  }
};
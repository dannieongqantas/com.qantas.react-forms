'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Dropdown;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Dropdown(props) {
  var children = props.children,
      opened = props.opened,
      disabled = props.disabled;

  var componentClass = (0, _classnames2.default)({
    'widget-form__element--hidden': !opened || disabled
  });
  return _react2.default.createElement(
    'div',
    { className: 'qfa1-dropdown-list__items-container-father ' + componentClass },
    _react2.default.createElement(
      'div',
      { className: 'qfa1-dropdown-list__items-container' },
      _react2.default.createElement(
        'div',
        { className: 'widget-form__group' },
        children
      )
    )
  );
}

Dropdown.propTypes = {
  opened: _propTypes2.default.bool.isRequired,
  children: _propTypes2.default.oneOfType([_propTypes2.default.array, _propTypes2.default.element]).isRequired,
  disabled: _propTypes2.default.bool
};
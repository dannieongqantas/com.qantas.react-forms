'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducerCreator;

var _ActionTypes = require('../../Constants/ActionTypes');

var _PeopleSelectorControlled = require('../../PeopleSelector/PeopleSelectorControlled');

var _stringBind = require('../../utils/stringBind');

function peopleSelectorReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (action.id && state.id !== action.id) {
    return state;
  }

  var newState = void 0;
  switch (action.type) {
    case _ActionTypes.SELECTION_CHANGE:
      newState = _extends({}, state, { value: action.value });
      if (action.copy) {
        newState.screenRead = (0, _stringBind.stringBind)(action.copy.ariaSelection, { selection: (0, _PeopleSelectorControlled.createSelectionText)(action.value, action.copy) });
      }
      return newState;
    case _ActionTypes.SELECTION_SETRANGE:
      newState = _extends({}, state, { range: action.range });
      return newState;
    default:
      return state;
  }
}

function reducerCreator() {
  var initialStateInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialStateInput;
    var action = arguments[1];
    return peopleSelectorReducer(state, action);
  };
}
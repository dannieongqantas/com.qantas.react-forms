'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.radioButtonReducer = radioButtonReducer;
exports.default = radioReducerCreator;

var _ActionTypes = require('../../Constants/ActionTypes');

var initialState = exports.initialState = {
  group: 'routeType',
  selectedItem: ''
};

function radioButtonReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (state.id !== action.group) {
    return state;
  }
  switch (action.type) {
    case _ActionTypes.RADIO_TOGGLE:
      return _extends({}, state, { selectedItem: action.id });
    default:
      return state;
  }
}

function radioReducerCreator() {
  var initialStateInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;

  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialStateInput;
    var action = arguments[1];
    return radioButtonReducer(state, action);
  };
}
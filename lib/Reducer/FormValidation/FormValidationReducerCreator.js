'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = FormValidationReducerCreator;

var _ActionTypes = require('../../Constants/ActionTypes');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function FormValidationReducerCreator() {
  var initialStateVal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { summaryFocus: false };

  return function FormValidationReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialStateVal;
    var action = arguments[1];

    switch (action.type) {
      case _ActionTypes.VALIDATION_TRIGGER:
        {
          var newState = _extends({}, state, { errors: _extends({}, state.errors, _defineProperty({}, action.id, action.validationMessage)) });
          return newState;
        }

      case _ActionTypes.VALIDATION_CLEAR:
        {
          var _newState = _extends({}, state);
          _newState.errors[action.id] = '';
          return _newState;
        }

      case _ActionTypes.VALIDATION_FOCUS_DONE:
        {
          return _extends({}, state, { summaryFocus: false });
        }

      case _ActionTypes.VALIDATION_FOCUS:
        {
          return _extends({}, state, { summaryFocus: true });
        }

      case _ActionTypes.VALIDATION_RESET:
        {
          return initialStateVal;
        }

      default:
        {
          return state;
        }
    }
  };
}
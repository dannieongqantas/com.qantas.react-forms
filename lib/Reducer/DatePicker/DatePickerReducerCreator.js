'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = reducerCreator;

var _ActionTypes = require('../../Constants/ActionTypes');

function datePickerReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (action.id && state.id !== action.id) {
    return state;
  }

  switch (action.type) {
    case _ActionTypes.CALENDAR_SELECT_ITEM:
      return _extends({}, state, {
        value: action.value
      });
    case _ActionTypes.CALENDAR_MIN_DATE:
      return _extends({}, state, {
        minDate: action.minDate
      });
    case _ActionTypes.CALENDAR_RANGE_DATE_CHANGE:
      return _extends({}, state, {
        rangeStartDate: action.rangeStartDate
      });
    case _ActionTypes.CALENDAR_DAY_LIMIT_CHANGE:
      return _extends({}, state, {
        daysLimit: action.daysLimit
      });
    default:
      return state;
  }
}

function reducerCreator() {
  var initialStateInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialStateInput;
    var action = arguments[1];
    return datePickerReducer(state, action);
  };
}
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = I18nReducerCreator;

var _deprecationNotice = require('../../utils/deprecationNotice');

var _deprecationNotice2 = _interopRequireDefault(_deprecationNotice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultState = {
  en: {}
}; /* NOTE: This is deprecated. Use the i18nReducerCreator in src/i18n now. */
function I18nReducerCreator() {
  var initialStateVal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultState;
  var language = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'en';

  (0, _deprecationNotice2.default)('src/Reducer/I18n/I18nReducerCreator is deprecated. Please switch to src/i18n/i18nReducer.');

  var i18nDataForLanguage = initialStateVal[language];

  if (!i18nDataForLanguage) {
    throw new Error('i18n error: widget has no translations for "' + language + '".');
  }

  return function I18nReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : i18nDataForLanguage;

    return state;
  };
}
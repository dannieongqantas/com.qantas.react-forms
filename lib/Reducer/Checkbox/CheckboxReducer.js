'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialState = undefined;
exports.default = checkboxReducer;

var _ActionTypes = require('../../Constants/ActionTypes');

var _createNewStateById = require('../../utils/createNewStateById');

var _createNewStateById2 = _interopRequireDefault(_createNewStateById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initialState = exports.initialState = {
  differentDropoffLocation: {
    id: 'differentDropoffLocation',
    checked: false,
    screenRead: ''
  }
};

function checkboxReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments[1];

  switch (action.type) {
    case _ActionTypes.CHECKBOX_TOGGLE:
      {
        var checked = !state[action.id].checked;
        return (0, _createNewStateById2.default)(state, { checked: checked, screenRead: action.screenRead }, action.id);
      }
    case _ActionTypes.CHECKBOX_SET_VALUE:
      return (0, _createNewStateById2.default)(state, { checked: action.checked }, action.id);
    default:
      return state;
  }
}
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOGIN_STYLES_URL = exports.LOGIN_API_JS_URL = exports.LOGIN_COMMON_JS_URL = exports.LOGIN_INIT_JS_URL = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _appendScriptStyles = require('../utils/appendScriptStyles');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Login Widget Confluence Link
 * https://confluence.qantas.com.au/pages/viewpage.action?spaceKey=LSL&title=Login+Widget+%28v2%29+Integration+Guide
 */

var LOGIN_INIT_JS_URL = exports.LOGIN_INIT_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/init.bundle.js';
var LOGIN_COMMON_JS_URL = exports.LOGIN_COMMON_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/common.bundle.js';
var LOGIN_API_JS_URL = exports.LOGIN_API_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/login.bundle.js';
var LOGIN_STYLES_URL = exports.LOGIN_STYLES_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/login.bundle.css';

var FflyerLogin = function (_Component) {
  _inherits(FflyerLogin, _Component);

  function FflyerLogin() {
    _classCallCheck(this, FflyerLogin);

    return _possibleConstructorReturn(this, (FflyerLogin.__proto__ || Object.getPrototypeOf(FflyerLogin)).apply(this, arguments));
  }

  _createClass(FflyerLogin, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // TODO: uncomment and use some init function possibly available from loyalty api?
      // if (typeof window.qff_auth === 'undefined') {
      var _props = this.props,
          cssPath = _props.cssPath,
          scriptPath = _props.scriptPath;

      (0, _appendScriptStyles.appendScript)(scriptPath, false);
      (0, _appendScriptStyles.appendStyles)([cssPath]);
      // }
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate() {
      return false;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          qffAuth = _props2.qffAuth,
          widgetRef = _props2.widgetRef,
          loginButtonTitle = _props2.loginButtonTitle,
          logoutButtonTitle = _props2.logoutButtonTitle,
          template = _props2.template,
          templateLogin = _props2.templateLogin,
          templateLogout = _props2.templateLogout,
          className = _props2.className,
          children = _props2.children,
          backgroundTheme = _props2.backgroundTheme;

      var containerClass = (0, _classnames2.default)('qfa1-ffLogin_container', className);
      return _react2.default.createElement(
        'div',
        { className: containerClass },
        children,
        _react2.default.createElement('div', { 'data-qff-auth': qffAuth,
          'data-prop-widget-ref': widgetRef,
          'data-prop-login-button-title': loginButtonTitle,
          'data-prop-logout-button-title': logoutButtonTitle,
          'data-prop-template': template,
          'data-prop-background-theme': backgroundTheme,
          'data-prop-template-login': templateLogin,
          'data-prop-template-logout': templateLogout })
      );
    }
  }]);

  return FflyerLogin;
}(_react.Component);

FflyerLogin.propTypes = {
  className: _propTypes2.default.string,
  qffAuth: _propTypes2.default.string,
  widgetRef: _propTypes2.default.string,
  loginButtonTitle: _propTypes2.default.string,
  logoutButtonTitle: _propTypes2.default.string,
  template: _propTypes2.default.string,
  children: _propTypes2.default.node,
  templateLogin: _propTypes2.default.string,
  templateLogout: _propTypes2.default.string,
  backgroundTheme: _propTypes2.default.string,
  scriptPath: _propTypes2.default.arrayOf(_propTypes2.default.string),
  cssPath: _propTypes2.default.string
};
FflyerLogin.defaultProps = {
  qffAuth: 'login',
  widgetRef: 'search',
  templateLogout: 'blank',
  backgroundTheme: 'transparent',
  templateLogin: 'blank',
  scriptPath: [LOGIN_INIT_JS_URL, LOGIN_COMMON_JS_URL, LOGIN_API_JS_URL],
  cssPath: LOGIN_STYLES_URL
};
exports.default = FflyerLogin;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Select = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _DropdownList = require('../DropdownList/DropdownList');

var _DropdownList2 = _interopRequireDefault(_DropdownList);

var _ComponentBlur = require('../ReusableComponents/ComponentBlur');

var _ComponentBlur2 = _interopRequireDefault(_ComponentBlur);

var _ArrowIcon = require('../Icons/ArrowIcon');

var _ArrowIcon2 = _interopRequireDefault(_ArrowIcon);

var _InputArea = require('../Input/InputArea');

var _InputArea2 = _interopRequireDefault(_InputArea);

var _Link = require('../Link/Link');

var _Link2 = _interopRequireDefault(_Link);

var _ScreenReader = require('../ScreenReader/ScreenReader');

var _ScreenReader2 = _interopRequireDefault(_ScreenReader);

var _idGenerator = require('../utils/idGenerator');

var _idGenerator2 = _interopRequireDefault(_idGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var COMPONENT_REF = 'Select';

var Select = exports.Select = function (_Component) {
  _inherits(Select, _Component);

  function Select(props) {
    _classCallCheck(this, Select);

    var _this = _possibleConstructorReturn(this, (Select.__proto__ || Object.getPrototypeOf(Select)).call(this, props));

    _this.arrowIconClick = _this.arrowIconClick.bind(_this);
    _this.clickInput = _this.clickInput.bind(_this);
    _this.clickLink = _this.clickLink.bind(_this);
    _this.id = props.id || (0, _idGenerator2.default)();
    return _this;
  }

  _createClass(Select, [{
    key: 'arrowIconClick',
    value: function arrowIconClick() {
      if (this.props.opened) {
        this.inputElement.blur();
        this.props.blurInput();
      } else {
        this.inputElement.focus();
        this.props.focusInput();
      }
    }
  }, {
    key: 'clickLink',
    value: function clickLink(event) {
      event.preventDefault();
      if (this.props.opened) {
        this.props.blurInput();
      } else {
        this.props.focusInput();
      }
    }
  }, {
    key: 'clickInput',
    value: function clickInput() {
      this.inputElement.focus();
    }
  }, {
    key: 'renderComboBox',
    value: function renderComboBox(_ref) {
      var _this2 = this;

      var inputId = _ref.inputId,
          activeDescendant = _ref.activeDescendant,
          listId = _ref.listId;

      var props = _objectWithoutProperties(this.props, []);

      var value = props.items[props.selectedItem].text;

      return _react2.default.createElement(
        _InputArea2.default,
        { ref: function ref(el) {
            _this2.inputElement = el;
          },
          id: inputId,
          className: 'qfa1-select__input',
          value: value,
          onBlur: props.blur,
          onFocus: this.props.focusInput,
          onClick: this.clickInput,
          popup: props.opened,
          role: 'combobox',
          activeDescendant: activeDescendant,
          onKeyDown: this.props.keyDownHandler,
          formLabel: props.formLabel,
          ariaLabel: props.ariaLabel,
          'aria-owns': listId,
          noEntry: true,
          validationMessage: props.validationMessage,
          readOnly: props.readOnly,
          disabled: props.disabled },
        _react2.default.createElement(_ArrowIcon2.default, {
          disabled: props.disabled,
          direction: props.opened ? 'up' : 'down',
          className: 'qfa1-arrow-icon__dropdown',
          onClick: this.arrowIconClick
        })
      );
    }
  }, {
    key: 'renderLink',
    value: function renderLink(_ref2) {
      var listId = _ref2.listId;

      var props = _objectWithoutProperties(this.props, []);

      return _react2.default.createElement(
        _Link2.default,
        { href: '#',
          title: props.formLabel,
          ariaMessage: props.ariaLabel,
          'aria-owns': listId,
          onClick: this.clickLink,
          role: 'combobox' },
        props.formLabel,
        _react2.default.createElement('span', { className: props.opened ? 'up-icon' : 'down-icon' })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var props = _objectWithoutProperties(this.props, []);

      var inputId = 'select-input-' + this.id;
      var descendantIdPrefix = 'select-picker-' + inputId;
      var activeDescendant = props.ariaNavigatedItem === -1 ? '' : '' + descendantIdPrefix + props.ariaNavigatedItem;
      var listId = descendantIdPrefix + 'list';
      var className = (0, _classnames2.default)('qfa1-select widget-form__group-container', props.className);

      return _react2.default.createElement(
        'div',
        { className: className, ref: function ref(el) {
            _this3[COMPONENT_REF] = el;
          } },
        props.type === 'combobox' ? this.renderComboBox({ inputId: inputId, activeDescendant: activeDescendant, listId: listId }) : this.renderLink({ listId: listId }),
        _react2.default.createElement(_ScreenReader2.default, { text: props.screenRead }),
        _react2.default.createElement(_DropdownList2.default, { items: props.items,
          selectedItem: props.navigatedItem,
          itemIdPrefix: descendantIdPrefix,
          opened: props.opened,
          handleHoverItem: this.props.handleHoverItem,
          clickAction: this.props.selectItem,
          listId: listId,
          disabled: props.disabled,
          formatter: props.formatter })
      );
    }
  }]);

  return Select;
}(_react.Component);

Select.propTypes = {
  selectedItem: _propTypes2.default.number.isRequired,
  items: _propTypes2.default.array.isRequired,
  keyDownHandler: _propTypes2.default.func.isRequired,
  blurInput: _propTypes2.default.func.isRequired,
  handleHoverItem: _propTypes2.default.func.isRequired,
  focusInput: _propTypes2.default.func.isRequired,
  opened: _propTypes2.default.bool,
  selectItem: _propTypes2.default.func.isRequired,
  navigatedItem: _propTypes2.default.number.isRequired,
  ariaNavigatedItem: _propTypes2.default.number.isRequired,
  blur: _propTypes2.default.func.isRequired,
  id: _propTypes2.default.string,
  formLabel: _propTypes2.default.string.isRequired,
  ariaLabel: _propTypes2.default.string.isRequired,
  screenRead: _propTypes2.default.string,
  className: _propTypes2.default.string,
  readOnly: _propTypes2.default.bool,
  type: _propTypes2.default.oneOf(['combobox', 'link']),
  disabled: _propTypes2.default.bool
};
Select.defaultProps = {
  className: '',
  type: 'combobox',
  disabled: false
};
exports.default = (0, _ComponentBlur2.default)(Select, COMPONENT_REF, function (props) {
  return props.opened ? props.blurInput : null;
});
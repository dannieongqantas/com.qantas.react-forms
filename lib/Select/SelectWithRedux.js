'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectWithRedux = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _Select = require('./Select');

var _Select2 = _interopRequireDefault(_Select);

var _ActionTypes = require('../Constants/ActionTypes');

var _KeyCodes = require('../Constants/KeyCodes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SelectWithRedux = exports.SelectWithRedux = function (_Component) {
  _inherits(SelectWithRedux, _Component);

  function SelectWithRedux(props) {
    _classCallCheck(this, SelectWithRedux);

    var _this = _possibleConstructorReturn(this, (SelectWithRedux.__proto__ || Object.getPrototypeOf(SelectWithRedux)).call(this, props));

    _this.navigateItems = _this.navigateItems.bind(_this);
    _this.selectItem = _this.selectItem.bind(_this);
    _this.focusInput = _this.focusInput.bind(_this);
    _this.keyDownHandler = _this.keyDownHandler.bind(_this);
    _this.handleHoverItem = _this.handleHoverItem.bind(_this);
    _this.blurInput = _this.blurInput.bind(_this);
    return _this;
  }

  _createClass(SelectWithRedux, [{
    key: 'navigateItems',
    value: function navigateItems(direction) {
      this.props.dispatch({
        type: _ActionTypes.SELECT_NAVIGATE_LIST,
        items: this.props.items,
        id: this.props.id,
        direction: direction
      });
    }
  }, {
    key: 'focusInput',
    value: function focusInput() {
      this.props.dispatch({
        type: _ActionTypes.SELECT_FOCUS_INPUT,
        id: this.props.id
      });
      this.props.dispatch({
        type: _ActionTypes.VALIDATION_FOCUS_DONE
      });
    }
  }, {
    key: 'selectItem',
    value: function selectItem() {
      if (this.props.validationMessage) this.props.dispatch({
        type: _ActionTypes.VALIDATION_CLEAR,
        id: this.props.id
      });

      return this.props.dispatch({
        type: _ActionTypes.SELECT_SELECT_ITEM,
        id: this.props.id,
        screenRead: this.props.ariaSelection,
        items: this.props.items,
        selectedItem: this.props.items[this.props.navigatedItem]
      });
    }
  }, {
    key: 'keyDownHandler',
    value: function keyDownHandler(e) {
      switch (e.keyCode) {
        case _KeyCodes.ESCAPE_KEY_CODE:
          e.preventDefault();
          if (this.props.opened) this.props.dispatch({ type: _ActionTypes.SELECT_BLUR, id: this.props.id });
          break;
        case _KeyCodes.RETURN_KEY_CODE:
          e.preventDefault();
          if (this.props.opened) return this.selectItem();
          return this.props.dispatch({ type: _ActionTypes.SELECT_FOCUS_INPUT, id: this.props.id });
        case _KeyCodes.ARROW_UP_KEY_CODE:
          e.preventDefault();
          return this.navigateItems(-1);
        case _KeyCodes.ARROW_DOWN_KEY_CODE:
          e.preventDefault();
          return this.navigateItems(1);
        case _KeyCodes.SPACE_KEY_CODE:
          e.preventDefault();
          return this.props.dispatch({
            type: this.props.opened ? _ActionTypes.SELECT_BLUR : _ActionTypes.SELECT_FOCUS_INPUT,
            id: this.props.id
          });
        default:
          return null;
      }
      return null;
    }
  }, {
    key: 'handleHoverItem',
    value: function handleHoverItem(item) {
      this.props.dispatch({ type: _ActionTypes.SELECT_HOVER_ITEM, item: item, id: this.props.id });
    }
  }, {
    key: 'blurInput',
    value: function blurInput() {
      this.props.dispatch({
        type: _ActionTypes.SELECT_BLUR,
        id: this.props.id
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_Select2.default, _extends({ navigateItems: this.navigateItems }, this.props, {
        selectItem: this.selectItem,
        focusInput: this.focusInput,
        keyDownHandler: this.keyDownHandler,
        blurInput: this.blurInput,
        handleHoverItem: this.handleHoverItem }));
    }
  }]);

  return SelectWithRedux;
}(_react.Component);

SelectWithRedux.propTypes = {
  dispatch: _propTypes2.default.func.isRequired,
  id: _propTypes2.default.string.isRequired,
  opened: _propTypes2.default.bool,
  items: _propTypes2.default.array.isRequired,
  ariaSelection: _propTypes2.default.string,
  validationMessage: _propTypes2.default.string,
  navigatedItem: _propTypes2.default.number,
  readOnly: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool
};
exports.default = (0, _reactRedux.connect)()(SelectWithRedux);
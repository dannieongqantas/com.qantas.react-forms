'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.mergeInputAndInitialStates = mergeInputAndInitialStates;
exports.default = SelectReducerCreator;

var _ActionTypes = require('../Constants/ActionTypes');

var _screenReading = require('../utils/screenReading');

var _stringBind = require('../utils/stringBind');

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var DEFAULT_INDEX = 0; // 10:00

var initialState = exports.initialState = {
  id: 'id',
  selectedItem: DEFAULT_INDEX,
  opened: false,
  navigatedItem: 0,
  ariaNavigatedItem: -1, // for i.e AT navigation, because if aria-active descendent has initial value then it won't read out desc
  screenRead: '',
  backendCode: '',
  defaultItemIndex: DEFAULT_INDEX
};

function mergeInputAndInitialStates(inputInitialState) {
  return _extends({}, initialState, inputInitialState, {
    defaultItemIndex: inputInitialState.selectedItem ? inputInitialState.selectedItem : initialState.defaultItemIndex
  });
}

function SelectReducerCreator() {
  var initialStateInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var initialStateVal = mergeInputAndInitialStates(initialStateInput);

  var result = function SelectReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialStateVal;
    var action = arguments[1];

    if (action.id !== state.id) {
      return state;
    }

    switch (action.type) {
      case _ActionTypes.SELECT_INITIALIZE:
        {
          return _extends({}, state, {
            navigatedItem: action.item,
            selectedItem: action.item,
            backendCode: action.items[action.item].backendCode
          });
        }
      case _ActionTypes.SELECT_POPULATE:
        {
          return _extends({}, state, {
            navigatedItem: action.item,
            items: action.items,
            selectedItem: action.item,
            backendCode: action.items[action.item].backendCode
          });
        }

      case _ActionTypes.SELECT_FOCUS_INPUT:
        {
          return _extends({}, state, { opened: true, navigatedItem: state.selectedItem });
        }

      case _ActionTypes.SELECT_BLUR:
        {
          return _extends({}, state, { opened: false, ariaNavigatedItem: -1 });
        }

      case _ActionTypes.SELECT_HOVER_ITEM:
        {
          return _extends({}, state, { navigatedItem: action.item, ariaNavigatedItem: action.item });
        }

      case _ActionTypes.SELECT_SELECT_ITEM:
        {
          var selectedItem = state.navigatedItem;
          var screenRead = (0, _screenReading.reread)(state.screenRead, (0, _stringBind.stringBind)(action.screenRead, { selection: action.items[selectedItem].ariaLabel }));
          return _extends({}, state, {
            selectedItem: selectedItem,
            opened: false,
            screenRead: screenRead,
            backendCode: action.items[selectedItem].backendCode
          });
        }

      case _ActionTypes.SELECT_NAVIGATE_LIST:
        {
          var newNavigatedItem = state.navigatedItem + action.direction;

          if (newNavigatedItem >= action.items.length) newNavigatedItem = -1;else if (newNavigatedItem < -1) newNavigatedItem = action.items.length - 1;

          return _extends({}, state, { navigatedItem: newNavigatedItem, ariaNavigatedItem: newNavigatedItem });
        }

      case _ActionTypes.SELECT_ERROR:
        {
          return _extends({}, state, {
            validationMessage: action.validationMessage
          });
        }

      case _ActionTypes.SELECT_CLEAR_ERROR:
        {
          var validationMessage = state.validationMessage,
              newState = _objectWithoutProperties(state, ['validationMessage']); // eslint-disable-line no-unused-vars


          return newState;
        }

      case _ActionTypes.SELECT_DEFAULT_SELECTED_ITEM:
        {
          var _validationMessage = state.validationMessage,
              _newState = _objectWithoutProperties(state, ['validationMessage']); // eslint-disable-line no-unused-vars


          return _extends({}, _newState, {
            selectedItem: _newState.defaultItemIndex,
            opened: false
          });
        }

      case _ActionTypes.SELECT_SET_SELECTED_ITEM:
        {
          // Assists in setting selectedItem for when you want to render the
          // input with a value already selected.
          var itemIndex = action.itemIndex;
          var defaultItemIndex = state.defaultItemIndex;

          var selectedItemIndex = Number.isInteger(itemIndex) && itemIndex >= 0 ? itemIndex : defaultItemIndex;
          return _extends({}, state, {
            selectedItem: selectedItemIndex
          });
        }

      default:
        {
          return state;
        }
    }
  };

  result.initialState = initialStateVal;
  return result;
}
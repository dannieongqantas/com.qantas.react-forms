'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ExclamationIcon = require('../Icons/ExclamationIcon');

var _ExclamationIcon2 = _interopRequireDefault(_ExclamationIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ErrorItem = function ErrorItem(_ref) {
  var error = _ref.error;
  return _react2.default.createElement(
    'li',
    { className: 'form-validation-summary__item', 'aria-hidden': 'true' },
    error
  );
};

ErrorItem.propTypes = { error: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.element]) };

var FormValidationSummary = function (_Component) {
  _inherits(FormValidationSummary, _Component);

  function FormValidationSummary() {
    _classCallCheck(this, FormValidationSummary);

    return _possibleConstructorReturn(this, (FormValidationSummary.__proto__ || Object.getPrototypeOf(FormValidationSummary)).apply(this, arguments));
  }

  _createClass(FormValidationSummary, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.refs.summary.setAttribute('focusable', true);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.summaryFocus) {
        this.refs.summary.focus();
        this.props.onFocus();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          showForSrOnly = _props.showForSrOnly,
          validations = _props.validations,
          summaryText = _props.summaryText;


      var errors = validations.filter(function (message) {
        return !!message;
      }).map(function (message) {
        return message;
      });

      var summaryClass = (0, _classnames2.default)('form-validation-summary', {
        'form-validation-summary--show-for-sr-only': showForSrOnly,
        'widget-form__element--hidden': errors.length === 0
      });

      return _react2.default.createElement(
        'div',
        { className: summaryClass, ref: 'summary', tabIndex: '0' },
        _react2.default.createElement(_ExclamationIcon2.default, { className: 'form-validation-summary__icon' }),
        _react2.default.createElement(
          'div',
          { className: 'form-validation-summary__title', 'aria-label': summaryText + ', ' + errors.join(', ') },
          _react2.default.createElement(
            'strong',
            null,
            summaryText
          ),
          _react2.default.createElement(
            'span',
            { className: 'widget-form__element--aria-hidden' },
            errors.join(', ')
          )
        ),
        _react2.default.createElement(
          'ul',
          { className: 'form-validation-summary__list', 'aria-hidden': 'true' },
          errors.map(function (error, index) {
            return _react2.default.createElement(ErrorItem, { error: error, key: index });
          })
        )
      );
    }
  }]);

  return FormValidationSummary;
}(_react.Component);

FormValidationSummary.propTypes = {
  validations: _propTypes2.default.array.isRequired,
  summaryText: _propTypes2.default.string.isRequired,
  summaryFocus: _propTypes2.default.bool,
  showForSrOnly: _propTypes2.default.bool,
  onFocus: _propTypes2.default.func
};
FormValidationSummary.defaultProps = {
  showForSrOnly: false,
  summaryFocus: false,
  onFocus: function onFocus() {}
};
exports.default = FormValidationSummary;
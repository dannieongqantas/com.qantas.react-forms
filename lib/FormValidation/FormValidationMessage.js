'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _BorderTriangle = require('../Icons/BorderTriangle');

var _BorderTriangle2 = _interopRequireDefault(_BorderTriangle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ValidationMessage = function (_Component) {
  _inherits(ValidationMessage, _Component);

  function ValidationMessage() {
    _classCallCheck(this, ValidationMessage);

    return _possibleConstructorReturn(this, (ValidationMessage.__proto__ || Object.getPrototypeOf(ValidationMessage)).apply(this, arguments));
  }

  _createClass(ValidationMessage, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          message = _props.message,
          hide = _props.hide,
          hideTriangle = _props.hideTriangle;


      var messageClass = (0, _classnames2.default)('form-validation-message', {
        'widget-form__element--hidden': hide || !message
      });

      return _react2.default.createElement(
        'div',
        { className: messageClass },
        _react2.default.createElement(
          'div',
          { className: 'form-validation-message__container' },
          !hideTriangle && _react2.default.createElement(_BorderTriangle2.default, { innerColor: '#fcebcd', outerColor: '#fcebcd', positionFromLeft: '20px', innerSize: '10px', outerSize: '10px', zIndex: '0' }),
          _react2.default.createElement(
            'div',
            { className: 'form-validation-message__text' },
            this.props.message
          )
        )
      );
    }
  }]);

  return ValidationMessage;
}(_react.Component);

ValidationMessage.propTypes = {
  message: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.element]),
  hide: _propTypes2.default.bool,
  hideTriangle: _propTypes2.default.bool
};
ValidationMessage.defaultProps = {
  hide: false,
  hideTriangle: false
};
exports.default = ValidationMessage;
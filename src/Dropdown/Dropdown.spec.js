import React from 'react';
import { shallow, mount } from 'enzyme';
import Dropdown from './Dropdown';
function createComponent(props, shallowRender = true) {
  const newProps = {
    opened: true,
    ...props
  };
  return (shallowRender ? shallow : mount)(<Dropdown {...newProps} />);
}

describe('Dropdown', () => {
  let component;

  it('should render children', () => {
    const props = {
      children: [
        <input type="text" />,
        <input type="password" />
      ]
    };
    component = createComponent(props);
    expect(component.contains(<input type="text" />)).to.be.true;
    expect(component.contains(<input type="password" />)).to.be.true;
  });

  it('should contain a hidden class when not opened', () => {
    component = createComponent({ opened: false });
    expect(component.hasClass('widget-form__element--hidden')).to.be.true;
  });

  it('should contain a hidden class when disabled', () => {
    component = createComponent({ disabled: true });
    expect(component.hasClass('widget-form__element--hidden')).to.be.true;
  });
});

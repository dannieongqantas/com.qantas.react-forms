import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Dropdown(props) {
  const { children, opened, disabled } = props;
  const componentClass = classNames({
    'widget-form__element--hidden': !opened || disabled
  });
  return <div className={`qfa1-dropdown-list__items-container-father ${componentClass}`}>
    <div className="qfa1-dropdown-list__items-container">
      <div className="widget-form__group">
        {children}
      </div>
    </div>
  </div>;
}

Dropdown.propTypes = {
  opened: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]).isRequired,
  disabled: PropTypes.bool
};

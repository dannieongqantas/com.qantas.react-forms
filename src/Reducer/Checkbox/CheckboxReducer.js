import { CHECKBOX_TOGGLE, CHECKBOX_SET_VALUE } from '../../Constants/ActionTypes';
import createNewState from '../../utils/createNewStateById';
export const initialState = {
  differentDropoffLocation: {
    id: 'differentDropoffLocation',
    checked: false,
    screenRead: ''
  }
};

export default function checkboxReducer(state = initialState, action) {
  switch (action.type) {
    case CHECKBOX_TOGGLE: {
      const checked = !state[action.id].checked;
      return createNewState(state, { checked, screenRead: action.screenRead }, action.id);
    }
    case CHECKBOX_SET_VALUE:
      return createNewState(state, { checked: action.checked }, action.id);
    default:
      return state;
  }
}


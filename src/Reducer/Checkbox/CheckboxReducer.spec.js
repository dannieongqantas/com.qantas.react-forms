import { CHECKBOX_TOGGLE, CHECKBOX_SET_VALUE } from '../../Constants/ActionTypes';
import deepFreeze from 'deep-freeze';
import checkboxReducer, { initialState } from './CheckboxReducer';

const id = 'differentDropoffLocation';
deepFreeze(initialState);
deepFreeze(initialState[id]);

describe('CheckboxReducer', () => {
  it('should return the initial state', () => {
    const reducerState = checkboxReducer(undefined, {});
    expect(reducerState).to.deep.equal(initialState);
  });

  it(`should handle the ${CHECKBOX_TOGGLE} action`, () => {
    const startState = {
      differentDropoffLocation: {
        checked: false
      }
    };

    let reducerState = checkboxReducer(startState, { type: CHECKBOX_TOGGLE, id });
    expect(reducerState.differentDropoffLocation.checked).to.be.true;
    reducerState = checkboxReducer(reducerState, { type: CHECKBOX_TOGGLE, id });
    expect(reducerState.differentDropoffLocation.checked).to.be.false;
  });

  it(`should handle the ${CHECKBOX_SET_VALUE} action`, () => {
    const startState = {
      differentDropoffLocation: {
        checked: false
      }
    };
    const reducerState = checkboxReducer(startState, { type: CHECKBOX_SET_VALUE, checked: true, id });
    expect(reducerState.differentDropoffLocation.checked).to.be.true;
  });
});

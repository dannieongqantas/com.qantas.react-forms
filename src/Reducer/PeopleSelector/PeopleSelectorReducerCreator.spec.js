import PeopleSelectorReducerCreator from './PeopleSelectorReducerCreator';
import i18nData from '../../../test/mock/i18n';
import { SELECTION_CHANGE, SELECTION_SETRANGE } from '../../Constants/ActionTypes';

const i18n = { ...i18nData.shared, ...i18nData.hotel };

describe('PeopleSelectorReducerCreator', () => {
  it('should create a reducer with given initial state', () => {
    const initialState = { value: { adults: 1 } };
    const peopleSelectorReducer = PeopleSelectorReducerCreator(initialState);
    expect(peopleSelectorReducer()).to.equal(initialState);
  });

  it('reducer should handle SELECTION_CHANGE', () => {
    const initialState = { value: { adults: 1 } };
    const peopleSelectorReducer = PeopleSelectorReducerCreator(initialState);
    const newState = peopleSelectorReducer(initialState, {
      type: SELECTION_CHANGE,
      value: { adults: 2 }
    });
    expect(newState).to.eql({
      value: { adults: 2 }
    });
  });

  it('reducer should handle SELECTION_CHANGE with screen read', () => {
    const initialState = { value: { adults: 1 } };
    const copy = i18n.guests;
    const peopleSelectorReducer = PeopleSelectorReducerCreator(initialState);
    const newState = peopleSelectorReducer(initialState, {
      type: SELECTION_CHANGE,
      value: { adults: 2 },
      copy
    });

    expect(newState).to.eql({
      value: { adults: 2 },
      screenRead: 'You have chosen 2 Adults'
    });
  });

  it('reducer should handle SELECTION_SETRANGE', () => {
    const initialState = { range: { adults: { } } };
    const copy = i18n.guests;
    const peopleSelectorReducer = PeopleSelectorReducerCreator(initialState);
    const newState = peopleSelectorReducer(initialState, {
      type: SELECTION_SETRANGE,
      range: { adults: { rangeStart: 15, rangeEnd: 20 } },
      copy
    });

    expect(newState.range.adults.rangeStart).to.eql(15);
  });
});

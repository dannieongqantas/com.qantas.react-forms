import { SELECTION_CHANGE, SELECTION_SETRANGE } from '../../Constants/ActionTypes';
import { createSelectionText } from '../../PeopleSelector/PeopleSelectorControlled';
import { stringBind } from '../../utils/stringBind';

function peopleSelectorReducer(state = {}, action = {}) {
  if (action.id && state.id !== action.id) {
    return state;
  }

  let newState;
  switch (action.type) {
    case SELECTION_CHANGE:
      newState = { ...state, value: action.value };
      if (action.copy) {
        newState.screenRead = stringBind(action.copy.ariaSelection, { selection: createSelectionText(action.value, action.copy) });
      }
      return newState;
    case SELECTION_SETRANGE:
      newState = { ...state, range: action.range };
      return newState;
    default:
      return state;
  }
}

export default function reducerCreator(initialStateInput = {}) {
  return (state = initialStateInput, action) => peopleSelectorReducer(state, action);
}

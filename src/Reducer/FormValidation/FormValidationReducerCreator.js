import { VALIDATION_TRIGGER,
  VALIDATION_CLEAR,
  VALIDATION_RESET,
  VALIDATION_FOCUS_DONE,
  VALIDATION_FOCUS } from '../../Constants/ActionTypes';

export default function FormValidationReducerCreator(initialStateVal = { summaryFocus: false }) {
  return function FormValidationReducer(state = initialStateVal, action) {
    switch (action.type) {
      case VALIDATION_TRIGGER: {
        const newState = { ...state, errors: { ...state.errors, [action.id]: action.validationMessage } };
        return newState;
      }

      case VALIDATION_CLEAR: {
        const newState = { ...state };
        newState.errors[action.id] = '';
        return newState;
      }

      case VALIDATION_FOCUS_DONE: {
        return { ...state, summaryFocus: false };
      }

      case VALIDATION_FOCUS: {
        return { ...state, summaryFocus: true };
      }

      case VALIDATION_RESET: {
        return initialStateVal;
      }

      default: {
        return state;
      }
    }
  };
}

import FormValidationReducerCreator from './FormValidationReducerCreator';
import { VALIDATION_TRIGGER,
  VALIDATION_CLEAR,
  VALIDATION_RESET,
  VALIDATION_FOCUS,
  VALIDATION_FOCUS_DONE } from '../../Constants/ActionTypes';

const initialState = {
  errors: {
    pickupLocation: ''
  },
  summaryFocus: false
};

import deepFreeze from 'deep-freeze';
deepFreeze(initialState);

const id = 'pickupLocation';

const FormValidationReducer = FormValidationReducerCreator(initialState);

describe('FormValidationReducerCreator', () => {
  it('should return the initial state', () => {
    const reducerState = FormValidationReducer(undefined, {});
    expect(reducerState).to.deep.equal(initialState);
  });

  it(`should handle ${VALIDATION_TRIGGER}`, () => {
    const expectedState = { ...initialState, errors: { pickupLocation: 'testing' } };

    const reducerState = FormValidationReducer(initialState, { type: VALIDATION_TRIGGER, validationMessage: 'testing', id });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${VALIDATION_CLEAR}`, () => {
    const startState = { ...initialState, errors: { pickupLocation: 'testing' } };

    const expectedState = { ...startState, errors: { pickupLocation: '' } };

    const reducerState = FormValidationReducer(startState, { type: VALIDATION_CLEAR, id });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${VALIDATION_RESET}`, () => {
    const reducerState = FormValidationReducer(initialState, { type: VALIDATION_RESET, id });
    expect(reducerState).to.deep.equal(initialState);
  });

  it(`should handle ${VALIDATION_FOCUS}`, () => {
    const reducerState = FormValidationReducer(initialState, { type: VALIDATION_FOCUS });
    expect(reducerState).to.deep.equal({ ...initialState, summaryFocus: true });
  });

  it(`should handle ${VALIDATION_FOCUS_DONE}`, () => {
    const reducerState = FormValidationReducer({ ...initialState, summaryFocus: true }, { type: VALIDATION_FOCUS_DONE });
    expect(reducerState).to.deep.equal({ ...initialState, summaryFocus: false });
  });
});

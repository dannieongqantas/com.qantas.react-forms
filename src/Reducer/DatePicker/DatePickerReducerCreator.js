import {
  CALENDAR_SELECT_ITEM,
  CALENDAR_MIN_DATE,
  CALENDAR_RANGE_DATE_CHANGE,
  CALENDAR_DAY_LIMIT_CHANGE
} from '../../Constants/ActionTypes';

function datePickerReducer(state = {}, action = {}) {
  if (action.id && state.id !== action.id) {
    return state;
  }

  switch (action.type) {
    case CALENDAR_SELECT_ITEM:
      return {
        ...state,
        value: action.value
      };
    case CALENDAR_MIN_DATE:
      return {
        ...state,
        minDate: action.minDate
      };
    case CALENDAR_RANGE_DATE_CHANGE:
      return {
        ...state,
        rangeStartDate: action.rangeStartDate
      };
    case CALENDAR_DAY_LIMIT_CHANGE:
      return {
        ...state,
        daysLimit: action.daysLimit
      };
    default:
      return state;
  }
}

export default function reducerCreator(initialStateInput = {}) {
  return (state = initialStateInput, action) => datePickerReducer(state, action);
}

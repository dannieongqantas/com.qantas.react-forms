import radioReducerCreator from './RadioReducerCreator';
import { RADIO_TOGGLE } from '../../Constants/ActionTypes';

const initialState = {
  id: 'tripType',
  selectedItem: 'return',
  radios: [
    {
      id: 'return'
    },
    {
      id: 'oneway'
    }
  ]
};

describe('RadioReducerCreator', () => {
  it('should create a reducer with given initial state', () => {
    const radioReducer = radioReducerCreator(initialState);
    expect(radioReducer()).to.equal(initialState);
  });

  it('should set the selectedItem on RADIO_TOGGLE', () => {
    const toggleaction = {
      type: RADIO_TOGGLE,
      id: 'oneway',
      group: 'tripType'
    };

    const radioReducer = radioReducerCreator(initialState);
    const newState = radioReducer(initialState, toggleaction);
    expect(newState.selectedItem).to.equal('oneway');
  });
});

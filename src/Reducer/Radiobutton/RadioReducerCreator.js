import { RADIO_TOGGLE } from '../../Constants/ActionTypes';
export const initialState = {
  group: 'routeType',
  selectedItem: ''
};

export function radioButtonReducer(state = {}, action = {}) {
  if (state.id !== action.group) {
    return state;
  }
  switch (action.type) {
    case RADIO_TOGGLE:
      return { ...state, selectedItem: action.id };
    default:
      return state;
  }
}

export default function radioReducerCreator(initialStateInput = initialState) {
  return (state = initialStateInput, action) => radioButtonReducer(state, action);
}

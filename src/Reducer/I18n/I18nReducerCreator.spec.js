import I18nReducerCreator from './I18nReducerCreator';

const baseState = {
  en: {
    some: 'value',
    another: 'string'
  }
};

describe('I18nReducerCreator', () => {
  describe('reducer creator default values', () => {
    it('should set `en` as the default language', () => {
      expect(I18nReducerCreator(baseState)()).to.eql(baseState.en);
    });

    it('should set the state to an empty object if no initialState is provided', () => {
      expect(I18nReducerCreator()()).to.eql({});
    });
  });

  it('should throw an error if the language is not found in the initialState', () => {
    expect(() => I18nReducerCreator(baseState, 'fr')()).to.throw(Error);
  });
});

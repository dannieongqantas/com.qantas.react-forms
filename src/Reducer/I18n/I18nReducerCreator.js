/* NOTE: This is deprecated. Use the i18nReducerCreator in src/i18n now. */
import deprecationNotice from '../../utils/deprecationNotice';

const defaultState = {
  en: {}
};

export default function I18nReducerCreator(initialStateVal = defaultState, language = 'en') {
  deprecationNotice('src/Reducer/I18n/I18nReducerCreator is deprecated. Please switch to src/i18n/i18nReducer.');

  const i18nDataForLanguage = initialStateVal[language];

  if (!i18nDataForLanguage) {
    throw new Error(`i18n error: widget has no translations for "${language}".`);
  }

  return function I18nReducer(state = i18nDataForLanguage) {
    return state;
  };
}

import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';

import breakpoints from '../grid/breakpoints';
import i18nData from '../../test/mock/i18n';

import createComponentStub from '../../test/utils/ComponentStub';

const proxyquire = require('proxyquire').noCallThru();
const CalendarStub = createComponentStub();
const ArrowIconStub = createComponentStub();
const CalendarDropdownComponent = proxyquire('./CalendarDropdown', {
  './Calendar': CalendarStub,
  '../Icons/ArrowIcon': ArrowIconStub
}).default;

function renderComponent(props, newProps) {
  const propsAggregate = { ...props, ...newProps };
  return TestUtils.renderIntoDocument(<CalendarDropdownComponent {...propsAggregate} />);
}

describe('CalendarDropdown', () => {
  describe('component', () => {
    const props = {
      i18n: i18nData,
      isOpened: true,
      selectedDate: new Date(2016, 0, 30),
      calendarStartDate: new Date(2016, 0, 1),
      minDate: new Date(2016, 0, 10),
      maxDate: new Date(2017, 0, 8),
      isMobile: false,
      isRangeEnabled: true,
      rangeStartDate: new Date(2016, 0, 10),
      rangeEndDate: new Date(2016, 0, 29),
      selectedDateArrowDirection: 'right',
      dispatch: sinon.spy(),
      selectDate: sinon.spy()
    };

    describe('render', () => {
      it('should render properly', () => {
        const renderedComponent = renderComponent(props);
        const dom = findDOMNode(renderedComponent);
        expect(dom).to.exist;
      });

      it('should not render when not opened', () => {
        const renderedComponent = renderComponent(props, { isOpened: false });
        const dom = findDOMNode(renderedComponent);
        expect(dom).to.not.exist;
      });

      it('should render previous month arrow icon with correct properties', () => {
        const renderedComponent = renderComponent(props, { calendarStartDate: new Date(2016, 1, 1) });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);

        const leftArrowProps = arrowIcons[0].props;

        expect(leftArrowProps.className).to.be.equal('date-picker__arrow date-picker__arrow-left');
        expect(leftArrowProps.disabled).to.be.equal(false);
        expect(leftArrowProps.direction).to.be.equal('left');
      });

      it('should render next month arrow icon with correct properties', () => {
        const renderedComponent = renderComponent(props, { calendarStartDate: new Date(2016, 0, 1) });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);

        const rightArrowProps = arrowIcons[1].props;

        expect(rightArrowProps.className).to.be.equal('date-picker__arrow date-picker__arrow-right');
        expect(rightArrowProps.disabled).to.be.equal(false);
        expect(rightArrowProps.direction).to.be.equal('right');
      });

      it('should not render previous month arrow icon when on min date month', () => {
        const renderedComponent = renderComponent(props, { calendarStartDate: new Date(2016, 0, 1) });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);

        const leftArrowProps = arrowIcons[0].props;
        expect(leftArrowProps.disabled).to.be.equal(true);
      });

      it('should not render next month arrow icon when on max date month', () => {
        const renderedComponent = renderComponent(props, { calendarStartDate: new Date(2017, 0, 1) });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);

        const rightArrowProps = arrowIcons[1].props;

        expect(rightArrowProps.disabled).to.be.equal(true);
      });

      it('should render calendar', () => {
        const renderedComponent = renderComponent({ ...props, isMobile: true });
        const calendars = TestUtils.scryRenderedComponentsWithType(renderedComponent, CalendarStub);
        expect(calendars.length).to.be.equal(1);
        const calendarProps = calendars[0].props;
        expect(calendarProps.year).to.be.equal(2016);
        expect(calendarProps.month).to.be.equal(0);
        expect(calendarProps.today).to.be.equal(props.today);
        expect(calendarProps.selectedDate).to.be.equal(props.selectedDate);
        expect(calendarProps.i18n).to.be.equal(props.i18n);
        expect(calendarProps.id).to.be.equal(props.id);
      });
    });

    describe('render desktop', () => {
      it('should render two months', () => {
        const renderedComponent = renderComponent(props, { width: breakpoints.medium + 1 });
        const calendars = TestUtils.scryRenderedComponentsWithType(renderedComponent, CalendarStub);
        expect(calendars.length).to.be.equal(2);
        expect(calendars[0].props.month).to.be.equal(0);
        expect(calendars[1].props.month).to.be.equal(1);
      });

      it('should not render next arrow when calendar start month is penultimate', () => {
        const renderedComponent = renderComponent(props, {
          width: breakpoints.medium + 1,
          calendarStartDate: new Date(2016, 11, 1)
        });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);
        const rightArrowProps = arrowIcons[1].props;

        expect(rightArrowProps.disabled).to.be.equal(true);
      });

      it('should not render next arrow when calendar start month has max date possible', () => {
        const renderedComponent = renderComponent(props, {
          width: breakpoints.medium + 1,
          calendarStartDate: new Date(2017, 0, 1)
        });
        const arrowIcons = TestUtils.scryRenderedComponentsWithType(renderedComponent, ArrowIconStub);
        const rightArrowProps = arrowIcons[1].props;

        expect(rightArrowProps.disabled).to.be.equal(true);
      });
    });

    describe('componentDidUpdate', () => {
      let renderedComponent;
      let getBoundingClientRectStub;
      let scrollByStub;
      const scrollCalendarInViewOnOpen = 'scrollBehaviour';
      const elemPosition = 10;

      describe('when scrollCalendarInViewOnOpen prop is not provided', () => {
        beforeEach(() => {
          renderedComponent = renderComponent(props);
          renderedComponent.wrapperElement = {
            getBoundingClientRect: () => {}
          };
          getBoundingClientRectStub = sinon.stub(renderedComponent.wrapperElement, 'getBoundingClientRect');
          scrollByStub = sinon.stub(window, 'scrollBy');
        });

        afterEach(() => {
          getBoundingClientRectStub.reset();
          scrollByStub.reset();
        });

        it('should not call scrollBy', () => {
          renderedComponent.componentDidUpdate({});
          expect(scrollByStub).to.not.have.been.called;
        });
      });

      describe('when calendar is opened', () => {
        beforeEach(() => {
          renderedComponent = renderComponent(props, {
            isOpened: true,
            scrollCalendarInViewOnOpen
          });
          renderedComponent.wrapperElement = {
            getBoundingClientRect: () => {}
          };
          getBoundingClientRectStub = sinon.stub(renderedComponent.wrapperElement, 'getBoundingClientRect').returns({ bottom: elemPosition });
          scrollByStub = sinon.stub(window, 'scrollBy');
        });

        afterEach(() => {
          getBoundingClientRectStub.reset();
          scrollByStub.reset();
        });

        it('should call scrollBy with the correct options when calendar was previously closed', () => {
          const distanceToScroll = elemPosition - window.innerHeight;
          renderedComponent.componentDidUpdate({ isOpened: false });
          expect(scrollByStub).to.have.been.calledWith({
            behavior: scrollCalendarInViewOnOpen,
            top: distanceToScroll,
            left: 0
          });
        });

        it('should not call scrollBy when calendar was previously open', () => {
          renderedComponent.componentDidUpdate({ isOpened: true });
          expect(scrollByStub).to.not.have.been.called;
        });
      });

      describe('when calendar is closed', () => {
        beforeEach(() => {
          renderedComponent = renderComponent(props, {
            isOpened: false,
            scrollCalendarInViewOnOpen
          });
          renderedComponent.wrapperElement = {
            getBoundingClientRect: () => {}
          };
          getBoundingClientRectStub = sinon.stub(renderedComponent.wrapperElement, 'getBoundingClientRect');
        });

        it('should not call scrollBy when calendar was previously open', () => {
          renderedComponent.componentDidUpdate({ isOpened: true });
          expect(scrollByStub).to.not.have.been.called;
        });

        it('should not call scrollBy when calendar was previously closed', () => {
          renderedComponent.componentDidUpdate({ isOpened: false });
          expect(scrollByStub).to.not.have.been.called;
        });
      });
    });
  });
});

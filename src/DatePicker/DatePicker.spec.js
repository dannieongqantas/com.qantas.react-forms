import React from 'react';
import TestUtils from 'react-dom/test-utils';
const proxyquire = require('proxyquire').noCallThru();
import ComponentStub from '../../test/utils/ComponentStub';
import i18nData from '../../test/mock/i18n';
import * as dateUtils from '../utils/date';
const i18n = { ...i18nData.shared, ...i18nData.flight };

const screenReadText = 'transformedWord';
const DatePickerControlledStub = ComponentStub();
const rereadSpy = sinon.spy();
const today = new Date(2016, 4, 10);
const DatePicker = proxyquire('./DatePicker', {
  './DatePickerControlled': DatePickerControlledStub,
  '../utils/date': {
    ...dateUtils,
    currentDate: () => today,
    transformDateToLongWords: () => screenReadText
  },
  '../ReusableComponents/WindowResize': {
    getWindowWidth: () => 1024
  },
  '../utils/screenReading': {
    reread(oldText, newText) {
      rereadSpy();
      return newText;
    }
  }
}).default;

const onChangeSpy = sinon.spy();

const defaultProps = {
  i18n,
  ...i18n.pickupDate,
  daysLimit: 90,
  isRangeEnabled: false,
  onChange: onChangeSpy,
  hasEventRange: true,
  eventRangeStartDate: 'Jun 1, 2019 12:00:00 AM',
  eventRangeEndDate: 'Jun 7, 2019 12:00:00 AM'
};

const defaultControlledProps = {
  ...defaultProps,
  value: today
};

function createComponent(props) {
  return TestUtils.renderIntoDocument(<DatePicker {...props} />);
}

describe('DatePicker', () => {
  describe('Component', () => {
    it('should render controlled component properly', () => {
      const component = createComponent(defaultControlledProps);
      const datePicketControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      expect(datePicketControlled).to.exist;
      expect(datePicketControlled.props.selectedDate).to.equal(component.props.value);
      expect(datePicketControlled.props.daysLimit).to.equal(90);
      expect(datePicketControlled.props.minDate).to.eql(today);
      expect(datePicketControlled.props.maxDate).to.eql(dateUtils.plusDays(today, 89));
      expect(datePicketControlled.props.isOpened).to.equal(false);
      expect(datePicketControlled.props.hasEventRange).to.equal(true);
      expect(datePicketControlled.props.eventRangeStartDate).to.equal('Jun 1, 2019 12:00:00 AM');
      expect(datePicketControlled.props.eventRangeEndDate).to.equal('Jun 7, 2019 12:00:00 AM');
    });
  });

  it('should have initial state', () => {
    const component = createComponent(defaultControlledProps);
    expect(component.state).to.eql({
      isOpened: false,
      calendarStartDate: new Date(2016, 4, 1),
      dateNavigated: today,
      rangeEndDate: today,
      width: 1024,
      screenRead: ''
    });
  });

  describe('actions', () => {
    it('should handle openCalendar on controlled component', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      datePickerControlled.props.openCalendar();
      expect(component.state.isOpened).to.equal(true);
      expect(component.state.calendarStartDate).to.eql(new Date(2016, 4, 1));
      expect(component.state.rangeEndDate).to.equal(today);
      expect(component.state.dateNavigated).to.equal(today);
    });


    it('should handle closeCalendar on controlled component', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      datePickerControlled.props.openCalendar();
      datePickerControlled.props.closeCalendar();
      expect(component.state.isOpened).to.equal(false);
      expect(component.state.screenRead).to.equal('');
    });

    it('should handle changeCalendarWidth on controlled component', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      datePickerControlled.props.changeCalendarWidth(1000);
      expect(component.state.width).to.equal(1000);
    });

    it('should handle toggleCalendarMonth on controlled component', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      datePickerControlled.props.toggleCalendarMonth(1);
      expect(component.state.calendarStartDate).to.eql(new Date(2016, 5, 1));
    });

    it('navigateToDate - should retain same state when calendar is not opened', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const dateNavigated = new Date(2016, 4, 9);
      const previousState = component.state;
      datePickerControlled.props.navigateToDate(dateNavigated, 'minMessage', 'maxMessage');
      expect(component.state).to.equal(previousState);
    });


    it('should handle navigateToDate on controlled component', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const dateNavigated = new Date(2016, 4, 12);
      datePickerControlled.props.openCalendar();
      datePickerControlled.props.navigateToDate(dateNavigated, 'minMessage', 'maxMessage');
      expect(component.state.dateNavigated).to.equal(dateNavigated);
      expect(component.state.rangeEndDate).to.equal(dateNavigated);
      expect(component.state.screenRead).to.equal(screenReadText);
    });

    it('navigateToDate - should not change when new navigated date is in past', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const dateNavigated = new Date(2016, 4, 9);
      datePickerControlled.props.openCalendar();
      const previousNavigatedDate = component.state.dateNavigated;
      datePickerControlled.props.navigateToDate(dateNavigated, 'minMessage', 'maxMessage');
      expect(component.state.dateNavigated).to.equal(previousNavigatedDate);
      expect(component.state.screenRead).to.equal('minMessage');
      expect(rereadSpy).to.have.been.called;
    });

    it('navigateToDate - should not change when new navigated date is after max days', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const dateNavigated = new Date(2017, 4, 9);
      datePickerControlled.props.openCalendar();
      const previousNavigatedDate = component.state.dateNavigated;
      datePickerControlled.props.navigateToDate(dateNavigated, 'minMessage', 'maxMessage');
      expect(component.state.dateNavigated).to.equal(previousNavigatedDate);
      expect(component.state.screenRead).to.equal('maxMessage');
      expect(rereadSpy).to.have.been.called;
    });

    it('navigateToDate - should change calendar start date when navigated date is not visible', () => {
      const component = createComponent(defaultControlledProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const dateNavigated = new Date(2016, 6, 2);
      datePickerControlled.props.openCalendar();
      datePickerControlled.props.navigateToDate(dateNavigated, 'minMessage', 'maxMessage');
      expect(component.state.dateNavigated).to.equal(dateNavigated);
      expect(component.state.calendarStartDate).to.eql(new Date(2016, 5, 1));
      expect(component.state.rangeEndDate).to.equal(dateNavigated);
      expect(component.state.screenRead).to.equal(screenReadText);
    });


    it('should handle onChange on controlled component', () => {
      const component = createComponent(defaultProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const newValue = dateUtils.plusDays(today, 1);
      datePickerControlled.props.onChange(newValue);
      expect(onChangeSpy).to.have.been.calledWith(newValue);
    });
  });

  describe('Value is uncontrolled', () => {
    it('should manage value in state', () => {
      const component = createComponent(defaultProps);
      expect(component.state.value).to.be.defined;
    });

    it('should honor initialValue', () => {
      const initialValue = today;
      const component = createComponent({ ...defaultProps, initialValue });
      expect(component.state.value).to.be.equal(initialValue);
    });

    it('onChange should set new state', () => {
      const newValue = dateUtils.plusDays(today, 1);
      const component = createComponent(defaultProps);
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      datePickerControlled.props.selectDate(newValue);
      expect(component.state.value).to.be.equal(newValue);
    });
  });

  describe('Disabled state', () => {
    it('should return previous state on openCalendar on controlled component', () => {
      const component = createComponent({ ...defaultControlledProps, disabled: true });
      const datePickerControlled = TestUtils.findRenderedComponentWithType(component, DatePickerControlledStub);
      const prevState = component.state;
      datePickerControlled.props.openCalendar();
      expect(component.state).to.deep.equal(prevState);
    });
  });
});

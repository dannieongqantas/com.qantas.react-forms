import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import smoothscroll from 'smoothscroll-polyfill';
import Calendar from './Calendar';
import ArrowIcon from '../Icons/ArrowIcon';
import CalendarStarIcon from '../Icons/CalendarStarIcon';
import { firstDateInMonth, plusMonths } from '../utils/date';

export default class CalendarDropdown extends Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired,
    isMobile: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    selectedDate: PropTypes.instanceOf(Date).isRequired,
    isOpened: PropTypes.bool.isRequired,
    calendarStartDate: PropTypes.instanceOf(Date).isRequired,
    minDate: PropTypes.instanceOf(Date).isRequired,
    maxDate: PropTypes.instanceOf(Date).isRequired,
    isRangeEnabled: PropTypes.bool.isRequired,
    hasEventRange: PropTypes.bool,
    rangeStartDate: PropTypes.instanceOf(Date).isRequired,
    rangeEndDate: PropTypes.instanceOf(Date).isRequired,
    eventRangeStartDate: PropTypes.instanceOf(Date),
    eventRangeEndDate: PropTypes.instanceOf(Date),
    selectedDateArrowDirection: PropTypes.string.isRequired,
    descendantId: PropTypes.string.isRequired,
    selectDate: PropTypes.func.isRequired,
    navigateToDate: PropTypes.func.isRequired,
    toggleCalendarMonth: PropTypes.func.isRequired,
    ariaToggleNextMonth: PropTypes.string.isRequired,
    ariaTogglePreviousMonth: PropTypes.string.isRequired,
    maxMonthMessage: PropTypes.string.isRequired,
    minMonthMessage: PropTypes.string.isRequired,
    scrollCalendarInViewOnOpen: PropTypes.oneOf(['smooth', 'instant'])
  };

  componentDidMount() {
    if (!!this.props.scrollCalendarInViewOnOpen) {
      smoothscroll.polyfill();
    }
  }

  componentDidUpdate(prevProps) {
    const { scrollCalendarInViewOnOpen, isOpened } = this.props;

    const isScrollable = !!scrollCalendarInViewOnOpen && !!this.wrapperElement;
    const didOpen = prevProps.isOpened === false && isOpened === true;

    if (isScrollable && didOpen) {
      const elemPosition = this.wrapperElement.getBoundingClientRect().bottom;
      const distanceToScroll = elemPosition - window.innerHeight;
      const options = {
        behavior: scrollCalendarInViewOnOpen,
        top: distanceToScroll,
        left: 0
      };

      window.scrollBy(options);
    }
  }

  handleCalendarToggle = toggleAmount => {
    this.props.toggleCalendarMonth(toggleAmount);
  };

  resetToLastNavigatedDate = () => {
    this.props.navigateToDate(this.props.selectedDate);
  };

  render() {
    const { ...props } = this.props;
    const legendText = '= Offer period';

    if (!props.isOpened) return null;

    const calendarOneStartDate = props.calendarStartDate;

    const calendars = [
      <Calendar {...props} month={calendarOneStartDate.getMonth()} year={calendarOneStartDate.getFullYear()} key="0" />
    ];

    const hideLeft = calendarOneStartDate.getTime() <= firstDateInMonth(props.minDate).getTime();

    let hideRight = calendarOneStartDate.getTime() >= firstDateInMonth(props.maxDate).getTime();

    // first calendar row

    if (!props.isMobile) {
      const calendarTwoStartDate = plusMonths(calendarOneStartDate, 1);
      calendars.push(<Calendar {...props} month={calendarTwoStartDate.getMonth()}
          year={calendarTwoStartDate.getFullYear()}
                                          key="1" />);
      hideRight = hideRight || calendarTwoStartDate.getTime() >= firstDateInMonth(props.maxDate).getTime();
    }

    const leftArrowClass = classNames('date-picker__arrow date-picker__arrow-left', {
      'date-picker__arrow--hidden': hideLeft
    });

    const leftArrowOnClick = () => this.handleCalendarToggle(-1);
    const rightArrowOnClick = () => this.handleCalendarToggle(1);
    return (
      <div
        className="date-picker__dropdown"
        onMouseLeave={this.resetToLastNavigatedDate}
        ref={(wrapperElement) => { this.wrapperElement = wrapperElement; }}>
        <div className="date-picker__dropdown-wrapper">
          {props.children}
          <ArrowIcon
            className={leftArrowClass}
            disabled={hideLeft}
            aria-label={hideLeft ? props.minMonthMessage : props.ariaTogglePreviousMonth}
            circle
            direction="left"
            onClick={leftArrowOnClick}
          />
          <ArrowIcon
            className="date-picker__arrow date-picker__arrow-right"
            disabled={hideRight}
            aria-label={hideLeft ? props.maxMonthMessage : props.ariaToggleNextMonth}
            circle
            direction="right"
            onClick={rightArrowOnClick}
          />
          <div className="date-picker__calendar-container">{calendars}</div>
          {props.hasEventRange && (
            <div className="date-picker__legend">
              <CalendarStarIcon className="date-picker__legend-star">{CalendarStarIcon} /></CalendarStarIcon>
              <span className="date-picker__legend-text">{legendText}</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

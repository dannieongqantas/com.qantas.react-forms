import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePickerControlled from './DatePickerControlled';
import { plusDays, plusMonths, getDaysInMonth, transformDateToLongWords, currentDate } from '../utils/date';
import { reread } from '../utils/screenReading';
import breakpoints from '../grid/breakpoints';
import { getWindowWidth } from '../ReusableComponents/WindowResize';

import {
  CALENDAR_SELECT_ITEM,
  CALENDAR_VIEWING_MONTH_TOGGLE,
  CALENDAR_NAVIGATE_DAY,
  CALENDAR_WIDTH_CHANGE,
  CALENDAR_CLOSE,
  CALENDAR_OPEN
} from '../Constants/ActionTypes';

export function calculateMaxDate(date, daysLimit) {
  return plusDays(date, daysLimit - 1);
}

function initializeState(initialDate) {
  const state = { isOpened: false, screenRead: '', width: getWindowWidth() };
  const selectedDate = initialDate || currentDate();
  state.dateNavigated = selectedDate;
  state.calendarStartDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
  state.rangeEndDate = selectedDate;
  return state;
}

export default class DatePicker extends Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired,
    formLabel: PropTypes.string.isRequired,
    value: PropTypes.instanceOf(Date),
    initialValue: PropTypes.instanceOf(Date),
    minDate: PropTypes.instanceOf(Date),
    daysLimit: PropTypes.number,
    disabled: PropTypes.bool,
    isRangeEnabled: PropTypes.bool,
    hasEventRange: PropTypes.bool,
    rangeStartDate: PropTypes.instanceOf(Date),
    eventRangeStartDate: PropTypes.instanceOf(Date),
    eventRangeEndDate: PropTypes.instanceOf(Date),
    selectedDateArrowDirection: PropTypes.string,
    onChange: PropTypes.func,
    ariaLabel: PropTypes.string.isRequired,
    ariaToggleNextMonth: PropTypes.string.isRequired,
    ariaTogglePreviousMonth: PropTypes.string.isRequired,
    pickerType: PropTypes.string,
    scrollCalendarInViewOnOpen: PropTypes.oneOf(['smooth', 'instant']),
    onCalendarClose: PropTypes.func
  };

  static defaultProps = {
    daysLimit: 100,
    isRangeEnabled: false,
    languageCodeForDateFormat: 'en',
    selectedDateArrowDirection: 'right',
    minDate: currentDate(),
    rangeStartDate: currentDate(),
    pickerType: '',
    hasEventRange: false,
    eventRangeStartDate: currentDate(),
    eventRangeEndDate: currentDate()
  };

  constructor(props) {
    super(props);
    const { value, initialValue } = props;
    const state = initializeState(value || initialValue);
    if (!value) {
      state.value = initialValue || currentDate();
    }
    this.state = state;
  }

  onChange = (value) => {
    if (!this.props.value) {
      this.send(CALENDAR_SELECT_ITEM, { value });
    } else {
      this.send(CALENDAR_CLOSE);
    }
    this.props.onChange(value);
  }

  focus = () => {
    if (this.datePickerControlled) {
      this.datePickerControlled.focus();
    }
  }

  selectedDate = () => this.props.value || this.state.value;

  maxDate = () => calculateMaxDate(this.props.minDate, this.props.daysLimit);

  update = (action, data) => {
    const state = this.state;
    const props = this.props;
    switch (action) {
      case CALENDAR_SELECT_ITEM:
        return {
          ...state,
          isOpened: false,
          screenRead: '',
          value: data.value,
          dateNavigated: data.value
        };
      case CALENDAR_CLOSE:
        return {
          ...state,
          isOpened: false,
          screenRead: ''
        };
      case CALENDAR_OPEN: {
        if (!props.disabled) {
          const currentSelectedDate = this.selectedDate();
          let resetCalendarStartDate = new Date(currentSelectedDate.getFullYear(), currentSelectedDate.getMonth(), 1);

          if (props.isRangeEnabled
          && props.rangeStartDate.getMonth() !== currentSelectedDate.getMonth()
          && state.width >= breakpoints.medium)
            resetCalendarStartDate = plusMonths(resetCalendarStartDate, -1);

          return {
            ...state,
            isOpened: true,
            calendarStartDate: resetCalendarStartDate,
            rangeEndDate: currentSelectedDate,
            dateNavigated: currentSelectedDate
          };
        }
        return state;
      }
      case CALENDAR_VIEWING_MONTH_TOGGLE: {
        const newCalendarStartDate = plusMonths(state.calendarStartDate, data.toggleAmount);
        return {
          ...state,
          calendarStartDate: newCalendarStartDate
        };
      }
      case CALENDAR_WIDTH_CHANGE:
        return { ...state, width: data.width };
      case CALENDAR_NAVIGATE_DAY: {
        const dateNavigated = data.dateNavigated;

        if (!state.isOpened) {
          return state;
        } else if (props.minDate.getTime() > dateNavigated.getTime()) {
          const screenReaderText = reread(state.screenRead, data.minMessage);
          return { ...state, screenRead: screenReaderText };
        } else if (this.maxDate().getTime() < dateNavigated.getTime()) {
          const screenReaderText = reread(state.screenRead, data.maxMessage);
          return { ...state, screenRead: screenReaderText };
        }

        let calendarStartDate = state.calendarStartDate;
        let calendarEndDate = plusDays(calendarStartDate, getDaysInMonth(calendarStartDate.getFullYear(), calendarStartDate.getMonth()));

        if (state.width >= breakpoints.medium)
          calendarEndDate = plusMonths(calendarEndDate, 1);

        if (dateNavigated.getTime() >= calendarEndDate.getTime())
          calendarStartDate = plusMonths(calendarStartDate, 1);

        if (dateNavigated.getTime() < calendarStartDate.getTime())
          calendarStartDate = plusMonths(calendarStartDate, -1);

        return {
          ...state,
          dateNavigated,
          calendarStartDate,
          rangeEndDate: dateNavigated,
          screenRead: transformDateToLongWords(dateNavigated, props.i18n)
        };
      }
      default:
        return state;
    }
  }

  send = (action, data) => {
    const state = this.update(action, data);
    if (this.state !== state) {
      this.setState(state);
    }
  }

  render() {
    const { ...props } = this.props;
    return (
      <DatePickerControlled
        ref={(datePickerControlled) => { this.datePickerControlled = datePickerControlled; }}
        {...props}
        {...this.state}
        maxDate={this.maxDate()}
        selectedDate={this.selectedDate()}
        openCalendar={() => this.send(CALENDAR_OPEN)}
        closeCalendar={() => this.send(CALENDAR_CLOSE)}
        changeCalendarWidth={(width) => this.send(CALENDAR_WIDTH_CHANGE, { width })}
        toggleCalendarMonth={(toggleAmount) => this.send(CALENDAR_VIEWING_MONTH_TOGGLE, { toggleAmount })}
        navigateToDate={(dateNavigated, minMessage, maxMessage) => this.send(CALENDAR_NAVIGATE_DAY, { dateNavigated, minMessage, maxMessage })}
        selectDate={(value) => this.onChange(value)}
      />
    );
  }
}

import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';

import i18nData from '../../test/mock/i18n';

import createComponentStub from '../../test/utils/ComponentStub';

const proxyquire = require('proxyquire').noCallThru();
const CalendarRowStub = createComponentStub('tr');
const CalendarComponent = proxyquire('./Calendar', {
  './CalendarRow': CalendarRowStub
}).default;

describe('Calendar', () => {
  describe('component', () => {
    const props = {
      i18n: i18nData.shared,
      year: 2016,
      month: 0,
      id: 'pickupDate',
      selectedDate: new Date(2016, 0, 30),
      today: new Date(2016, 0, 12),
      minDate: new Date(2016, 0, 10),
      maxDate: new Date(2017, 0, 8),
      dateNavigated: new Date(2016, 0, 29),
      isRangeEnabled: true,
      rangeStartDate: new Date(2016, 0, 10),
      rangeEndDate: new Date(2016, 0, 29),
      selectedDateArrowDirection: 'right',
      dispatch: sinon.spy(),
      selectDate: sinon.spy()
    };


    describe('render', () => {
      const renderedComponent = TestUtils.renderIntoDocument(<CalendarComponent {...props} />);
      const tableRows = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'tr');

      it('should render properly', () => {
        const dom = findDOMNode(renderedComponent);
        expect(dom).to.exist;
        expect(tableRows.length).to.be.equal(8);
      });


      it('should render month year', () => {
        expect(tableRows[0].textContent).to.be.equal('january 2016');
      });

      it('should render week days', () => {
        const daysNodes = tableRows[1].childNodes;
        expect(daysNodes.length).to.be.equal(7);
        for (let i = 0; i < daysNodes.length; i++)
          expect(daysNodes[i].textContent).to.be.equal(props.i18n.weekdays[i + 1].short);
      });

      it('should render a row for each week', () => {
        const renderedCalendarRows = TestUtils.scryRenderedComponentsWithType(renderedComponent, CalendarRowStub);
        expect(renderedCalendarRows.length).to.be.equal(5);
        // verify static props
        for (let i = 0; i < renderedCalendarRows.length; i++) {
          const rowProps = renderedCalendarRows[i].props;
          expect(rowProps.today).to.be.equal(props.today);
          expect(rowProps.selectedDate).to.be.equal(props.selectedDate);
          expect(rowProps.id).to.be.equal(props.id);
          expect(rowProps.i18n).to.be.equal(props.i18n);
          expect(rowProps.maxDay).to.be.equal(31);
        }
        expect(renderedCalendarRows[0].props.startDate).to.deep.equal(new Date(2016, 0, 1));
        expect(renderedCalendarRows[1].props.startDate).to.deep.equal(new Date(2016, 0, 4));
        expect(renderedCalendarRows[2].props.startDate).to.deep.equal(new Date(2016, 0, 11));
        expect(renderedCalendarRows[3].props.startDate).to.deep.equal(new Date(2016, 0, 18));
        expect(renderedCalendarRows[4].props.startDate).to.deep.equal(new Date(2016, 0, 25));
      });
    });

    describe('render months with different number of weeks', () => {
      it('should render a month with 6 weeks correctly', () => {
        const may = 4;
        const renderedComponent = TestUtils.renderIntoDocument(<CalendarComponent {...props} month={may} />);
        const renderedCalendarRows = TestUtils.scryRenderedComponentsWithType(renderedComponent, CalendarRowStub);
        expect(renderedCalendarRows.length).to.be.equal(6);

        expect(renderedCalendarRows[0].props.startDate).to.deep.equal(new Date(2016, may, 1));
        expect(renderedCalendarRows[1].props.startDate).to.deep.equal(new Date(2016, may, 2));
        expect(renderedCalendarRows[2].props.startDate).to.deep.equal(new Date(2016, may, 9));
        expect(renderedCalendarRows[3].props.startDate).to.deep.equal(new Date(2016, may, 16));
        expect(renderedCalendarRows[4].props.startDate).to.deep.equal(new Date(2016, may, 23));
        expect(renderedCalendarRows[5].props.startDate).to.deep.equal(new Date(2016, may, 30));
      });

      it('should render a month with 4 weeks correctly', () => {
        const feb = 1;
        const renderedComponent = TestUtils.renderIntoDocument(<CalendarComponent {...props} month={feb}
                                                                                             year={2010} />);
        const renderedCalendarRows = TestUtils.scryRenderedComponentsWithType(renderedComponent, CalendarRowStub);
        expect(renderedCalendarRows.length).to.be.equal(4);

        expect(renderedCalendarRows[0].props.startDate).to.deep.equal(new Date(2010, feb, 1));
        expect(renderedCalendarRows[1].props.startDate).to.deep.equal(new Date(2010, feb, 8));
        expect(renderedCalendarRows[2].props.startDate).to.deep.equal(new Date(2010, feb, 15));
        expect(renderedCalendarRows[3].props.startDate).to.deep.equal(new Date(2010, feb, 22));
      });
    });
  });
});

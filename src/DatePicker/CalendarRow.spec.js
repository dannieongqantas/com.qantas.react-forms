import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';

import i18nData from '../../test/mock/i18n';

import CalendarRowComponent from './CalendarRow';

function renderComponent(props, newProps) {
  const propsAggregate = { ...props, ...newProps };
  return TestUtils.renderIntoDocument(
    <table>
      <tbody>
        <CalendarRowComponent {...propsAggregate} />
      </tbody>
    </table>
  );
}

const i18n = { ...i18nData.shared, ...i18nData.car };

describe('CalendarRow', () => {
  describe('component', () => {
    const props = {
      i18n,
      year: 2016,
      month: 0,
      id: 'pickupDate',
      startDate: new Date(2016, 0, 1),
      maxDay: 31,
      selectedDate: new Date(2016, 0, 30),
      dateNavigated: new Date(2016, 0, 29),
      minDate: new Date(2016, 0, 10),
      maxDate: new Date(2017, 0, 8),
      isRangeEnabled: true,
      rangeStartDate: new Date(2016, 0, 10),
      rangeEndDate: new Date(2016, 0, 29),
      selectedDateArrowDirection: 'right',
      descendantId: 'calendarRow',
      dispatch: sinon.spy(),
      selectDate: sinon.spy(),
      navigateToDate: sinon.spy(),
      hasEventRange: true,
      eventRangeStartDate: new Date(2016, 0, 11),
      eventRangeEndDate: new Date(2016, 0, 14)
    };

    describe('render', () => {
      it('should render properly', () => {
        const renderedComponent = renderComponent(props);
        const dom = findDOMNode(renderedComponent);
        expect(dom).to.exist;

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        expect(columns.length).to.be.equal(7);
      });

      it('should render a row not starting with monday', () => {
        const renderedComponent = renderComponent(props);
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        expect(columns[0].textContent).to.be.empty;
        expect(columns[1].textContent).to.be.empty;
        expect(columns[2].textContent).to.be.empty;
        expect(columns[3].textContent).to.be.empty;
        expect(columns[4].textContent).to.be.equal('1');
        expect(columns[5].textContent).to.be.equal('2');
        expect(columns[6].textContent).to.be.equal('3');
      });

      it('should render a row starting with monday', () => {
        const startDate = new Date(2016, 0, 4);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        expect(columns[0].textContent).to.be.equal('4');
        expect(columns[1].textContent).to.be.equal('5');
        expect(columns[2].textContent).to.be.equal('6');
        expect(columns[3].textContent).to.be.equal('7');
        expect(columns[4].textContent).to.be.equal('8');
        expect(columns[5].textContent).to.be.equal('9');
        expect(columns[6].textContent).to.be.equal('10');
      });

      it('should render a row starting with monday but has less than 7 days', () => {
        const startDate = new Date(2016, 1, 29);
        const renderedComponent = renderComponent(props, { startDate, maxDay: 29 });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        expect(columns[0].textContent).to.be.equal('29');
        expect(columns[1].textContent).to.be.empty;
        expect(columns[2].textContent).to.be.empty;
        expect(columns[3].textContent).to.be.empty;
        expect(columns[4].textContent).to.be.empty;
        expect(columns[5].textContent).to.be.empty;
        expect(columns[6].textContent).to.be.empty;
      });

      it('should render past days in current month disabled', () => {
        const startDate = new Date(2016, 0, 4);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        expect(columns[0].getAttribute('class')).to.contain('disabled');
        expect(columns[1].getAttribute('class')).to.contain('disabled');
        expect(columns[2].getAttribute('class')).to.contain('disabled');
        expect(columns[3].getAttribute('class')).to.contain('disabled');
        expect(columns[4].getAttribute('class')).to.contain('disabled');
        expect(columns[5].getAttribute('class')).to.contain('disabled');
        expect(columns[5].getAttribute('class')).to.contain('disabled');
        expect(columns[6].getAttribute('class')).to.contain('enabled');
      });

      it('should render days after days limit disabled in last month', () => {
        const startDate = new Date(2017, 0, 9);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        expect(columns[0].getAttribute('class')).to.contain('disabled');
        expect(columns[1].getAttribute('class')).to.contain('disabled');
        expect(columns[2].getAttribute('class')).to.contain('disabled');
        expect(columns[3].getAttribute('class')).to.contain('disabled');
        expect(columns[4].getAttribute('class')).to.contain('disabled');
        expect(columns[5].getAttribute('class')).to.contain('disabled');
        expect(columns[5].getAttribute('class')).to.contain('disabled');
        expect(columns[6].getAttribute('class')).to.contain('disabled');
      });

      it('should highlight selected day', () => {
        const startDate = new Date(2016, 0, 25);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        // selected date is 30 Jan 2016
        expect(columns[5].getAttribute('class')).to.contain('selected');

        const arrowDiv = findDOMNode(columns[5]).querySelectorAll('div')[0];
        expect(arrowDiv.getAttribute('class')).to.contain('pointing-right');
      });

      it('should highlight days falling in date range -1', () => {
        const startDate = new Date(2016, 0, 25);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        // 25-28 Jan 2016 as date range is from 10-29 Jan
        expect(columns[0].getAttribute('class')).to.contain('in-range');
        expect(columns[1].getAttribute('class')).to.contain('in-range');
        expect(columns[2].getAttribute('class')).to.contain('in-range');
        expect(columns[3].getAttribute('class')).to.contain('in-range');
      });

      it('should not highlight days not falling in date range', () => {
        const startDate = new Date(2016, 0, 25);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        // 31 Jan 2016 is outside of range
        expect(columns[6].getAttribute('class')).to.not.contain('in-range');
      });

      it('should highlight hovered/navigated day', () => {
        const startDate = new Date(2016, 0, 25);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');

        // date navigated is is 29 Jan 2016
        expect(columns[4].getAttribute('class')).to.contain('hovered');
      });

      it('should highlight first day of range by right pointing arrow', () => {
        const startDate = new Date(2016, 0, 4);
        const renderedComponent = renderComponent(props, { startDate });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        // rangeStartDate is 10 Jan
        const arrowDiv = findDOMNode(columns[6]).querySelectorAll('div')[0];
        expect(arrowDiv.getAttribute('class')).to.contain('pointing-right');
      });

      it('should render star on selected event date', () => {
        const startDate = new Date(2016, 0, 11);
        const renderedComponent = renderComponent(props, { startDate, maxDay: 29 });
        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        expect(columns[0].textContent).to.be.equal('11Star');
      });
    });

    describe('user actions', () => {
      it('click on past day should not trigger select date', () => {
        const renderedComponent = renderComponent(props);

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        TestUtils.Simulate.click(columns[5]);

        expect(props.selectDate).to.not.have.been.called;
      });

      it('click on day after max day should not trigger select date', () => {
        const startDate = new Date(2017, 0, 9);

        const renderedComponent = renderComponent(props, { startDate });

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        TestUtils.Simulate.click(columns[1]);

        expect(props.selectDate).to.not.have.been.called;
      });

      it('click on valid day should trigger select date', () => {
        const startDate = new Date(2016, 0, 4);

        const renderedComponent = renderComponent(props, { startDate });

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        // click on 10 Jan 2016
        TestUtils.Simulate.click(columns[6]);

        expect(props.selectDate).to.have.been.calledWith(new Date(2016, 0, 10));
      });

      it('should call onNavigate when hovered', () => {
        const startDate = new Date(2016, 0, 4);

        const renderedComponent = renderComponent(props, { startDate });

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        // mouse over on 10 Jan 2016
        TestUtils.Simulate.mouseOver(columns[6]);

        expect(props.navigateToDate).to.have.been.calledWith(new Date(2016, 0, 10));
      });

      it('should call onNavigate on touch start', () => {
        const startDate = new Date(2016, 0, 4);

        const renderedComponent = renderComponent(props, { startDate });

        const columns = findDOMNode(renderedComponent).querySelectorAll('td');
        // mouse over on 10 Jan 2016
        TestUtils.Simulate.touchStart(columns[6]);

        expect(props.navigateToDate).to.have.been.calledWith(new Date(2016, 0, 10));
      });
    });
  });
});

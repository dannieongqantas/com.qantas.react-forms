import TestUtils from 'react-dom/test-utils';
import React from 'react';
import i18nData from '../../test/mock/i18n';
const proxyquire = require('proxyquire').noCallThru();
import { findDOMNode } from 'react-dom';
import ComponentStub from '../../test/utils/ComponentStub';
import {
  RETURN_KEY_CODE,
  ARROW_LEFT_KEY_CODE,
  ARROW_UP_KEY_CODE,
  ARROW_RIGHT_KEY_CODE,
  ARROW_DOWN_KEY_CODE,
  ESCAPE_KEY_CODE,
  SPACE_KEY_CODE,
  N_KEY_CODE,
  P_KEY_CODE
} from '../Constants/KeyCodes';
import { plusDays } from '../utils/date';

const CalendarDropdown = ComponentStub();
const CalendarIcon = ComponentStub();

let isIE = false;
const { DatePickerControlled } = proxyquire('./DatePickerControlled', {
  './CalendarDropdown': CalendarDropdown,
  '../Icons/CalendarIcon': CalendarIcon,
  '../utils/userAgent': {
    isIE() {
      return isIE;
    },
    isEdge() {
      return false;
    }
  },
  '../utils/idGenerator': () => 'random'
});

const i18n = { ...i18nData.shared, ...i18nData.car };
const selectDateSpy = sinon.spy();
const blurSpy = sinon.spy();
const navigateToDateSpy = sinon.spy();
const closeCalendarSpy = sinon.spy();
const openCalendarSpy = sinon.spy();
const dateFormatterSpy = sinon.spy();
const sandbox = sinon.sandbox.create();

const props = {
  i18n,
  ...i18n.pickupDate,
  ariaLabel: i18n.aria.dateLabel,
  selectedDate: new Date(2016, 0, 10),
  dateNavigated: new Date(2016, 0, 30),
  minDate: new Date(2016, 0, 10),
  maxDate: new Date(2017, 0, 8),
  width: 1024,
  disabled: false,
  isOpened: true,
  isRangeEnabled: true,
  rangeStartDate: new Date(2016, 0, 10),
  rangeEndDate: new Date(2016, 0, 29),
  selectedDateArrowDirection: 'right',
  blur: blurSpy,
  selectDate: selectDateSpy,
  navigateToDate: navigateToDateSpy,
  closeCalendar: closeCalendarSpy,
  openCalendar: openCalendarSpy,
  dateFormatter: dateFormatterSpy,
  dateFormat: 'ddd D MMM YYYY'
};

function createComponent(newProps = {}) {
  return TestUtils.renderIntoDocument(<DatePickerControlled {...props} {...newProps} />);
}

describe('DatePickerControlled', () => {
  afterEach(() => {
    sandbox.reset();
  });

  describe('label', () => {
    it('should contain the correct label from i18n', () => {
      const renderedComponent = createComponent();
      const label = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'label');
      expect(label.textContent).to.equal(i18n.pickupDate.formLabel);
    });
  });

  describe('input field', () => {
    let renderedComponent;
    let input;
    const selectedDate = new Date(2016, 0, 10);

    before(() => {
      renderedComponent = createComponent({
        selectedDate
      });
      input = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    });

    it('should create an input field', () => {
      expect(input).to.exist;
    });

    it('should have the selectedDate as value', () => {
      expect(dateFormatterSpy).to.have.been.calledWith(selectedDate, props.dateFormat);
    });

    it('should open calendar when focused', () => {
      TestUtils.Simulate.focus(input);
      expect(openCalendarSpy).to.have.been.called;
    });

    it('should call the blur function when input is blurred', () => {
      TestUtils.Simulate.blur(input);
      expect(blurSpy).to.have.been.called;
    });

    it('should contain the readonly attribute', () => {
      expect(input.getAttribute('readonly')).to.exist;
    });

    it('should go to previous week on arrow up key', () => {
      TestUtils.Simulate.keyDown(input, { keyCode: ARROW_UP_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(plusDays(props.dateNavigated, -7));
    });

    it('should go to next week on arrow down key', () => {
      TestUtils.Simulate.keyDown(input, { keyCode: ARROW_DOWN_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(plusDays(props.dateNavigated, 7));
    });

    it('should go to previous day on arrow left key', () => {
      TestUtils.Simulate.keyDown(input, { keyCode: ARROW_LEFT_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(plusDays(props.dateNavigated, -1));
    });

    it('should go to next day on arrow right key', () => {
      TestUtils.Simulate.keyDown(input, { keyCode: ARROW_RIGHT_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(plusDays(props.dateNavigated, 1));
    });

    it(`should hanlde ${P_KEY_CODE} key`, () => {
      TestUtils.Simulate.keyDown(input, { keyCode: P_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(new Date(2015, 11, 1));
    });

    it(`should hanlde ${N_KEY_CODE} key`, () => {
      TestUtils.Simulate.keyDown(input, { keyCode: N_KEY_CODE });
      expect(navigateToDateSpy).to.have.been.calledWith(new Date(2016, 2, 1));
    });

    function openClose(keyCode) {
      it(`should handle ${keyCode} key`, () => {
        TestUtils.Simulate.keyDown(input, { keyCode });
        expect(selectDateSpy).to.have.been.calledWith(props.dateNavigated);
      });

      it(`${keyCode} should retain same state when calendar is closed`, () => {
        const component = createComponent({ isOpened: false });
        const inputField = TestUtils.findRenderedDOMComponentWithTag(component, 'input');

        TestUtils.Simulate.keyDown(inputField, { keyCode });

        expect(openCalendarSpy).to.have.been.called;
      });
    }

    openClose(RETURN_KEY_CODE);
    openClose(SPACE_KEY_CODE);

    it('should handle escape key press', () => {
      TestUtils.Simulate.keyDown(input, { keyCode: ESCAPE_KEY_CODE });
      expect(closeCalendarSpy).to.have.been.called;
    });
  });

  describe('toggleNextMonth', () => {
    it('should provide the correct date difference for forward months', () => {
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 1, 5), 1)).to.equal(24);
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 1, 6), 1)).to.equal(23);
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 3, 25), 1)).to.equal(6);
    });

    it('should provide the correct date difference for backward months', () => {
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 1, 5), -1)).to.equal(-35);
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 1, 5), -1, new Date(2015, 0, 27))).to.equal(-9);
      expect(DatePickerControlled.toggleMonthDiff(new Date(2015, 1, 5), -1, new Date(2015, 1, 2))).to.equal(-35); // this will get picked up by redux and rejected with a screen read
    });
  });

  describe('CalendarDropdown', () => {
    let renderedComponent;
    let calendarDropdownComponent;

    before(() => {
      renderedComponent = createComponent();
      calendarDropdownComponent = TestUtils.findRenderedComponentWithType(renderedComponent, CalendarDropdown);
    });

    it('should render the CalendarDropdown component', () => {
      expect(calendarDropdownComponent).to.exist;
    });

    it('should pass all props', () => {
      const expectedProps = {
        ...renderedComponent.props,
        descendantId: 'datepicker-input-random',
        disabled: false,
        isMobile: false,
        maxMonthMessage: i18n.pickupDate.maxMonthMessage,
        minMonthMessage: i18n.pickupDate.minMonthMessage
      };
      expect(calendarDropdownComponent.props).to.deep.equal(expectedProps);
    });
  });

  describe('ScreenReader', () => {
    let renderedComponent;

    it('should render activedescendants when using IE for jaws to work', () => {
      isIE = true;
      renderedComponent = createComponent();
      const activeDescendants = findDOMNode(renderedComponent).querySelectorAll('[role="application"]');
      expect(activeDescendants.length).to.equal(1);
    });

    it('should not render activedescendants when not using IE', () => {
      isIE = false;
      renderedComponent = createComponent();
      const activeDescendants = findDOMNode(renderedComponent).querySelectorAll('[role="application"]');
      expect(activeDescendants.length).to.equal(0);
    });
  });

  describe('disabled state', () => {
    let disabledRenderedComponent;
    let input;
    let calendarIcon;
    const selectedDate = new Date(2016, 0, 10);

    before(() => {
      disabledRenderedComponent = createComponent({
        selectedDate,
        disabled: true
      });
      input = TestUtils.findRenderedDOMComponentWithTag(disabledRenderedComponent, 'input');
      calendarIcon = TestUtils.findRenderedComponentWithType(disabledRenderedComponent, CalendarIcon);
    });

    it('should contain the disabled attribute', () => {
      expect(input.getAttribute('disabled')).to.exist;
    });

    it('should pass disabled class to the icon', () => {
      expect(calendarIcon.props.fillClass).to.equal('date-picker__calendar-icon-fill date-picker__calendar-icon__disabled');
    });

    it('should pass disabled class to the container', () => {
      expect(findDOMNode(disabledRenderedComponent).getAttribute('class')).to.equal('widget-form__group-container  date-picker__disabled');
    });
  });

  describe('focus', () => {
    let renderedComponent;
    let focusSpy;

    const selectedDate = new Date(2016, 0, 10);

    beforeEach(() => {
      renderedComponent = createComponent({
        selectedDate
      });

      focusSpy = sandbox.spy(renderedComponent.refs.input, 'focus');
    });

    it('should create an input field', () => {
      renderedComponent.focus();
      expect(focusSpy).to.have.been.called;
    });
  });

  describe('componentDidUpdate', () => {
    const onCalendarCloseStub = sinon.stub();
    let renderedComponent;

    afterEach(() => {
      sandbox.reset();
      onCalendarCloseStub.reset();
    });

    describe('when onCalendarClose prop is not provided', () => {
      beforeEach(() => {
        renderedComponent = createComponent({});
      });

      it('should not call onCalendarClose', () => {
        renderedComponent.componentDidUpdate({
          onCalendarClose: onCalendarCloseStub
        });
        expect(onCalendarCloseStub).to.not.have.been.called;
      });
    });

    describe('when calendar is closed', () => {
      beforeEach(() => {
        renderedComponent = createComponent({
          isOpened: false,
          onCalendarClose: onCalendarCloseStub
        });
      });

      it('should call onCalendarClose when previous state was open', () => {
        renderedComponent.componentDidUpdate({ isOpened: true });
        expect(onCalendarCloseStub).to.have.been.called;
      });

      it('should not call onCalendarClose when previous state was closed', () => {
        renderedComponent.componentDidUpdate({ isOpened: false });
        expect(onCalendarCloseStub).to.not.have.been.called;
      });
    });

    describe('when calendar is opened', () => {
      beforeEach(() => {
        renderedComponent = createComponent({
          isOpened: true,
          onCalendarClose: onCalendarCloseStub
        });
      });

      it('should not call onCalendarClose when previous state was open', () => {
        renderedComponent.componentDidUpdate({ isOpened: true });
        expect(onCalendarCloseStub).to.not.have.been.called;
      });

      it('should not call onCalendarClose when previous state was closed', () => {
        renderedComponent.componentDidUpdate({ isOpened: false });
        expect(onCalendarCloseStub).to.not.have.been.called;
      });
    });
  });
});

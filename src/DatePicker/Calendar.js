import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CalendarRow from './CalendarRow';
import { getDayBeginFromMonday, getDaysInMonth, plusDays } from '../utils/date';

const DAYS_IN_WEEK = 7;

export default class Calendar extends Component {

  static propTypes = {
    i18n: PropTypes.object.isRequired,
    year: PropTypes.number.isRequired,
    month: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    // below ones get passed to calendar row
    selectedDate: PropTypes.instanceOf(Date).isRequired,
    minDate: PropTypes.instanceOf(Date).isRequired,
    maxDate: PropTypes.instanceOf(Date).isRequired,
    isRangeEnabled: PropTypes.bool.isRequired,
    selectedDateArrowDirection: PropTypes.string.isRequired,
    descendantId: PropTypes.string.isRequired,
    selectDate: PropTypes.func.isRequired,
    navigateToDate: PropTypes.func.isRequired
  };

  static calculateNumberOfRows(daysInMonth, numberOfDaysInFirstRow) {
    // always adding one for first row
    return Math.ceil((daysInMonth - numberOfDaysInFirstRow) / DAYS_IN_WEEK) + 1;
  }

  createCalendarRows(props) {
    const daysInMonth = getDaysInMonth(props.year, props.month);
    let calendarRowStartDate = new Date(props.year, props.month, 1);
    const numberOfDaysInFirstRow = DAYS_IN_WEEK - getDayBeginFromMonday(calendarRowStartDate) + 1;
    const numberOfWeeks = Calendar.calculateNumberOfRows(daysInMonth, numberOfDaysInFirstRow);

    const rows = [];
    // first calendar row
    rows.push(
      <CalendarRow {...props} key={0} startDate={calendarRowStartDate} maxDay={daysInMonth} />
    );

    // second row onwards so start with i = 1
    calendarRowStartDate = plusDays(calendarRowStartDate, numberOfDaysInFirstRow);
    for (let i = 1; i < numberOfWeeks; i++) {
      rows.push(
        <CalendarRow {...props} key={i} startDate={calendarRowStartDate} maxDay={daysInMonth} />
      );
      calendarRowStartDate = plusDays(calendarRowStartDate, DAYS_IN_WEEK);
    }
    return rows;
  }

  render() {
    const { ...props } = this.props;
    const rows = this.createCalendarRows(props);

    return <div className="date-picker__calendar">
      <table className="date-picker__calendar-table">
        <tbody>
          <tr className="date-picker__calendar-heading">
            <th colSpan="7">{`${props.i18n.months[props.month + 1].long} ${props.year}`}</th>
          </tr>
          <tr className="date-picker__calendar-weekday-headings">
            {[1, 2, 3, 4, 5, 6, 7].map((day) => <th key={day}>
              <div>{props.i18n.weekdays[day].short}</div>
            </th>)}
          </tr>
          <tr aria-hidden="true">
            <td className="date-picker__calendar-heading-padding"></td>
          </tr>
          {rows}
        </tbody>
      </table>
    </div>;
  }
}

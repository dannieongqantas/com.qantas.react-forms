import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getDayBeginFromMonday, plusDays, transformDateToLongWords } from '../utils/date';
import CalendarStarIcon from '../Icons/CalendarStarIcon';
import classNames from 'classnames';

const DAYS_IN_WEEK = 7;

export default class CalendarRow extends Component {
  static appliedClasses(isSelected, isDisabled, isHovered, isInRange) {
    return classNames('date-picker__calendar-weekdays-items', {
      'date-picker__calendar-weekdays-items--selected': isSelected,
      'date-picker__calendar-weekdays-items--disabled': isDisabled,
      'date-picker__calendar-weekdays-items--enabled': !isDisabled,
      'date-picker__calendar-weekdays-items--hovered': isHovered,
      'date-picker__calendar-weekdays-items--in-range': isInRange
    });
  }

  static isDisabled(date, minDate, maxDate) {
    return minDate.getTime() > date.getTime() || maxDate.getTime() < date.getTime();
  }

  static propTypes = {
    startDate: PropTypes.instanceOf(Date).isRequired,
    maxDay: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    selectedDate: PropTypes.instanceOf(Date).isRequired,
    dateNavigated: PropTypes.instanceOf(Date).isRequired,
    minDate: PropTypes.instanceOf(Date).isRequired,
    maxDate: PropTypes.instanceOf(Date).isRequired,
    isRangeEnabled: PropTypes.bool.isRequired,
    hasEventRange: PropTypes.bool.isRequired,
    rangeStartDate: PropTypes.instanceOf(Date).isRequired,
    rangeEndDate: PropTypes.instanceOf(Date).isRequired,
    eventRangeStartDate: PropTypes.instanceOf(Date),
    eventRangeEndDate: PropTypes.instanceOf(Date),
    selectedDateArrowDirection: PropTypes.string.isRequired,
    descendantId: PropTypes.string.isRequired,
    selectDate: PropTypes.func.isRequired,
    navigateToDate: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
    this.handleHover = this.handleHover.bind(this);
  }

  clickHandler(date) {
    this.props.selectDate(date);
  }

  handleHover(date) {
    this.props.navigateToDate(date);
  }

  validDaysForRow(props) {
    const diff = (props.maxDay - props.startDate.getDate());
    return diff >= DAYS_IN_WEEK ? DAYS_IN_WEEK : diff + 1;
  }

  createEmptyElement(key) {
    return <td key={key}></td>;
  }

  createDayElement(date, props, key) {
    const isDisabled = CalendarRow.isDisabled(date, props.minDate, props.maxDate);
    const isSelected = props.selectedDate && props.selectedDate.getTime() === date.getTime();
    const isHovered = date.getTime() === props.dateNavigated.getTime();
    const isInRange = props.isRangeEnabled && props.rangeStartDate.getTime() <= date.getTime() && date.getTime() < props.rangeEndDate.getTime();
    const hasEventRange = !isDisabled && props.hasEventRange && props.eventRangeStartDate.getTime() <= date.getTime() && date.getTime() <= props.eventRangeEndDate.getTime();
    const className = CalendarRow.appliedClasses(isSelected, isDisabled, isHovered, isInRange);

    const newProps = {
      className,
      key
    };

    if (!isDisabled) {
      newProps.onClick = (e) => {
        e.currentTarget.blur();
        this.clickHandler(date);
      };

      newProps.onMouseOver = () => this.handleHover(date);
      newProps.onTouchStart = () => this.handleHover(date);
      newProps['aria-label'] = transformDateToLongWords(date, props.i18n);
      newProps.role = 'option'; // Need role option for iOS voiceover
      newProps.tabIndex = -1;
    }

    const triangle = isSelected ? <div
      className={`date-picker__calendar-weekdays-items-triangle-pointing-${props.selectedDateArrowDirection}`}></div> : null;

    const star = hasEventRange && <CalendarStarIcon className="date-picker__calendar-star" />;

    const rangeStartTriangle = props.isRangeEnabled && props.rangeStartDate.getTime() === date.getTime() ? <div
      className={'date-picker__calendar-weekdays-items-triangle-pointing-right'}></div> : null;


    return <td {...newProps}>
      {triangle}
      {rangeStartTriangle}
      <div className="date-picker__calendar-weekdays-items-content">
        <span className="date-picker__calendar-weekdays-items-text">
          {date.getDate()}
        </span>
      </div>
        {star}
    </td>;
  }

  render() {
    const { ...props } = this.props;
    const startDay = getDayBeginFromMonday(props.startDate) - 1;
    const daysInRow = [];

    for (let i = 0; i < startDay; i++)
      daysInRow.push(this.createEmptyElement(i));

    const validDays = this.validDaysForRow(props);
    let date = props.startDate;
    for (let i = startDay; i < validDays; i++) {
      daysInRow.push(this.createDayElement(date, props, i));
      date = plusDays(date, 1);
    }

    // if there are any columns still left
    for (let i = validDays; i < DAYS_IN_WEEK; i++)
      daysInRow.push(this.createEmptyElement(i));

    return <tr>
      {daysInRow}
    </tr>;
  }
}


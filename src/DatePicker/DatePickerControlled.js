import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CalendarIcon from '../Icons/CalendarIcon';
import CalendarDropdown from './CalendarDropdown';
import ComponentBlur from '../ReusableComponents/ComponentBlur';
import WindowResize from '../ReusableComponents/WindowResize';
import DateFormatterWrapper from '../ReusableComponents/DateFormatter';
import {
  transformDateToLongWords,
  plusDays,
  plusMonths,
  firstDateInMonth,
  dateDiff,
  maxDate
} from '../utils/date';
import BorderTriangle from '../Icons/BorderTriangle';
import classNames from 'classnames';
import ScreenReader from '../ScreenReader/ScreenReader';
import InputArea from '../Input/InputArea';
import breakpoints from '../grid/breakpoints';
import { isIE, isEdge } from '../utils/userAgent';
import { stringBind } from '../utils/stringBind';
import idGenerator from '../utils/idGenerator';

import {
  RETURN_KEY_CODE,
  ARROW_LEFT_KEY_CODE,
  ARROW_UP_KEY_CODE,
  ARROW_RIGHT_KEY_CODE,
  ARROW_DOWN_KEY_CODE,
  ESCAPE_KEY_CODE,
  SPACE_KEY_CODE,
  N_KEY_CODE,
  P_KEY_CODE
} from '../Constants/KeyCodes';

const COMPONENT_REF = 'DatePickerControlled';

const Triangle = ({ opened }) => {
  const triangleClass = classNames('date-picker__triangle-container-father', {
    'widget-form__element--hidden': !opened
  });
  return <div className={triangleClass}>
    <div className="date-picker__triangle-container">
      <BorderTriangle />
    </div>
  </div>;
};

Triangle.propTypes = {
  opened: PropTypes.bool.isRequired
};

export class DatePickerControlled extends Component {
  static toggleMonthDiff(dateNavigated, direction, minDate) {
    let newDate = firstDateInMonth(plusMonths(dateNavigated, direction));

    if (minDate && dateNavigated.getMonth() !== minDate.getMonth()) {
      newDate = maxDate(newDate, minDate);
    }

    return direction * (dateDiff(newDate, dateNavigated) || direction);
  }

  static defaultProps = {
    className: '',
    disabled: false,
    dateFormat: 'ddd D MMM YYYY',
    readOnly: false
  };

  static propTypes = {
    i18n: PropTypes.object.isRequired,
    dateFormat: PropTypes.string,
    languageCodeForDateFormat: PropTypes.string.isRequired,
    formLabel: PropTypes.string.isRequired,
    selectedDate: PropTypes.instanceOf(Date),
    dateNavigated: PropTypes.instanceOf(Date),
    isOpened: PropTypes.bool.isRequired,
    width: PropTypes.number.isRequired,
    minDate: PropTypes.instanceOf(Date).isRequired,
    maxDate: PropTypes.instanceOf(Date).isRequired,
    isRangeEnabled: PropTypes.bool.isRequired,
    disabled: PropTypes.bool,
    rangeStartDate: PropTypes.instanceOf(Date).isRequired,
    rangeEndDate: PropTypes.instanceOf(Date).isRequired,
    selectedDateArrowDirection: PropTypes.string.isRequired,
    selectDate: PropTypes.func.isRequired,
    pickerType: PropTypes.string,
    navigateToDate: PropTypes.func.isRequired,
    closeCalendar: PropTypes.func.isRequired,
    openCalendar: PropTypes.func.isRequired,
    changeCalendarWidth: PropTypes.func.isRequired,
    toggleCalendarMonth: PropTypes.func.isRequired,
    blur: PropTypes.func.isRequired,
    id: PropTypes.string,
    ariaLabel: PropTypes.string.isRequired,
    screenRead: PropTypes.string,
    className: PropTypes.string,
    ariaToggleNextMonth: PropTypes.string.isRequired,
    ariaTogglePreviousMonth: PropTypes.string.isRequired,
    dateFormatter: PropTypes.func.isRequired,
    onCalendarClose: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.isIE = isIE() || isEdge();
    this.descendant = 0;
    this.id = props.id || idGenerator();
  }

  componentWillUpdate(newProps) {
    if (this.isIE) {
      if (this.refs.screen1) this.refs.screen1.innerHTML = newProps.screenRead;
      if (this.refs.screen2) this.refs.screen2.innerHTML = newProps.screenRead;
    }
  }

  componentDidUpdate(prevProps) {
    const { onCalendarClose, isOpened } = this.props;

    if (!!onCalendarClose && prevProps.isOpened === true && isOpened === false) {
      onCalendarClose();
    }
  }

  focus = () => {
    if (this.refs.input) {
      this.refs.input.focus();
    }
  }

  handleIconClick = () => {
    if (this.props.isOpened) {
      this.refs.input.blur();
      this.closeCalendar();
    } else {
      this.refs.input.focus();
    }
  }

  handleInputClick = () => {
    this.refs.input.focus();
    this.handleInputFocus();
  }

  handleInputFocus = () => {
    this.props.openCalendar();
  }

  closeCalendar = () => {
    this.props.closeCalendar();
  }

  keyDownHandler = (e) => {
    const { dateNavigated, minDate, ...props } = this.props;
    let dateDifference;
    switch (e.keyCode) {
      case P_KEY_CODE:
        e.preventDefault();
        dateDifference = DatePickerControlled.toggleMonthDiff(dateNavigated, -1, minDate);
        this.navigateToDate(dateDifference, props.minMonthMessage, props.maxMonthMessage); // make sure its not 0 so screen reader reads it out
        break;
      case N_KEY_CODE:
        e.preventDefault();
        dateDifference = DatePickerControlled.toggleMonthDiff(dateNavigated, 1);
        this.navigateToDate(dateDifference || 1, props.minMonthMessage, props.maxMonthMessage); // make sure its not 0 so screen reader reads it out
        break;
      case SPACE_KEY_CODE:
        if (this.props.pickerType !== 'hasChild') {
          this.selectDate(e);
        }
        break;
      case RETURN_KEY_CODE:
        this.selectDate(e);
        break;
      case ESCAPE_KEY_CODE:
        e.preventDefault();
        this.closeCalendar();
        break;
      case ARROW_LEFT_KEY_CODE:
        e.preventDefault();
        this.navigateToDate(-1, props.minDateMessage, props.maxDateMessage);
        break;
      case ARROW_RIGHT_KEY_CODE:
        e.preventDefault();
        this.navigateToDate(1, props.minDateMessage, props.maxDateMessage);
        break;
      case ARROW_UP_KEY_CODE:
        e.preventDefault();
        this.navigateToDate(-7, props.minWeekMessage, props.maxWeekMessage);
        break;
      case ARROW_DOWN_KEY_CODE:
        e.preventDefault();
        this.navigateToDate(7, props.minWeekMessage, props.maxWeekMessage);
        break;
      default:
        break;
    }
  }

  navigateToDate(days, minMessage, maxMessage) {
    const dateNavigated = plusDays(this.props.dateNavigated, days);
    this.props.navigateToDate(dateNavigated, minMessage, maxMessage);
  }

  selectDate(evt) {
    evt.preventDefault();
    if (!this.props.isOpened) {
      this.handleInputFocus();
    } else {
      this.props.selectDate(this.props.dateNavigated);
    }
  }

  renderIEScreenReader(inputId) {
    if (!this.isIE) return null;

    return <div role="application" className="widget-form__element--aria-hidden">
      <div role="option" ref="screen1" id={inputId + 0}></div>
      <div role="option" ref="screen2" id={inputId + 1}></div>
    </div>;
  }

  render() {
    const { ...props } = this.props;

    const inputId = `datepicker-input-${this.id}`;

    // Toggles between active descendants as a hack for aria-live on IE
    const ariaPropsForIE = {
      activeDescendant: props.screenRead && this.isIE ? inputId + this.descendant++ % 2 : '' // Toggles between inputId0 or inputId1
    };

    const ariaLabel = stringBind(props.ariaLabel, {
      dateSelected: transformDateToLongWords(props.selectedDate, props.i18n),
      maxDays: props.daysLimit
    });

    const isDisabledClass = props.disabled ? `${props.className} date-picker__disabled` : props.className;
    const calendarIconClass = props.disabled ? 'date-picker__calendar-icon-fill date-picker__calendar-icon__disabled' : 'date-picker__calendar-icon-fill';
    const classname = classNames('widget-form__group-container', isDisabledClass);

    return (
      <div className={classname} onKeyDown={this.keyDownHandler}>
        <div ref={(el) => { this[COMPONENT_REF] = el; }}>

          <InputArea disabled={props.disabled} className="date-picker__input"
                     id={inputId}
                     ref="input"
                     onFocus={this.handleInputFocus}
                     onClick={this.handleInputClick}
                     value={props.dateFormatter(props.selectedDate, props.dateFormat)}
                     noEntry
                     onBlur={props.blur}
                     formLabel={props.formLabel}
                     ariaLabel={ariaLabel}
                     popup={props.isOpened} {...ariaPropsForIE}
                     readOnly={props.readOnly}>

            <CalendarIcon className="date-picker__calendar-icon" fillClass={calendarIconClass}
                          onClick={this.handleIconClick} />

            {this.renderIEScreenReader(inputId)}
            <ScreenReader ariaAtomic ariaRole="live" ariaRelevant="text" ariaLabel={props.screenRead} />

            <Triangle opened={props.isOpened} />
          </InputArea>

          <CalendarDropdown isMobile={props.width < breakpoints.medium} {...props}
                            descendantId={inputId}
          />
        </div>
      </div>
    );
  }
}

const ComponentWithBlur = ComponentBlur(DatePickerControlled, COMPONENT_REF, (props) => props.closeCalendar);

const ComponentWithDateFormatter = DateFormatterWrapper(ComponentWithBlur, (props) => props.languageCodeForDateFormat);

export default WindowResize(ComponentWithDateFormatter, breakpoints.medium, (props, windowWidth) => props.changeCalendarWidth(windowWidth));

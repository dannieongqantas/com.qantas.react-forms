import React from 'react';
import ReactDOM from 'react-dom';
import ComponentStub from '../../test/utils/ComponentStub';
import TestUtils from 'react-dom/test-utils';
const proxyquire = require('proxyquire').noCallThru();

const ButtonStub = ComponentStub();

const Collapsible = proxyquire('./Collapsible', {
  '../Button/Button': ButtonStub
}).default;

describe('Collapsible', () => {
  const contentClass = 'qfa1-collapsible__content-container';
  const expandedClass = 'qfa1-collapsible__container--expanded';
  const collapsedClass = 'qfa1-collapsible__container--collapsed';
  let renderedComponent;
  let children;
  let onToggle;

  describe('mounting component with children', () => {
    before(() => {
      onToggle = sinon.spy();
      children = <div className="child-component">Test</div>;
      renderedComponent = TestUtils.renderIntoDocument(<Collapsible onToggle={onToggle}>{children}</Collapsible>);
    });

    afterEach(() => {
      onToggle.reset();
    });

    it('should render the children inside the content Div', () => {
      const contentDiv = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, contentClass);
      const childComponent = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, 'child-component');
      expect(contentDiv.innerHTML).to.eq(childComponent.outerHTML);
    });

    it('should display the fake button', () => {
      TestUtils.findRenderedComponentWithType(renderedComponent, ButtonStub);
    });

    it('should set the state isExpanded to false', () => {
      expect(renderedComponent.state.isExpanded).to.eq(false);
    });

    it('should render the collapsed class', () => {
      expect(ReactDOM.findDOMNode(renderedComponent).className).to.contain(collapsedClass);
    });

    it('should not render the expanded class', () => {
      expect(ReactDOM.findDOMNode(renderedComponent).className).to.not.contain(expandedClass);
    });

    describe('dispatch events', () => {
      afterEach(() => {
        renderedComponent.setState({ isExpanded: false });
      });

      context('when emitting the formModalExpanded event', () => {
        let open;
        before(() => {
          open = new Event('formModalExpanded');
          document.addEventListener('formModalExpanded', () => {}, false);
        });

        after(() => {
          document.removeEventListener('formModalExpanded', () => {}, false);
        });

        it('should set the state isExpanded to true', () => {
          expect(renderedComponent.state.isExpanded).to.eq(false);
          document.dispatchEvent(open);
          expect(renderedComponent.state.isExpanded).to.eq(true);
        });

        it('should call the onToggle prop passing the value true', () => {
          expect(onToggle).to.not.have.been.called;
          document.dispatchEvent(open);
          expect(onToggle).to.have.been.calledWith(true);
        });

        it('should not render the button', () => {
          document.dispatchEvent(open);
          const button = TestUtils.scryRenderedComponentsWithType(renderedComponent, ButtonStub);
          expect(button.length).to.eq(0);
        });

        it('should not render the collapsed class', () => {
          document.dispatchEvent(open);
          expect(ReactDOM.findDOMNode(renderedComponent).className).to.not.contain(collapsedClass);
        });

        it('should render the expanded class', () => {
          document.dispatchEvent(open);
          expect(ReactDOM.findDOMNode(renderedComponent).className).to.contain(expandedClass);
        });
      });

      context('when emitting the formModalCollapsed event', () => {
        let close;
        before(() => {
          close = new Event('formModalCollapsed');
          document.addEventListener('formModalCollapsed', () => {}, false);
        });

        beforeEach(() => {
          renderedComponent.setState({ isExpanded: true });
        });

        after(() => {
          document.removeEventListener('formModalCollapsed', () => {}, false);
        });

        it('should set the state isExpanded to false', () => {
          document.dispatchEvent(close);
          expect(renderedComponent.state.isExpanded).to.eq(false);
        });

        it('should call the onToggle prop passing the value false', () => {
          expect(onToggle).to.not.have.been.called;
          document.dispatchEvent(close);
          expect(onToggle).to.have.been.calledWith(false);
        });

        it('should not render the button, then render the button', () => {
          let button = TestUtils.scryRenderedComponentsWithType(renderedComponent, ButtonStub);
          expect(button.length).to.eq(0);
          document.dispatchEvent(close);
          button = TestUtils.scryRenderedComponentsWithType(renderedComponent, ButtonStub);
          expect(button.length).to.eq(1);
        });

        it('should render the collapsed class', () => {
          document.dispatchEvent(close);
          expect(ReactDOM.findDOMNode(renderedComponent).className).to.contain(collapsedClass);
        });

        it('should not render the expanded class', () => {
          document.dispatchEvent(close);
          expect(ReactDOM.findDOMNode(renderedComponent).className).to.not.contain(expandedClass);
        });
      });
    });
  });

  context('when passing a custom class', () => {
    before(() => {
      children = <div className="child-component">Test</div>;
      renderedComponent = TestUtils.renderIntoDocument(<Collapsible className="testClass">{children}</Collapsible>);
    });

    it('should render the testClass className', () => {
      expect(ReactDOM.findDOMNode(renderedComponent).className).to.contain('testClass');
    });
  });
});

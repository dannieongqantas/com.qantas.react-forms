import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Button from '../Button/Button';

export default class Collapsible extends Component {
  static propTypes = {
    children: PropTypes.array,
    className: PropTypes.string,
    onToggle: PropTypes.func
  };

  static defaultProps = {
    className: '',
    onToggle: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false
    };
  }

  componentDidMount() {
    document.addEventListener('formModalExpanded', () => this.setExpandSate(true));
    document.addEventListener('formModalCollapsed', () => this.setExpandSate(false));
  }

  setExpandSate(isExpanded = false) {
    this.props.onToggle(isExpanded);
    this.setState({ isExpanded });
  }

  render() {
    const props = this.props;
    const { isExpanded } = this.state;

    const containerClasses = classnames('qfa1-collapsible__container', {
      'qfa1-collapsible__container--expanded': isExpanded,
      'qfa1-collapsible__container--collapsed': !isExpanded
    }, props.className);

    return (
      <div className={containerClasses}>
        <div className="qfa1-collapsible__content-container">{props.children}</div>
        {!isExpanded && <div className="qfa1-collapsible__button-container">
          <div className="widget-form__group-container">
            <Button type="button" className="qfa1-collapsible__button" />
          </div>
        </div>}
      </div>
    );
  }
}

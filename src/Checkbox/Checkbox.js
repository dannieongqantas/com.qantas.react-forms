import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { SPACE_KEY_CODE } from '../Constants/KeyCodes';
import CheckboxIcon from '../Icons/CheckboxIcon';
import ScreenReader from '../ScreenReader/ScreenReader';
import { ariaAsync } from '../utils/async';
import idGenerator from '../utils/idGenerator';

export default class Checkbox extends Component {
  static propTypes = {
    formLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
    checked: PropTypes.bool.isRequired,
    ariaLabel: PropTypes.string,
    screenRead: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.string,
    onChange: PropTypes.func.isRequired
  };

  static defaultProps = {
    className: 'widget-form__group-container',
    centered: false,
    ariaLabel: ''
  };

  constructor(props) {
    super(props);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);

    this.id = props.id || `checkbox-${idGenerator()}`;

    this.state = {
      focused: false
    };
  }

  handleFocus() {
    this.setState({ focused: true });
  }

  handleBlur() {
    ariaAsync(() => this.setState({ focused: false }));
  }

  handleKeyDown(e) {
    if (e.keyCode === SPACE_KEY_CODE) this.props.onChange();
  }

  render() {
    const { ...props } = this.props;

    const containerClass = classNames('qfa1-checkbox__container', {
      'qfa1-checkbox__container--focused': this.state.focused
    });

    return <div className={props.className}>
      <div className="widget-form__group">
        <div className={containerClass} >
          <input checked={props.checked}
                 id={this.id}
                 onChange={props.onChange}
                 type="checkbox"
                 aria-label={props.ariaLabel || props.formLabel}
                 className="qfa1-checkbox__checkbox"
                 role="checkbox"
                 aria-checked={props.checked}
                 onFocus={this.handleFocus}
                 onBlur={this.handleBlur} />
          <label onClick={this.handleClick} htmlFor={this.id} className="qfa1-checkbox__label">
            <CheckboxIcon checked={props.checked} focused={this.state.focused} />
            <span className="qfa1-checkbox__label-text">{props.formLabel}</span>
          </label>
          <ScreenReader text={props.screenRead} />
        </div>
      </div>
    </div>;
  }
}

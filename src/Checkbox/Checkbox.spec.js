import React from 'react';
import TestUtils from 'react-dom/test-utils';
const proxyquire = require('proxyquire').noCallThru();

import { SPACE_KEY_CODE, TAB_KEY_CODE } from '../Constants/KeyCodes';
import ComponentStub from '../../test/utils/ComponentStub';
const id = 'checkbox-id';

const ScreenReaderStub = ComponentStub();
const Checkbox = proxyquire('./Checkbox', {
  '../ScreenReader/ScreenReader': ScreenReaderStub
}).default;

const ChildComponent = ComponentStub();

function createComponent(newProps = {}) {
  const props = {
    checked: false,
    onChange: sinon.stub(),
    targetId: 'checkbox',
    formLabel: 'test label',
    ariaLabel: 'readCheckbox',
    ariaLiveChecked: 'You have checked this',
    ariaLiveUnchecked: 'You have no checked this',
    id,
    ...newProps };

  return TestUtils.renderIntoDocument(
    <Checkbox {...props}>
      <ChildComponent />
    </Checkbox>
  );
}

describe('Checkbox', () => {
  it('should be checked', () => {
    const renderedComponent = createComponent({ checked: true });
    const inputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    expect(inputElement.checked).to.be.true;
    expect(inputElement.getAttribute('aria-checked')).to.equal('true');
  });

  it('should handleFocus', () => {
    const renderedComponent = createComponent();
    renderedComponent.handleFocus();
    expect(renderedComponent.state.focused).to.equal(true);
  });

  it('should handleBlur', () => {
    const renderedComponent = createComponent();
    renderedComponent.handleBlur();
    expect(renderedComponent.state.focused).to.equal(false);
  });

  it('should call onChange when Space key is triggered', () => {
    const eventMock = { keyCode: SPACE_KEY_CODE };
    const onChangeSpy = sinon.spy();
    const renderedComponent = createComponent({ onChange: onChangeSpy });
    renderedComponent.handleKeyDown(eventMock);
    expect(onChangeSpy).to.have.been.calledOnce;
  });

  it('shouldn\'t call onChange when TAB or Other key is triggered', () => {
    const eventMock = { keyCode: TAB_KEY_CODE };
    const onChangeSpy = sinon.spy();
    const renderedComponent = createComponent({ onChange: onChangeSpy });
    renderedComponent.handleKeyDown(eventMock);
    expect(onChangeSpy).to.have.callCount(0);
  });

  it('should use formLabel as type string', () => {
    const renderedComponent = createComponent({ formLabel: 'Remember me' });
    expect(renderedComponent.props.formLabel).to.equal('Remember me');
  });

  it('should use formLabel as type element', () => {
    const element = <span>{'Remember me'}</span>;
    const renderedComponent = createComponent({ formLabel: element });
    expect(renderedComponent.props.formLabel).to.equal(element);
    expect(renderedComponent.props.formLabel.type).to.equal('span');
  });

  it('should use formLabel as ariaLabel when ariaLabel is not provided', () => {
    const renderedComponent = createComponent({ formLabel: 'yes!', ariaLabel: '' });
    const inputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    expect(inputElement.getAttribute('aria-label')).to.equal('yes!');
  });

  it('should contain the text from formLabel prop', () => {
    const formLabel = 'hello';
    const renderedComponent = createComponent({ formLabel });
    const childComponent = TestUtils.findRenderedDOMComponentWithClass(renderedComponent, 'qfa1-checkbox__label-text');
    expect(childComponent.textContent).to.equal(formLabel);
  });

  it('should pass the screenRead prop to the ScreenReader component', () => {
    const screenRead = 'hello you';
    const renderedComponent = createComponent({ screenRead });
    const screenReaderStub = TestUtils.findRenderedComponentWithType(renderedComponent, ScreenReaderStub);
    expect(screenReaderStub.props.text).to.equal(screenRead);
  });
});

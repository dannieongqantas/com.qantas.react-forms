import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Triangle from '../Icons/BorderTriangle';

export default class ValidationMessage extends Component {
  static propTypes = {
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    hide: PropTypes.bool,
    hideTriangle: PropTypes.bool
  };

  static defaultProps = {
    hide: false,
    hideTriangle: false
  };

  render() {
    const { message, hide, hideTriangle } = this.props;

    const messageClass = classNames('form-validation-message', {
      'widget-form__element--hidden': hide || !message
    });

    return <div className={messageClass}>
      <div className="form-validation-message__container">
        {!hideTriangle && <Triangle innerColor="#fcebcd" outerColor="#fcebcd" positionFromLeft="20px" innerSize="10px" outerSize="10px" zIndex="0" />}
        <div className="form-validation-message__text">
          {this.props.message}
        </div>
      </div>
    </div>;
  }
}

import React from 'react';
import TestUtils from 'react-dom/test-utils';
import FormValidationMessage from './FormValidationMessage';
import { findDOMNode } from 'react-dom';

function createComponent(newProps = {}) {
  const props = {
    message: 'hello world',
    show: false,
    ...newProps };

  return TestUtils.renderIntoDocument(
    <FormValidationMessage {...props} />
  );
}

describe('FormValidationMessage', () => {
  it('should not render component the show flag is false', () => {
    const renderedComponent = createComponent({ hide: true });
    expect(findDOMNode(renderedComponent).getAttribute('class')).to.contain('hidden');
  });

  it('should render the provided message', () => {
    const message = 'light sabers are awesome!';
    const renderedComponent = createComponent({ hide: false, message });
    expect(findDOMNode(renderedComponent).textContent).to.equal(message);
  });

  it('should not render Triangle child component when hideTriangle is true', () => {
    const renderedComponent = createComponent({ hideTriangle: true });
    const triangleComponent = TestUtils.scryRenderedDOMComponentsWithClass(renderedComponent, 'qfa1-border-triangle__outer');
    expect(triangleComponent.length).to.equal(0);
  });
});

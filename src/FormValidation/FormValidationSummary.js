import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ExclamationIcon from '../Icons/ExclamationIcon';

const ErrorItem = ({ error }) => <li className="form-validation-summary__item" aria-hidden="true">{error}</li>;

ErrorItem.propTypes = { error: PropTypes.oneOfType([PropTypes.string, PropTypes.element]) };

export default class FormValidationSummary extends Component {
  static propTypes = {
    validations: PropTypes.array.isRequired,
    summaryText: PropTypes.string.isRequired,
    summaryFocus: PropTypes.bool,
    showForSrOnly: PropTypes.bool,
    onFocus: PropTypes.func
  };

  static defaultProps = {
    showForSrOnly: false,
    summaryFocus: false,
    onFocus: () => {}
  };

  componentDidMount() {
    this.refs.summary.setAttribute('focusable', true);
  }

  componentDidUpdate() {
    if (this.props.summaryFocus) {
      this.refs.summary.focus();
      this.props.onFocus();
    }
  }

  render() {
    const { showForSrOnly, validations, summaryText } = this.props;

    const errors = validations
      .filter(message => !!message)
      .map(message => message);

    const summaryClass = classNames('form-validation-summary', {
      'form-validation-summary--show-for-sr-only': showForSrOnly,
      'widget-form__element--hidden': errors.length === 0
    });

    return <div className={summaryClass} ref="summary" tabIndex="0">
      <ExclamationIcon className="form-validation-summary__icon" />
      <div className="form-validation-summary__title" aria-label={`${summaryText}, ${errors.join(', ')}`}>

        <strong>{summaryText}</strong>
        <span className="widget-form__element--aria-hidden">{errors.join(', ')}</span>
      </div>
      <ul className="form-validation-summary__list" aria-hidden="true">
        {errors.map((error, index) => <ErrorItem error={error} key={index} />)}
      </ul>
    </div>;
  }
}

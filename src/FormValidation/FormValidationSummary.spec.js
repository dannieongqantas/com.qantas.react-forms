import React from 'react';
import { render, findDOMNode } from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import FormValidationSummary from './FormValidationSummary';
import { VALIDATION_FOCUS_DONE } from '../Constants/ActionTypes';

function createComponent(newProps = {}) {
  const props = {
    validations: [],
    summaryText: 'Something went wrong',
    id: 'test',
    dispatch: () => {},
    ...newProps };

  return TestUtils.renderIntoDocument(
    <FormValidationSummary {...props} />
  );
}

const missingLocation = 'missing pickup location';
const missingTime = 'missing pickup time';
const validationErrors = [missingLocation, missingTime];

describe('FormValidationSummary', () => {
  it('should not render component when there are no errors', () => {
    const renderedComponent = createComponent();
    expect(findDOMNode(renderedComponent).getAttribute('class')).to.contain('hidden');
  });

  it('should show a list of errors when error is provided', () => {
    const summaryText = 'something gone wrong';

    const renderedComponent = createComponent({
      validations: validationErrors,
      summaryText
    });

    const items = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
    const summary = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'strong');

    expect(summary.textContent).to.equal(summaryText);
    expect(findDOMNode(renderedComponent)).to.exist;
    expect(items.length).to.equal(2);

    expect(items[0].textContent).to.equal(validationErrors[0]);
    expect(items[1].textContent).to.equal(validationErrors[1]);
  });

  it(`should dispatch ${VALIDATION_FOCUS_DONE} after rerender only when the summaryFocus is true`, () => {
    const node = document.createElement('div');
    const onFocusSpy = sinon.spy();

    render(<FormValidationSummary validations={[]} onFocus={onFocusSpy} />, node);

    render(<FormValidationSummary validations={[]} onFocus={onFocusSpy} />, node);
    expect(onFocusSpy).to.not.have.been.called;

    render(<FormValidationSummary summaryFocus validations={[]} onFocus={onFocusSpy} />, node);

    expect(onFocusSpy).to.have.been.called;
  });

  describe('accessibility', () => {
    const id = 'test';
    const screenReaderClass = 'form-validation-summary--show-for-sr-only';
    let renderedComponent;
    let summaryElm;

    context('when not passing an showForSrOnly prop', () => {
      before(() => {
        renderedComponent = createComponent({ id, validations: validationErrors });
        summaryElm = findDOMNode(renderedComponent);
      });

      it('should render a hidden span with the errors', () => {
        const summary = summaryElm.querySelector('.widget-form__element--aria-hidden');
        expect(summary.textContent).to.equal(`${missingLocation}, ${missingTime}`);
      });

      it('should be focusable', () => {
        expect(summaryElm.getAttribute('tabindex')).to.equal('0');
      });

      it('should not render the screen reader class', () => {
        expect(findDOMNode(renderedComponent).className).to.not.contain(screenReaderClass);
      });
    });

    context('when passing an showForSrOnly prop', () => {
      context('and setting the showForSrOnly prop is set to false', () => {
        before(() => {
          renderedComponent = createComponent({ id, validations: validationErrors, showForSrOnly: false });
        });

        it('should not render the screen reader class', () => {
          expect(findDOMNode(renderedComponent).className).to.not.contain(screenReaderClass);
        });
      });

      context('and setting the showForSrOnly prop is set to true', () => {
        before(() => {
          renderedComponent = createComponent({ id, validations: validationErrors, showForSrOnly: true });
        });

        it('should not render the screen reader class', () => {
          expect(findDOMNode(renderedComponent).className).to.contain(screenReaderClass);
        });
      });
    });
  });
});

import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';
import locationData from '../../test/mock/locations';
const proxyquire = require('proxyquire').noCallThru();
const DropdownList = proxyquire('./DropdownList', {}).default;

const locations = locationData.car;
function createComponent(newProps = {}) {
  const props = {
    items: locations,
    value: 'syd',
    id: 'pickupLocation',
    dispatch: () => {},
    clickClose: () => {},
    selectedItem: -1,
    opened: true,
    itemIdPrefix: 'kami',
    hoverAction: 'blahblah',
    noResultsText: 'No results matching {value}',
    clickAction: () => {},
    ...newProps
  };

  return TestUtils.renderIntoDocument(
    <DropdownList {...props} />
  );
}

describe('DropdownList', () => {
  describe('Component', () => {
    it('Displays correct number of results', () => {
      const renderedComponent = createComponent();
      let expectedValue = locations.length;
      const options = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
      expect(options.length).to.equal(expectedValue);

      // Remove one item and check if it still returns the correct result
      const newLocations = locations.slice(0, locations.length - 1);
      const newRenderedComponent = createComponent({ items: newLocations });
      expectedValue--;
      const newOptions = TestUtils.scryRenderedDOMComponentsWithTag(newRenderedComponent, 'li');
      expect(newOptions.length).to.equal(expectedValue);
    });

    it('should contain a tabIndex=-1 in the ul to prevent firefox tabbing', () => {
      const renderedComponent = createComponent();
      const list = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'ul');
      expect(list.getAttribute('tabindex')).to.equal('-1');
    });

    it('Displays no results when not opened', () => {
      const carCityListItems = createComponent({ opened: false });
      expect(findDOMNode(carCityListItems).getAttribute('class')).to.contain('hidden');
    });

    it('should display the default not found when location is not found', () => {
      const value = 'Albion';
      const noLocation = [];
      const renderedComponent = createComponent({ items: noLocation, value });
      const carCityListItemsElements = findDOMNode(renderedComponent).querySelectorAll('li div');
      expect(carCityListItemsElements[0].textContent).to.equal(`No results matching '${value}'`);
    });

    it('should display the correct result when location is not found', () => {
      const value = 'Albion';
      const noLocation = [];
      const renderedComponent = createComponent({ items: noLocation, value });
      const carCityListItemsElements = findDOMNode(renderedComponent).querySelectorAll('li div');

      expect(carCityListItemsElements[0].textContent).to.equal(`No results matching '${value}'`);
    });

    it('should display the correct result when location is not found highlighting the result value', () => {
      const value = 'Albion';
      const noLocation = [];
      const renderedComponent = createComponent({ items: noLocation, value, noResultsText: 'I can {value} this {value} now' });
      const carCityListItemsElements = findDOMNode(renderedComponent).querySelectorAll('li div');
      const notFoundItemElements = carCityListItemsElements[0].childNodes;
      const expectedValues = ['I can ', `'${value}'`, ' this ', `'${value}'`, ' now'];
      for (let i = 0; i < notFoundItemElements.length; i++) {
        expect(notFoundItemElements[i].textContent).to.equal(expectedValues[i]);
        if (i % 2 === 0) {
          expect(notFoundItemElements[i].nodeName).to.equal('SPAN');
        } else {
          expect(notFoundItemElements[i].nodeName).to.equal('STRONG');
        }
      }
    });

    it('Should render the child component', () => {
      const renderedComponent = createComponent({ children: <p>Message</p> });
      expect(findDOMNode(renderedComponent).querySelectorAll('p')[0].textContent).to.equal('Message');
    });

    it('should render the correct formatting for one location', () => {
      const value = 'Alb';
      const oneLocation = [locations[0]];
      const renderedComponent = createComponent({ items: oneLocation, value, formatter: item => {
        const modifiedItem = item;
        modifiedItem.text = modifiedItem.name;
        return modifiedItem;
      } });
      const carCityListItemsElements = findDOMNode(renderedComponent).querySelectorAll('li div');
      const formattedElements = carCityListItemsElements[0].childNodes;
      const formattedWords = oneLocation;

      expect(formattedElements.length).to.equal(formattedWords.length);

      formattedWords.forEach((words, index) => {
        expect(formattedElements[index].textContent).to.equal(words.text);
      });
    });
  });

  describe('User interacts with items', () => {
    let dispatchSpy;
    let clickActionSpy;
    let renderedComponent;
    let handleHoverItemSpy;
    const id = 'pickupLocation';
    const hoverAction = 'hoverAction';

    beforeEach(() => {
      dispatchSpy = sinon.spy();
      clickActionSpy = sinon.spy();
      handleHoverItemSpy = sinon.spy();
      renderedComponent = createComponent({ dispatch: dispatchSpy, id, hoverAction, clickAction: clickActionSpy, handleHoverItem: handleHoverItemSpy });
    });

    it('should call dispatch with the correct action when clicked', () => {
      const options = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
      TestUtils.Simulate.click(options[0]);
      expect(clickActionSpy).to.have.been.called;
    });

    it('should call dispatch with the correct action when hovered', () => {
      const options = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
      const item = 0;
      TestUtils.Simulate.mouseOver(options[item]);
      expect(handleHoverItemSpy).to.have.been.called;
    });

    it('should call dispatch with the correct action when touch on touch device', () => {
      const options = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
      const item = 0;
      TestUtils.Simulate.touchStart(options[item]);
      expect(handleHoverItemSpy).to.have.been.called;
    });
  });

  describe('accessibility', () => {
    let renderedComponent;
    let options;
    const selectedItem = 1;
    const itemIdPrefix = 'yolo';
    beforeEach(() => {
      renderedComponent = createComponent({ selectedItem, itemIdPrefix });
      options = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'li');
    });

    it('should contain the role=option attribute', () => {
      options.forEach((item) => {
        expect(item.getAttribute('role')).to.be.equal('option');
      });
    });

    it('should rendered the correct ids for aria-activedescendant', () => {
      options.forEach((item, index) => {
        expect(item.getAttribute('id')).to.be.equal(itemIdPrefix + index);
      });
    });

    it('should contain the correct aria-selected value', () => {
      options.forEach((item, index) => {
        expect(item.getAttribute('aria-selected')).to.be.equal((index === selectedItem).toString());
      });
    });
  });
});

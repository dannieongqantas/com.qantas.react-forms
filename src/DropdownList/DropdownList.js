import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Dropdown from '../Dropdown/Dropdown';

export default class DropdownList extends Component {
  static propTypes = {
    items: PropTypes.array,
    handleHoverItem: PropTypes.func.isRequired,
    clickAction: PropTypes.func.isRequired,
    listId: PropTypes.string.isRequired,
    noResultsText: PropTypes.string.isRequired,
    itemIdPrefix: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    opened: PropTypes.bool.isRequired,
    value: PropTypes.string,
    selectedItem: PropTypes.number,
    formatter: PropTypes.func,
    children: PropTypes.node,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    items: [],
    value: '',
    clickClose: false,
    opened: false,
    selectedItem: -1,
    noResultsText: '',
    handleHoverItem: () => {},
    formatter: item => item,
    disabled: false
  };

  constructor(props) {
    super(props);
    this.scrollWithinView = this.scrollWithinView.bind(this);
    this.createNotFound = this.createNotFound.bind(this);
    this.createItem = this.createItem.bind(this);
  }

  componentDidUpdate() {
    this.scrollWithinView();
  }

  createItem(item, itemsCount) {
    const { selectedItem, itemIdPrefix, ...props } = this.props;
    const { text, ariaLabel } = item;
    const selected = itemsCount === selectedItem;

    return <li key={itemsCount}
               role="option"
               aria-label={ariaLabel}
               id={itemIdPrefix + itemsCount}
               className={classNames('qfa1-dropdown-list__list-item', { 'qfa1-dropdown-list__list-item--selected': selected })}
               onMouseOver={() => props.handleHoverItem(itemsCount)}
               onTouchStart={() => props.handleHoverItem(itemsCount)}
               aria-selected={selected}
               onClick={props.clickAction}
               tabIndex="-1">
      <div>
        {text}
      </div>
    </li>;
  }

  createNotFound(itemsCount) {
    const { itemIdPrefix, selectedItem, value, noResultsText } = this.props;
    const selected = itemsCount === selectedItem;

    let noResults = noResultsText.split(/(\{value})/g);
    noResults = noResults.map((item, index) => (index % 2 === 0 ? <span key={index}>{item}</span> : <strong key={index}>{`'${value}'`}</strong>));

    return <li
      className={classNames('qfa1-dropdown-list__list-item', { 'qfa1-dropdown-list__list-item--selected': selected })}
      aria-selected={selected}
      id={itemIdPrefix + itemsCount}
      onMouseOver={() => this.props.handleHoverItem(itemsCount)}
      onTouchStart={() => this.props.handleHoverItem(itemsCount)}
      role="option"
      onClick={this.props.clickAction}>
      <div>
        {noResults}
      </div>
    </li>;
  }

  scrollWithinView() {
    if (this.refs.list && this.props.selectedItem !== -1) {
      const listHeight = this.refs.list.clientHeight;
      const listScrollHeight = this.refs.list.scrollHeight;
      if (listHeight < listScrollHeight) {
        const { childNodes } = this.refs.list;
        let sumHeight = 0;
        for (let i = 0; i < this.props.selectedItem; i++)
          sumHeight += childNodes[i].offsetHeight;

        const selectedItemHeight = childNodes[this.props.selectedItem].offsetHeight;
        const minHeight = this.refs.list.scrollTop - selectedItemHeight;
        const elmHeight = listHeight + selectedItemHeight - 1; // plus border
        const maxHeight = minHeight + elmHeight;
        if (!(minHeight < sumHeight && sumHeight < maxHeight))
          this.refs.list.scrollTop = sumHeight;
      }
    }
  }

  render() {
    const { value, opened, items, formatter, children, disabled, ...props } = this.props;

    const formattedItems = items.map(item => formatter(item, value));
    const list = formattedItems.length ?
      formattedItems.map((item, index) => this.createItem(item, index))
      : this.createNotFound(0);

    return <Dropdown opened={opened} disabled={disabled}>
      <ul className="qfa1-dropdown-list__list" role="listbox" id={props.listId} ref="list" tabIndex="-1">
        {list}
      </ul>
      {children}
    </Dropdown>;
  }
}

import React from 'react';

import PropTypes from 'prop-types';

export const LoadingIndicator = ({ srText }) => (
  <div className="qfa1-loading-indicator">
    <div className="sk-three-bounce loader__icon">
      <span className="show-for-sr">{srText}</span>
      <div className="sk-child sk-bounce1"></div> {' '}
      <div className="sk-child sk-bounce2"></div> {' '}
      <div className="sk-child sk-bounce3"></div> {' '}
    </div>
  </div>
);

LoadingIndicator.propTypes = {
  srText: PropTypes.string.isRequired
};

export default LoadingIndicator;

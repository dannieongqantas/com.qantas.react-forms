import React from 'react';
import { shallow } from 'enzyme';
const proxyquire = require('proxyquire').noCallThru();
const LoadingIndicator = proxyquire('./LoadingIndicator', {}).LoadingIndicator;

describe('LoadingIndicator', () => {
  let loadingDiv;
  let loadingText;
  let props;
  let wrapper;

  before(() => {
    props = {
      srText: 'Loading'
    };

    wrapper = shallow(<LoadingIndicator {...props} />);
    loadingDiv = wrapper.find('.qfa1-loading-indicator');
    loadingText = wrapper.find('.show-for-sr');
  });

  it('should display the loader', () => {
    expect(loadingDiv).to.have.length(1);
  });

  it('should display the loading text', () => {
    expect(loadingText.text()).to.eq(props.srText);
  });
});

## Typeahead

### Import to your project
```js
import Typeahead from 'com.qantas.react-forms/Typeahead';
```
```scss
@import '~com.qantas.react-forms/scss/Typeahead';
```

### Example Usage (Without flux/Redux)
```js
export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            value: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSelection = this.handleSelection.bind(this);
    }
    
    handleChange(e) {
        const { value } = e.target;
        this.setState({ value: e.target.value });
        locationPromise(value).then(data => {
            this.setState({ items: data.items });
        });
    }
    
    handleSelection(item) {
    
    }

    render() {
        return <form>
            <Typeahead id="pickupLocation"
                formLabel="Pickup Location"
                items={this.state.items}
                minLength={3}
                value={this.state.value}
                onChange={this.handleChange}
                onSelectItem={this.handleSelection}
                />
        </form>
    }
}
```

| Prop          | PropType      | Description  | Example |
| ------------- |:-------------:| ------------:| -------:| 
| formLabel | string.isRequired | The form label | ```'Pickup Location'``` |
| ariaLabel | string | The aria label, defaults to the formLabel | ```'Pickup Location'``` |
| items | array.isRequired | the list of items displayed | ```[{text: 'option 1', ariaLabel: 'select option 1'}]``` |
| value | string.isRequired | The value inside the input field | ```'Sydney City'``` |
| screenRead | string.isRequired | The text that will be prompted by the screenReader | ```'Alert, you have selected'``` |
| ariaClearButton | string | The text describing the clear button | default: ```'clear button'``` |
| noResultsText | string.isRequired | The text shown in the options when no filteredLocations is an empty array | ```You cannot find '' blah``` |
| minLength | number | The minimum number of characters required to open the list | default ```3``` |
| onChange | func.isRequired | The change event trigger | ```e => { console.log(e.target.value) }``` |
| onFocus | func | The focus event trigger | ```() => { alert('focused') }``` |
| onBlur | func | The blur event trigger (only bluring of the whole component) | ```() => { alert('blurred') }``` |
| onSelectItem | func.isRequired | The select event trigger | ```(selectedItemNumber) => {}```|
| onClearField | func | When the clear button is pressed | ```() => { alert('clear field') }```|
| clearValidation | func | What happens when the validation is cleared| ```() => {}``` |


import React from 'react';
import { findDOMNode } from 'react-dom';
import cloneDeep from 'lodash/cloneDeep';
import rawLocationsData from '../../test/mock/locations';
import i18nData from '../../test/mock/i18n';
const i18nDataClone = cloneDeep(i18nData);
const locations = cloneDeep(rawLocationsData.car);

import createComponentStub from '../../test/utils/ComponentStub';
const proxyquire = require('proxyquire').noCallThru();

const TypeaheadListStub = createComponentStub();

const InputArea = proxyquire('../Input/InputArea', {
  '../utils/userAgent': {
    isIE() {
      return false;
    },
    isTouchDevice() {
      return true;
    }
  }
});

const { DEBOUNCE_TIME } = InputArea;

export const ControlledTypeaheadComponent = proxyquire('./ControlledTypeahead', {
  '../DropdownList/DropdownList': TypeaheadListStub,
  '../Input/InputArea': InputArea.default,
  '../utils/idGenerator': () => 'random'
}).ControlledTypeahead;

const i18n = { ...i18nDataClone.shared, ...i18nDataClone.car };
function createComponent(newProps = {}) {
  const props = {
    i18n,
    locations,
    pickupLocation: 'syd',
    value: '',
    label: 'hello world',
    items: locations,
    opened: true,
    lastValue: '',
    navigatedItem: -1,
    formLabel: i18n.pickupLocation.formLabel,
    ariaLabel: i18n.pickupLocation.ariaLabel,
    dispatch: () => {
    },
    ...newProps
  };

  return TestUtils.renderIntoDocument(
    <ControlledTypeaheadComponent {...props} />
  );
}

describe('ControlledTypeahead', () => {
  describe('component', () => {
    it('should render properly', () => {
      const renderedComponent = createComponent();
      const renderedElement = findDOMNode(renderedComponent);
      expect(renderedElement).to.exist;
    });

    it('should render with the correct value in the input', () => {
      const expectedText = 'ChiChi';
      const renderedComponent = createComponent({ value: expectedText });
      const input = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      expect(input.value).to.equal(expectedText);
    });

    it('should pass the correct props', () => {
      const renderedComponent = createComponent({
        items: ['bulma'],
        value: 'krillin',
        opened: true,
        children: <p />
      });

      const expectedProps = {
        items: ['bulma'],
        value: 'krillin',
        opened: true,
        clickAction: renderedComponent.selectItem,
        children: <p />
      };

      const CarCityListItems = TestUtils
        .findRenderedComponentWithType(renderedComponent, TypeaheadListStub);

      Object.keys(expectedProps).forEach(key => {
        expect(CarCityListItems.props[key]).to.deep.equal(expectedProps[key]);
      });
    });

    it('should not render the close button if opened prop is false', () => {
      const renderedComponent = createComponent({ opened: false });
      const btn = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'button');
      expect(btn.length).to.equal(0);
    });

    it('should not render the close button if opened prop is true and there is no value', () => {
      const renderedComponent = createComponent({ opened: true, value: '' });
      const btn = TestUtils.scryRenderedDOMComponentsWithTag(renderedComponent, 'button');
      expect(btn.length).to.equal(0);
    });
  });

  describe('user actions with input', () => {
    let renderedComponent;
    let dispatchSpy;
    let onItemSelectSpy;
    let focusHandlerSpy;
    let handleChangeSpy;
    let input;
    const initialValue = 'hello';
    const id = 'pickupLocation';
    const suggestions = 'suggestions';
    const selectionMsg = 'selectionMsg';

    beforeEach(() => {
      handleChangeSpy = sinon.spy();
      focusHandlerSpy = sinon.spy();
      dispatchSpy = sinon.spy();
      onItemSelectSpy = sinon.spy();
      renderedComponent = createComponent({
        dispatch: dispatchSpy,
        value: initialValue,
        onItemSelect: onItemSelectSpy,
        ariaSuggestions: suggestions,
        ariaSelection: selectionMsg,
        focusHandler: focusHandlerSpy,
        handleChange: handleChangeSpy,
        id
      });
      input = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    });

    it('should focus by dispatching the onFocus action', () => {
      TestUtils.Simulate.focus(input);
      expect(focusHandlerSpy).to.have.been.called;
    });

    it(`should change by calling the correct onChange function after a debounce of ${DEBOUNCE_TIME}ms`, (done) => {
      const TextInput = 'test';
      TestUtils.Simulate.change(input, { target: { value: TextInput } });
      setTimeout(() => {
        expect(handleChangeSpy).to.not.have.been.called;
      }, DEBOUNCE_TIME - 50);

      setTimeout(() => {
        expect(handleChangeSpy).to.have.been.called;
        done();
      }, DEBOUNCE_TIME + 50);
    });

    // There is an android chrome issue where the onchange is being triggered by other events
    it('should not dispatch when there is the text has not actually changed', () => {
      const TextInput = initialValue;
      TestUtils.Simulate.change(input, { target: { value: TextInput } });
      expect(dispatchSpy).to.not.have.been.called;
    });
  });

  describe('accessibility', () => {
    let renderedComponent;
    let input;

    beforeEach(() => {
      renderedComponent = createComponent({});
      input = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    });

    it('should contain the aria-autocomplete=list attribute', () => {
      expect(input.getAttribute('aria-autocomplete')).to.equal('list');
    });

    it('should contain the combobox role', () => {
      expect(input.getAttribute('role')).to.equal('combobox');
    });

    // Can Remove, leaving functionality as may hit where this may need to be update in widgets
    // const label = `${i18n.pickupLocation.formLabel}, ${i18n.pickupLocation.ariaLabel}`.replace(/-/g, ' ');
    // it(`should contain the aria-label=${label}`, () => {
    //   expect(input.getAttribute('aria-label')).to.equal(label);
    // });

    it('should not contain the aria-activedescendant attribute by default', () => {
      expect(input.getAttribute('aria-activedescendant')).to.be.null;
    });

    it('should contain the aria-activedescendant attribute', () => {
      const navNum = 5;
      renderedComponent = createComponent({ navigatedItem: navNum });
      input = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      expect(input.getAttribute('aria-activedescendant'))
        .to.equal(`typeahead-list-item-random-${navNum}`);
    });
  });

  describe('disabled state', () => {
    context('disabled false', () => {
      let disabledRenderedComponent;
      let input;

      before(() => {
        disabledRenderedComponent = createComponent();
        input = TestUtils.findRenderedDOMComponentWithTag(disabledRenderedComponent, 'input');
      });

      it('should not contain the disabled attribute', () => {
        expect(input.getAttribute('disabled')).not.to.exist;
      });

      it('should pass disabled class to the container', () => {
        expect(findDOMNode(disabledRenderedComponent).getAttribute('class')).to.equal('widget-form__group-container');
      });
    });

    context('disabled true', () => {
      let disabledRenderedComponent;
      let input;

      before(() => {
        disabledRenderedComponent = createComponent({
          disabled: true
        });
        input = TestUtils.findRenderedDOMComponentWithTag(disabledRenderedComponent, 'input');
      });

      it('should contain the disabled attribute', () => {
        expect(input.getAttribute('disabled')).to.exist;
      });

      it('should pass disabled class to the container', () => {
        expect(findDOMNode(disabledRenderedComponent).getAttribute('class')).to.equal('widget-form__group-container qfa1-typeahead--disabled');
      });
    });
  });
});


import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import DropdownList from '../DropdownList/DropdownList';
import ComponentBlur from '../ReusableComponents/ComponentBlur';
import ClearButton from '../Icons/ClearButton';
import InputArea from '../Input/InputArea';
import ScreenReader from '../ScreenReader/ScreenReader';
import idGenerator from '../utils/idGenerator';

const COMPONENT_REF = 'Typeahead';

export class ControlledTypeahead extends Component {
  static defaultProps = {
    value: '',
    items: [],
    ariaSuggestions: '',
    className: '',
    ariaClearButton: 'clear button',
    opened: false,
    placeholder: '',
    clearButtonTabbable: true,
    selectOnFocus: false,
    disabled: false
  };

  static propTypes = {
    handleHoverItem: PropTypes.func.isRequired,
    clearField: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    id: PropTypes.string,
    isLabelInline: PropTypes.bool,
    selectItem: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    focusHandler: PropTypes.func.isRequired,
    blurInput: PropTypes.func.isRequired,
    keyDownHandler: PropTypes.func.isRequired,
    items: PropTypes.array,
    opened: PropTypes.bool,
    formLabel: PropTypes.string.isRequired,
    ariaLabel: PropTypes.string,
    noResultsText: PropTypes.string.isRequired,
    value: PropTypes.string,
    navigatedItem: PropTypes.number,
    validationMessage: PropTypes.string,
    ariaSuggestions: PropTypes.string,
    screenRead: PropTypes.string,
    className: PropTypes.string,
    ariaClearButton: PropTypes.string,
    blur: PropTypes.func.isRequired,
    children: PropTypes.node,
    clearButtonTabbable: PropTypes.bool,
    selectOnFocus: PropTypes.bool,
    disabled: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.id = props.id || idGenerator();
  }

  focus = () => {
    this.refs.input.focus();
  }

  render() {
    const {
      blur,
      children,
      isLabelInline,
      items,
      navigatedItem,
      noResultsText,
      opened,
      value,
      ...props
    } = this.props;

    const inputId = `typeahead-input-${this.id}`;
    const descendantIdPrefix = `typeahead-list-item-${this.id}-`;
    const activeDescendant = navigatedItem === -1 ? '' : descendantIdPrefix + navigatedItem;

    const listId = `${descendantIdPrefix}list`;

    const classname = classNames('widget-form__group-container', props.className, {
      'qfa1-typeahead--disabled': props.disabled
    });

    return (
      <div className={classname}>
        <div ref={(el) => { this[COMPONENT_REF] = el; }} onBlur={blur}>
          <InputArea ref="input"
                     id={inputId}
                     isLabelInline={isLabelInline}
                     aria-owns={listId}
                     aria-autocomplete="list"
                     role="combobox"
                     popup={opened}
                     placeholder={props.placeholder}
                     formLabel={props.formLabel}
                     ariaLabel={props.ariaLabel || props.formLabel}
                     activeDescendant={activeDescendant}
                     value={value}
                     selectOnFocus={props.selectOnFocus}
                     onFocus={props.focusHandler}
                     onKeyDown={props.keyDownHandler}
                     onChange={props.handleChange}
                     validationMessage={props.validationMessage}
                     disabled={props.disabled} >

            <ClearButton tabbable={props.clearButtonTabbable} className="qfa1-typeahead__close-button" onClick={() => { this.refs.input.focus(); props.clearField(); }} hide={!value} focusable title={props.ariaClearButton} />

          </InputArea>

          <ScreenReader text={props.screenRead} />

          <DropdownList
            formatter={props.formatter}
            noResultsText={noResultsText}
            itemIdPrefix={descendantIdPrefix}
            selectedItem={navigatedItem}
            items={items}
            listId={listId}
            value={value}
            opened={opened}
            handleHoverItem={props.handleHoverItem}
            clickAction={props.selectItem}
            >
            {children}
          </DropdownList>
        </div>
      </div>
    );
  }
}

export default ComponentBlur(ControlledTypeahead, COMPONENT_REF, (props) => props.blurInput);

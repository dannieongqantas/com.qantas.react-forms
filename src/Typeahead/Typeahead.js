import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RETURN_KEY_CODE, ARROW_UP_KEY_CODE, ARROW_DOWN_KEY_CODE, TAB_KEY_CODE } from '../Constants/KeyCodes';
import ControlledTypeahead from './ControlledTypeahead';

export default class Typeahead extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onSelectItem: PropTypes.func.isRequired,
    clearValidation: PropTypes.func,
    onClearField: PropTypes.func,
    minLength: PropTypes.number,
    value: PropTypes.string,
    items: PropTypes.array,
    isLabelInline: PropTypes.bool,
    clearButtonTabbable: PropTypes.bool,
    selectOnFocus: PropTypes.bool,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    value: '',
    minLength: 3,
    onBlur: () => {
    },
    onFocus: () => {
    },
    clearValidation: () => {
    },
    onSelectItem: () => {
    },
    onClearField: () => {
    },
    clearButtonTabbable: true,
    selectOnFocus: false,
    disabled: false
  };

  constructor(props) {
    super(props);
    this.state = { opened: false, focused: false, navigatedItem: -1 };
    this.isOpen = this.isOpen.bind(this);
    this.handleHoverItem = this.handleHoverItem.bind(this);
    this.keyDownHandler = this.keyDownHandler.bind(this);
    this.navigateList = this.navigateList.bind(this);
    this.clearField = this.clearField.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.blurInput = this.blurInput.bind(this);
    this.selectItem = this.selectItem.bind(this);
  }

  focus = () => {
    this.typeahead.focus();
  }

  selectItem(event) {
    const selectedValue = (this.props.items && (this.state.navigatedItem >= 0)) ? this.props.items[this.state.navigatedItem] : this.props.items[0];
    if (!!selectedValue) {
      this.props.onSelectItem(selectedValue, this.state.navigatedItem, event);
      this.props.clearValidation();
      this.setState({ opened: false });
    }
  }

  clearField() {
    this.props.onClearField();
    this.props.clearValidation();
  }

  handleHoverItem(item) {
    this.setState({ navigatedItem: item });
  }

  handleFocus() {
    this.setState({ navigatedItem: -1, opened: true, focused: true });
    this.props.onFocus();
  }

  handleChange(e) {
    const value = e.target.value;
    if (value !== this.props.value) {
      this.props.onChange(value);
      this.setState({ navigatedItem: -1, opened: this.state.focused });
    }
  }

  keyDownHandler(e) {
    switch (e.keyCode) {
      case RETURN_KEY_CODE:
        e.preventDefault();
        return this.selectItem();
      case TAB_KEY_CODE:
        if (this.state.navigatedItem > -1 || !!this.props.value) {
          this.selectItem(e);
        }
        return true;
      case ARROW_UP_KEY_CODE:
        e.preventDefault();
        return this.navigateList(-1);
      case ARROW_DOWN_KEY_CODE:
        e.preventDefault();
        return this.navigateList(1);
      default:
        return null;
    }
  }

  navigateList(direction) {
    let newNavigatedItem = this.state.navigatedItem + direction;
    const UPPER_BOUND = Math.max(this.props.items.length, 1);

    if (newNavigatedItem >= UPPER_BOUND)
      newNavigatedItem = -1;
    else if (newNavigatedItem < -1)
      newNavigatedItem = UPPER_BOUND - 1;

    this.setState({ navigatedItem: newNavigatedItem });
  }

  blurInput() {
    if (this.state.focused) {
      this.setState({ opened: false, focused: false });
      this.props.onBlur();
    }
  }

  isOpen() {
    const { opened } = this.state;
    const { items, value, minLength } = this.props;
    // If it's opened and has items and value is greater than the minimum value then it should be opened.
    return !!(opened && items && (value.length >= minLength));
  }

  render() {
    const opened = this.isOpen();
    return <ControlledTypeahead
      {...this.props}
      ref={(typeahead) => { this.typeahead = typeahead; }}
      navigatedItem={this.state.navigatedItem}
      opened={opened}
      selectItem={this.selectItem}
      blurInput={this.blurInput}
      handleChange={this.handleChange}
      handleHoverItem={this.handleHoverItem}
      focusHandler={this.handleFocus}
      keyDownHandler={this.keyDownHandler}
      clearField={this.clearField} />;
  }
}

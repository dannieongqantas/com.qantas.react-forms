import React from 'react';
import Typeahead from './Typeahead';
import { findDOMNode } from 'react-dom';
import { RETURN_KEY_CODE, ARROW_UP_KEY_CODE, ARROW_DOWN_KEY_CODE, TAB_KEY_CODE } from '../Constants/KeyCodes';

const onBlurSpy = sinon.spy();
const onFocusSpy = sinon.spy();
const onSelectItemSpy = sinon.spy();
const onChangeSpy = sinon.spy();
const clearValidationSpy = sinon.spy();
const items = [{ text: 'hello' }, { text: 'world' }];

function createComponent(newProps = {}) {
  const props = {
    onBlur: onBlurSpy,
    onFocus: onFocusSpy,
    onSelectItem: onSelectItemSpy,
    items,
    onChange: onChangeSpy,
    clearValidation: clearValidationSpy,
    ...newProps
  };

  return TestUtils.renderIntoDocument(
    <Typeahead {...props} />
  );
}

describe('disabled prop', () => {
  context('disabled false', () => {
    let typeaheadComponent;

    before(() => {
      typeaheadComponent = createComponent({ value: 'hello world' });
    });

    it('should be disabled false by default', () => {
      expect(typeaheadComponent.props.disabled).to.be.false;
    });
  });

  context('disabled true', () => {
    let typeaheadComponent;

    before(() => {
      typeaheadComponent = createComponent({ value: 'hello world', disabled: true });
    });

    it('should be disabled true when disabled true is passed as prop', () => {
      expect(typeaheadComponent.props.disabled).to.be.true;
    });
  });
});

describe('Typeahead', () => {
  let typeaheadComponent;
  let typeaheadComponentNode;

  beforeEach(() => {
    typeaheadComponent = createComponent({ value: 'hello world' });
    typeaheadComponentNode = findDOMNode(typeaheadComponent);
  });

  describe('onBlur', () => {
    beforeEach(() => {
      typeaheadComponent.state.opened = true;
      typeaheadComponent.state.focused = true;
      onBlurSpy.reset();
    });

    it('should set the opened state to false', done => {
      TestUtils.Simulate.blur(typeaheadComponentNode.querySelector('.widget-form__group-container>div'));
      setTimeout(() => {
        expect(typeaheadComponent.state.opened).to.be.false;
        done();
      });
    });

    it('should set the focused state to false', done => {
      TestUtils.Simulate.blur(typeaheadComponentNode.querySelector('.widget-form__group-container>div'));
      setTimeout(() => {
        expect(typeaheadComponent.state.focused).to.be.false;
        done();
      });
    });

    it('should call the onBlur function prop', (done) => {
      TestUtils.Simulate.blur(typeaheadComponentNode.querySelector('.widget-form__group-container>div'));
      setTimeout(() => {
        expect(onBlurSpy).to.have.been.called;
        done();
      });
    });

    it('should not call onBlur when not focused', (done) => {
      typeaheadComponent.state.focused = false;
      TestUtils.Simulate.blur(typeaheadComponentNode.querySelector('.widget-form__group-container>div'));
      setTimeout(() => {
        expect(onBlurSpy).to.not.have.been.called;
        done();
      });
    });
  });

  describe('onFocus', () => {
    beforeEach(() => {
      typeaheadComponent.state.opened = false;
      typeaheadComponent.state.focused = false;
      typeaheadComponent.state.navigatedItem = 1;
      TestUtils.Simulate.focus(typeaheadComponentNode.querySelector('input'));
    });

    it('should set the opened state to true', done => {
      // sets it asynchronously
      setTimeout(() => {
        expect(typeaheadComponent.state.opened).to.be.true;
        done();
      });
    });

    it('should set the focused state to true', done => {
      // sets it asynchronously
      setTimeout(() => {
        expect(typeaheadComponent.state.focused).to.be.true;
        done();
      });
    });

    it('should reset the navigated item', done => {
      // sets it asynchronously
      setTimeout(() => {
        expect(typeaheadComponent.state.navigatedItem).to.equal(-1);
        done();
      });
    });

    it('should call the onFocus function prop', () => {
      expect(onFocusSpy).to.have.been.called;
    });
  });

  describe('KeyStrokes', () => {
    function keyStroke(componentNode, stroke) {
      TestUtils.Simulate.keyDown(componentNode.querySelector('input'), { keyCode: stroke });
    }

    it('should not handle the tab key no valid value and navigated state is -1', () => {
      const emptyTypeaheadComponent = createComponent({ value: '' });
      const emptytypeaheadComponentNode = findDOMNode(emptyTypeaheadComponent);
      emptyTypeaheadComponent.state.navigatedItem = -1;
      keyStroke(emptytypeaheadComponentNode, TAB_KEY_CODE);
      expect(onSelectItemSpy).not.to.have.been.called;
      expect(clearValidationSpy).not.to.have.been.called;
    });

    it('should handle the tab key when navigated state is -1 but has a valid value', () => {
      typeaheadComponent.state.opened = true;
      typeaheadComponent.state.navigatedItem = -1;
      keyStroke(typeaheadComponentNode, TAB_KEY_CODE);
      expect(onSelectItemSpy).to.have.been.called;
      expect(clearValidationSpy).to.have.been.called;
      expect(typeaheadComponent.state.opened).to.be.false;
    });

    it('should handle the tab key when the navigated state is more than -1', () => {
      typeaheadComponent.state.opened = true;
      typeaheadComponent.state.navigatedItem = 1;
      keyStroke(typeaheadComponentNode, TAB_KEY_CODE);
      expect(onSelectItemSpy).to.have.been.calledWith(items[1], 1);
      expect(clearValidationSpy).to.have.been.called;
      expect(typeaheadComponent.state.opened).to.be.false;
    });

    it('should handle the return key', () => {
      typeaheadComponent.state.opened = true;
      typeaheadComponent.state.navigatedItem = 1;
      keyStroke(typeaheadComponentNode, RETURN_KEY_CODE);
      expect(onSelectItemSpy).to.have.been.calledWith(items[1], 1);
      expect(clearValidationSpy).to.have.been.called;
      expect(typeaheadComponent.state.opened).to.be.false;
    });

    it('should handle the return key when navigated state is -1', () => {
      typeaheadComponent.state.opened = true;
      typeaheadComponent.state.navigatedItem = -1;
      keyStroke(typeaheadComponentNode, RETURN_KEY_CODE);
      expect(onSelectItemSpy).to.have.been.calledWith(items[0], -1);
      expect(clearValidationSpy).to.have.been.called;
      expect(typeaheadComponent.state.opened).to.be.false;
    });

    it('should handle the up key', () => {
      typeaheadComponent.state.navigatedItem = 1;
      keyStroke(typeaheadComponentNode, ARROW_UP_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(0);
      keyStroke(typeaheadComponentNode, ARROW_UP_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(-1);
      keyStroke(typeaheadComponentNode, ARROW_UP_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(items.length - 1);
    });

    it('should handle the down key', () => {
      typeaheadComponent.state.navigatedItem = items.length - 1;
      keyStroke(typeaheadComponentNode, ARROW_DOWN_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(-1);
      keyStroke(typeaheadComponentNode, ARROW_DOWN_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(0);
      keyStroke(typeaheadComponentNode, ARROW_DOWN_KEY_CODE);
      expect(typeaheadComponent.state.navigatedItem).to.equal(1);
    });
  });
});

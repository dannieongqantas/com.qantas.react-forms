import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isIE, isEdge } from '../utils/userAgent';

export default class ScreenReader extends Component {
  static propTypes = {
    ariaLive: PropTypes.oneOf(['polite', 'assertive']),
    ariaRelevant: PropTypes.oneOf(['additions', 'removals', 'text']),
    ariaAtomic: PropTypes.bool,
    ariaLabel: PropTypes.string,
    ariaRole: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    text: PropTypes.string
  };

  static defaultProps = {
    ariaLive: 'polite',
    ariaRole: 'status'
  };

  constructor(props) {
    super(props);

    this.elementSwitch = 'div';
    this.isIE = isIE() || isEdge();
  }

  componentDidMount() {
    this.elementSwitch = this.elementSwitch === 'div' ? 'span' : 'div';
  }

  shouldComponentUpdate(nextProps) {
    return (this.props.text !== nextProps.text) ||
      (this.props.ariaLabel !== nextProps.ariaLabel);
  }

  componentDidUpdate() {
    this.elementSwitch = this.elementSwitch === 'div' ? 'span' : 'div';
  }

  render() {
    const { text, ariaRole, ariaLive, ariaAtomic, ariaRelevant, ariaLabel } = this.props;

    const props = {
      className: 'widget-form__element--aria-hidden',
      role: ariaRole,
      'aria-atomic': ariaAtomic,
      'aria-relevant': ariaRelevant,
      'aria-label': ariaLabel,
      'aria-live': ariaLive
    };

    // This forces a full rerender of this element to work in IE
    const textElm = this.elementSwitch === 'div' ? <div {...props}>{text}</div> : <span {...props}>{text}</span>;

    return <span {...props}>
      {this.isIE ? textElm : text}
    </span>;
  }
}

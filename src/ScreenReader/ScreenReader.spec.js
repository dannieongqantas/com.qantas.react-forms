import TestUtils from 'react-dom/test-utils';
import { findDOMNode } from 'react-dom';
import React from 'react';
import ScreenReader from './ScreenReader';

describe('ScreenReader', () => {
  it('should render component with text and polite aria-live', () => {
    const text = 'hello world';
    const reactElement = TestUtils.renderIntoDocument(
      <ScreenReader text={text} />
    );
    const renderedElement = findDOMNode(reactElement);

    expect(renderedElement.textContent).to.equal(text);
    expect(renderedElement.getAttribute('aria-live')).to.equal('polite');
  });

  it('should render component with text with assertive aria-live', () => {
    const text = 'You need to know!';
    const reactElement = TestUtils.renderIntoDocument(
      <ScreenReader text={text} ariaLive="assertive" />
    );
    const renderedElement = findDOMNode(reactElement);

    expect(findDOMNode(reactElement).textContent).to.equal(text);
    expect(renderedElement.getAttribute('aria-live')).to.equal('assertive');
  });
});

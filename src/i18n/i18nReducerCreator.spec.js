const getDataForLanguageCodeStub = sinon.stub().returns('returned language data');
const transformLanguageCodeStub = sinon.stub().returns('transformed language code');
const i18nReducerCreator = proxyquire('../src/i18n/i18nReducerCreator', {
  './getDataForLanguageCode': getDataForLanguageCodeStub,
  './transformLanguageCode': transformLanguageCodeStub
});

describe('i18nReducerCreator', () => {
  beforeEach(() => {
    getDataForLanguageCodeStub.reset();
    transformLanguageCodeStub.reset();
  });

  it('should return a function', () => {
    expect(typeof i18nReducerCreator({}, 'language-code-fake')).to.equal('function');
  });

  it('should return a reducer that sets initial state as expected', () => {
    const fakeLanguageData = {};
    const fakeLanguageCode = 'language-code-fake';

    const reducer = i18nReducerCreator(fakeLanguageData, fakeLanguageCode);
    const result = reducer();

    expect(transformLanguageCodeStub.calledWith('language-code-fake')).to.equal(true);
    expect(getDataForLanguageCodeStub.calledWith(fakeLanguageData, 'transformed language code')).to.equal(true);

    expect(result).to.equal('returned language data');
  });
});

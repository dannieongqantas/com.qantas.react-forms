import getDataForLanguageCode from './getDataForLanguageCode';

describe('getDataForLanguageCode', () => {
  const enData = { helloLabel: 'Hello' };
  const enAuData = { helloLabel: "G'day" };
  const frData = { helloLabel: 'Bonjour' };
  const idData = { helloLabel: 'Halo' };
  const zhhansData = { helloLabel: '' };
  const testLanguageData = {
    en: enData,
    'en-au': enAuData,
    fr: frData,
    id: idData,
    'zh-hans': zhhansData
  };

  it('should use specific language if we have that', () => {
    expect(getDataForLanguageCode(testLanguageData, 'en-au')).to.equal(enAuData);
    expect(getDataForLanguageCode(testLanguageData, 'zh-hans')).to.equal(zhhansData);
    expect(getDataForLanguageCode(testLanguageData, 'fr')).to.equal(frData);
    expect(getDataForLanguageCode(testLanguageData, 'id')).to.equal(idData);
  });

  it('should fall back to general language bundle if specific code does not exist', () => {
    expect(getDataForLanguageCode(testLanguageData, 'en-us')).to.equal(enData);
  });

  it('should return English language bundle if language code not found / is malformed', () => {
    expect(getDataForLanguageCode(testLanguageData, 'abcdefg')).to.equal(enData);
    expect(getDataForLanguageCode(testLanguageData, 'abc-fdsagd')).to.equal(enData);
    expect(getDataForLanguageCode(testLanguageData, '')).to.equal(enData);
    expect(getDataForLanguageCode(testLanguageData, null)).to.equal(enData);
  });
});

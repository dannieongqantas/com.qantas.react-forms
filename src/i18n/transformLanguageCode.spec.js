import transformLanguageCode from './transformLanguageCode';

describe('transformLanguageCode', () => {
  it('should convert Chinese simplified codes correctly', () => {
    expect(transformLanguageCode('zh-cn')).to.equal('zh-hans');
    expect(transformLanguageCode('zh-sg')).to.equal('zh-hans');
  });

  it('should convert Chinese traditional codes correctly', () => {
    expect(transformLanguageCode('zh-tw')).to.equal('zh-hant');
    expect(transformLanguageCode('zh-hk')).to.equal('zh-hant');
  });

  it('should return lowercase version of code for easier comparison elsewhere', () => {
    expect(transformLanguageCode('zh-Hans')).to.equal('zh-hans');
    expect(transformLanguageCode('zh-HK')).to.equal('zh-hant');
  });

  it('should handle languageCode being empty / not defined', () => {
    expect(transformLanguageCode(null)).to.equal(null);
    expect(transformLanguageCode(undefined)).to.equal(undefined);
    expect(transformLanguageCode('')).to.equal('');
  });
});

import getRegionCode from './getRegionCode';

describe('getRegionCode', () => {
  it('should return the asian region', () => {
    expect(getRegionCode('ja-JP')).to.eq('as');
    expect(getRegionCode('zh-TW')).to.eq('as');
    expect(getRegionCode('zh-CN')).to.eq('as');
    expect(getRegionCode('id-ID')).to.eq('as');
  });

  it('should return the americas region', () => {
    expect(getRegionCode('es-AR')).to.eq('am');
    expect(getRegionCode('es-CL')).to.eq('am');
    expect(getRegionCode('es-MX')).to.eq('am');
  });

  it('should return the europe region', () => {
    expect(getRegionCode('fr-FR')).to.eq('eu');
    expect(getRegionCode('de-DE')).to.eq('eu');
  });

  it('should return the Australian region', () => {
    expect(getRegionCode('en-AU')).to.eq('au');
    expect(getRegionCode('en-RANDOM')).to.eq('au');
  });
});

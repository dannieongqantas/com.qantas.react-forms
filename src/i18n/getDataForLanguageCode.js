/* Works out which language JSON to use based on the language code.

  Option 1. Check if data for specific language+region/language+script/etc.; e.g. fr-ca, zh-hans
  Option 2. Check if data for base language code; e.g. fr, en
  Default option: use English, which should always be provided; e.g. "", null.

  Parameters:
  - allLanguagesData is a JSON object of all language JSON objects keyed by language code. Example:
      {
        "en": { helloLabel: "Hello" },
        "en-au": { helloLabel: "G'day" },
        "fr": { helloLabel: "Bonjour" }
      }

      NOTE: An "en" key with English labels should always exist to use as default.

  - languageCode is a language code string. Examples: "en", "en-au", "fr", "zh-hans", "", null

  NOTE:
    - All language codes (including languageCode parameter) should be lowercase.
*/

export function getDataForLanguageCode(allLanguagesData, languageCode) {
  return allLanguagesData[languageCode] ||
        (languageCode && allLanguagesData[languageCode.substring(0, 2)]) ||
        allLanguagesData.en;
}

export default getDataForLanguageCode;

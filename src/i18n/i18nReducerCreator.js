import getDataForLanguageCode from './getDataForLanguageCode';
import transformLanguageCode from './transformLanguageCode';

/*
  Example use:

	YourAppReducer.js
	---

	import i18nReducerCreator from 'com.qantas.widget-provider-engine/lib/i18n/i18nReducerCreator';
	import config from 'somewhere';
  	import allLanguagesData from '../i18n/';
  	// ^^ where index.js exports all language files keyed by lowercase language code

	export default combineReducers({
	  some: otherReducer,
	  i18n: i18nReducerCreator(allLanguagesData, config.languageCode)
	});

  Note: If you find you need custom behaviour, don't use this -- create your own version of i18nReducer in your widget.
  (e.g. Manage Booking widget does this to support using `variant` to change some labels)
*/

const i18nReducerCreator = (allLanguagesData, languageCode) => {
  const transformedLanguageCode = transformLanguageCode(languageCode);
  const initialState = getDataForLanguageCode(allLanguagesData, transformedLanguageCode);

  return (state = initialState) => state;
};

export default i18nReducerCreator;

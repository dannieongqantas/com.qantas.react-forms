export default function getRegionCode(languageCode) {
  if (process.env.NODE_ENV !== 'production') {
    console.error('Mappings need to be updated and tested. Remove this once updated'); //eslint-disable-line
  }
  const splitLanguageCode = languageCode.toLowerCase().split('-');

  const regionFromLanguageCode = splitLanguageCode[1];
  switch (regionFromLanguageCode) {
    case 'jp':
    case 'tw':
    case 'cn':
    case 'id':
      return 'as';
    case 'ar':
    case 'cl':
    case 'mx':
      return 'am';
    case 'de':
    case 'fr':
      return 'eu';
    // TODO: Need to add other mappings
    // case '':
    //   return 'sp';
    // case '':
    //   return 'af';
    default:
      return 'au';
  }
}

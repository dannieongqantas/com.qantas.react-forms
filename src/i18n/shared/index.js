import en from './en';
import fr from './fr';
import es from './es';
import de from './de';
import ja from './ja';
import id from './id';
import zhhans from './zh-hans';
import zhhant from './zh-hant';

export default {
  en,
  fr,
  es,
  de,
  ja,
  id,
  'zh-hans': zhhans,
  'zh-hant': zhhant
};

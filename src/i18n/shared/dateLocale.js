export default {
  en: () => import(/* webpackMode: "eager", webpackChunkName: "date-fns-en" */ 'date-fns/locale/en'),
  de: () => import(/* webpackChunkName: "date-fns-de" */ 'date-fns/locale/de'),
  es: () => import(/* webpackChunkName: "date-fns-es" */ 'date-fns/locale/es'),
  fr: () => import(/* webpackChunkName: "date-fns-fr" */ 'date-fns/locale/fr'),
  ja: () => import(/* webpackChunkName: "date-fns-ja" */ 'date-fns/locale/ja'),
  id: () => import(/* webpackChunkName: "date-fns-id" */ 'date-fns/locale/id'),
  'zh-hans': () => import(/* webpackChunkName: "date-fns-zh_cn" */ 'date-fns/locale/zh_cn'),
  'zh-hant': () => import(/* webpackChunkName: "date-fns-zh_tw" */ 'date-fns/locale/zh_tw')
};

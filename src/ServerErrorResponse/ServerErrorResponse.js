import React from 'react';

import PropTypes from 'prop-types';

const ServerErrorResponse = ({
	children,
	header
}) => <div className="server-error-card card">
  <h3 className="server-error-card__header card__heading">{header}</h3>
  <div className="server-error-card__message card__description">{children}</div>
</div>;

ServerErrorResponse.propTypes = {
  header: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)])
};

export default ServerErrorResponse;

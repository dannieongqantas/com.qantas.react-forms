import { shallow } from 'enzyme';

import ServerErrorResponse from './ServerErrorResponse';

describe('ServerErrorResponse', () => {
  const header = 'This is a summary';
  const children = 'This is the children prop';
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ServerErrorResponse header={header}>{children}</ServerErrorResponse>);
  });

  it('should render the summary text in a heading', () => {
    expect(wrapper.find('.server-error-card__header').text()).to.eq(header);
  });

  it('should render the message', () => {
    expect(wrapper.find('.server-error-card__message').text()).to.eq(children);
  });
});

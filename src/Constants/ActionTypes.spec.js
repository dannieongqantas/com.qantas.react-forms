import * as ActionTypes from './ActionTypes';

describe('ActionTypes', () => {
  it('should have the same value as the variable name', () => {
    Object.keys(ActionTypes)
      .forEach(item => expect(item).to.equal(ActionTypes[item]));
  });
});

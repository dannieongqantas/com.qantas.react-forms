import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RETURN_KEY_CODE, SPACE_KEY_CODE } from '../Constants/KeyCodes';
const INCREMENT_INTERVAL = 250;

export default class Svg extends Component {
  static propTypes = {
    tabbable: PropTypes.bool,
    backgroundColor: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
    className: PropTypes.string,
    role: PropTypes.string,
    incremental: PropTypes.bool
  };

  static defaultProps = {
    tabbable: false,
    disabled: false,
    incremental: false,
    // Background color for IE10, so transparent area can be clicked
    backgroundColor: '#fff'
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.incrementalStart = this.incrementalStart.bind(this);
    this.incrementalEnd = this.incrementalEnd.bind(this);

    this.interval = null;
  }

  handleKeyDown(e) {
    if (this.props.disabled) return;

    if ([RETURN_KEY_CODE, SPACE_KEY_CODE].indexOf(e.keyCode) > -1) {
      e.preventDefault();
      e.stopPropagation();
      this.props.onClick();
    }
  }

  incrementalStart() {
    this.props.onClick();
    this.interval = setInterval(this.props.onClick, INCREMENT_INTERVAL);
  }

  incrementalEnd(e) {
    e.preventDefault();
    clearInterval(this.interval);
  }

  render() {
    const { tabbable, backgroundColor, className, role, onClick, incremental, ...props } = this.props;

    const a11yProps = {
      'aria-hidden': true,
      role: 'presentation',
      tabIndex: '-1'
    };

    const style = { backgroundColor };

    const containerDivProps = {
      tabIndex: tabbable ? '0' : '-1',
      'aria-label': props['aria-label']
    };

    const incrementalProps = {};
    if (incremental) {
      incrementalProps.onTouchStart = this.incrementalStart;
      incrementalProps.onTouchEnd = this.incrementalEnd;
    }

    return <div className={className} onKeyDown={this.handleKeyDown} {...containerDivProps} role={role} onClick={onClick}>
      <svg {...props} version="1.1" {...a11yProps} {...incrementalProps} style={style} focusable="false">
        {this.props.children}
      </svg>
    </div>;
  }
}

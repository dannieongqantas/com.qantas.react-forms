import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';


export default class PlusIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    color: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    color: '#323232',
    className: '',
    onClick: () => {
    }
  };

  render() {
    return <Svg
      className={this.props.className}
      width="30px"
      height="30px"
      viewBox="0 0 30 30"
      onClick={this.props.onClick}
      incremental
      disabled={this.props.disabled}>
      <title>plus</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-278.000000, -1963.000000)" fill={this.props.color}>
          <g transform="translate(15.000000, 1872.000000)">
            <g transform="translate(10.000000, 60.000000)">
              <path
                d="M269,45 L269,41 L267,41 L267,45 L263,45 L263,47 L267,47 L267,51 L269,51 L269,47 L273,47 L273,45 L269,45 Z" />
            </g>
          </g>
        </g>
      </g>
    </Svg>;
  }
}

import React from 'react';
import Svg from './Svg';
import { findDOMNode } from 'react-dom';

describe('Svg', () => {
  let svgComponent;
  let svg;
  let onClickSpy;

  beforeEach(() => {
    onClickSpy = sinon.spy();

    svgComponent = TestUtils.renderIntoDocument(
      <Svg onClick={onClickSpy} incremental />
    );
    svg = findDOMNode(svgComponent).querySelector('svg');
  });

  it('should contain the focusable="false" attribute to prevent ie from focusing on Svgs', () => {
    expect(svg.getAttribute('focusable')).to.equal('false');
  });

  it('should contain the version="1.1" attribute to prevent ie from focusing on Svgs', () => {
    expect(svg.getAttribute('version')).to.equal('1.1');
  });

  it('should call the onClick function incrementally', (done) => {
    TestUtils.Simulate.touchStart(svg);
    const interval = 250;
    setTimeout(() => {
      TestUtils.Simulate.touchEnd(svg);
      expect(onClickSpy).to.have.been.calledThrice;

      setTimeout(() => {
        // interval should be stopped and not called more times.
        expect(onClickSpy).to.have.been.calledThrice;
        done();
      }, interval + 50);
    }, interval * 2 + 50);
  });
});

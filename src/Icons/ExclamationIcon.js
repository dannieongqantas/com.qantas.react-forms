import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SvgComponent from './Svg';

export default class ExclamationIcon extends Component {
  static propTypes = {
    className: PropTypes.string
  };

  static defaultProps = {
    className: ''
  };

  componentDidMount() {
    // Todo: After react upgrade check if this will work
    this.refs.text.setAttribute('font-weight', '600'); // For some reason font-weight doesnt work as a prop
  }

  componentDidUpdate() {
    this.refs.text.setAttribute('font-weight', '600');
  }

  render() {
    return <SvgComponent className={this.props.className} width="20px" height="20px" viewBox="0 0 20 20" backgroundColor="">
      <title>error icon</title>
      <g stroke="none" strokeWidth="1.5" fill="none" fillRule="evenodd">
        <g transform="translate(-30.000000, -432.000000)">
          <g transform="translate(15.000000, 98.000000)">
            <g transform="translate(0.000000, 318.000000)">
              <g>
                <g transform="translate(16.000000, 17.000000)">
                  <path d="M16.601377,8.89555594 C17.1328743,8.40101081 17.1328743,7.59898919 16.601377,7.10444406 C16.4622516,6.974939 3.04502015,-0.825426978 2.96573664,-0.86244336 L2.95573489,-0.868351342 L2.95506062,-0.868037644 C2.77474822,-0.949912862 2.57505042,-1 2.36130521,-1 C1.68736726,-1 1.13176459,-0.543150924 1.02303773,0.0553851516 L1,15.7336006 C1,16.4329383 1.6094323,17 2.36102427,17 C2.57342092,17 2.77177017,16.9508017 2.95067783,16.8702335 L16.601377,8.89555594 Z" stroke="#323232" transform="translate(9.000000, 8.000000) rotate(-90.000000) translate(-9.000000, -8.000000) " />
                  <text ref="text" fontFamily="Arial Black, Gadget, sans-serif" fontSize="11" fill="#323232">
                    <tspan x="7.2" y="13">!</tspan>
                  </text>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </SvgComponent>;
  }
}

import React, { Component } from 'react';

import PropTypes from 'prop-types';

export default class BorderTriangle extends Component {
  static propTypes = {
    innerColor: PropTypes.string,
    outerColor: PropTypes.string,
    positionFromLeft: PropTypes.string,
    innerSize: PropTypes.string,
    outerSize: PropTypes.string,
    zIndex: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  };

  static defaultProps = {
    innerColor: '#f4f5f6',
    outerColor: '#dadada',
    positionFromLeft: '50%',
    innerSize: '9px',
    outerSize: '11px',
    zIndex: 300
  };

  render() {
    const { ...props } = this.props;
    const innerStyles = {
      borderBottomColor: props.innerColor,
      left: props.positionFromLeft,
      borderBottomWidth: props.innerSize,
      borderRightWidth: props.innerSize,
      borderLeftWidth: props.innerSize,
      marginLeft: `-${props.innerSize}`,
      zIndex: props.zIndex + 1
    };

    const outerStyles = {
      borderBottomColor: props.outerColor,
      left: props.positionFromLeft,
      borderBottomWidth: props.outerSize,
      borderRightWidth: props.outerSize,
      borderLeftWidth: props.outerSize,
      marginLeft: `-${props.outerSize}`,
      zIndex: props.zIndex
    };

    return <div>
      <span className="qfa1-border-triangle__outer" style={outerStyles} />
      <span className="qfa1-border-triangle__inner" style={innerStyles} />
    </div>;
  }
}

import TestUtils from 'react-dom/test-utils';
import ComponentStub from '../../test/utils/ComponentStub';
const Svg = ComponentStub();
const ClassicRewardsIcon = proxyquire('Icons/CalendarStarIcon', {
  './Svg': Svg
});

const iconOnClickStub = () => {};
const baseProps = {
  className: 'someClassName',
  fillClass: 'someClassName',
  height: '13px',
  width: '12px',
  onClick: iconOnClickStub
};

function createComponent(newProps = {}) {
  const props = {
    ...baseProps,
    ...newProps
  };

  const shallowRenderer = TestUtils.createRenderer();
  shallowRenderer.render(<ClassicRewardsIcon {...props} />);
  return shallowRenderer.getRenderOutput();
}

describe('CalendarStarIcon', () => {
  const renderedComponent = createComponent({});
  const { height, width, className, children, onClick } = renderedComponent.props;

  it('should render with className as the passed props', () => {
    expect(className).to.equal(baseProps.className);
  });

  it('should render with width and height as the passed props', () => {
    expect(height).to.equal(baseProps.height);
    expect(width).to.equal(baseProps.width);
  });

  it('should render as an Svg component', () => {
    expect(renderedComponent.type).to.equal(Svg);
  });

  it('should render with a title and the g element', () => {
    expect(children.length).to.equal(2);
    expect(children[0].type).to.equal('title');
    expect(children[1].type).to.equal('g');
  });

  it('should pass the onClick prop type to the svg element', () => {
    expect(onClick).to.equal(baseProps.onClick);
  });

  it('should pass the default props when invalid props are passed', () => {
    const invalidProps = {
      className: undefined,
      height: undefined,
      width: undefined,
      onClick: undefined
    };
    const invalidComponent = createComponent(invalidProps);
    expect(invalidComponent.props.height).to.equal('12px');
    expect(invalidComponent.props.width).to.equal('13px');
    expect(invalidComponent.props.className).to.equal('');
  });
});

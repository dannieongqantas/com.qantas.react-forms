import { shallow } from 'enzyme';
import RadiobuttonIcon from './RadiobuttonIcon';
import Svg from './Svg';

const sandbox = sinon.sandbox.create();

const defaultProps = {
  size: '24',
  checked: false,
  focused: false,
  disabled: false
};

describe('RadiobuttonIcon', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<RadiobuttonIcon />);
  });

  afterEach(() => {
    sandbox.reset();
  });

  it('should render with default props', () => {
    expect(wrapper).to.exist;
    expect(wrapper.instance().props.size).to.equal(defaultProps.size);
    expect(wrapper.instance().props.checked).to.equal(defaultProps.checked);
    expect(wrapper.instance().props.focused).to.equal(defaultProps.focused);
    expect(wrapper.instance().props.disabled).to.equal(defaultProps.disabled);
  });

  it('should render 1 SVG by default', () => {
    expect(Svg).to.have.length(1);
  });

  it('should render 2 SVGs by default if checked=true', () => {
    wrapper = shallow(<RadiobuttonIcon checked />);

    expect(wrapper.find(Svg)).to.have.length(2);
  });
});


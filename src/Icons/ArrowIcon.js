import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Svg from './Svg';

export default class ArrowIcon extends Component {
  static propTypes = {
    tabbable: PropTypes.bool,
    onClick: PropTypes.func,
    className: PropTypes.string,
    direction: PropTypes.oneOf(['left', 'right', 'down', 'up']).isRequired,
    hide: PropTypes.bool,
    size: PropTypes.string,
    circle: PropTypes.bool,
    disabled: PropTypes.bool,
    'aria-label': PropTypes.string
  };

  static defaultProps = {
    tabbable: false,
    className: '',
    onClick: () => {},
    hide: false,
    size: '24px',
    circle: false
  };

  constructor(props) {
    super(props);
    this.renderLeft = this.renderLeft.bind(this);
    this.renderRight = this.renderRight.bind(this);
    this.renderUp = this.renderUp.bind(this);
    this.renderDown = this.renderDown.bind(this);
  }

  renderRight() {
    const circle = this.props.circle ? <circle fill="#FFFFFF" cx="24" cy="24" r="24" /> : null;
    return <g transform="translate(-562, -240)">
      <g transform="translate(-5, 176)">
        <g transform="translate(35, 64)">
          <g transform="translate(532, 0)">
            {circle}
            <path d="M34,19.8026316 L31.2456897,17 L25,23.3552632 L18.7543103,17 L16,19.8026316 L25,29 L34,19.8026316 Z" transform="translate(25, 23) rotate(-90) translate(-25, -23)" />
          </g>
        </g>
      </g>
    </g>;
  }

  renderLeft() {
    const circle = this.props.circle ? <circle fill="#FFFFFF" cx="24" cy="24" r="24" /> : null;
    return <g transform="translate(-30, -240)">
      <g transform="translate(-5, 176)">
        <g transform="translate(35, 64)">
          <g transform="translate(24, 24) scale(-1, 1) translate(-24, -24) ">
            {circle}
            <path d="M34,19.8026316 L31.2456897,17 L25,23.3552632 L18.7543103,17 L16,19.8026316 L25,29 L34,19.8026316 Z" transform="translate(25, 23) rotate(-90) translate(-25, -23)" />
          </g>
        </g>
      </g>
    </g>;
  }

  renderUp() {
    return <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-1172, -1755)">
        <g transform="translate(425, 1408)">
          <g transform="translate(532, 281)">
            <path d="M248,87.8026316 L245.24569,85 L239,91.3552632 L232.75431,85 L230,87.8026316 L239,97 L248,87.8026316 Z" transform="translate(239, 91) scale(1, -1) translate(-239, -91) " />
          </g>
        </g>
      </g>
    </g>;
  }

  renderDown() {
    return <g transform="translate(-1972, -1755)" >
      <g transform="translate(485, 1478)">
        <g transform="translate(1275, 210)">
          <path d="M245,88.8026316 L242.24569,86 L236,92.3552632 L229.75431,86 L227,88.8026316 L236,98 L245,88.8026316 Z" />
        </g>
      </g>
    </g>;
  }

  render() {
    const { ...props } = this.props;
    if (props.hide) return null;
    const svgBody = {
      left: this.renderLeft,
      right: this.renderRight,
      down: this.renderDown,
      up: this.renderUp
    }[props.direction]();

    const className = classNames(props.className, 'qfa1-arrow-icon', {
      'qfa1-arrow-icon--disabled': props.disabled
    });

    return <Svg width={props.size}
                height={props.size}
                viewBox="0 0 48 48"
                tabbable={props.tabbable}
                role="button"
                disabled={props.disabled}
                aria-label={props['aria-label']}
                className={className}
                type="button"
                onClick={props.disabled ? () => {} : props.onClick}
                onMouseDown={e => e.preventDefault()} /* Prevent highlighting when clicking fast */
                backgroundColor={this.props.circle ? '' : '#FFFFFF'} >
      <title>Arrow {props.direction}</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        {svgBody}
      </g>

    </Svg>;
  }
}

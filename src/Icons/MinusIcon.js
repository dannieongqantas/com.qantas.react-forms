import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';

export default class MinusIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    color: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    color: '#404040',
    className: '',
    onClick: () => {
    }
  };

  render() {
    return <Svg
      className={this.props.className}
      width="30px"
      height="30px"
      viewBox="0 0 30 6"
      onClick={this.props.onClick}
      incremental
      disabled={this.props.disabled}>
      <title>minus</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-32.000000, -1967.000000)" fill={this.props.color}>
          <g transform="translate(15.000000, 1872.000000)">
            <g transform="translate(0.000000, 50.000000)">
              <rect x="27" y="48" width="10" height="2" />
            </g>
          </g>
        </g>
      </g>
    </Svg>;
  }
}

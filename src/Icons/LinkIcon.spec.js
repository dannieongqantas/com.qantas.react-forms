import TestUtils from 'react-dom/test-utils';
import ComponentStub from '../../test/utils/ComponentStub';
const Svg = ComponentStub();
const LinkIcon = proxyquire('Icons/LinkIcon', {
  './Svg': Svg
});

const baseProps = {
  className: 'someClassName',
  fillClass: 'fillClassName',
  width: '25px',
  height: '25px',
  title: 'icon name'
};

function createComponent(newProps = {}) {
  const props = {
    ...baseProps,
    ...newProps
  };

  const shallowRenderer = TestUtils.createRenderer();
  shallowRenderer.render(<LinkIcon {...props} />);
  return shallowRenderer.getRenderOutput();
}


describe('LinkIcon', () => {
  const renderedComponent = createComponent({});
  const { height, width, className, children } = renderedComponent.props;

  it('should render with className as the passed props', () => {
    expect(className).to.equal(baseProps.className);
  });

  it('should render with width and height as the passed props', () => {
    expect(height).to.equal(baseProps.height);
    expect(width).to.equal(baseProps.width);
  });

  it('should render as an Svg component', () => {
    expect(renderedComponent.type).to.equal(Svg);
  });

  it('should render with a title and the g element', () => {
    expect(children.length).to.equal(2);
    expect(children[0].type).to.equal('title');
    expect(children[1].type).to.equal('g');
  });

  it('should pass the fillClass prop type to the g element', () => {
    expect(children[1].props.children.props.className).to.equal(baseProps.fillClass);
  });

  it('should pass the default props when invalid props are passed', () => {
    const invalidProps = {
      className: undefined,
      height: undefined,
      width: undefined,
      title: undefined,
      fillClass: undefined
    };
    const defaultComponent = createComponent(invalidProps);

    expect(defaultComponent.props.height).to.equal('20px');
    expect(defaultComponent.props.width).to.equal('20px');
    expect(defaultComponent.props.className).to.equal('svg__link-icon');
    expect(defaultComponent.props.children[1].props.children.props.className).to.equal('svg__link-icon__fill');
  });
});


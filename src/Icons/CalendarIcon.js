import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';

export default class CalendarIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    fillClass: PropTypes.string,
    onClick: PropTypes.func
  };

  static defaultProps = {
    className: '',
    fillClass: '',
    width: '15px',
    height: '16px',
    onClick: () => {}
  };

  render() {
    const { ...props } = this.props;
    return <Svg className={props.className}
                onClick={props.onClick}
                width={props.width}
                height={props.height}
                viewBox="0 0 30 32">

      <title>CALENDAR</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1612.000000, -1745.000000)" className={props.fillClass}>
          <g transform="translate(425.000000, 1408.000000)">
            <g transform="translate(860.000000, 262.000000)">
              <g transform="translate(327.000000, 75.000000)">
                <path d="M10,12 L14,12 L14,16 L10,16 L10,12 Z" />
                <path d="M16,12 L20,12 L20,16 L16,16 L16,12 Z" />
                <path d="M22,12 L26,12 L26,16 L22,16 L22,12 Z" />
                <path d="M4,18 L8,18 L8,22 L4,22 L4,18 Z" />
                <path d="M10,18 L14,18 L14,22 L10,22 L10,18 Z" />
                <path d="M16,18 L20,18 L20,22 L16,22 L16,18 Z" />
                <path d="M22,18 L26,18 L26,22 L22,22 L22,18 Z" />
                <path d="M4,24 L8,24 L8,28 L4,28 L4,24 Z" />
                <path d="M10,24 L14,24 L14,28 L10,28 L10,24 Z" />
                <path d="M16,24 L20,24 L20,28 L16,28 L16,24 Z" />
                <path d="M26,4 L26,1 C26,0.45 25.552,0 25,0 L21,0 C20.448,0 20,0.45 20,1 L20,4 L10,4 L10,1 C10,0.45 9.552,0 9,0 L5,0 C4.448,0 4,0.45 4,1 L4,4 L0,4 L0,32 L30,32 L30,4 L26,4 L26,4 Z M22,2 L24,2 L24,6 L22,6 L22,2 Z M6,2 L8,2 L8,6 L6,6 L6,2 Z M2,10 L28,10 L28,30 L2,30 L2,10 Z" />
              </g>
            </g>
          </g>
        </g>
      </g>

    </Svg>;
  }
}

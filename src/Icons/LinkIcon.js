import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';

export default class LinkIcon extends Component {
  static propTypes = {
    title: PropTypes.string,
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    fillClass: PropTypes.string,
    onClick: PropTypes.func
  };

  static defaultProps = {
    title: '',
    className: 'svg__link-icon',
    fillClass: 'svg__link-icon__fill',
    width: '20px',
    height: '20px',
    onClick: () => {}
  };

  render() {
    const { fillClass, className, onClick, width, height, title } = this.props;
    return <Svg backgroundColor="transparent"
                className={className}
                onClick={onClick}
                width={width}
                height={height}
                aria-hidden="true"
                viewBox="0 0 15 20">

      <title>{title}</title>
      <g transform="translate(5, 8.000000)" stroke="none" strokeWidth="1" fillRule="evenodd" >
        <g className={fillClass} >
          <polygon points="0 3 0 3.99939605 9.06000042 3.99939605 6.78459383 6.28999996 7.5 7 11 3.5 7.5 0 6.78459383 0.707000017 9.06276842 3" />
        </g>
      </g>
    </Svg>;
  }
}

import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';

import ClearButton from './ClearButton';

describe('ClearButton', () => {
  it('should render properly', () => {
    const renderedComponent = TestUtils.renderIntoDocument(<ClearButton hide={false} />);
    const renderedElement = findDOMNode(renderedComponent);
    expect(renderedElement).to.exist;
  });

  it('should hide the component by default', () => {
    const renderedComponent = TestUtils.renderIntoDocument(<ClearButton />);
    const renderedElement = findDOMNode(renderedComponent);
    expect(renderedElement).to.not.exist;
  });
});

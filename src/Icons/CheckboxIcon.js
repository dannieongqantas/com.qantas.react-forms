import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Svg from './Svg';

export default class CheckboxIcon extends Component {
  static propTypes = {
    size: PropTypes.string,
    checked: PropTypes.bool,
    focused: PropTypes.bool
  };

  static defaultProps = {
    size: '24',
    checked: false,
    focused: false
  };

  constructor(props) {
    super(props);
    this.renderBox = this.renderBox.bind(this);
    this.renderTick = this.renderTick.bind(this);
  }

  renderBox() {
    const { size, focused } = this.props;
    const boxClass = classNames('checkbox-icon__box-border', {
      'checkbox-icon__box-border--focused': focused
    });

    return <Svg className="checkbox-icon__box"
                width={`${size}px`}
                height={`${size}px`}
                viewBox={`0 0 ${size} ${size}`}>
      <title>Checkbox</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1286.000000, -1544.000000)" className={boxClass} strokeWidth="4" fill="#FFFFFF">
          <g transform="translate(485.000000, 1478.000000)">
            <rect x="801" y="66" width={size} height={size}></rect>
          </g>
        </g>
      </g>
    </Svg>;
  }

  renderTick() {
    const { size, checked } = this.props;
    if (!checked) return null;

    return (<Svg className="checkbox-icon__tick" width={`${size}px`} height={`${size}px`} viewBox={`0 0 ${size * 2} ${size * 2}`} version="1.1" backgroundColor="">
      <title>selected</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1297.000000, -1557.000000)" fill="#4A4A4A">
          <g transform="translate(497.000000, 1490.000000)">
            <path d="M823.010797,78.5423199 L823.010797,98.365835 L819.170797,98.365835 L819.170797,74.365835 L823.010797,74.365835 L823.010797,74.7023199 L830.10527,74.7023199 L830.10527,78.5423199 L823.010797,78.5423199 Z" transform="translate(824.638034, 86.365835) rotate(-135.000000) translate(-824.638034, -86.365835) "></path>
          </g>
        </g>
      </g>
    </Svg>);
  }

  render() {
    return <div className="checkbox-icon" role="presentation" onMouseDown={e => e.preventDefault()} /* Prevent highlighting when clicking fast */>
      {this.renderBox()}
      {this.renderTick()}
    </div>;
  }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';

export default class ClearButton extends Component {
  static propTypes = {
    size: PropTypes.string,
    className: PropTypes.string,
    svgClass: PropTypes.string,
    circleClass: PropTypes.string,
    crossClass: PropTypes.string,
    title: PropTypes.string,
    onClick: PropTypes.func,
    hide: PropTypes.bool,
    focusable: PropTypes.bool,
    tabbable: PropTypes.bool
  };

  static defaultProps = {
    size: '20px',
    className: '',
    svgClass: 'svg__clear-button',
    circleClass: 'svg__clear-button-circle',
    crossClass: 'svg__clear-button-cross',
    hide: true,
    focusable: false,
    title: '',
    tabbable: true
  };

  render() {
    const props = this.props;

    if (props.hide) return null;

    return (<Svg onClick={props.onClick}
                tabbable={props.tabbable}
                className={props.className} role="button" aria-label={props.title}
                width={props.size}
                height={props.size}
                viewBox="0 0 40 40">
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1150.000000, -1548.000000)">
          <g transform="translate(485.000000, 1478.000000)">
            <g>
              <g className={props.svgClass} transform="translate(665.000000, 70.000000)">
                <circle className={props.circleClass} id="Oval-23" cx="20" cy="20" r="20" />
                <path d="M20.1548387,17.4915676 L25.5517241,12 L28,14.4912281 L22.6094421,20 L28,25.5087719 L25.5517241,28 L20.1548387,22.5084324 L20,22.6666667 L19.8451613,22.5084324 L14.4482759,28 L12,25.5087719 L17.3905579,20 L12,14.4912281 L14.4482759,12 L19.8451613,17.4915676 L20,17.3333333 L20.1548387,17.4915676 Z" className={this.props.crossClass} />
              </g>
            </g>
          </g>
        </g>
      </g>
    </Svg>);
  }
}

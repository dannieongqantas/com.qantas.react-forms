import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Svg from './Svg';

export default class RadiobuttonIcon extends Component {
  static propTypes = {
    size: PropTypes.string,
    checked: PropTypes.bool,
    focused: PropTypes.bool,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    size: '24',
    checked: false,
    focused: false,
    disabled: false
  };

  constructor(props) {
    super(props);
    this.renderBox = this.renderBox.bind(this);
    this.renderTick = this.renderTick.bind(this);
  }

  renderBox() {
    const { size, focused, disabled } = this.props;
    const boxClass = classNames('radiobutton-icon__box-border', {
      'radiobutton-icon__box-border--focused': focused,
      'radiobutton-icon__box-border--disabled': disabled
    });


    return <Svg backgroundColor=""
                className="radiobutton-icon__box"
                width={`${size}px`}
                height={`${size}px`}
                viewBox={`0 0 ${size} ${size}`}>
      <title>Radiobutton</title>
      <g stroke="none" strokeWidth="2" fill="none" fillRule="evenodd">
        <g transform="translate(-1286.000000, -1543.000000)" className={boxClass}>
          <g transform="translate(485.000000, 1478.000000)">
            <circle cx="813" cy="77" r="10" fill="#ffffff" width="24" height="24"></circle>
          </g>
        </g>
      </g>
    </Svg>;
  }

  renderTick() {
    const { size, checked } = this.props;
    if (!checked) return null;

    return (<Svg className="radiobutton-icon__check" backgroundColor="" width={`${size}px`} fill="none" height={`${size}px`} viewBox={`0 0 ${size * 2} ${size * 2}`} version="1.1">
      <title>selected</title>
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1297.000000, -1555.000000)">
          <g transform="translate(497.000000, 1490.000000)">
            <circle cx="824" cy="89" r="8" fill="#4A4A4A" width={size} height={size} />
          </g>
        </g>
      </g>
    </Svg>);
  }

  render() {
    return (
      <div
        className="radiobutton-icon"
        role="presentation"
        onMouseDown={e => e.preventDefault()}
        onClick={this.props.onClick}
      >
        {this.renderBox()}
        {this.renderTick()}
      </div>
    );
  }
}

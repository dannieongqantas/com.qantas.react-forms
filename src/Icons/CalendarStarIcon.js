import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Svg from './Svg';

export default class CalendarStarIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    fillClass: PropTypes.string,
    onClick: PropTypes.func
  };

  static defaultProps = {
    className: '',
    fillClass: '',
    width: '13px',
    height: '12px',
    onClick: () => {}
  };

  render() {
    const props = this.props;
    return (
      <Svg
        className={props.className}
        onClick={props.onClick}
        width={props.width}
        height={props.height}
        viewBox="0 0 306 294"
        backgroundColor="transparent"
      >
        <title>Star</title>
        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="star" fill="#323232">
            <g id="rate-star-button">
              <polygon
                id="Path"
                points="153 224.775 247.35 293.625 211.65 181.425 306 115.125 191.25 115.125 153 0.375 114.75 115.125 0 115.125 94.35 181.425 58.65 293.625"
              />
            </g>
          </g>
        </g>
      </Svg>
    );
  }
}

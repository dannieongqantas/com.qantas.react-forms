import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import classNames from 'classnames';
import FormValidationMessage from '../FormValidation/FormValidationMessage';
import WindowResize from '../ReusableComponents/WindowResize';
import breakpoints from '../grid/breakpoints';
import { scrollToTop } from '../utils/DOM';
import { isTouchDevice, isIE } from '../utils/userAgent';
import calculateSize from 'calculate-size';

/* @todo Code including handling isLabelInline duplicated across Input and InputArea. Investigate if we can merge these components or at least have InputArea use Input. */

// Delay only for touch device
// Adding a debounce time causes an issue for the NVDA screen reader,
// typing 'Sydney' then 'Sydney P' in the typeahead causes it to think there are 20 results
export const DEBOUNCE_TIME = isTouchDevice() ? 250 : 0;

export class InputArea extends Component {
  static propTypes = {
    activeDescendant: PropTypes.string,
    ariaLabel: PropTypes.string.isRequired,
    autoComplete: PropTypes.oneOf(['off', 'on']),
    children: PropTypes.node,
    className: PropTypes.string,
    dropdown: PropTypes.bool,
    focused: PropTypes.bool,
    focusMe: PropTypes.func,
    formLabel: PropTypes.string.isRequired,
    id: PropTypes.string,
    isLabelInline: PropTypes.bool,
    noEntry: PropTypes.bool,
    onBlur: PropTypes.func,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func,
    onKeyDown: PropTypes.func,
    popup: PropTypes.bool,
    required: PropTypes.bool,
    type: PropTypes.oneOf(['text']),
    validationMessage: PropTypes.string,
    value: PropTypes.string,
    windowWidth: PropTypes.number,
    selectOnFocus: PropTypes.bool
  };

  static defaultProps = {
    activeDescendant: '',
    dropdown: false,
    isLabelInline: false,
    noEntry: false,
    onChange: () => { },
    onFocus: () => { },
    required: false,
    type: 'text',
    validationMessage: '',
    selectOnFocus: false
  };

  constructor(props) {
    super(props);

    this.debouncedOnChange = debounce(this.debouncedOnChange, DEBOUNCE_TIME);

    this.state = {
      value: props.value,
      inlineLabelWidth: null
    };
  }

  componentDidMount() {
    this.setInlineLabelWidthIfNeeded();
  }

  componentWillReceiveProps(props) {
    this.setState({ value: props.value });
  }

  componentDidUpdate() {
    if (this.props.focused) {
      this.focus();
    }

    this.inputElement.setAttribute('value', this.props.value);

    this.setInlineLabelWidthIfNeeded(); // in case it didn't get set in componentDidMount
  }

  setInlineLabelWidthIfNeeded() {
    if (this.props.isLabelInline && !this.state.inlineLabelWidth) {
      const { formLabel } = this.props;
      const el = this.labelElement;
      const { fontFamily, fontSize, fontWeight, paddingLeft, paddingRight } = window.getComputedStyle(el);
      const calcWidth = calculateSize(formLabel, { font: fontFamily, fontSize, fontWeight }).width;
      const inlineLabelWidth = parseInt(calcWidth, 10) + parseInt(paddingLeft, 10) + parseInt(paddingRight, 10);
      this.setState({ inlineLabelWidth: `${inlineLabelWidth}px` });
    }
  }

  focus = () => {
    this.inputElement.focus();
  }

  blur = () => {
    this.inputElement.blur();
  }

  handleChange = (event) => {
    const { value } = event.target;
    this.setState({ value });
    this.debouncedOnChange({ ...event });
  }

  debouncedOnChange = (event) => {
    this.props.onChange(event);
  }

  render() {
    const {
      children,
      popup,
      formLabel,
      ariaLabel,
      activeDescendant,
      validationMessage,
      windowWidth,
      noEntry,
      dropdown,
      isLabelInline,
      ...props
    } = this.props;

    const validationError = validationMessage && !popup;

    const inputProps = {
      ...props,
      autoComplete: 'off',
      'aria-required': this.props.required,
      value: this.state.value
    };

    // don't pass invalid props to the input element
    delete inputProps.focused;

    if (!props.readOnly && !noEntry) {
      inputProps.onChange = this.handleChange;
    } else if (noEntry) {
      // Jaws cant handle readOnly and keyboard actions. Without readOnly, it will bring up keyboard on touch devices.
      if (isTouchDevice()) {
        inputProps.readOnly = true;
      } else {
        inputProps.onChange = () => {};
      }
    }

    if (typeof popup !== 'undefined') {
      inputProps['aria-haspopup'] = popup;
      inputProps['aria-expanded'] = popup;
    }

    inputProps['aria-invalid'] = !!validationError;
    inputProps.className = classNames('qfa1-input', props.className, {
      'qfa1-input--validation-error': validationError
    });

    inputProps.onFocus = (event) => {
      if (windowWidth < breakpoints.medium) {
        scrollToTop(this.labelElement);
      }
      if (props.selectOnFocus) {
        const input = event.target;
        setTimeout(() => { // required for Edge and iOS
          input.setSelectionRange(0, input.value.length);
        }, 0);
      }
      props.onFocus(event);
    };

    inputProps.onMouseUp = (event) => {
      if (props.selectOnFocus) {
        event.preventDefault();
      }
    };

    // only add when it exists
    if (activeDescendant)
      inputProps['aria-activedescendant'] = activeDescendant;

    // Aria Label
    inputProps['aria-label'] = isIE() ? `${this.props.value}, ` : '';
    inputProps['aria-label'] += `${formLabel}, `;
    inputProps['aria-label'] += validationError ? validationMessage : ariaLabel;

    const classes = classNames({
      'widget-form__group': !dropdown,
      'widget-form__group--label-inline': isLabelInline
    });

    const inputStyles = {
      paddingLeft: isLabelInline ? this.state.inlineLabelWidth : ''
    };
    /* ^ Note: At the moment, we expect `isLabelInline` to be set to `false` when input is focused
        (collapsing/expanding container on homepage).
        If in future, the <input> needs to be focused while isLabelInline === true, we just need to:
        - add `isFocused` to state that reflects whether focused or not
        - expand `inputStyles` `paddingLeft` condition to `(isLabelInline && !isFocused)`
        - add a `display` style to the label like `display: isFocused ? 'none' : ''`
    */

    return <div className={classes}>
      <label ref={(el) => { this.labelElement = el; }}
        className="widget-form__label"
        htmlFor={inputProps.id}>
        {formLabel}
      </label>


      <input
        ref={(el) => { this.inputElement = el; }}
        style={inputStyles}
        {...inputProps}
      />
      {children}
      <FormValidationMessage hide={popup} message={validationMessage} />
    </div>;
  }
}

export default WindowResize(InputArea, breakpoints.medium);

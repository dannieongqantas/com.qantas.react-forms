import React from 'react';
import { findDOMNode, render } from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import ComponentStub from '../../test/utils/ComponentStub';
const proxyquire = require('proxyquire').noCallThru();

const scrollToTop = {
  scrollToTop: sinon.spy()
};

const formValidationMessage = ComponentStub();

let isIE = false;

import './InputArea'; // import so it's saved in the cache
const labelWidth = '100px';
const calculateSizeStub = sinon.stub().returns({ width: labelWidth });

const { InputArea } = proxyquire('./InputArea', {
  '../utils/DOM': scrollToTop,
  '../FormValidation/FormValidationMessage': formValidationMessage,
  'calculate-size': calculateSizeStub,
  '../utils/userAgent': {
    isIE() {
      return isIE;
    },
    isTouchDevice() {
      return false;
    }
  }
});

const props = {
  formLabel: 'hello you',
  ariaLabel: 'coolios'
};

function createComponent(newProps = {}) {
  return TestUtils.renderIntoDocument(
    <InputArea {...props} {...newProps} />
  );
}

describe('InputArea', () => {
  beforeEach(() => {
    isIE = false;
    scrollToTop.scrollToTop = sinon.spy();
  });

  it('should render an input element', () => {
    const input = createComponent();
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm).to.exist;
  });

  it('should return a debounce delay timer of 0 when not on a touch device', () => {
    const { DEBOUNCE_TIME } = proxyquire('./InputArea', {
      '../utils/userAgent': {
        isTouchDevice: () => false
      }
    });

    expect(DEBOUNCE_TIME).to.equal(0);
  });

  it('should should props as attributes', () => {
    const expectedValue = 'testId';
    const input = createComponent({ id: expectedValue });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm.getAttribute('id')).to.be.equal(expectedValue);
  });

  it('should also set aria-required when required is flagged', () => {
    const input = createComponent({ required: true });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm.getAttribute('required')).to.exist;
    expect(inputElm.getAttribute('aria-required')).to.exist;
  });

  it('should also set aria-activedescendant attribute when activeDescendant is passed down', () => {
    const expectedValue = 'something';
    const input = createComponent({ activeDescendant: expectedValue });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm.getAttribute('aria-activedescendant')).to.be.equal(expectedValue);
  });

  it('should also set aria-haspopup and aria-expanded attribute when popup is passed down', () => {
    const popup = true;
    const input = createComponent({ popup });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm.getAttribute('aria-haspopup')).to.be.equal(popup.toString());
    expect(inputElm.getAttribute('aria-expanded')).to.be.equal(popup.toString());
  });

  it('should also set aria-invalid and contain an validation-error style class', () => {
    const input = createComponent({ validationMessage: 'isInvalid', popup: false });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    expect(inputElm.getAttribute('aria-invalid')).to.be.equal('true');
    expect(inputElm.getAttribute('class')).to.be.contain('validation-error');
  });

  it('should call scrollToTop when InputArea is in a mobile and when focused', () => {
    const input = createComponent({ windowWidth: 100 });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    TestUtils.Simulate.focus(inputElm);
    expect(scrollToTop.scrollToTop).to.have.been.called;
  });

  it('should not call scrollToTop when InputArea is in a desktop and when focused', () => {
    const input = createComponent({ windowWidth: 1000 });
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
    TestUtils.Simulate.focus(inputElm);
    expect(scrollToTop.scrollToTop).to.not.have.been.called;
  });

  it('should contain a label with the formLabel prop as text', () => {
    const text = 'hello world';
    const input = TestUtils.renderIntoDocument(<InputArea formLabel={text} />);
    const labelElm = TestUtils.findRenderedDOMComponentWithTag(input, 'label');
    expect(labelElm.textContent).to.equal(text);
  });

  it('should contain a formValidationMessage component when error props are passed', () => {
    const error = 'omg!';
    const input = TestUtils.renderIntoDocument(<InputArea validationMessage={error} popup={false} />);
    const formValidationElm = TestUtils.findRenderedComponentWithType(input, formValidationMessage);
    expect(formValidationElm.props.message).to.equal(error);
    expect(formValidationElm.props.hide).to.be.false;
  });

  it('should call onFocus when focused', () => {
    const node = document.createElement('div');
    const input = render(<InputArea {...props} />, node);
    const focusSpy = sinon.spy(input.inputElement, 'focus');
    render(<InputArea {...props} focused />, node);
    expect(focusSpy).to.have.been.called;
  });

  describe('aria-label', () => {
    const formLabel = 'test item';
    const ariaLabel = 'read item';
    const value = 'the value';

    it('should create an aria label containing the form label and aria label', () => {
      const input = TestUtils.renderIntoDocument(<InputArea value={value} formLabel={formLabel}
                                                            ariaLabel={ariaLabel} />);
      const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
      expect(inputElm.getAttribute('aria-label')).to.equal(`${formLabel}, ${ariaLabel}`);
    });

    it('should contain the value in the aria-label when the userAgent is MSIE', () => {
      isIE = true;
      const input = TestUtils.renderIntoDocument(<InputArea value={value} formLabel={formLabel}
                                                            ariaLabel={ariaLabel} />);
      const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
      expect(inputElm.getAttribute('aria-label')).to.equal(`${value}, ${formLabel}, ${ariaLabel}`);
    });

    it('should create an aria label containing the form label and error message when there is an error', () => {
      const validationMessage = 'you got it wrong';
      const input = TestUtils.renderIntoDocument(<InputArea formLabel={formLabel} ariaLabel={ariaLabel} validationError
                                                            validationMessage={validationMessage} />);
      const inputElm = TestUtils.findRenderedDOMComponentWithTag(input, 'input');
      expect(inputElm.getAttribute('aria-label')).to.equal(`${formLabel}, ${validationMessage}`);
    });
  });

  describe('isLabelInline prop', () => {
    const paddingStyles = {
      paddingLeft: '0px',
      paddingRight: '0px'
    };
    const fontStyles = {
      fontFamily: 'family',
      fontSize: '16px',
      fontWeight: 'bold'
    };
    let prevComputedStylesFunc;
    const getComputedStyleStub = sinon.stub().returns({ ...fontStyles, ...paddingStyles });
    const inlineLabelClass = 'widget-form__group--label-inline';
    let reactComponent;

    before(() => {
      prevComputedStylesFunc = window.getComputedStyle;
      window.getComputedStyle = getComputedStyleStub;
    });

    after(() => {
      window.getComputedStyle = prevComputedStylesFunc;
    });

    context('when set to false', () => {
      before(() => {
        reactComponent = TestUtils.renderIntoDocument(
          <InputArea formLabel="magickarp" maxLength={6} isLabelInline={false} />);
      });

      it('should set the inlineLabelWidth to null', () => {
        expect(reactComponent.state.inlineLabelWidth).to.eq(null);
      });

      it(`should not contain the class ${inlineLabelClass}`, () => {
        expect(findDOMNode(reactComponent).className).to.not.contain(inlineLabelClass);
      });

      it('should set the input to contain paddingLeft equal to an empty value', () => {
        const inputNode = TestUtils.findRenderedDOMComponentWithTag(reactComponent, 'input');
        expect(findDOMNode(inputNode).style.paddingLeft).to.eq('');
      });
    });

    context('when set to true', () => {
      before(() => {
        reactComponent = TestUtils.renderIntoDocument(
          <InputArea formLabel="magickarp" maxLength={6} isLabelInline />);
      });

      it('calculate-size to have been called with the label and computed styles', () => {
        const { fontFamily: font, ...styles } = fontStyles;
        expect(calculateSizeStub).to.have.been.calledWith('magickarp', { font, ...styles });
      });

      it('should set the inlineLabelWidth', () => {
        expect(reactComponent.state.inlineLabelWidth).to.eq(labelWidth);
      });

      it(`should contain the class ${inlineLabelClass}`, () => {
        expect(findDOMNode(reactComponent).className).to.contain(inlineLabelClass);
      });

      it('should set the input to contain paddingLeft equal to the state', () => {
        const inputNode = TestUtils.findRenderedDOMComponentWithTag(reactComponent, 'input');
        expect(findDOMNode(inputNode).style.paddingLeft).to.eq(labelWidth);
      });
    });
  });
});

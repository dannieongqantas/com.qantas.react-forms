import React from 'react';
import { findDOMNode } from 'react-dom';
import proxyquire from 'proxyquire';
proxyquire.noCallThru().noPreserveCache();
const textIdent = '100px';
const calculateSizeStub = sinon.stub().returns({ width: textIdent });
const Input = proxyquire('./Input', {
  'calculate-size': calculateSizeStub
}).default;

describe('Input', () => {
  let component;
  let onChangeSpy;
  let onBlurSpy;

  beforeEach(() => {
    onChangeSpy = sinon.spy();
    onBlurSpy = sinon.spy();
    component = TestUtils.renderIntoDocument(
      <Input formLabel="magickarp"
             id="geodude"
             aria-label="oddish"
             containerClassName="bellsprout"
             value="voltorb"
             onChange={onChangeSpy}
             onBlur={onBlurSpy}
             error={'pikachu escaped!'} />);
  });

  it('should create a label with text "magickarp" and for "geodude"', () => {
    const label = TestUtils.findRenderedDOMComponentWithTag(component, 'label');
    expect(label.textContent).to.equal('magickarp');
    expect(label.getAttribute('for')).to.equal('geodude');
  });

  it('should create an input with aria label "oddish" and value "voltorb" and id "geodude', () => {
    const input = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(input.getAttribute('aria-label')).to.equal('oddish');
    expect(input.getAttribute('id')).to.equal('geodude');
    expect(input.value).to.equal('voltorb');
  });

  it('should fall under a class "bellsprout"', () => {
    const element = findDOMNode(component);
    expect(element.getAttribute('class')).to.equal('bellsprout');
  });

  it('should call the onChange prop function when user changes the input', () => {
    const input = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    TestUtils.Simulate.change(input, { target: { value: 'a' } });
    expect(onChangeSpy).to.have.been.calledWith('a');
  });

  it('should call the onBlur prop function when user the input loses focus', () => {
    const input = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    TestUtils.Simulate.blur(input, { target: { value: 'b' } });
    expect(onBlurSpy).to.have.been.calledWith('b');
  });

  it('should render ValidationMessage component with input', () => {
    const validationMessage = TestUtils.findRenderedDOMComponentWithClass(component, 'form-validation-message__text');
    expect(validationMessage).to.be.ok;
  });

  it('should render ValidationMessage component with message "pikachu escaped!"', () => {
    const validationMessage = TestUtils.findRenderedDOMComponentWithClass(component, 'form-validation-message__text');
    expect(validationMessage.textContent).to.equal('pikachu escaped!');
  });


  it('should render input with maxLength to be 6', () => {
    component = TestUtils.renderIntoDocument(
      <Input formLabel="magickarp" maxLength={6} />);
    const renderedComponent = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(findDOMNode(renderedComponent).getAttribute('maxlength')).to.equal('6');
  });

  describe('isLabelInline prop', () => {
    const paddingStyles = {
      paddingRight: '0px'
    };
    const fontStyles = {
      fontFamily: 'family',
      fontSize: '16px',
      fontWeight: 'bold'
    };
    let prevComputedStylesFunc;
    const getComputedStyleStub = sinon.stub().returns({ ...fontStyles, ...paddingStyles });
    const inlineLabelClass = 'widget-form__group--label-inline';
    let reactComponent;

    before(() => {
      prevComputedStylesFunc = window.getComputedStyle;
      window.getComputedStyle = getComputedStyleStub;
    });

    after(() => {
      window.getComputedStyle = prevComputedStylesFunc;
    });

    context('when set to false', () => {
      before(() => {
        reactComponent = TestUtils.renderIntoDocument(
          <Input formLabel="magickarp" maxLength={6} isLabelInline={false} />);
      });

      it('should set the inlineTextIdent to null', () => {
        expect(reactComponent.state.inlineTextIdent).to.eq(null);
      });

      it(`should not contain the class ${inlineLabelClass}`, () => {
        expect(findDOMNode(reactComponent).className).to.not.contain(inlineLabelClass);
      });

      it('should set the input to contain textIndent equal to an empty value', () => {
        const inputNode = TestUtils.findRenderedDOMComponentWithTag(reactComponent, 'input');
        expect(findDOMNode(inputNode).style.textIndent).to.eq('');
      });
    });

    context('when set to true', () => {
      before(() => {
        reactComponent = TestUtils.renderIntoDocument(
          <Input formLabel="magickarp" maxLength={6} isLabelInline />);
      });

      it('calculate-size to have been called with the label and computed styles', () => {
        const { fontFamily: font, ...styles } = fontStyles;
        expect(calculateSizeStub).to.have.been.calledWith('magickarp', { font, ...styles });
      });

      it('should set the inlineTextIdent', () => {
        expect(reactComponent.state.inlineTextIdent).to.eq(textIdent);
      });

      it(`should contain the class ${inlineLabelClass}`, () => {
        expect(findDOMNode(reactComponent).className).to.contain(inlineLabelClass);
      });

      it('should set the input to contain textIndent equal to the state', () => {
        const inputNode = TestUtils.findRenderedDOMComponentWithTag(reactComponent, 'input');
        expect(findDOMNode(inputNode).style.textIndent).to.eq(textIdent);
      });
    });
  });
});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import idGenerator from '../utils/idGenerator';
import ValidationMessage from '../FormValidation/FormValidationMessage';
import classNames from 'classnames';
import calculateSize from 'calculate-size';

/* @todo Code including handling isLabelInline duplicated across Input and InputArea. Investigate if we can merge these components or at least have InputArea use Input. */

export default class Input extends Component {
  static propTypes = {
    formLabel: PropTypes.string.isRequired,
    containerClassName: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    error: PropTypes.string,
    id: PropTypes.string,
    isLabelInline: PropTypes.bool
  };

  static defaultProps = {
    formLabel: '',
    containerClassName: '',
    isLabelInline: false,
    onChange: () => {},
    onBlur: () => {},
    error: '',
    id: ''
  };

  constructor(props) {
    super(props);

    this.id = props.id || `input-${idGenerator()}`;
    this.state = {
      inlineTextIdent: null
    };
  }

  componentDidMount() {
    this.setInlineTextIdentIfNeeded();
  }

  componentDidUpdate() {
    this.setInlineTextIdentIfNeeded(); // in case it didn't get set in componentDidMount
  }

  setInlineTextIdentIfNeeded() {
    if (this.props.isLabelInline && !this.state.inlineTextIdent) {
      const { formLabel } = this.props;
      const { fontFamily, fontSize, fontWeight, paddingRight } = window.getComputedStyle(this.labelRefs);
      const calcWidth = calculateSize(formLabel, { font: fontFamily, fontSize, fontWeight }).width;
      const inlineTextIdent = parseInt(calcWidth, 10) + parseInt(paddingRight, 10);
      this.setState({ inlineTextIdent: `${inlineTextIdent}px` });
    }
  }

  render() {
    const { formLabel, isLabelInline, containerClassName, onChange, onBlur, error, ...otherInputProps } = this.props;

    const containerClassNames = classNames({
      'widget-form__group--label-inline': isLabelInline
    }, containerClassName);

    const inputClassNames = classNames({
      'qfa1-input--validation-error': !!error
    }, 'qfa1-input');

    const inputStyles = {
      textIndent: isLabelInline ? this.state.inlineTextIdent : null
    };

    /* ^ Note: At the moment, we expect `isLabelInline` to be set to `false` when input is focused
        (collapsing/expanding container on homepage).
        If in future, the <input> needs to be focused while isLabelInline === true, we just need to:
        - add `isFocused` to state that reflects whether focused or not
        - expand `inputStyles` `paddingLeft` condition to `(isLabelInline && !isFocused)`
        - add a `display` style to the label like `display: isFocused ? 'none' : ''`
    */

    return <div className={containerClassNames}>
      <label className="widget-form__label" htmlFor={this.id} ref={node => { this.labelRefs = node; }}>{formLabel}</label>
      <input {...otherInputProps}
             id={this.id}
             className={inputClassNames}
             onChange={e => onChange(e.target.value)}
             onBlur={e => onBlur(e.target.value)}
             style={inputStyles} />
      <ValidationMessage hide={!error} message={error} />
    </div>;
  }
}

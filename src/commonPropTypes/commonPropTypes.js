import PropTypes from 'prop-types';

export const formChildren = PropTypes.oneOfType([
  // A single element
  PropTypes.element,

  // A single boolean. Allows conditional syntax:
  // <Form>{bool && <element />}</Form>
  PropTypes.bool,

  // A combination of the above
  PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.bool
  ]))
]).isRequired;

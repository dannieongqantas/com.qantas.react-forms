import React from 'react';
import { shallow } from 'enzyme';
const proxyquire = require('proxyquire').noCallThru();
const appendScriptStub = sinon.stub();
const appendStylesStub = sinon.stub();

const FflyerLogin = proxyquire('./FFlyerLoginForm', {
  '../utils/appendScriptStyles': {
    appendScript: appendScriptStub,
    appendStyles: appendStylesStub
  }
}).default;

const baseProps = {
  className: 'someClass',
  qffAuth: 'login',
  widgetRef: 'propWidgetRef',
  loginButtonTitle: 'loginButtonTitle',
  logoutButtonTitle: 'logoutButtonTitle',
  template: 'template',
  templateLogin: 'templateLogin',
  templateLogout: 'templateLogout',
  backgroundTheme: 'dark',
  scriptPath: ['stagingBundleUrl', 'stagingAPIUrl'],
  cssPath: 'linkPath'
};

const createFlyerForm = (props = baseProps) => {
  const passedProp = { ...baseProps, props };
  return shallow(<FflyerLogin {...passedProp} ><p>Heading</p></FflyerLogin>);
};

describe('Frequent Flyer Login Form', () => {
  let renderedOutput;

  after(() => {
    window.qff_auth = undefined;
  });

  context('when qff_auth is undefined', () => {
    before(() => {
      renderedOutput = createFlyerForm();
      renderedOutput.instance().componentDidMount();
    });

    after(() => {
      appendScriptStub.reset();
      appendStylesStub.reset();
    });

    it('should create script elements and styles from the config URLs', () => {
      expect(appendScriptStub).to.be.calledWith(baseProps.scriptPath, false);
      expect(appendStylesStub).to.be.calledWith([baseProps.cssPath]);
    });

    it('should render within a div tag', () => {
      expect(renderedOutput.type()).to.equal('div');
    });

    it('should set the className of the container to the default class with the passed prop', () => {
      expect(renderedOutput.props().className).to.equal('qfa1-ffLogin_container someClass');
    });

    it('should render the children element when passed', () => {
      expect(renderedOutput.props().children[0].type).to.equal('p');
    });

    it('should pass the necessary props as the login widget attributes', () => {
      const renderedLogin = renderedOutput.props().children[1];
      expect(renderedLogin.props).to.deep.equal({
        'data-qff-auth': baseProps.qffAuth,
        'data-prop-widget-ref': baseProps.widgetRef,
        'data-prop-login-button-title': baseProps.loginButtonTitle,
        'data-prop-logout-button-title': baseProps.logoutButtonTitle,
        'data-prop-template': baseProps.template,
        'data-prop-template-logout': baseProps.templateLogout,
        'data-prop-template-login': baseProps.templateLogin,
        'data-prop-background-theme': baseProps.backgroundTheme
      });
    });
  });

  // TODO: Uncomment out when fix in place
  // context('when qff_auth is not undefined', () => {
  //   it('should not create script elements and styles from the config URLs', () => {
  //     window.qff_auth = {};
  //     renderedOutput = createFlyerForm();
  //     renderedOutput.instance().componentDidMount();
  //     expect(appendScriptStub).not.to.be.called;
  //     expect(appendStylesStub).not.to.be.called;
  //   });
  // });
});


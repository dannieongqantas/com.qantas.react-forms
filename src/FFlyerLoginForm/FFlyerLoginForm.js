import { appendScript, appendStyles } from '../utils/appendScriptStyles';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 * Login Widget Confluence Link
 * https://confluence.qantas.com.au/pages/viewpage.action?spaceKey=LSL&title=Login+Widget+%28v2%29+Integration+Guide
 */

export const LOGIN_INIT_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/init.bundle.js';
export const LOGIN_COMMON_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/common.bundle.js';
export const LOGIN_API_JS_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/login.bundle.js';
export const LOGIN_STYLES_URL = 'https://cdn.qantasloyalty.com/assets/widgets/login/v2/login.bundle.css';

export default class FflyerLogin extends Component {
  static propTypes = {
    className: PropTypes.string,
    qffAuth: PropTypes.string,
    widgetRef: PropTypes.string,
    loginButtonTitle: PropTypes.string,
    logoutButtonTitle: PropTypes.string,
    template: PropTypes.string,
    children: PropTypes.node,
    templateLogin: PropTypes.string,
    templateLogout: PropTypes.string,
    backgroundTheme: PropTypes.string,
    scriptPath: PropTypes.arrayOf(PropTypes.string),
    cssPath: PropTypes.string
  };

  static defaultProps = {
    qffAuth: 'login',
    widgetRef: 'search',
    templateLogout: 'blank',
    backgroundTheme: 'transparent',
    templateLogin: 'blank',
    scriptPath: [LOGIN_INIT_JS_URL, LOGIN_COMMON_JS_URL, LOGIN_API_JS_URL],
    cssPath: LOGIN_STYLES_URL
  };

  componentDidMount() {
    // TODO: uncomment and use some init function possibly available from loyalty api?
    // if (typeof window.qff_auth === 'undefined') {
    const { cssPath, scriptPath } = this.props;
    appendScript(scriptPath, false);
    appendStyles([cssPath]);
    // }
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { qffAuth, widgetRef, loginButtonTitle, logoutButtonTitle, template, templateLogin, templateLogout, className, children, backgroundTheme } = this.props;
    const containerClass = classNames('qfa1-ffLogin_container', className);
    return <div className={containerClass}>
      {children}
      <div data-qff-auth={qffAuth}
        data-prop-widget-ref={widgetRef}
        data-prop-login-button-title={loginButtonTitle}
        data-prop-logout-button-title={logoutButtonTitle}
        data-prop-template={template}
        data-prop-background-theme={backgroundTheme}
        data-prop-template-login={templateLogin}
        data-prop-template-logout={templateLogout}></div>
    </div>;
  }
}

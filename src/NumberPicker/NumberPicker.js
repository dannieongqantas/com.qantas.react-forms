import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberPickerControlled from './NumberPickerControlled';
import { reread } from '../utils/screenReading';
import { stringBind } from '../utils/stringBind';

export default class NumberPicker extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.number,
    initialValue: PropTypes.number,
    ariaLabel: PropTypes.string,
    rangeStart: PropTypes.number,
    rangeEnd: PropTypes.number,
    ariaMinLimitMessage: PropTypes.string,
    ariaMaxLimitMessage: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    information: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  };

  constructor(props) {
    super(props);
    this.state = {
      screenRead: ''
    };
    if (!props.hasOwnProperty('value')) {
      this.state.value = props.initialValue || 0;
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    const { rangeStart, rangeEnd, ariaMinLimitMessage, ariaMaxLimitMessage } = this.props;
    if (isNaN(value)) {
      return;
    }

    if (value < rangeStart) {
      this.setState({
        screenRead: reread(this.state.screenRead, stringBind(ariaMinLimitMessage, { minValue: rangeStart }))
      });
      return;
    }

    if (value > rangeEnd) {
      this.setState({
        screenRead: reread(this.state.screenRead, stringBind(ariaMaxLimitMessage, { maxValue: rangeEnd }))
      });
      return;
    }

    if (!this.props.hasOwnProperty('value')) {
      this.setState({
        value
      });
    }
    this.props.onChange(value);
  }

  render() {
    const value = this.props.hasOwnProperty('value') ? this.props.value : this.state.value;
    return <NumberPickerControlled
      rangeStart={this.props.rangeStart}
      rangeEnd={this.props.rangeEnd}
      ariaLabel={this.props.ariaLabel}
      value={value}
      label={this.props.label}
      screenRead={this.state.screenRead}
      onChange={this.onChange}
      information={this.props.information}
    />;
  }
}

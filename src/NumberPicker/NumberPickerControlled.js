import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import PlusIcon from '../Icons/PlusIcon';
import MinusIcon from '../Icons/MinusIcon';
import ScreenReader from '../ScreenReader/ScreenReader';
import Tooltip from '../Tooltip/Tooltip';
import { ARROW_UP_KEY_CODE, ARROW_DOWN_KEY_CODE, ZERO_KEY_CODE } from '../Constants/KeyCodes';
import idGenerator from '../utils/idGenerator';

const DEFAULT_RANGE_END = 10;

export default class NumberPickerControlled extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    rangeStart: PropTypes.number.isRequired,
    rangeEnd: PropTypes.number.isRequired,
    ariaLabel: PropTypes.string.isRequired,
    screenRead: PropTypes.string,
    information: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),
    id: PropTypes.string,
    onChange: PropTypes.func.isRequired
  };

  static defaultProps = {
    rangeStart: 0,
    rangeEnd: DEFAULT_RANGE_END
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.id = props.id || `number-picker-input-${idGenerator()}`;
  }

  handleKeyDown(event) {
    const { keyCode } = event;

    switch (keyCode) {
      case ARROW_UP_KEY_CODE:
        event.preventDefault();
        this.props.onChange(this.clampValueByRange(1));
        break;
      case ARROW_DOWN_KEY_CODE:
        event.preventDefault();
        this.props.onChange(this.clampValueByRange(-1));
        break;
      default:
        break;
    }

    if (ZERO_KEY_CODE <= keyCode && keyCode < ZERO_KEY_CODE + DEFAULT_RANGE_END) {
      event.preventDefault();
      this.props.onChange(keyCode - ZERO_KEY_CODE);
    }
  }

  clampValueByRange(increment) {
    let newValue = this.props.value + increment;
    if (newValue < this.props.rangeStart) {
      newValue = this.props.rangeStart;
    }
    if (newValue > this.props.rangeEnd) {
      newValue = this.props.rangeEnd;
    }
    return newValue;
  }

  render() {
    const { screenRead, label, value, ariaLabel, rangeStart, rangeEnd, information } = this.props;
    const plusIconClasses = classNames('qfa1-numberpicker__plus-icon', {
      'qfa1-numberpicker__plus-icon--disabled': value >= rangeEnd
    });

    const minusIconClasses = classNames('qfa1-numberpicker__minus-icon', {
      'qfa1-numberpicker__minus-icon--disabled': value <= rangeStart
    });

    return <div className="qfa1-numberpicker__group widget-form__group">
      <label className="widget-form__label" htmlFor={this.id}>{label}</label>
      <Tooltip align="right">
        {information}
      </Tooltip>
      <input className="qfa1-input qfa1-numberpicker__input" type="number" value={value} aria-label={ariaLabel} id={this.id}
             aria-valuemin={rangeStart} aria-valuemax={rangeEnd} aria-valuenow={value} role="spinbutton"
             onKeyDown={this.handleKeyDown} onChange={() => {}} />
      <MinusIcon className={minusIconClasses} disabled={value <= rangeStart} onClick={() => this.props.onChange(this.clampValueByRange(-1))} />
      <PlusIcon className={plusIconClasses} disabled={value >= rangeEnd} onClick={() => this.props.onChange(this.clampValueByRange(1))} />
      <ScreenReader text={screenRead} />
    </div>;
  }
}

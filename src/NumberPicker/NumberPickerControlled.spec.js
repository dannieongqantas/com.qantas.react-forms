import React from 'react';
import TestUtils from 'react-dom/test-utils';
import { findDOMNode } from 'react-dom';
const proxyquire = require('proxyquire').noCallThru();
import ComponentStub from '../../test/utils/ComponentStub';
import { ARROW_UP_KEY_CODE, ARROW_DOWN_KEY_CODE, ZERO_KEY_CODE } from '../Constants/KeyCodes';
const PlusIconStub = ComponentStub();
const MinusIconStub = ComponentStub();
const TooltipStub = ComponentStub();

const NumberPicker = proxyquire('./NumberPickerControlled', {
  '../Icons/PlusIcon': PlusIconStub,
  '../Icons/MinusIcon': MinusIconStub,
  '../Tooltip/Tooltip': TooltipStub
}).default;

describe('NumberPickerControlled', () => {
  let component;
  let onChangeSpy;
  const label = 'hello';
  const value = 5;
  const ariaLabel = 'speak from screen';
  const information = <div>some text message</div>;

  beforeEach(() => {
    onChangeSpy = sinon.spy();
    component = TestUtils.renderIntoDocument(<NumberPicker
      label={label}
      onChange={onChangeSpy}
      value={value}
      rangeStart={0}
      rangeEnd={10}
      ariaLabel={ariaLabel}
      information={information}
    />);
  });

  it('should render a label and input with a minus and plus icon', () => {
    const labelElm = TestUtils.findRenderedDOMComponentWithTag(component, 'label');
    expect(labelElm).to.exist;

    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(inputElm).to.exist;

    const minusElm = TestUtils.findRenderedComponentWithType(component, MinusIconStub);
    expect(minusElm).to.exist;

    const plusElm = TestUtils.findRenderedComponentWithType(component, PlusIconStub);
    expect(plusElm).to.exist;
  });

  it('should pass the label prop into the label', () => {
    const labelElm = TestUtils.findRenderedDOMComponentWithTag(component, 'label');
    expect(labelElm.textContent).to.equal(label);
  });

  it('should pass the value prop into the input', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(inputElm.value).to.equal(value.toString());
  });

  it('should contain the correct aria-value attributes', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(inputElm.getAttribute('aria-valuemax')).to.equal('10');
    expect(inputElm.getAttribute('aria-valuemin')).to.equal('0');
    expect(inputElm.getAttribute('aria-valuenow')).to.equal(value.toString());
    expect(inputElm.getAttribute('aria-valuemax')).to.equal('10');
  });

  it('should contain the correct aria-label value', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(inputElm.getAttribute('aria-label')).to.equal(ariaLabel);
  });

  it('should contain the correct role value', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    expect(inputElm.getAttribute('role')).to.equal('spinbutton');
  });

  it('should handle the minus icon click', () => {
    const minusElm = TestUtils.findRenderedComponentWithType(component, MinusIconStub);
    TestUtils.Simulate.click(findDOMNode(minusElm));
    expect(onChangeSpy).to.have.been.calledWith(4);
  });

  it('should handle the plus icon click', () => {
    const plusElm = TestUtils.findRenderedComponentWithType(component, PlusIconStub);
    TestUtils.Simulate.click(findDOMNode(plusElm));
    expect(onChangeSpy).to.have.been.calledWith(6);
  });

  it('should include the tooltip', () => {
    const toolTipElm = TestUtils.findRenderedComponentWithType(component, TooltipStub);
    expect(toolTipElm.props.children).to.deep.equal(information);
    expect(toolTipElm.props.align).to.equal('right');
  });

  it('should handle the up key press', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    TestUtils.Simulate.keyDown(findDOMNode(inputElm), {
      keyCode: ARROW_UP_KEY_CODE
    });
    expect(onChangeSpy).to.have.been.calledWith(6);
  });

  it('should handle the down key press', () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    TestUtils.Simulate.keyDown(findDOMNode(inputElm), {
      keyCode: ARROW_DOWN_KEY_CODE
    });
    expect(onChangeSpy).to.have.been.calledWith(4);
  });

  const testFunction = (keyCode, expectedValue) => () => {
    const inputElm = TestUtils.findRenderedDOMComponentWithTag(component, 'input');
    TestUtils.Simulate.keyDown(findDOMNode(inputElm), {
      keyCode
    });
    expect(onChangeSpy).to.have.been.calledWith(expectedValue);
  };

  for (let i = 0; i < 10; i++) {
    it(`should handle the number ${i}key press`, testFunction(ZERO_KEY_CODE + i, i));
  }
});

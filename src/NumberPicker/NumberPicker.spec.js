import React from 'react';
import TestUtils from 'react-dom/test-utils';
import ComponentStub from '../../test/utils/ComponentStub';

const proxyquire = require('proxyquire').noCallThru();
const NumberPickerControlledStub = ComponentStub();

const NumberPicker = proxyquire('./NumberPicker', {
  './NumberPickerControlled': NumberPickerControlledStub
}).default;

describe('NumberPicker', () => {
  let component;
  let onChangeSpy;
  const label = 'hello';
  const value = 5;
  const rangeStart = 0;
  const rangeEnd = 10;
  const ariaLabel = 'screen read me';
  const information = 'info about number picker';

  beforeEach(() => {
    onChangeSpy = sinon.spy();
    component = TestUtils.renderIntoDocument(<NumberPicker
      label={label}
      onChange={onChangeSpy}
      value={value}
      rangeStart={rangeStart}
      rangeEnd={rangeEnd}
      information={information}
      ariaLabel={ariaLabel}
    />);
  });

  it('should render properly', () => {
    const numberPickerControlled = TestUtils.findRenderedComponentWithType(component, NumberPickerControlledStub);
    expect(numberPickerControlled).to.exist;
  });

  it('should pass the props', () => {
    const numberPickerControlled = TestUtils.findRenderedComponentWithType(component, NumberPickerControlledStub);
    expect(numberPickerControlled.props).to.eql({
      ariaLabel,
      label,
      value,
      rangeStart,
      rangeEnd,
      screenRead: '',
      information,
      onChange: component.onChange
    });
  });

  it('should manage screen read in state', () => {
    expect(component.state).to.eql({ screenRead: '' });
  });

  describe('value is uncontrolled', () => {
    it('should manage value in state', () => {
      component = TestUtils.renderIntoDocument(<NumberPicker
        label={label}
        onChange={onChangeSpy}
        rangeStart={rangeStart}
        rangeEnd={rangeEnd}
        information={information}
      />);
      expect(component.state).to.eql({ value: 0, screenRead: '' });
    });

    it('should honor initialValue', () => {
      component = TestUtils.renderIntoDocument(<NumberPicker
        ariaLabel={ariaLabel}
        initialValue={5}
        label={label}
        onChange={onChangeSpy}
        rangeStart={rangeStart}
        rangeEnd={rangeEnd}
        information={information}
      />);
      const numberPickerControlled = TestUtils.findRenderedComponentWithType(component, NumberPickerControlledStub);

      expect(component.state).to.eql({ value: 5, screenRead: '' });
      expect(numberPickerControlled.props).to.eql({
        ariaLabel,
        label,
        value: 5,
        rangeStart,
        rangeEnd,
        onChange: component.onChange,
        information,
        screenRead: ''
      });
    });
  });

  describe('handleChange', () => {
    it('should handle onChange', () => {
      component = TestUtils.renderIntoDocument(<NumberPicker
        label={label}
        onChange={onChangeSpy}
        rangeStart={rangeStart}
        rangeEnd={rangeEnd}
      />);
      component.onChange(5);
      expect(component.state).to.eql({ value: 5, screenRead: '' });
    });

    describe('range', () => {
      it('should not update when changed value is outside of range - minus', () => {
        component = TestUtils.renderIntoDocument(<NumberPicker
          label={label}
          onChange={onChangeSpy}
          value={0}
          rangeStart={0}
          rangeEnd={10}
          ariaMinLimitMessage="Min value reached {minValue}"
          information={information}
        />);
        component.onChange(-1);
        expect(onChangeSpy).to.not.have.been.called;
        expect(component.state).to.eql({ screenRead: 'Min value reached 0' });
      });

      it('should not update when changed value is outside of range - plus', () => {
        component = TestUtils.renderIntoDocument(<NumberPicker
          label={label}
          onChange={onChangeSpy}
          value={10}
          rangeStart={0}
          rangeEnd={10}
          ariaMaxLimitMessage="Max value reached {maxValue}"
          information={information}
        />);
        component.onChange(11);
        expect(onChangeSpy).to.not.have.been.called;
        expect(component.state).to.eql({ screenRead: 'Max value reached 10' });
      });
    });
  });
});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PeopleSelectorControlled from './PeopleSelectorControlled';

const OPEN = 'OPEN';
const SELECTION_CHANGE = 'SELECTION_CHANGE';
const BLUR = 'BLUR';
const INPUT_BLUR = 'INPUT_BLUR';
const FOCUS = 'FOCUS';
const CLOSE_AND_FOCUS = 'CLOSE_AND_FOCUS';

export default class PeopleSelector extends Component {
  static propTypes = {
    initialValue: PropTypes.object,
    copy: PropTypes.shape({
      adults: PropTypes.object.isRequired,
      youths: PropTypes.shape({
        ariaLabel: PropTypes.string,
        ariaMaxLimitMessage: PropTypes.string,
        ariaMinLimitMessage: PropTypes.string,
        label: PropTypes.string,
        plural: PropTypes.string,
        singular: PropTypes.string
      }),
      children: PropTypes.object.isRequired,
      infants: PropTypes.object.isRequired,
      buttonText: PropTypes.string.isRequired,
      formLabel: PropTypes.string.isRequired,
      ariaLabel: PropTypes.string.isRequired,
      ariaSelectButton: PropTypes.string.isRequired,
      information: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
      ])
    }).isRequired,
    value: PropTypes.object,
    range: PropTypes.object,
    validationMessage: PropTypes.string,
    onChange: PropTypes.func.isRequired
  };

  static defaultProps = {
    range: {
      adults: {
        rangeStart: 1,
        rangeEnd: 9
      },
      youths: {
        rangeStart: 0,
        rangeEnd: 9
      },
      children: {
        rangeStart: 0,
        rangeEnd: 9
      },
      infants: {
        rangeStart: 0,
        rangeEnd: 9
      }
    },
    copy: {
      youths: {}
    }
  };

  constructor(props) {
    super(props);
    const state = {
      opened: false,
      focused: false
    };
    if (!props.value) {
      state.value = props.initialValue || { adults: 1, children: 0, infants: 0 };
    }
    this.state = state;
  }

  send(action, data) {
    const state = this.update(action, data);
    if (this.state !== state) {
      this.setState(state);
    }
  }

  update(action, data) {
    const state = this.state;
    switch (action) {
      case OPEN: {
        return { ...state, opened: true, focused: true };
      }
      case BLUR: {
        return { ...state, opened: false, focused: false };
      }
      case INPUT_BLUR: {
        return { ...state, focused: false };
      }
      case FOCUS: {
        if (!state.focused) {
          return { ...state, opened: true, focused: true };
        }
        return state;
      }
      case CLOSE_AND_FOCUS: {
        return { ...state, opened: false, focused: true };
      }
      case SELECTION_CHANGE: {
        const props = this.props;
        props.onChange(data);
        if (!props.value) {
          return { ...state, value: data };
        }
        return state;
      }
      default:
        return state;
    }
  }

  render() {
    const value = this.props.value || this.state.value;
    return (
      <PeopleSelectorControlled
        {...this.props} {...this.state}
        value={value}
        blur={() => this.send(BLUR)}
        onFocus={() => this.send(FOCUS)}
        inputBlur={() => this.send(INPUT_BLUR)}
        closeAndFocus={() => this.send(CLOSE_AND_FOCUS)}
        open={() => this.send(OPEN)}
        onChange={(newValue) => this.send(SELECTION_CHANGE, newValue)}
      />
    );
  }
}

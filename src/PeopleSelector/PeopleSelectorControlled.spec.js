import React from 'react';
import TestUtils from 'react-dom/test-utils';
const proxyquire = require('proxyquire').noCallThru();
import ComponentStub from '../../test/utils/ComponentStub';
import i18nData from '../../test/mock/i18n';
import InputArea from '../Input/InputArea';
import {
  RETURN_KEY_CODE,
  ESCAPE_KEY_CODE,
  SPACE_KEY_CODE
} from '../Constants/KeyCodes';

const i18n = { ...i18nData.shared, ...i18nData.flight };

const ArrowIconStub = ComponentStub();
const InputAreaStub = InputArea;
const DropdownStub = ComponentStub();
const NumberPickerStub = ComponentStub();
const { PeopleSelectorControlled, createSelectionText } = proxyquire('./PeopleSelectorControlled', {
  '../Icons/ArrowIcon': ArrowIconStub,
  '../Dropdown/Dropdown': DropdownStub,
  '../NumberPicker/NumberPicker': NumberPickerStub
});

const onChangeSpy = sinon.spy();
const openSpy = sinon.spy();
const onFocusSpy = sinon.spy();
const closeAndFocusSpy = sinon.spy();
const defaultProps = {
  opened: true,
  value: {
    adults: 1,
    youths: 2,
    children: 2,
    infants: 3
  },
  range: {
    adults: {
      rangeStart: 1,
      rangeEnd: 9
    },
    youths: {
      rangeStart: 0,
      rangeEnd: 9
    },
    children: {
      rangeStart: 0,
      rangeEnd: 9
    },
    infants: {
      rangeStart: 0,
      rangeEnd: 9
    }
  },
  id: 'test',
  copy: i18n.passengers,
  onChange: onChangeSpy,
  blur: () => {},
  open: openSpy,
  onFocus: onFocusSpy,
  closeAndFocus: closeAndFocusSpy
};

function createComponent(props) {
  return TestUtils.renderIntoDocument(<PeopleSelectorControlled {...defaultProps} {...props} />);
}

describe('PeopleSelectorControlled', () => {
  describe('Component', () => {
    it('should render the arrow icon with the correct direction', () => {
      let component = createComponent({ opened: true });
      let arrowIcon = TestUtils.findRenderedComponentWithType(component, ArrowIconStub);
      expect(arrowIcon.props.direction).to.equal('up');

      component = createComponent({ opened: false });
      arrowIcon = TestUtils.findRenderedComponentWithType(component, ArrowIconStub);
      expect(arrowIcon.props.direction).to.equal('down');
    });

    it('should render an InputArea', () => {
      const expectedProps = {
        popup: true,
        formLabel: i18n.passengers.formLabel,
        ariaLabel: i18n.passengers.ariaLabel,
        noEntry: true,
        value: `1 ${i18n.passengers.adults.singular}, 2 ${i18n.passengers.youths.plural}, 2 ${i18n.passengers.children.plural}, 3 ${i18n.passengers.infants.plural}`
      };
      const component = createComponent();
      const inputArea = TestUtils.findRenderedComponentWithType(component, InputAreaStub);
      Object.keys(expectedProps).forEach((prop) => {
        expect(expectedProps[prop]).to.equal(inputArea.props[prop]);
      });
    });

    it('should render a Dropdown', () => {
      const component = createComponent();
      const passengersDropdown = TestUtils.findRenderedComponentWithType(component, DropdownStub);
      expect(passengersDropdown.props.children.type).to.equal('div');
    });

    it('should render a button with correct aria label', () => {
      const component = createComponent();
      const button = TestUtils.findRenderedDOMComponentWithTag(component, 'button');
      expect(button.getAttribute('aria-label')).to.equal(i18n.passengers.ariaSelectButton);
    });

    it('should render number pickers to pick adults, youth, children and infants ', () => {
      const component = createComponent();

      const expectedAdultProps = {
        information: undefined,
        value: defaultProps.value.adults,
        rangeStart: defaultProps.range.adults.rangeStart,
        rangeEnd: defaultProps.range.adults.rangeEnd,
        label: i18n.passengers.adults.label,
        ariaLabel: i18n.passengers.adults.ariaLabel,
        ariaMinLimitMessage: i18n.passengers.adults.ariaMinLimitMessage,
        ariaMaxLimitMessage: i18n.passengers.adults.ariaMaxLimitMessage
      };

      const expectedYouthProps = {
        information: undefined,
        value: defaultProps.value.youths,
        rangeStart: defaultProps.range.youths.rangeStart,
        rangeEnd: defaultProps.range.youths.rangeEnd,
        label: i18n.passengers.youths.label,
        ariaLabel: i18n.passengers.youths.ariaLabel,
        ariaMinLimitMessage: i18n.passengers.youths.ariaMinLimitMessage,
        ariaMaxLimitMessage: i18n.passengers.youths.ariaMaxLimitMessage
      };

      const expectedChildrenProps = {
        information: undefined,
        value: defaultProps.value.children,
        rangeStart: defaultProps.range.children.rangeStart,
        rangeEnd: defaultProps.range.children.rangeEnd,
        label: i18n.passengers.children.label,
        ariaLabel: i18n.passengers.children.ariaLabel,
        ariaMinLimitMessage: i18n.passengers.children.ariaMinLimitMessage,
        ariaMaxLimitMessage: i18n.passengers.children.ariaMaxLimitMessage
      };

      const expectedInfantProps = {
        information: undefined,
        value: defaultProps.value.infants,
        rangeStart: defaultProps.range.infants.rangeStart,
        rangeEnd: defaultProps.range.infants.rangeEnd,
        label: i18n.passengers.infants.label,
        ariaLabel: i18n.passengers.infants.ariaLabel,
        ariaMinLimitMessage: i18n.passengers.infants.ariaMinLimitMessage,
        ariaMaxLimitMessage: i18n.passengers.infants.ariaMaxLimitMessage
      };

      const numberPickers = TestUtils.scryRenderedComponentsWithType(component, NumberPickerStub);
      expect(numberPickers[0].props).to.contain(expectedAdultProps);
      expect(numberPickers[1].props).to.contain(expectedYouthProps);
      expect(numberPickers[2].props).to.contain(expectedChildrenProps);
      expect(numberPickers[3].props).to.contain(expectedInfantProps);
    });
  });

  describe('numberPickerChange', () => {
    it('should trigger selection change for adults', () => {
      const component = createComponent();
      component.numberPickerChange('adults', 2);
      expect(onChangeSpy).to.have.been.calledWith({
        adults: 2,
        youths: 2,
        children: 2,
        infants: 3
      });
    });

    it('should trigger selection change for youths', () => {
      const component = createComponent();
      component.numberPickerChange('youths', 4);
      expect(onChangeSpy).to.have.been.calledWith({
        adults: 1,
        youths: 4,
        children: 2,
        infants: 3
      });
    });

    it('should trigger selection change for children', () => {
      const component = createComponent();
      component.numberPickerChange('children', 0);
      expect(onChangeSpy).to.have.been.calledWith({
        adults: 1,
        youths: 2,
        children: 0,
        infants: 3
      });
    });

    it('should trigger selection change for infants', () => {
      const component = createComponent();
      component.numberPickerChange('infants', 0);
      expect(onChangeSpy).to.have.been.calledWith({
        adults: 1,
        youths: 2,
        children: 2,
        infants: 0
      });
    });
  });

  describe('Key presses', () => {
    beforeEach(() => {
      openSpy.reset();
    });

    it('should handle return key - opened', () => {
      const renderedComponent = createComponent({ opened: true });
      const inputArea = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.keyDown(inputArea, { keyCode: RETURN_KEY_CODE });
      expect(closeAndFocusSpy).to.have.been.called;
    });

    it('should handle return key - closed', () => {
      const renderedComponent = createComponent({ opened: false });
      const inputArea = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.keyDown(inputArea, { keyCode: RETURN_KEY_CODE });
      expect(openSpy).to.have.been.called;
    });

    it('should handle space key - opened', () => {
      const renderedComponent = createComponent({ opened: true });
      const inputArea = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.keyDown(inputArea, { keyCode: SPACE_KEY_CODE });
      expect(closeAndFocusSpy).to.have.been.called;
    });

    it('should handle space key - closed', () => {
      const renderedComponent = createComponent({ opened: false });
      const inputArea = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.keyDown(inputArea, { keyCode: SPACE_KEY_CODE });
      expect(openSpy).to.have.been.called;
    });

    it('should handle escape key - opened', () => {
      const renderedComponent = createComponent({ opened: true });
      const inputArea = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.keyDown(inputArea, { keyCode: ESCAPE_KEY_CODE });
      expect(closeAndFocusSpy).to.have.been.called;
    });
  });

  describe('createSelectionText', () => {
    const copy = {
      adults: {
        singular: 'big person',
        plural: 'big people'
      },
      youths: {
        singular: 'teen',
        plural: 'teens'
      },
      children: {
        singular: 'kid',
        plural: 'kids'
      },
      infants: {
        singular: 'baby',
        plural: 'babies'
      }
    };

    it('should construct the correct sentence', () => {
      let sentence = createSelectionText({ adults: 1, youths: 1, children: 1, infants: 1 }, copy);
      expect(sentence).to.equal('1 big person, 1 teen, 1 kid, 1 baby');

      sentence = createSelectionText({ adults: 1, youths: 2, children: 1, infants: 1 }, copy);
      expect(sentence).to.equal('1 big person, 2 teens, 1 kid, 1 baby');

      sentence = createSelectionText({ adults: 2, youths: 1, children: 1, infants: 1 }, copy);
      expect(sentence).to.equal('2 big people, 1 teen, 1 kid, 1 baby');

      sentence = createSelectionText({ adults: 1, youths: 1, children: 2, infants: 1 }, copy);
      expect(sentence).to.equal('1 big person, 1 teen, 2 kids, 1 baby');

      sentence = createSelectionText({ adults: 1, youths: 1, children: 1, infants: 2 }, copy);
      expect(sentence).to.equal('1 big person, 1 teen, 1 kid, 2 babies');

      sentence = createSelectionText({ adults: 5, youths: 5, children: 7, infants: 3 }, copy);
      expect(sentence).to.equal('5 big people, 5 teens, 7 kids, 3 babies');
    });

    it('should construct the correct sentence when parameters zero', () => {
      let sentence = createSelectionText({ adults: 1, children: 0, infants: 1 }, copy);
      expect(sentence).to.equal('1 big person, 1 baby');

      sentence = createSelectionText({ adults: 2, children: 1, infants: 0 }, copy);
      expect(sentence).to.equal('2 big people, 1 kid');

      sentence = createSelectionText({ adults: 0, youths: 1, infants: 0 }, copy);
      expect(sentence).to.equal('1 teen');

      sentence = createSelectionText({ adults: 1, youths: 5, infants: 0 }, copy);
      expect(sentence).to.equal('1 big person, 5 teens');

      sentence = createSelectionText({ adults: 0, children: 5, infants: 3 }, copy);
      expect(sentence).to.equal('5 kids, 3 babies');

      sentence = createSelectionText({ adults: 3, children: 0, infants: 0 }, copy);
      expect(sentence).to.equal('3 big people');

      sentence = createSelectionText({ adults: 0, children: 5, infants: 0 }, copy);
      expect(sentence).to.equal('5 kids');

      sentence = createSelectionText({ adults: 0, children: 0, infants: 3 }, copy);
      expect(sentence).to.equal('3 babies');

      sentence = createSelectionText({ adults: 0, children: 0, infants: 0 }, copy);
      expect(sentence).to.equal('');
    });
  });
});

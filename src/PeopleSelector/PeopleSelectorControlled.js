import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ComponentBlur from '../ReusableComponents/ComponentBlur';
import ArrowIcon from '../Icons/ArrowIcon';
import InputArea from '../Input/InputArea';
import Dropdown from '../Dropdown/Dropdown';
import Button from '../Button/Button';
import NumberPicker from '../NumberPicker/NumberPicker';
import ScreenReader from '../ScreenReader/ScreenReader';
import { stringBind } from '../utils/stringBind';
import idGenerator from '../utils/idGenerator';
import {
  RETURN_KEY_CODE,
  ESCAPE_KEY_CODE,
  SPACE_KEY_CODE
} from '../Constants/KeyCodes';

/* @note
 * It would be good if we could pass in a cleaner object into this component, without splitting up value, copy etc.
*/

export function createSelectionText(people, copy) {
  return Object.keys(people).reduce((previousText, key) => {
    let text = '';
    if (people[key]) {
      if (previousText) text += ', ';
      text += people[key] > 1 ? `${people[key]} ${copy[key].plural}` : `${people[key]} ${copy[key].singular}`;
    }
    return previousText + text;
  }, '');
}

export class PeopleSelectorControlled extends Component {
  static propTypes = {
    value: PropTypes.object.isRequired,
    range: PropTypes.object,
    blur: PropTypes.func.isRequired,
    copy: PropTypes.shape({
      adults: PropTypes.object.isRequired,
      youths: PropTypes.shape({
        ariaLabel: PropTypes.string,
        ariaMaxLimitMessage: PropTypes.string,
        ariaMinLimitMessage: PropTypes.string,
        label: PropTypes.string,
        plural: PropTypes.string,
        singular: PropTypes.string
      }),
      children: PropTypes.object.isRequired,
      infants: PropTypes.object.isRequired,
      buttonText: PropTypes.string.isRequired,
      formLabel: PropTypes.string.isRequired,
      ariaLabel: PropTypes.string.isRequired,
      ariaSelectButton: PropTypes.string.isRequired,
      information: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
      ])
    }).isRequired,
    opened: PropTypes.bool.isRequired,
    focused: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    validationMessage: PropTypes.string,
    open: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    id: PropTypes.string,
    inputBlur: PropTypes.func.isRequired,
    closeAndFocus: PropTypes.func.isRequired,
    className: PropTypes.string,
    screenRead: PropTypes.string
  };

  static defaultProps = {
    className: '',
    screenRead: ''
  };

  static componentRef = 'peopleSelectorControlled';

  constructor(props) {
    super(props);

    this.id = props.id || `people-selector-input-${idGenerator()}`;
  }

  numberPickerChange = (key, value) => {
    const props = this.props;
    props.onChange({ ...props.value, [key]: value });
  }

  keyDownHandler = e => {
    switch (e.keyCode) {
      case SPACE_KEY_CODE:
      case RETURN_KEY_CODE:
        e.preventDefault();
        this.toggleDropdown();
        break;
      case ESCAPE_KEY_CODE:
        e.preventDefault();
        if (this.props.opened) {
          this.props.closeAndFocus();
        }
        break;
      default:
        break;
    }
  }

  toggleDropdown = () => {
    if (this.props.opened) {
      this.props.closeAndFocus();
    } else {
      this.props.open();
    }
  }

  render() {
    const { opened, focused, range, value, copy, className, blur, screenRead, validationMessage } = this.props;

    const combinedValue = createSelectionText(value, copy);

    const classname = classNames('widget-form__group-container', className);

    return (
      <div className={classname} onKeyDown={this.keyDownHandler}>
        <div ref={(el) => { this[this.constructor.componentRef] = el; }} onBlur={blur}>
          <InputArea
            ref="input"
            className="qfa1-people-selector__input"
            value={combinedValue}
            id={this.id}
            onBlur={this.props.inputBlur}
            onFocus={this.props.onFocus}
            onClick={this.props.open}
            popup={opened}
            formLabel={copy.formLabel}
            ariaLabel={copy.ariaLabel}
            focused={focused}
            validationMessage={validationMessage}
            noEntry >
            <ArrowIcon
              className="qfa1-arrow-icon__dropdown"
              direction={opened ? 'up' : 'down'}
              onClick={this.toggleDropdown} />
          </InputArea>

          <ScreenReader text={screenRead} />

          <Dropdown opened={opened}>
            <div className="qfa1-dropdown">
              {
                Object.keys(value).map(key =>
                  <NumberPicker
                    key={key}
                    rangeStart={range[key].rangeStart}
                    rangeEnd={range[key].rangeEnd}
                    value={value[key]}
                    label={copy[key].label}
                    ariaLabel={copy[key].ariaLabel}
                    ariaMinLimitMessage={copy[key].ariaMinLimitMessage}
                    ariaMaxLimitMessage={copy[key].ariaMaxLimitMessage}
                    information={copy[key].information}
                    onChange={(val) => { this.numberPickerChange(key, val); }}
                  />
                )
              }
              <Button
                type="button"
                text={copy.buttonText}
                className="qfa1-people-selector__button"
                ariaLabel={stringBind(copy.ariaSelectButton, { value: combinedValue })}
                onClick={this.props.closeAndFocus} />
            </div>
          </Dropdown>
        </div>
      </div>
    );
  }
}

export default ComponentBlur(PeopleSelectorControlled, PeopleSelectorControlled.componentRef, props => (props.opened ? props.blur() : null));

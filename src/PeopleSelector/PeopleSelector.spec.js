import React from 'react';
import TestUtils from 'react-dom/test-utils';
const proxyquire = require('proxyquire').noCallThru();
import ComponentStub from '../../test/utils/ComponentStub';
import i18nData from '../../test/mock/i18n';

const i18n = { ...i18nData.shared, ...i18nData.flight };

const PeopleSelectorControlledStub = ComponentStub();
const PeopleSelector = proxyquire('./PeopleSelector', {
  './PeopleSelectorControlled': PeopleSelectorControlledStub
}).default;

const onChangeSpy = sinon.spy();

const defaultProps = {
  value: {
    adults: 1,
    youths: 2,
    children: 2,
    infants: 3
  },
  copy: i18n.passengers,
  onChange: onChangeSpy
};

function createComponent(props) {
  return TestUtils.renderIntoDocument(<PeopleSelector {...props} />);
}

describe('PeopleSelector', () => {
  describe('Component', () => {
    it('should render properly', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      expect(peopleSelectorControlled).to.exist;
      expect(peopleSelectorControlled.props.value).to.equal(component.props.value);
      expect(peopleSelectorControlled.props.copy).to.equal(component.props.copy);
      expect(peopleSelectorControlled.props.opened).to.equal(false);
    });
  });

  it('should have initial state', () => {
    const component = createComponent(defaultProps);
    expect(component.state).to.eql({ opened: false, focused: false });
  });

  describe('actions', () => {
    it('should handle open on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.open();
      expect(component.state.opened).to.equal(true);
    });


    it('should handle blur on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.open();
      peopleSelectorControlled.props.blur();
      expect(component.state.opened).to.equal(false);
      expect(component.state.focused).to.equal(false);
    });

    it('should handle onFocus on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.onFocus();
      expect(component.state.opened).to.equal(true);
      expect(component.state.focused).to.equal(true);
    });

    it('should handle closeAndFocus on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.closeAndFocus();
      expect(component.state.opened).to.equal(false);
      expect(component.state.focused).to.equal(true);
    });

    it('should handle onChange on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      const newValue = { adult: 2, children: 1, infants: 0 };
      peopleSelectorControlled.props.onChange(newValue);
      expect(onChangeSpy).to.have.been.calledWith(newValue);
    });

    it('should handle inputBlur on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.onFocus();
      peopleSelectorControlled.props.inputBlur();
      expect(component.state.opened).to.equal(true);
      expect(component.state.focused).to.equal(false);
    });

    it('should handle open() on controlled component', () => {
      const component = createComponent(defaultProps);
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.open();
      expect(component.state.opened).to.equal(true);
      expect(component.state.focused).to.equal(true);
    });
  });

  describe('Value is uncontrolled', () => {
    it('should manage value in state', () => {
      const component = createComponent({ copy: defaultProps.copy, onChange: onChangeSpy });
      expect(component.state.value).to.be.defined;
    });

    it('should honor initialValue', () => {
      const initialValue = { adult: 2, children: 1, infants: 0 };
      const component = createComponent({ initialValue, copy: defaultProps.copy, onChange: onChangeSpy });
      expect(component.state.value).to.be.equal(initialValue);
    });

    it('onChange should set new state', () => {
      const newValue = { adult: 2, children: 1, infants: 0 };
      const component = createComponent({ copy: defaultProps.copy, onChange: onChangeSpy });
      const peopleSelectorControlled = TestUtils.findRenderedComponentWithType(component, PeopleSelectorControlledStub);
      peopleSelectorControlled.props.onChange(newValue);
      expect(component.state.value).to.be.equal(newValue);
    });
  });
});

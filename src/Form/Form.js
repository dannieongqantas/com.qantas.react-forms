import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { formChildren as formChildrenPropType } from '../commonPropTypes/commonPropTypes';

export default class Form extends Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    onSubmit: PropTypes.func,
    onKeyDown: PropTypes.func,
    children: formChildrenPropType
  };

  static defaultProps = {
    className: ''
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleKeyDown(e) {
    if (this.props.onKeyDown)
      this.props.onKeyDown(e);
    else if (e.keyCode === 13)
      e.preventDefault();
  }

  handleSubmit(e) {
    (this.props.onSubmit && this.props.onSubmit(e)) || e.preventDefault();
  }


  render() {
    return <form onSubmit={this.handleSubmit} onKeyDown={this.handleKeyDown} autoComplete="off" noValidate
                 className={this.props.className} >
      {this.props.children}
    </form>;
  }
}

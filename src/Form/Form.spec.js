import React from 'react';
import Form from './Form';
import { mount } from 'enzyme';
import ComponentStub from '../../test/utils/ComponentStub';

const ChildComponent = ComponentStub('input');

describe('Form', () => {
  let form;

  describe('Initial minimum configuration', () => {
    beforeEach(() => {
      form = mount(<Form><ChildComponent /></Form>);
    });

    it('should render a form element', () => {
      expect(form.find('form')).to.have.length(1);
    });

    it('should render the childComponent', () => {
      expect(form.find(ChildComponent)).to.have.length(1);
    });

    it('should pass the className prop to the form', () => {
      expect(form.find('form').nodes[0].getAttribute('class')).to.equal('');
    });

    it('should turn off autocomplete and turn off validate', () => {
      expect(form.find('form').nodes[0].getAttribute('autocomplete')).to.equal('off');
      expect(form.find('form').nodes[0].getAttribute('noValidate')).to.exist;
    });
  });

  describe('With a custom onSubmit and onKeyDown method and custom class', () => {
    const className = 'testFormClass';
    const onSubmitSpy = sinon.spy();
    const onKeyDownSpy = sinon.spy();

    beforeEach(() => {
      form = mount(<Form className={className} onSubmit={onSubmitSpy} onKeyDown={onKeyDownSpy}><ChildComponent /></Form>);
    });

    it('should render a form element', () => {
      expect(form.find('form')).to.have.length(1);
    });

    it('should render the childComponent', () => {
      expect(form.find(ChildComponent)).to.have.length(1);
    });

    it('should pass the className prop to the form', () => {
      expect(form.find('form').nodes[0].getAttribute('class')).to.equal(className);
    });

    it('should fire onKeyDown on submit', () => {
      form.find('input').simulate('keydown');
      expect(onKeyDownSpy.called).to.equal(true);
    });

    it('should fire onSubmit on submit', () => {
      form.find('form').simulate('submit');
      expect(onSubmitSpy.called).to.equal(true);
    });
  });
});


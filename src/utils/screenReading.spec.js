import { reread } from './screenReading';

describe('screenReading', () => {
  describe('reread', () => {
    it('should add a full stop at the end to trigger a screen read update if the texts are the same', () => {
      const oldText = 'hello world';
      const newText = 'hello world';

      expect(reread(oldText, newText)).to.equal(`${newText}.`);
    });

    it('should not alter the text when the new text is different to the old text', () => {
      const oldText = 'hello world';
      const newText = 'hello yoda';

      expect(reread(oldText, newText)).to.equal(newText);
    });

    it('should remove a full stop when text are the same and already contain a fullstop', () => {
      const oldText = 'hello world.';
      const newText = 'hello world.';

      expect(reread(oldText, newText)).to.equal('hello world');
    });
  });
});

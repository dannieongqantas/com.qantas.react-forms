export default function idGenerator() {
  // Same as what React 15.3.0 uses for `__reactInternalInstance$` ID
  return Math.random().toString(36).slice(2);
}

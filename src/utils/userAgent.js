import canUseDOM from './canUseDOM';

export function isTouchDevice() {
  try {
    document.createEvent('TouchEvent');
    return true;
  } catch (e) {
    return false;
  }
}

// Try to avoid using browser detect unless neccessary
const ua = canUseDOM ? navigator.userAgent : 'nodejs';
export function isSafari() {
  return (ua.indexOf('Safari') !== -1 && ua.indexOf('Chrome') === -1);
}

// Returns ie10 or less
export function isIE() {
  return ua.indexOf('MSIE') !== -1;
}

export function isEdge() {
  return ua.indexOf('Edge') !== -1 || ua.indexOf('Trident') !== -1;
}

export function isFirefox() {
  return ua.indexOf('Firefox') !== -1;
}

import { expect } from 'chai';

import highlightWords, { createNonHighlightedWord } from './highlightWords';

describe('highlightWords', () => {
  describe('createNonHighlightedWord', () => {
    it('should create a non highlighted word object', () => {
      const text = 'Dragonball Z';
      const nonHighlightedWordObj = createNonHighlightedWord(text);
      const expectedResult = {
        highlighted: false,
        text
      };

      expect(nonHighlightedWordObj).to.be.deep.equals(expectedResult);
    });
  });

  describe('highlightWords', () => {
    it('should highlight the correct text regardless of case', () => {
      const word = 'Kamehameha and Final Flash';
      const target = 'haMEha';
      const highlightedObj = highlightWords(word, target);
      const expectedResult = [
        { highlighted: false, text: 'Kame' },
        { highlighted: true, text: 'hameha' },
        { highlighted: false, text: ' and Final Flash' }
      ];

      expect(highlightedObj).to.be.deep.equals(expectedResult);
    });

    it('should highlight multiple parts regardless of case', () => {
      const word = 'Goku cannot go on a diet';
      const target = 'go';
      const highlightedObj = highlightWords(word, target);
      const expectedResult = [
        { highlighted: true, text: 'Go' },
        { highlighted: false, text: 'ku cannot ' },
        { highlighted: true, text: 'go' },
        { highlighted: false, text: ' on a diet' }
      ];

      expect(highlightedObj).to.be.deep.equals(expectedResult);
    });

    it('should not highlight when text is not found', () => {
      const word = 'Yamacha\'s cat is hard to spot';
      const target = 'cannot find';
      const highlightedObj = highlightWords(word, target);

      const expectedResult = [{ highlighted: false, text: word }];

      expect(highlightedObj).to.be.deep.equals(expectedResult);
    });

    it('should not highlight when target is not provided', () => {
      const word = 'Frieza will obliterate everything';
      const target = '';
      const highlightedObj = highlightWords(word, target);

      const expectedResult = [{ highlighted: false, text: word }];

      expect(highlightedObj).to.be.deep.equals(expectedResult);
    });
  });
});

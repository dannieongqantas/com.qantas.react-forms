import * as date from '../utils/date';

const AppKey = 'hello';

let version = '1';
const proxyquire = require('proxyquire').noCallThru();
const { Store } = proxyquire('./LocalStore', {
  '../config': () => version
});

describe('LocalStore', () => {
  let localStorage;
  let localStorageGetItemSpy;
  let localStorageSetItemSpy;
  let localStorageRemoveItemSpy;
  let sandbox;

  const createStore = (appKey, versionKey, expiryDays) => new Store(localStorage, appKey, versionKey, expiryDays);

  beforeEach(() => {
    version = '1';
    sandbox = sinon.sandbox.create();
    localStorageGetItemSpy = sandbox.spy();
    localStorageSetItemSpy = sandbox.spy();
    localStorageRemoveItemSpy = sandbox.spy();
    localStorage = {
      getItem: (key) => {
        localStorageGetItemSpy(key);
        return JSON.stringify({ key: 'value', lastUpdatedDate: new Date(2016, 2, 10), versionKey: 'DEFAULT' });
      },
      setItem: (key, value) => {
        localStorageSetItemSpy(key, value);
      },
      removeItem: (key) => {
        localStorageRemoveItemSpy(key);
      }
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should remove data when the version has changed', () => {
    version = '2';
    createStore(AppKey);
    expect(localStorageRemoveItemSpy).to.have.been.calledWith(AppKey);
  });

  describe('expiry date', () => {
    const dateStubFromStartDate = (numDays) => {
      sandbox.stub(date, 'currentDate', () => new Date(2016, 2, 10 + numDays));
    };

    describe('default 30 days', () => {
      it('should remove data when time is after 30 days', () => {
        dateStubFromStartDate(31);
        createStore(AppKey);
        expect(localStorageRemoveItemSpy).to.have.been.calledWith(AppKey);
      });

      it('should not remove data when time is before 30 days', () => {
        dateStubFromStartDate(30);
        createStore(AppKey);
        expect(localStorageRemoveItemSpy).to.not.have.been.called;
      });
    });

    [5, 31, 40].forEach(n => {
      describe(`${n} days`, () => {
        it(`should remove data when time is after ${n} days`, () => {
          dateStubFromStartDate(n + 1);
          createStore(AppKey, 'DEFAULT', n);
          expect(localStorageRemoveItemSpy).to.have.been.calledWith(AppKey);
        });

        it(`should not remove data when time is before ${n} days`, () => {
          dateStubFromStartDate(n);
          createStore(AppKey, 'DEFAULT', n);
          expect(localStorageRemoveItemSpy).to.not.have.been.called;
        });
      });
    });
  });

  it('should not remove data when the version has not changed', () => {
    sandbox.stub(date, 'currentDate', () => new Date(2016, 3, 9));
    version = '1';
    createStore(AppKey);
    expect(localStorageRemoveItemSpy).to.not.have.been.called;
  });

  it('should get data for a given key - exists', () => {
    const store = createStore(AppKey);
    const value = store.get('key');
    expect(value).to.be.equal('value');
    expect(localStorageGetItemSpy).to.have.been.calledWith(AppKey);
  });

  it('should get data for a given key - undefined', () => {
    const store = createStore(AppKey);
    const value = store.get('key1');
    expect(value).to.be.equal(undefined);
    expect(localStorageGetItemSpy).to.have.been.calledWith(AppKey);
  });

  it('should get data for a given key - expired', () => {
    sandbox.stub(date, 'currentDate', () => new Date(2016, 2, 31));
    const store = createStore(AppKey);
    const value = store.get('key1');
    expect(value).to.be.equal(undefined);
    expect(localStorageGetItemSpy).to.have.been.calledWith(AppKey);
  });

  it('should set data for a given key', () => {
    sandbox.stub(date, 'currentDate', () => new Date(2016, 2, 31));
    const store = createStore(AppKey);
    store.set('foo', 'bar');
    expect(localStorageSetItemSpy).to.have.been.calledWith(AppKey, JSON.stringify({
      key: 'value',
      lastUpdatedDate: new Date(2016, 2, 31),
      versionKey: 'DEFAULT',
      foo: 'bar'
    }));
  });
});

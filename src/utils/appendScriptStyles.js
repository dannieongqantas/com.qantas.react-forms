export function appendScript(scripts, async) {
  if (scripts && Array.isArray(scripts)) {
    scripts.forEach((src) => {
      const script = document.createElement('script');
      script.src = src;
      script.async = (typeof async === 'undefined') ? true : async;
      document.body.appendChild(script);
    });
  }
}

export function appendStyles(styles) {
  if (styles && Array.isArray(styles)) {
    styles.forEach((href) => {
      const link = document.createElement('link');
      link.href = href;
      link.rel = 'stylesheet';
      link.type = 'text/css';
      document.head.appendChild(link);
    });
  }
}

// Causes a reread if the text is the same by appending or removing a full stop
export function reread(currentText, newText) {
  if (newText === currentText) {
    if (currentText[currentText.length - 1] === '.') {
      return currentText.substring(0, currentText.length - 1);
    }
    return `${currentText}.`;
  }
  return newText;
}

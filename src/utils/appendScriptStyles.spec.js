import { appendScript, appendStyles } from './appendScriptStyles';

let createElementStub;
let appendChildStub;

describe('appendScriptStyles', () => {
  describe('appendScript', () => {
    context('when the array is valid', () => {
      before(() => {
        createElementStub = sinon.stub(document, 'createElement').returns({});
        appendChildStub = sinon.stub(document.body, 'appendChild');
      });

      after(() => {
        createElementStub.restore();
        appendChildStub.restore();
      });

      it('should create a script element', () => {
        appendScript(['a.js', 'b.js'], true);
        expect(createElementStub).to.be.calledWith('script').twice;
      });

      it('should append the script to the body', () => {
        appendScript(['a.js', 'b.js'], true);
        expect(appendChildStub).to.be.called.twice;
      });

      it('should set the script src and async', () => {
        appendScript(['a.js'], false);
        expect(appendChildStub).to.be.calledWith({ src: 'a.js', async: false });
      });

      it('should set async true if parameter is undefined', () => {
        appendScript(['a.js']);
        expect(appendChildStub).to.be.calledWith({ src: 'a.js', async: true });
      });
    });

    context('when the array is invalid', () => {
      before(() => {
        createElementStub = sinon.stub(document, 'createElement');
      });

      after(() => {
        createElementStub.restore();
      });

      it('should not create script elements', () => {
        appendScript('sgsg', true);
        expect(createElementStub).not.to.be.called.twice;
      });
    });
  });

  describe('appendStyles', () => {
    context('when the array is valid', () => {
      before(() => {
        createElementStub = sinon.stub(document, 'createElement').returns({});
        appendChildStub = sinon.stub(document.head, 'appendChild');
      });

      after(() => {
        createElementStub.restore();
        appendChildStub.restore();
      });

      it('should create a link element', () => {
        appendStyles(['a.css', 'b.css']);
        expect(createElementStub).to.be.calledWith('link').twice;
      });

      it('should create a link element', () => {
        appendStyles(['a.css']);
        expect(appendChildStub).to.be.calledWith({ href: 'a.css', rel: 'stylesheet', type: 'text/css' });
      });
    });

    context('when the array is invalid', () => {
      before(() => {
        appendChildStub = sinon.stub(document.head, 'appendChild');
      });

      after(() => {
        appendChildStub.restore();
      });

      it('should not create the link element', () => {
        appendStyles();
        expect(appendChildStub).not.to.be.called;
      });
    });
  });
});

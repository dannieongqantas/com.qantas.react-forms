import { isSafari } from './userAgent';

// This is for osx safari whilst using voiceover
// When there are dom changes, it stops reading the labels. e.g
// When blurring from calendar to timepicker, calendar is removed from dom and timepicker is prefilled then it wont read timepicker label
export function ariaAsync(cb) {
  return isSafari() ? setTimeout(cb, 0) : cb();
}

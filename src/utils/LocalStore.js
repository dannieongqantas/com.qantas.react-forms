import { currentDate, plusDays } from './date';
const LAST_UPDATED_DATE_KEY = 'lastUpdatedDate';
const VERSION_KEY = 'versionKey';
const DAYS_SAVED_SEARCH_VALID_FOR = 30;

export class Store {
  constructor(localStore, appKey = '', version = 'DEFAULT', expiryLength = DAYS_SAVED_SEARCH_VALID_FOR) {
    if (!appKey)
      throw new Error('localStore must contain an appKey');

    this.appKey = appKey;
    this.localStore = localStore;
    this.currentVersion = version;
    this.expiryLength = expiryLength;
    this.checkDataReset();
  }

  set(key, value) {
    const storage = this.getStorage();
    storage[LAST_UPDATED_DATE_KEY] = currentDate();
    storage[key] = value;
    this.saveStorage(storage);
  }

  get(key) {
    return this.getStorage()[key];
  }

  checkDataReset() {
    if (this.isVersionChanged() || this.isExpired()) {
      this.localStore.removeItem(this.appKey);
      this.set(VERSION_KEY, this.currentVersion);
    }
  }

  isVersionChanged() {
    const storage = this.getStorage();
    const storedVersion = storage[VERSION_KEY];
    return this.currentVersion !== storedVersion;
  }

  isExpired() {
    const storage = this.getStorage();
    const lastUpdatedDateString = storage[LAST_UPDATED_DATE_KEY];
    return plusDays(new Date(lastUpdatedDateString), this.expiryLength).getTime() < currentDate().getTime();
  }

  saveStorage(storage) {
    try {
      // For safari private browsing mode
      this.localStore.setItem(this.appKey, JSON.stringify(storage));
    } catch (e) {} //eslint-disable-line
  }

  getStorage() {
    const appData = this.localStore.getItem(this.appKey);
    return JSON.parse(appData || '{}');
  }
}

const initializeStore = (appKey, version, expiryLength) => new Store(window.localStorage, appKey, version, expiryLength);
export default initializeStore;

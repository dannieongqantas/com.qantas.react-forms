export default function createNewState(oldState, newState, id) {
  const state = { ...oldState };
  state[id] = { ...oldState[id], ...newState };
  return state;
}

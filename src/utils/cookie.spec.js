import { Cookie } from './cookie';

describe('cookie', () => {
  context('get', () => {
    it('should get value for a key', () => {
      expect(new Cookie({ cookie: 'foo=bar;' }).get('foo')).to.equal('bar');
      expect(new Cookie({ cookie: 'key=value; foo=bar;' }).get('key')).to.equal('value');
      expect(new Cookie({ cookie: 'key=value; foo=key;' }).get('key')).to.equal('value');
      expect(new Cookie({ cookie: 'key=value; foo=key;' }).get('foo')).to.equal('key');

      expect(new Cookie({ cookie: 'ffkey=value; foo=key;' }).get('key')).to.be.empty;
    });
  });

  context('set', () => {
    let doc;
    let clock;

    beforeEach(() => {
      clock = sinon.useFakeTimers();
      doc = { cookie: '' };
    });

    afterEach(() => {
      clock.restore();
    });

    it('should set a new cookie', () => {
      new Cookie(doc).set('key', '123');
      expect(doc.cookie).to.eq('key=123;');
    });

    it('should set a new cookie with a 5 min expiry', () => {
      const expiryMins = 5;
      new Cookie(doc).set('key', '123', expiryMins);
      const date = new Date();
      const cookieExpiry = new Date(date.getTime() + 1000 * 60 * expiryMins).toUTCString();
      expect(doc.cookie).to.eq(`key=123; expires=${cookieExpiry};`);
    });

    it('should set a new cookie with a 10 min expiry', () => {
      const expiryMins = 10;
      new Cookie(doc).set('key', '123', expiryMins);
      const date = new Date();
      const cookieExpiry = new Date(date.getTime() + 1000 * 60 * expiryMins).toUTCString();
      expect(doc.cookie).to.eq(`key=123; expires=${cookieExpiry};`);
    });
  });
});

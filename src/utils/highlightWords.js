import escapeRegex from 'escape-string-regexp';

export function createNonHighlightedWord(text) {
  return {
    highlighted: false,
    text
  };
}

export default function highlightWords(words, target) {
  if (!target) return [{ highlighted: false, text: words }];

  const targetRegex = new RegExp(escapeRegex(target), 'gi');
  const matches = words.match(targetRegex) || [];
  const splitWords = words.split(targetRegex);
  const highlightedWords = [];
  splitWords.forEach((text, index) => {
    if (text) highlightedWords.push(createNonHighlightedWord(text));
    matches[index] ? highlightedWords.push({ highlighted: true, text: matches[index] }) : null;
  });
  return highlightedWords;
}

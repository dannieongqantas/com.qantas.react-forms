export function stringBind(str, items) {
  if (typeof str !== 'string') return str;
  let result = str;
  Object.keys(items).forEach(key => {
    const value = items[key];
    result = result.split(`{${key}}`).join(value);
  });
  return result;
}

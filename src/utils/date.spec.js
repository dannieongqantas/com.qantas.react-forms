import { currentTime,
    retrieveLocaleDateFormatter,
    transformDateToLongWords,
    getDayBeginFromMonday,
    plusDays,
    getDaysInMonth,
    dateFormatter,
    firstDateInMonth,
    isDateEqual,
    formatFlight,
    createDateTime,
    datePart,
    dateDiff,
    maxDate,
    serialize,
    deserialize,
    compare
} from './date';
import i18nData from '../../test/mock/i18n';
const i18n = { ...i18nData.shared, ...i18nData.car };

describe('date', () => {
  describe.skip('retrieveLocaleDateFormatter', () => {
    // TODO This block is failing in bamboo due to timeouts. Fix and stop skipping tests.
    it('should return a promise, which resolves to a function', done => {
      retrieveLocaleDateFormatter('zh-hk').then((formatter) => {
        expect(typeof formatter).to.eq('function');
        done();
      });
    });


    it('should return the correct format for the language', done => {
      retrieveLocaleDateFormatter('zh-hk').then((formatter) => {
        const result = formatter(new Date(2017, 4, 31), 'YYYY年 MMM D日 (ddd)');
        expect(result).to.equal('2017年 5月 31日 (周三)');
        done();
      });
    });

    it('should return the correct format for the language', done => {
      retrieveLocaleDateFormatter().then((formatter) => {
        const result = formatter(new Date(2017, 4, 31), 'YYYY年 MMM D日 (ddd)');
        expect(result).to.equal('2017年 May 31日 (Wed)');
        done();
      });
    });
  });

  describe('dateDiff', () => {
    it('should give the correct absolute difference in date', () => {
      const date1 = new Date(2015, 3, 15);
      for (let i = 0; i < 500; i++) {
        const date2 = plusDays(date1, i);
        expect(dateDiff(date1, date2)).to.equal(i);
        expect(dateDiff(date2, date1)).to.equal(i);
      }
    });
  });

  describe('maxDate', () => {
    it('should return the later date', () => {
      const date1 = new Date(2015, 3, 15);
      const date2 = new Date(2015, 4, 16);
      expect(maxDate(date1, date2)).to.equal(date2);
    });
  });

  describe('transformDateToLongWords', () => {
    it('should return the correct date words', () => {
      let result = transformDateToLongWords(new Date(2016, 0, 10), i18n);
      expect(result).to.equal('Sunday, the tenth of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 11), i18n);
      expect(result).to.equal('Monday, the eleventh of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 12), i18n);
      expect(result).to.equal('Tuesday, the twelfth of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 13), i18n);
      expect(result).to.equal('Wednesday, the thirteenth of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 14), i18n);
      expect(result).to.equal('Thursday, the fourteenth of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 1), i18n);
      expect(result).to.equal('Friday, the first of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 2), i18n);
      expect(result).to.equal('Saturday, the second of January 2016');

      result = transformDateToLongWords(new Date(2016, 0, 3), i18n);
      expect(result).to.equal('Sunday, the third of January 2016');
    });
  });

  describe('getDayBeginFromMonday', () => {
    it('should return days beginning from monday', () => {
      const MONDAY = new Date(2016, 0, 11);
      expect(MONDAY.getDay()).to.equal(1);

      for (let i = 0; i < 7; i++) {
        const day = plusDays(MONDAY, i);
        const dayOfTheWeekFromMonday = getDayBeginFromMonday(day);
        expect(dayOfTheWeekFromMonday).to.equal(i + 1);
      }
    });
  });

  describe('getDaysInMonth', () => {
    it('should get the correct days in each month for a non leap year', () => {
      const year = 2015;
      const expectedDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      for (let month = 0; month < 12; month++) {
        const daysInMonth = getDaysInMonth(year, month);
        expect(daysInMonth).to.equal(expectedDaysInMonth[month]);
      }
    });

    it('should get the correct days in each month for a leap year', () => {
      const year = 2016;
      const expectedDaysInMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      for (let month = 0; month < 12; month++) {
        const daysInMonth = getDaysInMonth(year, month);
        expect(daysInMonth).to.equal(expectedDaysInMonth[month]);
      }
    });
  });

  describe('plusDays', () => {
    it('should add the correct number of days', () => {
      const startDate = new Date(2016, 0, 20);
      const dateAdded = plusDays(startDate, 10);
      const expectedDate = new Date(2016, 0, 30);
      expect(dateAdded.getTime()).to.be.equal(expectedDate.getTime());
    });

    it('should add correctly even when years and months changes', () => {
      const startDate = new Date(2017, 11, 31);
      const dateAdded = plusDays(startDate, 365 + 31 + 1); // one year + one month (jan) + one day
      const expectedDate = new Date(2019, 1, 1);
      expect(dateAdded.getTime()).to.be.equal(expectedDate.getTime());
    });
  });

  describe('firstDateInMonth', () => {
    it('should return first date in month', () => {
      expect(firstDateInMonth(new Date(2016, 0, 1)).getTime())
                .to.be.equal(new Date(2016, 0, 1).getTime());

      expect(firstDateInMonth(new Date(2016, 0, 15)).getTime())
                .to.be.equal(new Date(2016, 0, 1).getTime());

      expect(firstDateInMonth(new Date(2016, 0, 31)).getTime())
                .to.be.equal(new Date(2016, 0, 1).getTime());
    });
  });

  describe('isDateEqual', () => {
    it('should compare two dates', () => {
      expect(isDateEqual(new Date(2016, 0, 1), new Date(2016, 0, 1))).to.be.true;
      expect(isDateEqual(new Date(2017, 0, 31), new Date(2017, 0, 31))).to.be.true;
      expect(isDateEqual(new Date(2016, 0, 1), new Date(2017, 0, 1))).to.be.false;
    });
  });

  describe('dateFormatter', () => {
    it('should convert to mm/dd/yyyy format', () => {
      const DATE = 12;
      const MONTH = 5;
      const YEAR = 1990;
      const result = dateFormatter(new Date(YEAR, MONTH - 1, DATE));
      const expectedResult = `${MONTH}/${DATE}/${YEAR}`;
      expect(result).to.equal(expectedResult);
    });
  });

  it('createDateTime', () => {
    expect(createDateTime(new Date(2012, 1, 1), '00:00')).to.eql(new Date(2012, 1, 1, 0, 0));
    expect(createDateTime(new Date(2012, 1, 1), '10:30')).to.eql(new Date(2012, 1, 1, 10, 30));
    expect(createDateTime(new Date(2012, 1, 1), '02:30')).to.eql(new Date(2012, 1, 1, 2, 30));
  });

  it('datePart', () => {
    expect(datePart(new Date(2012, 1, 1, 10, 30))).to.eql(new Date(2012, 1, 1));
  });

  describe('currentTime', () => {
    it('should get the current time', () => {
      const currentDateTime = new Date();
      const currentTimeObj = currentTime();

      expect(currentTimeObj.hours).to.equal(currentDateTime.getHours());
      expect(currentTimeObj.minutes).to.equal(currentDateTime.getMinutes());
      expect(currentTimeObj.seconds).to.equal(currentDateTime.getSeconds());
    });
  });

  describe('formatFlight', () => {
    it('should convert to yyyymmddhh00 format', () => {
      expect(formatFlight(new Date(2016, 0, 1))).to.equal('201601010000');
      expect(formatFlight(new Date(2016, 11, 1))).to.equal('201612010000');
      expect(formatFlight(new Date(2016, 11, 30))).to.equal('201612300000');
      const dateHours = new Date(2016, 11, 30);
      dateHours.setHours(6);
      expect(formatFlight(dateHours)).to.equal('201612300600');
    });
  });

  describe('serialize', () => {
    it('should return the serialized date', () => {
      expect(serialize(new Date(2015, 1, 1))).to.equal(`${new Date(2015, 1, 1).getTime()}-1-0-0`);
      expect(serialize(new Date(2016, 8, 9))).to.equal(`${new Date(2016, 8, 9).getTime()}-1-0-0`);
    });
  });

  describe('deserialize', () => {
    it('should return the a date object', () => {
      expect(deserialize('1474355519610-1-0-0')).to.be.a('date');
    });

    it('should return the a valid date', () => {
      const newDate = new Date(1474355519610);
      expect(deserialize('1474355519610-1-0-0').getTime()).to.equal(newDate.getTime());
    });
  });

  describe('compare', () => {
    it('should return true for dates greater than', () => {
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2015, 1, 0))).to.be.true;
      expect(compare(new Date(2016, 3, 1)).isGreaterThan(new Date(2015, 1, 0))).to.be.true;
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2015, 1, 1))).to.be.false;
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2016, 1, 0))).to.be.false;
    });

    it('should return true for equal dates when equality is set', () => {
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2015, 1, 0), true)).to.be.true;
      expect(compare(new Date(2016, 3, 1)).isGreaterThan(new Date(2015, 1, 0), true)).to.be.true;
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2015, 1, 1), true)).to.be.true;
      expect(compare(new Date(2015, 1, 1)).isGreaterThan(new Date(2016, 1, 0), true)).to.be.false;
    });
  });
});

import dateFnsFormat from 'date-fns/format';
import dateLocales from '../i18n/shared/dateLocale';
import transformLanguageCode from '../i18n/transformLanguageCode';
import getDataForLanguageCode from '../i18n/getDataForLanguageCode';
import { stringBind } from './stringBind';

// TODO - SME - REPLACE CAPITALIZE FUNCTION

// ripped straight from http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript/196991#196991
function capitalize(str) {
  return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}

export function retrieveLocaleDateFormatter(languageCode) {
  if (!languageCode && process.env.NODE_ENV !== 'production') {
    console.error('Make sure languageCode is passed to retrieveLocaleDateFormatter(). Defaulting to use \'en\' for languageCode'); //eslint-disable-line
  }
  const lang = transformLanguageCode(languageCode || 'en');
  const retrieveLocaleFunctions = getDataForLanguageCode(dateLocales, lang);
  return retrieveLocaleFunctions().then(locale => (date, format) => dateFnsFormat(date, format, { locale }));
}

// getDayBeginFromMonday returns 1-7 for monday-sunday
// the getDay method returns 0-6 for sunday-saturday
export function getDayBeginFromMonday(date) {
  return (date.getDay() + 6) % 7 + 1;
}

const MS_PER_DAY = 1000 * 60 * 60 * 24;

export function dateDiff(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor(Math.abs(utc2 - utc1) / MS_PER_DAY);
}

export function maxDate(a, b) {
  return (a.getTime() > b.getTime()) ? a : b;
}

export function transformDateToLongWords(date, i18n, defaultVal = '') {
  if (!(date instanceof Date)) return defaultVal;

  const weekDay = capitalize(i18n.weekdays[getDayBeginFromMonday(date)].long);
  const dayOfMonth = i18n.dayOfMonth[date.getDate()];
  const month = capitalize(i18n.months[date.getMonth() + 1].long);
  const year = date.getFullYear();

  return stringBind(i18n.aria.dateLong, {
    weekDay, month, year, dayOfMonth
  });
}

// get days in months
export function getDaysInMonth(year, month) {
  let adjustedMonth = month + 1;
  let adjustedYear = year;
  if (month === 12) {
    adjustedYear = year + 1;
    adjustedMonth = 0;
  }
  return new Date(adjustedYear, adjustedMonth, 0).getDate();
}

export function plusDays(date, days) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() + days);
}

export function plusMonths(date, months) {
  return new Date(date.getFullYear(), date.getMonth() + months, date.getDate());
}

export function currentDateTime() {
  return new Date();
}

export function currentDate() {
  const today = new Date();
  return new Date(today.getFullYear(),
    today.getMonth(),
    today.getDate());
}

export function currentTime() {
  const todayTime = new Date();
  return {
    hours: todayTime.getHours(),
    minutes: todayTime.getMinutes(),
    seconds: todayTime.getSeconds()
  };
}

export function firstDateInMonth(date) {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}

export function isDateEqual(date1, date2) {
  return date1.getTime() === date2.getTime();
}

/*
 * return mm/dd/yyyy
 */
export function dateFormatter(date) {
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const year = date.getFullYear();
  return `${month}/${day}/${year}`;
}

export function createDateTime(date, timeString) {
  const datePartVar = new Date(date);
  const timeParts = timeString.split(':');
  return new Date(datePartVar.getFullYear(), datePartVar.getMonth(), datePartVar.getDate(), timeParts[0], timeParts[1]);
}

export function datePart(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

function prependZero(value) {
  return value.length === 1 ? `0${value}` : value;
}

export function formatFlight(date) {
  const month = prependZero((date.getMonth() + 1).toString());
  const day = prependZero(date.getDate().toString());
  const year = date.getFullYear();
  const hour = (`0${date.getHours()}`).substr(-2);
  return `${year}${month}${day}${hour}00`;
}

export function serialize(date) {
  return `${date.getTime()}-1-0-0`;
}

export function deserialize(value) {
  const serialisedDate = value.split('-');
  return (new Date(parseInt(serialisedDate[0], 10)));
}

export function compare(date) {
  return {
    isGreaterThan: (compareDate, equality = false) => {
      if (equality) return (date.getTime() >= compareDate.getTime());
      return (date.getTime() > compareDate.getTime());
    }
  };
}

import createNewState from './createNewStateById';

const initialState = {
  id1: {
    name: 'darth maul'
  },
  id2: {
    name: 'dual light saber'
  }
};

const id = 'id1';

describe('createNewState function', () => {
  it('should create a new object', () => {
    const expectedState = createNewState(initialState, {}, id);
    expect(expectedState).to.not.equal(initialState);
    expect(expectedState).to.deep.equal(initialState);

    expect(expectedState[id]).to.not.equal(initialState[id]);
    expect(expectedState[id]).to.deep.equal(initialState[id]);
  });
});

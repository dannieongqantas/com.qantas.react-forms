import {
  isBlank,
  isExactLength,
  isAlphaCharsOnly,
  isAlphanumeric,
  isEmail
} from '../../src/utils/formValidators';

describe('Form validators', () => {
  describe('isBlank', () => {
    it('should return true for empty string', () => {
      expect(isBlank('')).to.equal(true);
    });

    it('should return false for a non-empty string', () => {
      expect(isBlank('hello world')).to.equal(false);
    });
  });

  describe('isExactLength', () => {
    it('should return true for a string the right length', () => {
      const result = isExactLength(10)('Lorem ipsu');
      expect(result).to.equal(true);
    });

    it('should return false for a string the wrong length', () => {
      const result = isExactLength(10)('Lorem');
      expect(result).to.equal(false);
    });
  });

  describe('isAlphaCharsOnly', () => {
    it('should return true if string only has alphabetic characters', () => {
      expect(isAlphaCharsOnly('Lorem')).to.equal(true);
    });

    it('should return false if string has any non-alphabetic characters', () => {
      expect(isAlphaCharsOnly('Lorem11')).to.equal(false);
    });
  });

  describe('isAlphanumeric', () => {
    it('should return true if only contains numbers and letters', () => {
      expect(isAlphanumeric('Lorem11')).to.equal(true);
    });

    it('should return false if contains other characters', () => {
      expect(isAlphanumeric('Lorem11@@@%')).to.equal(false);
    });
  });

  describe('isEmail', () => {
    it('should return true for valid email addresses', () => {
      expect(isEmail('steve@apple.com')).to.equal(true);
      expect(isEmail('disposable.style.email.with+symbol@example.com')).to.equal(true);
      expect(isEmail('admin@mailserver1')).to.equal(true);
      expect(isEmail('john@lookatallthesetopleveldomains.lol')).to.equal(true);
    });

    it('should return false for invalid email addresses', () => {
      expect(isEmail('steve.microsoft.com')).to.equal(false);
      expect(isEmail('this is"not\\allowed@example.com')).to.equal(false);
    });
  });
});


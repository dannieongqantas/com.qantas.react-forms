export class Cookie {
  constructor(document) {
    this.document = document;
  }

  get(key) {
    const cookies = this.document.cookie.split(';');
    const matchingCookie = cookies.find((cookie) => cookie.trim().indexOf(`${key}=`) === 0);
    if (matchingCookie)
      return matchingCookie.split('=')[1];
    return '';
  }

  set(key, value, expiryMins) {
    let cookie = `${key}=${value};`;

    if (expiryMins) {
      const date = new Date();
      const cookieExpiry = new Date(date.getTime() + 1000 * 60 * expiryMins).toUTCString();
      cookie += ` expires=${cookieExpiry};`;
    }
    this.document.cookie = cookie;
  }
}

const cookie = new Cookie(document);
export default cookie;

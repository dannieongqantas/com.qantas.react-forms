/*
 * All independent dom events
 */

const FRAME = 1000 / 60;

export const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

const SCROLL_DURATION = 150;

function scrollTick(ticks, height, perTick) {
  const { pageXOffset } = window;
  window.scrollTo(pageXOffset, height);
  if (ticks > 0)
    setTimeout(() => scrollTick(ticks - 1, height + perTick, perTick), FRAME);
}


export function scrollToTop(elm, ms = SCROLL_DURATION) {
  setTimeout(() => {
    const { pageYOffset } = window;
    if (elm) {
      const elmTop = elm.getBoundingClientRect().top;
      const ticks = Math.ceil(ms / FRAME);
      const perTick = elmTop / ticks;

      scrollTick(ticks - 1, pageYOffset + perTick, perTick);
    }
  }, 0);
}

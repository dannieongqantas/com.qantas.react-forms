import idGenerator from './idGenerator';

describe('idGenerator', () => {
  it('should generate a unique id', () => {
    const id1 = idGenerator();
    const id2 = idGenerator();
    const id3 = idGenerator();

    expect(id1).to.not.equal(id2);
    expect(id2).to.not.equal(id3);
  });
});

import { stringBind } from './stringBind';

describe('stringBind', () => {
  it('should bind strings properly', () => {
    const text = 'my name is {name} and I know how to program in {language}. {language} is my favourite language';
    const expectedText = 'my name is yang and I know how to program in javascript. javascript is my favourite language';
    const resultingText = stringBind(text, {
      name: 'yang',
      language: 'javascript'
    });
    expect(expectedText).to.equal(resultingText);
  });

  it('should return the first parameter when the first parameter is not string', () => {
    expect(stringBind(undefined)).to.be.undefined;
    expect(stringBind(null)).to.be.null;
    expect(stringBind(3)).to.equal(3);
    const func = () => {};
    expect(stringBind(func)).to.equal(func);
    const obj = {};
    expect(stringBind(obj)).to.equal(obj);
  });
});

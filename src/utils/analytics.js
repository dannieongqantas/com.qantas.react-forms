import { currentDateTime } from './date';

function currentPagePathWithOrigin() {
  return `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}${window.location.pathname}`;
}

const url = currentPagePathWithOrigin();

export class AnalyticsInternal {
  constructor(dataLayer, module, categories) {
    this.dataLayer = dataLayer;
    this.module = module;
    this.categories = categories;
  }

  setEvent(event) {
    if (this.dataLayer && this.dataLayer.setEvent) {
      const sendEvent = {
        timestamp: currentDateTime(),
        module: this.module,
        categories: this.categories,
        points: 100,
        ...event
      };

      sendEvent.attributes = sendEvent.attributes || {};
      sendEvent.attributes.url = url;

      this.dataLayer.setEvent(sendEvent);
    }
  }

  setListing(items) {
    if (this.dataLayer && this.dataLayer.setListing) {
      this.dataLayer.setListing({
        url: this.url,
        categories: this.categories,
        name: this.module,
        items
      });
    }
  }
}

export default AnalyticsInternal.bind(null, window.dataLayer || {});


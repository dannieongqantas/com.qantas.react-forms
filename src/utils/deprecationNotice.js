export default function deprecationNotice(message) {
  if (process.ENV !== 'production') console.warn(`Deprecation notice - ${message}`); //eslint-disable-line
}

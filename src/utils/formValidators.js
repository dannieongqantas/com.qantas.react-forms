export function isBlank(value) {
  return value.length === 0;
}

export function isExactLength(length) {
  return value => value.length === length;
}

export function isAlphaCharsOnly(value) {
  return /^[A-Z]+$/i.test(value);
}

export function isAlphanumeric(value) {
  return /^[A-Z0-9]+$/i.test(value);
}

export function isEmail(value) {
  return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i.test(value);
}

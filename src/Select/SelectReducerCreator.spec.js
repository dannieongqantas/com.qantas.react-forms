import SelectReducerCreator from './SelectReducerCreator';
import i18n from '../../test/mock/i18n';
import {
  SELECT_FOCUS_INPUT,
  SELECT_BLUR,
  SELECT_SELECT_ITEM,
  SELECT_HOVER_ITEM,
  SELECT_NAVIGATE_LIST,
  SELECT_INITIALIZE,
  SELECT_POPULATE,
  SELECT_ERROR,
  SELECT_CLEAR_ERROR,
  SELECT_DEFAULT_SELECTED_ITEM,
  SELECT_SET_SELECTED_ITEM
} from '../Constants/ActionTypes';

const id = 'pickupTime';
const SelectReducer = SelectReducerCreator({ id });
const { initialState } = SelectReducer;

import deepFreeze from 'deep-freeze';
deepFreeze(initialState);

const items = [
  { text: 'hey there', ariaLabel: 'hey there', backendCode: 'heyt' },
  { text: 'hello', ariaLabel: 'hello', backendCode: 'hell' }
];

describe('SelectReducer', () => {
  it('should return the initial state', () => {
    const reducerState = SelectReducer(undefined, {});
    expect(reducerState).to.deep.equal(initialState);
  });

  it(`should handle ${SELECT_INITIALIZE}`, () => {
    const navigatedItem = 0;
    const expectedState = {
      ...initialState,
      navigatedItem,
      selectedItem: navigatedItem,
      backendCode: items[navigatedItem].backendCode
    };

    const reducerState = SelectReducer(initialState, { type: SELECT_INITIALIZE, id, item: navigatedItem, items });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${SELECT_POPULATE}`, () => {
    const navigatedItem = 0;
    const expectedState = {
      ...initialState,
      navigatedItem,
      selectedItem: navigatedItem,
      items,
      backendCode: items[navigatedItem].backendCode
    };
    const reducerState = SelectReducer(initialState, { type: SELECT_POPULATE, id, item: navigatedItem, items });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${SELECT_FOCUS_INPUT}`, () => {
    const expectedState = { ...initialState, opened: true, navigatedItem: initialState.selectedItem };

    const reducerState = SelectReducer(initialState, { type: SELECT_FOCUS_INPUT, id });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${SELECT_BLUR}`, () => {
    const expectedState = { ...initialState, opened: false };

    const reducerState = SelectReducer(initialState, { type: SELECT_BLUR, id });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${SELECT_HOVER_ITEM}`, () => {
    const item = 5;
    const expectedState = { ...initialState, navigatedItem: item, ariaNavigatedItem: item };

    const reducerState = SelectReducer(initialState, { type: SELECT_HOVER_ITEM, item, id });
    expect(reducerState).to.deep.equal(expectedState);
  });

  it(`should handle ${SELECT_SELECT_ITEM}`, () => {
    const item = 1;
    const screenRead = 'you have selected that';
    const startState = { ...initialState, navigatedItem: item, backendCode: items[1].backendCode };
    const expectedState = { ...startState, opened: false, selectedItem: item, screenRead };

    const reducerState = SelectReducer(startState, {
      type: SELECT_SELECT_ITEM,
      id,
      screenRead,
      items,
      i18nTime: i18n.car.time
    });

    expect(reducerState).to.deep.equal(expectedState);
  });

  describe(`${SELECT_NAVIGATE_LIST}`, () => {
    it('should handle +1 direction', () => {
      const direction = 1;
      const newNavigatedItem = initialState.navigatedItem + direction;
      const expectedState = { ...initialState, navigatedItem: newNavigatedItem, ariaNavigatedItem: newNavigatedItem };

      const reducerState = SelectReducer(initialState, { type: SELECT_NAVIGATE_LIST, id, direction, items });
      expect(reducerState).to.deep.equal(expectedState);
    });

    it('should handle -1 direction', () => {
      const direction = -1;
      const newDirection = initialState.navigatedItem + direction;
      const expectedState = { ...initialState, navigatedItem: newDirection, ariaNavigatedItem: newDirection };

      const reducerState = SelectReducer(initialState, { type: SELECT_NAVIGATE_LIST, id, direction, items });
      expect(reducerState).to.deep.equal(expectedState);
    });

    it('should handle the upper bound', () => {
      const startState = { ...initialState, navigatedItem: items.length - 1 };
      const expectedState = { ...startState, navigatedItem: -1 };

      const reducerState = SelectReducer(startState, { type: SELECT_NAVIGATE_LIST, direction: 1, id, items });
      expect(reducerState).to.deep.equal(expectedState);
    });

    it('should return to the upper bound from -1', () => {
      const startState = { ...initialState, navigatedItem: -1 };
      const expectedState = { ...startState, navigatedItem: items.length - 1, ariaNavigatedItem: items.length - 1 };

      const reducerState = SelectReducer(startState, { type: SELECT_NAVIGATE_LIST, direction: -1, id, items });
      expect(reducerState).to.deep.equal(expectedState);
    });

    it('should handle the lower bound', () => {
      const startState = { ...initialState, navigatedItem: 0 };
      const expectedState = { ...startState, navigatedItem: -1 };

      const reducerState = SelectReducer(startState, { type: SELECT_NAVIGATE_LIST, direction: -1, id, items });
      expect(reducerState).to.deep.equal(expectedState);
    });
  });

  it(`should handle ${SELECT_ERROR}`, () => {
    const errorMessage = 'An error occurred';
    const expectedState = {
      ...initialState,
      validationMessage: errorMessage
    };

    const reducerState = SelectReducer(initialState, {
      type: SELECT_ERROR,
      id,
      validationMessage: errorMessage
    });
    expect(reducerState).to.deep.equal(expectedState);
  });

  context(`should handle ${SELECT_CLEAR_ERROR}`, () => {
    it('from initial state', () => {
      const expectedState = {
        ...initialState
      };

      const reducerState = SelectReducer(initialState, {
        type: SELECT_CLEAR_ERROR,
        id
      });

      expect(reducerState).to.deep.equal(expectedState);
    });

    it('from validation error state', () => {
      const mockState = {
        ...initialState,
        validationMessage: 'validation message'
      };

      const expectedState = {
        ...initialState
      };

      const reducerState = SelectReducer(mockState, {
        type: SELECT_CLEAR_ERROR,
        id
      });

      expect(reducerState).to.deep.equal(expectedState);
    });
  });

  context(`should handle ${SELECT_DEFAULT_SELECTED_ITEM}`, () => {
    const commontestProps = {
      items: [{ ariaLabel: 'Mr.', text: 'Mr.' }],
      screenRead: 'abc',
      backendCode: '123',
      navigatedItem: 5,
      ariaNavigatedItem: 5
    };

    context('when no selectedItem is provided input initial state', () => {
      it('should set selectedItem to defaultItemIndex, clear validationMessage and close.', () => {
        const mockState = {
          ...initialState,
          ...commontestProps,
          selectedItem: 2,
          validationMessage: 'validation message',
          opened: true
        };

        const expectedState = {
          ...initialState,
          ...commontestProps,
          selectedItem: 0, // defined by DEFAULT_INDEX in ./SelectReducerCreator
          opened: false
        };

        const newState = SelectReducer(mockState, {
          type: SELECT_DEFAULT_SELECTED_ITEM,
          id
        });

        expect(newState).to.deep.equal(expectedState);
      });
    });

    context('when selectedItem is specified in input initial state', () => {
      it('should set selectedItem to defaultItemIndex, clear validationMessage and close.', () => {
        const defaultItemTestID = 'defaultItemTestID';

        const defaultItemTestReducer = SelectReducerCreator({ id: defaultItemTestID, selectedItem: 1 });

        const defaultItemTestInitialState = defaultItemTestReducer.initialState;

        deepFreeze(defaultItemTestInitialState);

        const mockState = {
          ...defaultItemTestInitialState,
          ...commontestProps,
          selectedItem: 2,
          validationMessage: 'validation message',
          opened: true
        };

        const expectedState = {
          ...defaultItemTestInitialState,
          ...commontestProps,
          selectedItem: 1, // defined by DEFAULT_INDEX in ./SelectReducerCreator
          opened: false
        };

        const newState = SelectReducer(mockState, {
          type: SELECT_DEFAULT_SELECTED_ITEM,
          id: defaultItemTestID
        });

        expect(newState).to.deep.equal(expectedState);
      });
    });
  });

  context(`should handle ${SELECT_SET_SELECTED_ITEM}`, () => {
    it('when selectedItem is exist', () => {
      const expectedState = {
        ...initialState,
        selectedItem: 1
      };
      const actualState = SelectReducer(initialState, {
        type: SELECT_SET_SELECTED_ITEM,
        id,
        itemIndex: 1
      });
      expect(actualState).to.deep.equal(expectedState);
    });

    it('when selectedItem is not exist', () => {
      const expectedState = {
        ...initialState,
        selectedItem: 0
      };
      const actualState = SelectReducer(initialState, {
        type: SELECT_SET_SELECTED_ITEM,
        id,
        itemIndex: -1
      });
      expect(actualState).to.deep.equal(expectedState);
    });

    it('when itemIndex is undefined', () => {
      const expectedState = {
        ...initialState,
        selectedItem: 0
      };
      const actualState = SelectReducer(initialState, {
        type: SELECT_SET_SELECTED_ITEM,
        id
      });
      expect(actualState).to.deep.equal(expectedState);
    });
  });
});

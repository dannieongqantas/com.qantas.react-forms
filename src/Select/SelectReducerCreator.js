import {
  SELECT_FOCUS_INPUT,
  SELECT_BLUR,
  SELECT_SELECT_ITEM,
  SELECT_HOVER_ITEM,
  SELECT_NAVIGATE_LIST,
  SELECT_POPULATE,
  SELECT_INITIALIZE,
  SELECT_ERROR,
  SELECT_CLEAR_ERROR,
  SELECT_DEFAULT_SELECTED_ITEM,
  SELECT_SET_SELECTED_ITEM
} from '../Constants/ActionTypes';

import { reread } from '../utils/screenReading';
import { stringBind } from '../utils/stringBind';

const DEFAULT_INDEX = 0; // 10:00

export const initialState = {
  id: 'id',
  selectedItem: DEFAULT_INDEX,
  opened: false,
  navigatedItem: 0,
  ariaNavigatedItem: -1, // for i.e AT navigation, because if aria-active descendent has initial value then it won't read out desc
  screenRead: '',
  backendCode: '',
  defaultItemIndex: DEFAULT_INDEX
};

export function mergeInputAndInitialStates(inputInitialState) {
  return {
    ...initialState,
    ...inputInitialState,
    defaultItemIndex: inputInitialState.selectedItem ? inputInitialState.selectedItem : initialState.defaultItemIndex
  };
}

export default function SelectReducerCreator(initialStateInput = {}) {
  const initialStateVal = mergeInputAndInitialStates(initialStateInput);

  const result = function SelectReducer(state = initialStateVal, action) {
    if (action.id !== state.id) {
      return state;
    }

    switch (action.type) {
      case SELECT_INITIALIZE: {
        return {
          ...state,
          navigatedItem: action.item,
          selectedItem: action.item,
          backendCode: action.items[action.item].backendCode
        };
      }
      case SELECT_POPULATE: {
        return {
          ...state,
          navigatedItem: action.item,
          items: action.items,
          selectedItem: action.item,
          backendCode: action.items[action.item].backendCode
        };
      }

      case SELECT_FOCUS_INPUT: {
        return { ...state, opened: true, navigatedItem: state.selectedItem };
      }

      case SELECT_BLUR: {
        return { ...state, opened: false, ariaNavigatedItem: -1 };
      }

      case SELECT_HOVER_ITEM: {
        return { ...state, navigatedItem: action.item, ariaNavigatedItem: action.item };
      }

      case SELECT_SELECT_ITEM: {
        const selectedItem = state.navigatedItem;
        const screenRead = reread(state.screenRead,
          stringBind(action.screenRead, { selection: action.items[selectedItem].ariaLabel })
        );
        return {
          ...state,
          selectedItem,
          opened: false,
          screenRead,
          backendCode: action.items[selectedItem].backendCode
        };
      }

      case SELECT_NAVIGATE_LIST: {
        let newNavigatedItem = state.navigatedItem + action.direction;

        if (newNavigatedItem >= action.items.length)
          newNavigatedItem = -1;
        else if (newNavigatedItem < -1)
          newNavigatedItem = action.items.length - 1;

        return { ...state, navigatedItem: newNavigatedItem, ariaNavigatedItem: newNavigatedItem };
      }

      case SELECT_ERROR: {
        return {
          ...state,
          validationMessage: action.validationMessage
        };
      }

      case SELECT_CLEAR_ERROR: {
        const { validationMessage, ...newState } = state; // eslint-disable-line no-unused-vars
        return newState;
      }

      case SELECT_DEFAULT_SELECTED_ITEM: {
        const { validationMessage, ...newState } = state; // eslint-disable-line no-unused-vars
        return {
          ...newState,
          selectedItem: newState.defaultItemIndex,
          opened: false
        };
      }


      case SELECT_SET_SELECTED_ITEM: {
        // Assists in setting selectedItem for when you want to render the
        // input with a value already selected.
        const { itemIndex } = action;
        const { defaultItemIndex } = state;
        const selectedItemIndex = Number.isInteger(itemIndex) && itemIndex >= 0 ? itemIndex : defaultItemIndex;
        return {
          ...state,
          selectedItem: selectedItemIndex
        };
      }

      default: {
        return state;
      }
    }
  };

  result.initialState = initialStateVal;
  return result;
}

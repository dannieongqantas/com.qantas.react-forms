import TestUtils from 'react-dom/test-utils';
import React from 'react';
import { findDOMNode } from 'react-dom';
import ComponentStub from '../../test/utils/ComponentStub';
import i18nData from '../../test/mock/i18n';

const proxyquire = require('proxyquire').noCallThru();

const DropdownListStub = ComponentStub();
const ArrowIcon = ComponentStub('ArrowIcon');

const { Select } = proxyquire('./Select', {
  '../DropdownList/DropdownList': DropdownListStub,
  '../Icons/ArrowIcon': ArrowIcon
});

const i18n = { ...i18nData.shared, ...i18nData.car };

function createComponent(newProps = {}) {
  const props = {
    items: i18n.time,
    selectedItem: 1,
    navigatedItem: 1,
    ariaNavigatedItem: 1,
    id: 'pickupTime',
    opened: true,
    initialItem: 0,
    ariaSelection: 'hello test',
    formLabel: 'timepicker',
    ariaLabel: 'time picker read',
    disabled: false,
    blur: () => {},
    dispatch: () => {},
    ...newProps };

  return TestUtils.renderIntoDocument(
    <Select {...props} />
  );
}

describe('Select', () => {
  describe('component', () => {
    it('should render properly', () => {
      const renderedComponent = createComponent();
      const renderedElement = findDOMNode(renderedComponent);
      expect(renderedElement).to.exist;
    });
  });

  describe('input element', () => {
    it('should update the value through the props', () => {
      const selectedItem = 3;
      const expectedTime = i18n.time[selectedItem].text;
      const renderedComponent = createComponent({ selectedItem });
      const renderedInputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      expect(renderedInputElement.value).to.equal(expectedTime);
    });

    it('should be read only', () => {
      const selectedItem = 3;
      const expectedTime = i18n.time[selectedItem].text;
      const renderedComponent = createComponent({ selectedItem });
      const renderedInputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.change(renderedInputElement, { target: { value: 'a' } });
      expect(renderedInputElement.value).to.equal(expectedTime);
    });

    it('should be disabled', () => {
      const renderedComponent = createComponent({ disabled: true });
      const renderedInputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      expect(renderedInputElement.getAttribute('disabled')).to.exist;
    });

    it('should call the correct blur function', () => {
      const blurStub = sinon.spy();
      const renderedComponent = createComponent({ blur: blurStub });
      const renderedInputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
      TestUtils.Simulate.blur(renderedInputElement);
      expect(blurStub).to.have.been.called;
    });
  });

  describe('link element', () => {
    let renderedComponent;
    let element;
    const screenRead = 'read this';
    const id = 'pickCompany';

    it('should render the select with link activator', () => {
      renderedComponent = createComponent({
        id,
        opened: false,
        ariaSelection: screenRead,
        type: 'link'
      });
      element = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'a');
      expect(element).to.exist;
    });

    context('when the Select is opened', () => {
      let focusInputSpy;
      before(() => {
        focusInputSpy = sinon.spy();
        renderedComponent = createComponent({
          id,
          opened: false,
          ariaSelection: screenRead,
          focusInput: focusInputSpy,
          type: 'link'
        });
        element = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'a');
      });

      it('should dispatch the focus event', () => {
        TestUtils.Simulate.click(findDOMNode(element));
        expect(focusInputSpy).to.have.been.called;
      });
    });

    context('when the Select is closed', () => {
      let blurInputSpy;
      before(() => {
        blurInputSpy = sinon.spy();
        renderedComponent = createComponent({
          id,
          opened: true,
          ariaSelection: screenRead,
          blurInput: blurInputSpy,
          type: 'link'
        });
        element = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'a');
      });

      it('should dispatch the blur event', () => {
        TestUtils.Simulate.click(findDOMNode(element));
        expect(blurInputSpy).to.have.been.called;
      });
    });
  });

  describe('user interacts with input', () => {
    let dispatchSpy;
    let renderedComponent;
    let renderedInputElement;
    let focusInputSpy;
    const screenRead = 'read this';
    const id = 'pickupTime';
    beforeEach(() => {
      dispatchSpy = sinon.spy();
      focusInputSpy = sinon.spy();
      renderedComponent = createComponent({
        dispatch: dispatchSpy,
        id,
        opened: true,
        ariaSelection: screenRead,
        focusInput: focusInputSpy
      });
      renderedInputElement = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'input');
    });

    it('should dispatch the correct focus event', () => {
      TestUtils.Simulate.focus(renderedInputElement);
      expect(focusInputSpy).to.have.been.called;
    });
  });

  describe('ArrowIcon', () => {
    let dispatchSpy;
    let renderedComponent;
    let element;
    let focusInputSpy;
    let blurInputSpy;
    const screenRead = 'read this';
    const id = 'pickupTime';

    context('when the Select is closed', () => {
      beforeEach(() => {
        dispatchSpy = sinon.spy();
        focusInputSpy = sinon.spy();
        renderedComponent = createComponent({
          dispatch: dispatchSpy,
          id,
          opened: false,
          ariaSelection: screenRead,
          focusInput: focusInputSpy
        });
        element = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'ArrowIcon');
      });

      it('should dispatch the focus event', () => {
        TestUtils.Simulate.click(findDOMNode(element));
        expect(focusInputSpy).to.have.been.called;
      });
    });

    context('when the Select is opened', () => {
      beforeEach(() => {
        dispatchSpy = sinon.spy();
        blurInputSpy = sinon.spy();
        renderedComponent = createComponent({
          dispatch: dispatchSpy,
          id,
          opened: true,
          ariaSelection: screenRead,
          blurInput: blurInputSpy
        });
        element = TestUtils.findRenderedDOMComponentWithTag(renderedComponent, 'ArrowIcon');
      });

      it('should dispatch the blur event', () => {
        TestUtils.Simulate.click(findDOMNode(element));
        expect(blurInputSpy).to.have.been.called;
      });
    });
  });
});

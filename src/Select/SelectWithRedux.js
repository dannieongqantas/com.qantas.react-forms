import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from './Select';
import {
  SELECT_FOCUS_INPUT,
  SELECT_BLUR,
  SELECT_HOVER_ITEM,
  SELECT_SELECT_ITEM,
  SELECT_NAVIGATE_LIST,
  VALIDATION_CLEAR,
  VALIDATION_FOCUS_DONE
} from '../Constants/ActionTypes';
import {
  RETURN_KEY_CODE,
  ARROW_UP_KEY_CODE,
  ARROW_DOWN_KEY_CODE,
  ESCAPE_KEY_CODE,
  SPACE_KEY_CODE
} from '../Constants/KeyCodes';

export class SelectWithRedux extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    opened: PropTypes.bool,
    items: PropTypes.array.isRequired,
    ariaSelection: PropTypes.string,
    validationMessage: PropTypes.string,
    navigatedItem: PropTypes.number,
    readOnly: PropTypes.bool,
    disabled: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.navigateItems = this.navigateItems.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.focusInput = this.focusInput.bind(this);
    this.keyDownHandler = this.keyDownHandler.bind(this);
    this.handleHoverItem = this.handleHoverItem.bind(this);
    this.blurInput = this.blurInput.bind(this);
  }

  navigateItems(direction) {
    this.props.dispatch({
      type: SELECT_NAVIGATE_LIST,
      items: this.props.items,
      id: this.props.id,
      direction
    });
  }

  focusInput() {
    this.props.dispatch({
      type: SELECT_FOCUS_INPUT,
      id: this.props.id
    });
    this.props.dispatch({
      type: VALIDATION_FOCUS_DONE
    });
  }

  selectItem() {
    if (this.props.validationMessage)
      this.props.dispatch({
        type: VALIDATION_CLEAR,
        id: this.props.id
      });

    return this.props.dispatch({
      type: SELECT_SELECT_ITEM,
      id: this.props.id,
      screenRead: this.props.ariaSelection,
      items: this.props.items,
      selectedItem: this.props.items[this.props.navigatedItem]
    });
  }

  keyDownHandler(e) {
    switch (e.keyCode) {
      case ESCAPE_KEY_CODE:
        e.preventDefault();
        if (this.props.opened)
          this.props.dispatch({ type: SELECT_BLUR, id: this.props.id });
        break;
      case RETURN_KEY_CODE:
        e.preventDefault();
        if (this.props.opened)
          return this.selectItem();
        return this.props.dispatch({ type: SELECT_FOCUS_INPUT, id: this.props.id });
      case ARROW_UP_KEY_CODE:
        e.preventDefault();
        return this.navigateItems(-1);
      case ARROW_DOWN_KEY_CODE:
        e.preventDefault();
        return this.navigateItems(1);
      case SPACE_KEY_CODE:
        e.preventDefault();
        return this.props.dispatch({
          type: this.props.opened ? SELECT_BLUR : SELECT_FOCUS_INPUT,
          id: this.props.id
        });
      default:
        return null;
    }
    return null;
  }

  handleHoverItem(item) {
    this.props.dispatch({ type: SELECT_HOVER_ITEM, item, id: this.props.id });
  }

  blurInput() {
    this.props.dispatch({
      type: SELECT_BLUR,
      id: this.props.id
    });
  }

  render() {
    return <Select navigateItems={this.navigateItems} {...this.props}
                   selectItem={this.selectItem}
                   focusInput={this.focusInput}
                   keyDownHandler={this.keyDownHandler}
                   blurInput={this.blurInput}
                   handleHoverItem={this.handleHoverItem} />;
  }
}

export default connect()(SelectWithRedux);

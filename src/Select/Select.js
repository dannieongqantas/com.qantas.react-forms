import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import DropdownList from '../DropdownList/DropdownList';
import ComponentBlur from '../ReusableComponents/ComponentBlur';
import ArrowIcon from '../Icons/ArrowIcon';
import InputArea from '../Input/InputArea';
import Link from '../Link/Link';
import ScreenReader from '../ScreenReader/ScreenReader';
import idGenerator from '../utils/idGenerator';

const COMPONENT_REF = 'Select';

export class Select extends Component {
  static propTypes = {
    selectedItem: PropTypes.number.isRequired,
    items: PropTypes.array.isRequired,
    keyDownHandler: PropTypes.func.isRequired,
    blurInput: PropTypes.func.isRequired,
    handleHoverItem: PropTypes.func.isRequired,
    focusInput: PropTypes.func.isRequired,
    opened: PropTypes.bool,
    selectItem: PropTypes.func.isRequired,
    navigatedItem: PropTypes.number.isRequired,
    ariaNavigatedItem: PropTypes.number.isRequired,
    blur: PropTypes.func.isRequired,
    id: PropTypes.string,
    formLabel: PropTypes.string.isRequired,
    ariaLabel: PropTypes.string.isRequired,
    screenRead: PropTypes.string,
    className: PropTypes.string,
    readOnly: PropTypes.bool,
    type: PropTypes.oneOf(['combobox', 'link']),
    disabled: PropTypes.bool
  };

  static defaultProps = {
    className: '',
    type: 'combobox',
    disabled: false
  };

  constructor(props) {
    super(props);
    this.arrowIconClick = this.arrowIconClick.bind(this);
    this.clickInput = this.clickInput.bind(this);
    this.clickLink = this.clickLink.bind(this);
    this.id = props.id || idGenerator();
  }

  arrowIconClick() {
    if (this.props.opened) {
      this.inputElement.blur();
      this.props.blurInput();
    } else {
      this.inputElement.focus();
      this.props.focusInput();
    }
  }

  clickLink(event) {
    event.preventDefault();
    if (this.props.opened) {
      this.props.blurInput();
    } else {
      this.props.focusInput();
    }
  }

  clickInput() {
    this.inputElement.focus();
  }

  renderComboBox({ inputId, activeDescendant, listId }) {
    const { ...props } = this.props;
    const value = props.items[props.selectedItem].text;

    return (
      <InputArea ref={(el) => { this.inputElement = el; }}
                 id={inputId}
                 className="qfa1-select__input"
                 value={value}
                 onBlur={props.blur}
                 onFocus={this.props.focusInput}
                 onClick={this.clickInput}
                 popup={props.opened}
                 role="combobox"
                 activeDescendant={activeDescendant}
                 onKeyDown={this.props.keyDownHandler}
                 formLabel={props.formLabel}
                 ariaLabel={props.ariaLabel}
                 aria-owns={listId}
                 noEntry
                 validationMessage={props.validationMessage}
                 readOnly={props.readOnly}
                 disabled={props.disabled}>

        <ArrowIcon
          disabled={props.disabled}
          direction={props.opened ? 'up' : 'down'}
          className="qfa1-arrow-icon__dropdown"
          onClick={this.arrowIconClick}
        />
      </InputArea>
    );
  }

  renderLink({ listId }) {
    const { ...props } = this.props;
    return (
      <Link href="#"
            title={props.formLabel}
            ariaMessage={props.ariaLabel}
            aria-owns={listId}
            onClick={this.clickLink}
            role="combobox">

        {props.formLabel}
        <span className={props.opened ? 'up-icon' : 'down-icon'}></span>
      </Link>
    );
  }

  render() {
    const { ...props } = this.props;
    const inputId = `select-input-${this.id}`;
    const descendantIdPrefix = `select-picker-${inputId}`;
    const activeDescendant = props.ariaNavigatedItem === -1 ? '' : `${descendantIdPrefix}${props.ariaNavigatedItem}`;
    const listId = `${descendantIdPrefix}list`;
    const className = classNames('qfa1-select widget-form__group-container', props.className);

    return <div className={className} ref={(el) => { this[COMPONENT_REF] = el; }}>

      {props.type === 'combobox' ?
        this.renderComboBox({ inputId, activeDescendant, listId }) :
        this.renderLink({ listId })}

      <ScreenReader text={props.screenRead} />

      <DropdownList items={props.items}
                    selectedItem={props.navigatedItem}
                    itemIdPrefix={descendantIdPrefix}
                    opened={props.opened}
                    handleHoverItem={this.props.handleHoverItem}
                    clickAction={this.props.selectItem}
                    listId={listId}
                    disabled={props.disabled}
                    formatter={props.formatter} />
    </div>;
  }
}

export default ComponentBlur(Select, COMPONENT_REF, props => (props.opened ? props.blurInput : null));

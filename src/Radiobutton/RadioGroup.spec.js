import React from 'react';
import RadioGroup from './RadioGroup';
import RadioButton from './Radiobutton';

import { mount } from 'enzyme';

describe('RadioGroup', () => {
  const radioButton1MockProps = {
    checked: 'true',
    formLabel: 'Gryfindor',
    id: 'gryffindor'
  };

  const radioButton2MockProps = {
    checked: 'false',
    formLabel: 'Ravenclaw',
    id: 'ravenclaw'
  };

  describe('basic functionality', () => {
    const onChangeStub = sinon.stub();
    let wrapper;
    before(() => {
      wrapper = mount(
        <RadioGroup className="someClass" id="hogwarts" legend="a legend" onChange={onChangeStub}>
          <RadioButton {...radioButton1MockProps} />
          <RadioButton {...radioButton2MockProps} />
        </RadioGroup>
      );
    });

    it('should render ok', () => {
      expect(wrapper).to.be.ok;
    });

    it('should have the right attributes on the root <div>', () => {
      const containerDiv = wrapper.find('div').at(0);
      expect(containerDiv).to.have.prop('role', 'radiogroup');
      expect(containerDiv).to.have.prop('className', 'someClass qfa1-radiobutton');
    });

    it('should contain a fieldset', () => {
      const fieldset = wrapper.find('.qfa1-radiobutton__radiogroup');
      expect(fieldset.type()).to.equal('fieldset');
    });

    it('should contain a legend', () => {
      expect(wrapper.find('legend').exists()).to.equal(true);
      expect(wrapper.find('legend').text()).to.equal('a legend');
    });

    it('should contain two RadioButton components', () => {
      expect(wrapper.find(RadioButton).length).to.equal(2);
    });

    it('should pass the appropriate properties to the RadioButton component', () => {
      const radioButton1 = wrapper.find(RadioButton).at(0);
      expect(radioButton1.props()).to.deep.contain({
        group: 'hogwarts',
        onChange: onChangeStub
      });

      expect(wrapper.find(RadioButton).at(1).props()).to.deep.contain({
        group: 'hogwarts',
        onChange: onChangeStub
      });

      expect(wrapper.find(RadioButton).at(0).prop('selectItem')).to.be.a.function;
      expect(wrapper.find(RadioButton).at(0).key()).to.equal('gryffindor');

      expect(wrapper.find(RadioButton).at(1).prop('selectItem')).to.be.a.function;
      expect(wrapper.find(RadioButton).at(1).key()).to.equal('ravenclaw');
    });

    it('should set the checked prop for the RadioButtons based on the RadioButton prop if selectedItem is null', () => {
      const radioButton1 = wrapper.find(RadioButton).at(0);
      expect(radioButton1.prop('checked')).to.equal(true);

      const radioButton2 = wrapper.find(RadioButton).at(1);
      expect(radioButton2.prop('checked')).to.equal(false);
    });
  });

  it('should set the checked prop for the RadioButtons based on prop if selectedItem is valid', () => {
    const wrapper = mount(
      <RadioGroup selectedItem="ravenclaw">
        <RadioButton {...radioButton1MockProps} />
        <RadioButton {...radioButton2MockProps} />
      </RadioGroup>
    );

    expect(wrapper.find(RadioButton).at(0).prop('checked')).to.equal(false);
    expect(wrapper.find(RadioButton).at(1).prop('checked')).to.equal(true);
  });

  it('pass the selectItem prop to the RadioButton if it has one', () => {
    const onChangeStub = sinon.stub();

    const wrapper = mount(
      <RadioGroup selectedItem="ravenclaw">
        <RadioButton {...radioButton1MockProps} selectItem={onChangeStub} />
        <RadioButton {...radioButton2MockProps} />
      </RadioGroup>
    );

    wrapper.find(RadioButton).at(0).prop('selectItem')();

    expect(onChangeStub).to.be.called;
  });
});


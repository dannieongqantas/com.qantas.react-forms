import { mount, shallow } from 'enzyme';

import ComponentStub from '../../test/utils/ComponentStub';

const ScreenReaderStub = ComponentStub('ScreenReaderStub');
const RadiobuttonIcon = ComponentStub('RadiobuttonIcon');
const TooltipStub = ComponentStub('TooltipStub');

const Radiobutton = proxyquire('Radiobutton/Radiobutton', {
  '../ScreenReader/ScreenReader': ScreenReaderStub,
  '../Icons/RadiobuttonIcon': RadiobuttonIcon,
  '../Tooltip/Tooltip': TooltipStub
});

const sandbox = sinon.sandbox.create();
const stubSelectItem = sandbox.stub();

const baseProps = {
  ariaLabel: 'An ARIA label',
  checked: false,
  className: 'someClassName',
  formLabel: 'A test label',
  group: 'button group',
  id: 'radioButton-id',
  selectItem: stubSelectItem,
  screenRead: 'Some screen reader text',
  information: 'some information',
  disabled: false
};

describe('RadioButton', () => {
  let component;
  let input;
  let label;
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<Radiobutton {...baseProps} />);
  });

  afterEach(() => {
    sandbox.reset();
  });

  describe('initial render', () => {
    it('should pass down the className to the wrapper', () => {
      expect(wrapper).to.have.className(baseProps.className);
    });

    it('should pass down the correct props to the input', () => {
      input = wrapper.find('input[type="radio"]');
      expect(input.props()).to.contain({
        'aria-checked': baseProps.checked,
        'aria-label': baseProps.ariaLabel,
        checked: baseProps.checked,
        id: baseProps.id,
        name: baseProps.group,
        disabled: baseProps.disabled
      });
    });

    it('should pass down the correct props to the label', () => {
      component = wrapper.find('label');
      expect(component).to.have.prop('htmlFor', baseProps.id);
      expect(component).to.have.className('qfa1-radiobutton__label');
      expect(component).to.have.className('qfa1-radiobutton__label__hasTooltip');
    });

    it('should pass down the correct props to the ScreenReader', () => {
      component = wrapper.find(ScreenReaderStub);
      expect(component).to.have.prop('text', baseProps.screenRead);
    });

    it('should pass down the correct props to the RadiobuttonIcon', () => {
      component = wrapper.find(RadiobuttonIcon);
      expect(component).to.have.prop('checked', baseProps.checked);
      expect(component).to.have.prop('focused', false);
      expect(component).to.have.prop('onClick', wrapper.handleIconClick);
      expect(component).to.have.prop('disabled', baseProps.disabled);
    });

    it('should render the label text', () => {
      component = wrapper.find('label');
      expect(component).to.have.text(baseProps.formLabel);
    });

    it('should render the tooltip with information passed as a text', () => {
      component = wrapper.find('TooltipStub');
      expect(component).to.have.text(baseProps.information);
    });

    it('should render the tooltip with information passed as a text', () => {
      component = wrapper.find('TooltipStub');
      expect(component).to.have.text(baseProps.information);
    });

    context('when no ariaLabel is provided', () => {
      beforeEach(() => {
        const props = {
          ...baseProps,
          ariaLabel: undefined
        };
        wrapper = mount(<Radiobutton {...props} />);
      });

      it('should pass the formLabel as the ariaLabel prop to the input', () => {
        input = wrapper.find('input[type="radio"]');
        expect(input).to.have.prop('aria-label', baseProps.formLabel);
      });
    });

    context('when a child component is provided', () => {
      beforeEach(() => {
        wrapper = mount(
          <Radiobutton {...baseProps}>
            <h1>I am a child</h1>
          </Radiobutton>
        );
      });

      it('should render the child component in place of the label text', () => {
        const child = wrapper.find('h1');
        expect(child).to.be.present();
      });
    });
  });

  describe('input control', () => {
    context('onChange event', () => {
      beforeEach(() => {
        input = wrapper.find('input[type="radio"]').first();
        input.simulate('change');
        label = wrapper.find('label').first();
      });

      it('should call the `selectItem` callback', () => {
        expect(stubSelectItem).to.have.been.calledOnce;
      });

      it('should add the `focused` className to the label', () => {
        expect(label).to.have.className('qfa1-radiobutton__label--focused');
      });
    });

    context('onFocus event', () => {
      beforeEach(() => {
        input = wrapper.find('input[type="radio"]').first();
        input.simulate('focus');
        label = wrapper.find('label').first();
      });

      it('should add the `focused` className to the label', () => {
        expect(label).to.have.className('qfa1-radiobutton__label--focused');
      });
    });

    context('onBlur event', () => {
      beforeEach(() => {
        input = wrapper.find('input[type="radio"]').first();
        input.simulate('blur');
        label = wrapper.find('label').first();
      });

      it('should remove the `focused` className to the label', () => {
        expect(label).to.not.have.className('qfa1-radiobutton__label--focused');
      });
    });
  });

  describe('handleIconClick', () => {
    let focusStub;
    let handleToggleStub;
    context('when handleIconClick prop is false', () => {
      beforeEach(() => {
        component = wrapper.instance();
        component.input = {
          focus: () => {}
        };
        focusStub = sinon.stub(component.input, 'focus');
        handleToggleStub = sinon.stub(component, 'handleToggle');
        component.handleIconClick();
      });

      afterEach(() => {
        focusStub.reset();
        handleToggleStub.reset();
      });

      it('should not trigger the input focus and toggle', () => {
        expect(focusStub).to.not.have.been.called;
        expect(handleToggleStub).to.not.have.been.called;
      });
    });

    context('when handleIconClick prop is true', () => {
      beforeEach(() => {
        wrapper = shallow(<Radiobutton {...baseProps} handleIconClick />);
        component = wrapper.instance();
        component.input = {
          focus: () => {}
        };
        focusStub = sinon.stub(component.input, 'focus');
        handleToggleStub = sinon.stub(component, 'handleToggle');
        component.handleIconClick();
      });

      afterEach(() => {
        focusStub.reset();
        handleToggleStub.reset();
      });

      it('should trigger the input focus and toggle', () => {
        expect(focusStub).to.have.been.called;
        expect(handleToggleStub).to.have.been.called;
      });
    });
  });
});

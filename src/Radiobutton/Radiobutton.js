import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Tooltip from '../Tooltip/Tooltip';
import RadiobuttonIcon from '../Icons/RadiobuttonIcon';
import ScreenReader from '../ScreenReader/ScreenReader';
import { ariaAsync } from '../utils/async';

export default class Radiobutton extends Component {
  static propTypes = {
    ariaLabel: PropTypes.string,
    checked: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element)
    ]),
    className: PropTypes.string,
    formLabel: PropTypes.string,
    group: PropTypes.string,
    id: PropTypes.string.isRequired,
    screenRead: PropTypes.string,
    selectItem: PropTypes.func,
    information: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),
    disabled: PropTypes.bool,
    handleIconClick: PropTypes.bool
  };

  static defaultProps = {
    formLabel: '',
    selectItem: () => {},
    information: '',
    disabled: false
  };

  constructor(props) {
    super(props);
    this.state = {
      focused: false
    };
  }

  handleToggle = () => {
    this.props.selectItem(this);
    this.setState({ focused: true });
  }

  handleFocus = () => {
    this.setState({ focused: true });
  }

  handleBlur = () => {
    ariaAsync(() => this.setState({ focused: false }));
  }

  handleIconClick = () => {
    if (!!this.props.handleIconClick) {
      this.input.focus();
      this.handleToggle();
    }
  }

  render() {
    const {
      ariaLabel,
      checked,
      children,
      className,
      formLabel,
      group,
      id,
      screenRead,
      information,
      disabled
    } = this.props;

    const labelClass = classNames('qfa1-radiobutton__label', {
      'qfa1-radiobutton__label--focused': this.state.focused,
      'qfa1-radiobutton__label__hasTooltip': information
    });

    return (
      <div className={classNames('qfa1-radiobutton__container', className)}>
        <input
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          onChange={this.handleToggle}
          id={id}
          type="radio"
          aria-label={ariaLabel || formLabel}
          className="qfa1-radiobutton__radio"
          role="radio"
          aria-checked={checked}
          checked={checked}
          name={group}
          disabled={disabled}
          ref={(input) => { this.input = input; }}
        />
        <label htmlFor={id} className={labelClass}>
          <RadiobuttonIcon
            checked={checked}
            focused={this.state.focused}
            onClick={this.handleIconClick}
            disabled={disabled}
          />
          {children || <span className="qfa1-radiobutton__label-text">{formLabel}</span>}
        </label>
        <ScreenReader text={screenRead} />
        <Tooltip align="right">
          {information}
        </Tooltip>
      </div>
    );
  }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 This is a helper to improve readibility
 You can set the checked radio button in one of 2 ways:
   1. Set the selectedItem to an ID of a RadioButton child when rendering the <RadioGroup />
     <RadioGroup selectedItem="ABC"><RadioButton id="ABC" /></RadioGroup>
   2. Omit the selectedItem prop and use a checked prop on the RadioButton child
     <RadioGroup><RadioButton id="ABC" checked /></RadioGroup>
 TODO: All decide on one method in a major version of this?
*/
function isRadioButtonChecked(radioGroup, radioButton) {
  const { selectedItem } = radioGroup.props;
  const { id, checked } = radioButton.props;
  const isChecked = id === selectedItem;
  return (selectedItem == null && checked) ? (checked === 'true') : isChecked;
}

export default class Radiogroup extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    selectedItem: PropTypes.string,
    onChange: PropTypes.func,
    legend: PropTypes.string,
    selectItem: PropTypes.func,
    children: PropTypes.arrayOf(PropTypes.element).isRequired, // Should always be [<Radiobutton />...] elements
    className: PropTypes.string
  };

  static defaultProps = {
    className: '',
    legend: ''
  };

  constructor(props) {
    super(props);
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem(item) {
    this.setState({
      selectedItem: item.props.id
    });
  }

  groupRadioButtonChildren() {
    const {
      id: radioGroupId,
      onChange,
      children
    } = this.props;

    // TODO In the future, we should use React.Children.map. I have not
    // changed this yet as it breaks unit tests.
    return children.map((radioButton) => {
      const {
        id: radioButtonId,
        selectItem
      } = radioButton.props;

      return React.cloneElement(radioButton, {
        key: radioButtonId,
        group: radioGroupId,
        checked: isRadioButtonChecked(this, radioButton),
        selectItem: selectItem || this.selectItem,
        onChange
      });
    });
  }

  render() {
    return (
      <div role="radiogroup" className={`${this.props.className} qfa1-radiobutton`}>
        <fieldset className="qfa1-radiobutton__radiogroup">
          <legend className="qfa1-radiobutton__legend">
            {this.props.legend}
          </legend>
          <div className="qfa1-radiobutton__radiogroup__wrapper">
            {this.groupRadioButtonChildren()}
          </div>
        </fieldset>
      </div>
    );
  }
}

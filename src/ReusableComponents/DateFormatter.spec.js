import React from 'react';
import { shallow } from 'enzyme';
import proxyquire, { noCallThru, noPreserveCache } from 'proxyquire';
noCallThru();
noPreserveCache();
const enFormatter = sinon.spy();
const jaFormatter = sinon.spy();
const retrieveLocaleDateFormatterStub = sinon.stub();
retrieveLocaleDateFormatterStub.withArgs('en').returns(Promise.resolve(enFormatter));
retrieveLocaleDateFormatterStub.withArgs('ja').returns(Promise.resolve(jaFormatter));
const DateFormatter = proxyquire('./DateFormatter', {
  '../utils/date': {
    retrieveLocaleDateFormatter: retrieveLocaleDateFormatterStub
  }
}).default;

const DummyComponent = (props) => <div {...props} />;

describe('DateFormatter', () => {
  let wrapper;
  const dummyProps = { className: 'css-class', languageCode: 'en', randomProp: 'ja' };

  afterEach(() => {
    retrieveLocaleDateFormatterStub.reset();
    enFormatter.reset();
    jaFormatter.reset();
  });

  describe('when callback is not passed', () => {
    before(() => {
      const WrappedComponent = DateFormatter(DummyComponent);
      wrapper = shallow(<WrappedComponent {...dummyProps} />);
    });

    it('should call retrieveLocaleDateFormatter with the value from the languageCode prop', () => {
      expect(retrieveLocaleDateFormatterStub).to.have.been.calledWith(dummyProps.languageCode);
    });

    it('should pass all props passed to the WrappedComponent', () => {
      const dummyComponentProps = wrapper.find(DummyComponent).props();
      expect(dummyComponentProps).to.deep.contain(dummyProps);
    });

    it('should pass a dateFormatter function to the WrappedComponent using the languageCode prop locale', () => {
      const dummyComponentProps = wrapper.find(DummyComponent).props();
      expect(typeof dummyComponentProps.dateFormatter).to.eq('function');
      dummyComponentProps.dateFormatter();
      expect(enFormatter).to.have.been.called;
      expect(jaFormatter).to.have.not.been.called;
    });
  });

  describe('when callback is passed', () => {
    before(() => {
      const WrappedComponent = DateFormatter(DummyComponent, (props) => props.randomProp);
      wrapper = shallow(<WrappedComponent {...dummyProps} />);
    });

    it('should call retrieveLocaleDateFormatter with the value from the randomProp prop', () => {
      expect(retrieveLocaleDateFormatterStub).to.have.been.calledWith(dummyProps.randomProp);
    });

    it('should pass a dateFormatter function to the WrappedComponent using the randomProp prop locale', () => {
      const dummyComponentProps = wrapper.find(DummyComponent).props();
      dummyComponentProps.dateFormatter();
      expect(enFormatter).to.have.not.been.called;
      expect(jaFormatter).to.have.been.called;
    });
  });
});

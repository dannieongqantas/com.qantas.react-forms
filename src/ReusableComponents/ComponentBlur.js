import React, { Component } from 'react';
import { TAB_KEY_CODE } from '../Constants/KeyCodes';

/*
 * ComponentBlur, handles blurring of components
 * @param {React Component} Component - The component being enhanced
 * @param {String} ref - The element reference being blurred
 * @param {func} payload - The payload function which creates an object to be dispatched
 *
 * @return {<ReactComponent blur={onBlur}>} - returns a component with a blur prop which can also be attached to input field blurs
 */

const COMPONENT_PREFIX = 'blurred';

export default function componentBlur(MyComponent, childComponentRef, payload = () => {}) {
  if (typeof payload !== 'function')
    throw new Error('ComponentBlur accepts (Component, actionType, payloadFunc)');

  const blurWrapperRef = `${COMPONENT_PREFIX}_${childComponentRef}`;

  return class ComponentBlur extends Component {
    constructor(props, context) {
      super(props, context);
      this.clickedOutside = false;
    }

    componentDidMount() {
      document.getElementsByTagName('body')[0].addEventListener('mousedown', this.leaveField, false);
      document.getElementsByTagName('body')[0].addEventListener('touchend', this.leaveField, false);
      document.getElementsByTagName('body')[0].addEventListener('keydown', this.leaveFieldByKeyDown, true);
    }

    componentWillUnmount() {
      document.getElementsByTagName('body')[0].removeEventListener('mousedown', this.leaveField, false);
      document.getElementsByTagName('body')[0].addEventListener('touchend', this.leaveField, false);
      document.getElementsByTagName('body')[0].removeEventListener('keydown', this.leaveFieldByKeyDown, true);
    }

    leaveFieldByKeyDown = (e) => {
      if (e.type === 'keydown' && e.keyCode !== TAB_KEY_CODE) return;
      this.checkIfBlurred(e.target);
    }

    onBlur = () => {
      if (!this.clickedOutside) {
        setTimeout(() => this.checkIfBlurred(document.activeElement), 0); // Hack to get voiceover working for pop overs on safari
      }
    }

    focus = () => {
      const childComponent = this[blurWrapperRef];

      if (childComponent && childComponent.focus) {
        childComponent.focus();
      }
    }

    leaveField = (e) => {
      this.clickedOutside = true;
      this.checkIfBlurred(e.target);
      setTimeout(() => { this.clickedOutside = false; }, 0); // to cancel clickedOutside if blurred
    }

    checkIfBlurred = (target) => {
      const insideElm = this[blurWrapperRef] && this[blurWrapperRef][childComponentRef] || null;

      if (!insideElm || !insideElm.contains(target)) {
        const payloadData = payload(this.props);
        if (typeof payloadData === 'function')
          payloadData();
      }
    }

    render() {
      return <MyComponent ref={(el) => { this[blurWrapperRef] = el; }} {...this.props} blur={this.onBlur} />;
    }
  };
}

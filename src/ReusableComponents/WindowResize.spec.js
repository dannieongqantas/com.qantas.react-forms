import React from 'react';
import TestUtils from 'react-dom/test-utils';
import WindowResize from './WindowResize';
import { findDOMNode, unmountComponentAtNode } from 'react-dom';
import ComponentStub from '../../test/utils/ComponentStub';
import each from 'lodash/each';

const DummyComponent = ComponentStub();

describe('WindowResize', () => {
  describe('Function', () => {
    it('should throw error when breakpoint is not a number or Array', () => {
      const throwError = () => {
        WindowResize(DummyComponent, ['breakme', 'teadwqeqw']);
      };
      expect(throwError).to.throw('WindowResize breakpoints should only accept a number or an array of numbers');
    });

    it('should throw error when breakpoint is not in ascending order', () => {
      const throwError = () => {
        WindowResize(DummyComponent, [3, 7, 2]);
      };
      expect(throwError).to.throw('WindowResize breakpoints should be in ascending order');
    });
  });

  describe('Using a Component', () => {
    const EnhancedDummyComponent = WindowResize(DummyComponent);
    let renderedComponent;
    let sandbox;
    let addEventListenerSpy;
    let removeEventListenerSpy;
    const passedProps = {
      obiWan: 'Kenobi',
      anakin: 'Skywalker'
    };

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      addEventListenerSpy = sandbox.stub(window, 'addEventListener');
      removeEventListenerSpy = sandbox.stub(window, 'removeEventListener');
      renderedComponent = TestUtils.renderIntoDocument(
        <EnhancedDummyComponent {...passedProps} />
      );
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should register the leaveField listener on mount', () => {
      expect(addEventListenerSpy).to.have.been.calledWith('resize', renderedComponent.onResize);
    });

    it('should unregister the leaveField listener on unmount', () => {
      unmountComponentAtNode(findDOMNode(renderedComponent).parentNode);
      expect(removeEventListenerSpy).to.have.been.calledWith('resize', renderedComponent.onResize);
    });

    it('should have passed down the props', () => {
      each(passedProps, (n, key) => {
        expect(renderedComponent.props[key]).to.equal(n);
      });
    });
  });

  describe('computeBreakpoint', () => {
    const EnhancedDummyComponent = WindowResize(DummyComponent);

    it('should compute the correct starting breakpoint position', () => {
      const breakpoint = EnhancedDummyComponent.computeBreakpoint(200, [300, 500]);
      expect(breakpoint).to.equal(0);
    });

    it('should compute the correct middle breakpoint position', () => {
      const breakpoint = EnhancedDummyComponent.computeBreakpoint(400, [300, 500]);
      expect(breakpoint).to.equal(1);
    });

    it('should compute the correct ending breakpoint position', () => {
      const breakpoint = EnhancedDummyComponent.computeBreakpoint(600, [300, 500]);
      expect(breakpoint).to.equal(2);
    });
  });
});


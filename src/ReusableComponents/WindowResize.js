import React, { Component } from 'react';
import throttle from 'lodash/throttle';
import canUseDOM from '../utils/canUseDOM';
const THROTTLE_RATE = 1000 / 60;
const DESKTOP_WIDTH = 1500;

/*
 * getWindowWidth
 *
 * A cross-browser solution
 * using clientWidth and clientHeight for IE8 and earlier
 * see: https://www.w3schools.com/jsref/prop_win_innerheight.asp
 * browser compatibility: https://developer.mozilla.org/en-US/docs/Web/API/Element/clientWidth
 */
export function getWindowWidth() {
  if (!canUseDOM) return DESKTOP_WIDTH;
  return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

/*
 * WindowResize
 *
 * @param {React Component} Component
 * @param {Array, Number} breakpoints - the breakpoints which will cause a rerender
 * @param {Function(props, windowWidth)} payload - the payload function, when this is defined and both is not then it will not pass the windowWidth property
 * @param {Boolean} both - sends a dispatch and also updates the windowWidth prop
 *
 * @return {<ReactComponent windowWidth={windowWidth} />} - returns a React Component with the windowWidth prop
 */

export default function WindowResize(MyComponent, breakpoints = [], payload = null, both = false) {
  const breakpointsArray = breakpoints instanceof Array ? breakpoints : [breakpoints];

  if (breakpointsArray.length && typeof breakpointsArray[0] !== 'number')
    throw new Error('WindowResize breakpoints should only accept a number or an array of numbers');

  breakpointsArray.length > 1 && breakpointsArray.reduce((previousVal, currentVal) => {
    if (typeof currentVal !== 'number')
      throw new Error('WindowResize breakpoints should only accept a number or an array of numbers');

    if (currentVal < previousVal)
      throw new Error('WindowResize breakpoints should be in ascending order');

    return currentVal;
  });

  class WindowResizeComponent extends Component {

    static computeBreakpoint(width, breakpointsVal) {
      if (breakpointsVal.length === 0) return null;

      if (width < breakpointsVal[0]) return 0;

      for (let i = 0; i < breakpointsVal.length - 1; i++)
        if (breakpointsVal[i] < width && width < breakpointsVal[i + 1])
          return i + 1;

      if (width > breakpointsVal[breakpointsVal.length - 1])
        return breakpointsVal.length;

      return 0;
    }

    constructor(props) {
      super(props);
      this.onResize = this.onResize.bind(this);
      const windowWidth = getWindowWidth();
      const currentBreakpoint = WindowResizeComponent.computeBreakpoint(windowWidth, breakpoints);
      this.state = {
        windowWidth,
        currentBreakpoint
      };
    }

    componentDidMount() {
      this.onResize = throttle(this.onResize, THROTTLE_RATE);

      if (this.resizeComponent.focus)
        this.focus = this.resizeComponent.focus.bind(this.resizeComponent);

      if (this.resizeComponent.blur)
        this.blur = this.resizeComponent.blur.bind(this.resizeComponent);

      window.addEventListener('resize', this.onResize);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.onResize);
    }

    onResize() {
      const windowWidth = getWindowWidth();
      const currentBreakpoint = WindowResizeComponent.computeBreakpoint(windowWidth, breakpointsArray);
      if (breakpointsArray.length === 0 || currentBreakpoint !== this.state.currentBreakpoint) {
        this.setState({ windowWidth, currentBreakpoint });
        if (payload && typeof payload === 'function') {
          payload(this.props, windowWidth);
        }
      }
    }

    focus = () => {
      if (this.resizeComponent && this.resizeComponent) {
        this.resizeComponent.focus();
      }
    };

    render() {
      // Only send payload if both is true or if no payload is defined
      const widthProp = !both && payload !== null ? {} : { windowWidth: this.state.windowWidth };
      return <MyComponent
        {...this.props}
        ref={(el) => { this.resizeComponent = el; }}
        {...widthProp}
      />;
    }
  }

  return WindowResizeComponent;
}

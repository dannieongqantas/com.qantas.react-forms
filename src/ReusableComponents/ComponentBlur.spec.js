import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode, unmountComponentAtNode } from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import ComponentBlur from './ComponentBlur';
import each from 'lodash/each';

const COMPONENT_REF = 'mockComponentRef';

class DummyComponent extends Component {
  static propTypes = {
    onBlur: PropTypes.func,
    blur: PropTypes.func
  };

  render() {
    return <div ref={(el) => { this[COMPONENT_REF] = el; }}>
      <input onBlur={this.props.blur} />
    </div>;
  }
}

const EnhancedDummyComponent = ComponentBlur(
  DummyComponent,
  COMPONENT_REF,
  () => ({
    type: 'Test'
  })
);

describe('ComponentBlur', () => {
  let renderedComponent;
  let sandbox;
  let addEventListenerSpy;
  let removeEventListenerSpy;
  const passedProps = {
    yoda: 'is tiny',
    sidious: 'is ugly'
  };

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    addEventListenerSpy = sandbox.stub(document.getElementsByTagName('body')[0], 'addEventListener');
    removeEventListenerSpy = sandbox.stub(document.getElementsByTagName('body')[0], 'removeEventListener');
    const renderedComponentWithContext = TestUtils.renderIntoDocument(
      <EnhancedDummyComponent {...passedProps} />
    );

    renderedComponent = TestUtils.findRenderedComponentWithType(renderedComponentWithContext, EnhancedDummyComponent);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render the component correctly', () => {
    const renderedElement = findDOMNode(renderedComponent);
    expect(renderedElement).to.exist;
  });

  it('should register the leaveField listener on mount', () => {
    expect(addEventListenerSpy).to.have.been.calledWith('mousedown', renderedComponent.leaveField, false);
  });

  it('should unregister the leaveField listener on unmount', () => {
    unmountComponentAtNode(findDOMNode(renderedComponent).parentNode);
    expect(removeEventListenerSpy).to.have.been.calledWith('mousedown', renderedComponent.leaveField, false);
  });

  it('should pass down the props', () => {
    each(passedProps, (n, key) => {
      expect(renderedComponent.props[key]).to.equal(n);
    });
  });

  it('checkIfBlurred does not throw when referenced component becomes undefined', () => {
    renderedComponent[`blurred${COMPONENT_REF}`] = undefined;
    expect(renderedComponent.checkIfBlurred()).to.not.throw;
  });
});

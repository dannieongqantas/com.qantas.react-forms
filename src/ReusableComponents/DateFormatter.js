import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { retrieveLocaleDateFormatter } from '../utils/date';

export default function dateFormatterWrapper(MyComponent, getLanguageCodeFunc) {
  return class DateFormatter extends Component {
    static propTypes = {
      languageCode: PropTypes.string
    };

    constructor(props) {
      super(props);

      this.state = {
        dateFormatter: () => {}
      };

      const languageCode = getLanguageCodeFunc ? getLanguageCodeFunc(props) : props.languageCode;

      retrieveLocaleDateFormatter(languageCode).then(dateFormatterFunc => {
        this.setState({ dateFormatter: dateFormatterFunc });
      });
    }

    focus = () => {
      if (this.myComponent && this.myComponent.focus) {
        this.myComponent.focus();
      }
    }

    render() {
      return <MyComponent {...this.props} ref={(myComponent) => { this.myComponent = myComponent; }} dateFormatter={this.state.dateFormatter} />;
    }
  };
}

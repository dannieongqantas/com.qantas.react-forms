import React from 'react';
import ButtonLink from './ButtonLink';
import { mount } from 'enzyme';

describe('ButtonLink', () => {
  let element;
  let onClickSpy;

  beforeEach(() => {
    onClickSpy = sinon.spy();
    element = mount(
      <ButtonLink text="Hello world" url="http://yolo.com" onClick={onClickSpy} className="hello world" ariaLabel="screenReader" />
    );
  });

  it('contain the correct text', () => {
    expect(element.text()).to.equal('Hello world');
  });

  it('should contain the correct classes', () => {
    expect(element.find('div').nodes[0].getAttribute('class')).to.contain('hello world');
  });

  it('contains the correct url', () => {
    expect(element.find('a').nodes[0].getAttribute('href')).to.equal('http://yolo.com');
  });

  it('contains the correct aria label', () => {
    expect(element.find('button').nodes[0].getAttribute('aria-label')).to.equal('screenReader');
  });

  describe('prefetch', () => {
    it('should not contain a prefetch link when not turned on', () => {
      expect(element.find('link').nodes.length).to.equal(0);
    });

    it('should contain a prefetch link with the url when turned on', () => {
      element.setProps({ prefetch: true });
      const nodes = element.find('link').nodes;
      expect(nodes.length).to.equal(1);
      expect(nodes[0].getAttribute('rel')).to.equal('prefetch');
      expect(nodes[0].getAttribute('href')).to.equal('http://yolo.com');
    });
  });

  it('should trigger the on click spy', () => {
    element.find('a').simulate('click');
    expect(onClickSpy).to.have.been.called;
  });
});

import { shallow } from 'enzyme';

describe('SubmitButton', () => {
  let wrapper;
  let btn;
  const SubmitButton = require('./Button').default; // eslint-disable-line

  const props = {
    text: 'hello',
    ariaLabel: 'cocopops'
  };

  before(() => {
    wrapper = shallow(<SubmitButton {...props} />);
    btn = wrapper.find('button').first();
  });

  afterEach(() => {
    wrapper = shallow(<SubmitButton {...props} />);
    btn = wrapper.find('button').first();
  });

  it('should contain the i18n message', () => {
    expect(btn.text()).to.equal(props.text);
  });

  it('should contain the aria label', () => {
    expect(btn.prop('aria-label')).to.equal(props.ariaLabel);
  });

  it('is enabled by default', () => {
    expect(btn.prop('disabled')).to.equal(false);
    expect(btn.html()).not.to.contain('disabled');
  });

  it('can be disabled using a "disabled" prop', () => {
    wrapper = shallow(<SubmitButton {...props} disabled />);
    btn = wrapper.find('button').first();

    expect(btn.prop('disabled')).to.equal(true);
    expect(btn.html()).to.contain('disabled');
  });
});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { RETURN_KEY_CODE } from '../Constants/KeyCodes';

export default class ButtonLink extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    type: PropTypes.string,
    className: PropTypes.string,
    ariaLabel: PropTypes.string,
    onClick: PropTypes.func,
    prefetch: PropTypes.bool
  };

  static defaultProps = {
    type: 'submit',
    className: '',
    onClick: () => {
    },
    prefetch: false
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.buttonClick = this.buttonClick.bind(this);
  }

  handleKeyDown(e) {
    if (e.keyCode === RETURN_KEY_CODE) this.buttonClick();
  }

  buttonClick() {
    this.refs.link.click();
  }

  render() {
    const { url, prefetch, ...props } = this.props;

    const prefetchLink = prefetch ? <link rel="prefetch" href={url} /> : null;

    const classnames = classNames('widget-form__group-container', props.className);

    return (
      <div className={classnames}>
        <div className="widget-form__group">
          {prefetchLink}
          <a className="widget-form__element--hidden"
             ref="link"
             onClick={props.onClick}
             href={url}>
          </a>
          <button className="qfa1-submit-button__button"
                  type={props.type}
                  onClick={this.buttonClick}
                  onKeyDown={this.handleKeyDown} aria-label={props.ariaLabel}>
            {props.text}
          </button>
        </div>
      </div>
    );
  }
}

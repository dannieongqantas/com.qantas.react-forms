import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RETURN_KEY_CODE } from '../Constants/KeyCodes';

export default class SubmitButton extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    type: PropTypes.string,
    className: PropTypes.string,
    ariaLabel: PropTypes.string,
    onClick: PropTypes.func,
    onBlur: PropTypes.func,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    type: 'submit',
    className: 'qfa1-submit-button__container-right size-big',
    onClick: () => {},
    onBlur: () => {},
    disabled: false
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  handleKeyDown(e) {
    if (e.keyCode === RETURN_KEY_CODE && this.props.type === 'submit') this.props.onClick(e);
  }

  render() {
    const props = this.props;

    return (
      <div className={props.className}>
        <div className="widget-form__group">
          <button className="qfa1-submit-button__button"
                  type={props.type}
                  onClick={props.onClick}
                  onBlur={props.onBlur}
                  onKeyDown={this.handleKeyDown}
                  aria-label={props.ariaLabel}
                  disabled={props.disabled}>{props.text}
          </button>
        </div>
      </div>
    );
  }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import InfoIcon from '../Icons/InfoIcon';
import BorderTriangle from '../Icons/BorderTriangle';

export default class Tooltip extends Component {
  static propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    align: PropTypes.string,
    children: PropTypes.node
  };

  static defaultProps = {
    align: 'center',
    title: 'Information Icon'
  };

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      posTop: 0,
      posLeft: 0
    };
  }

  getTooltipPosLeft = (width) => {
    const tooltipWidthOffset = 20;

    switch (this.props.align) {
      case 'center':
        return `-${width / 2}px`;
      case 'right':
        return `-${width - tooltipWidthOffset}px`;
      case 'left':
      default:
        return '0px';
    }
  }

  showTooltip = () => {
    const { width, height } = (this.refs.tooltipContent.getBoundingClientRect());
    const arrowOffset = 45;

    this.setState({
      posTop: `${- height - arrowOffset}px`,
      posLeft: this.getTooltipPosLeft(width),
      isVisible: true
    });
  }

  hideToolTip = () => {
    this.setState({
      isVisible: false
    });
  }

  render() {
    const { children, className, align, title } = this.props;
    const { isVisible, posTop, posLeft } = this.state;
    const tooltipClasses = classNames(className, 'qfa1-tool-tip');
    const contentClass = classNames('qfa1-tool-tip__content', { 'qfa1-tool-tip__hidden': !isVisible });
    const arrowClass = classNames('qfa1-tool-tip__arrow', `qfa1-tool-tip__arrow-${align}`);

    return children ? (
      <div className={tooltipClasses} onClick={this.showTooltip} onFocus={this.showTooltip} onBlur={this.hideToolTip}>
        <div tabIndex="0" className="qfa1-tool-tip__icon">
          <InfoIcon title={title} />
        </div>
        <div style={{ marginTop: posTop, marginLeft: posLeft }} ref="tooltipContent" className={contentClass}>
          {children}
          <div className={arrowClass}>
            <BorderTriangle innerColor="#bff4f2" outerColor="#bff4f2" />
          </div>
        </div>
      </div>
    ) :
    null;
  }
}

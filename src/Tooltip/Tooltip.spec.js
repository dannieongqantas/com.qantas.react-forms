import React from 'react';
import TestUtils from 'react-dom/test-utils';
import ComponentStub from '../../test/utils/ComponentStub';

const proxyquire = require('proxyquire').noCallThru();
const InfoIconStub = ComponentStub('div');
const BorderTriangleStub = ComponentStub('div');


const Tooltip = proxyquire('./Tooltip', {
  '../Icons/InfoIcon': InfoIconStub,
  '../Icons/BorderTriangle': BorderTriangleStub
}).default;

const baseProps = {
  className: 'some-class',
  title: 'info-icon',
  align: 'left',
  children: 'Some tooltip text'
};

const shallowRenderTooltip = (props = baseProps) => {
  const passedProp = { ...baseProps, ...props };
  const shallowRenderer = TestUtils.createRenderer();
  shallowRenderer.render(<Tooltip {...passedProp} />);
  const formOutput = shallowRenderer.getRenderOutput();
  return formOutput;
};

const renderTooltip = (props = baseProps) => {
  const passedProp = { ...baseProps, ...props };
  return TestUtils.renderIntoDocument(
    <Tooltip {...passedProp} />
  );
};

describe('Tooltips', () => {
  let renderedOutput;

  context('when the tooltip has content', () => {
    describe('render', () => {
      before(() => {
        renderedOutput = shallowRenderTooltip(null);
      });

      it('should create tooltip with a div element', () => {
        expect(renderedOutput.type).to.equal('div');
      });

      it('should create tooltip with ClassName passed as prop', () => {
        expect(renderedOutput.props.className).to.equal('some-class qfa1-tool-tip');
      });

      it('should set onMouseOver and onMouseOut events', () => {
        expect(renderedOutput.props.onMouseOver).isFunction;
        expect(renderedOutput.props.onMouseOut).isFunction;
      });
    });
  });

  context('when the tooltip does not have content', () => {
    describe('render', () => {
      before(() => {
        renderedOutput = shallowRenderTooltip({ children: '' });
      });

      it('should not create tooltip with a div element', () => {
        expect(renderedOutput).to.equal(null);
      });
    });
  });


  describe('Icon', () => {
    let iconComponent;
    before(() => {
      renderedOutput = shallowRenderTooltip(null, true);
      iconComponent = renderedOutput.props.children[0];
    });

    it('should contain the info Icon', () => {
      expect(iconComponent.props.children.props.title).to.equal(baseProps.title);
      expect(iconComponent.props.children.type).to.equal(InfoIconStub);
    });

    it('should have the tab index set', () => {
      expect(iconComponent.props.tabIndex).to.equal('0');
    });

    it('should have the onHover and onBlur events set', () => {
      expect(iconComponent.props.onFocus).isFunction;
      expect(iconComponent.props.onBlur).isFunction;
    });

    it('should have assigned className', () => {
      expect(iconComponent.props.className).to.equal('qfa1-tool-tip__icon');
    });
  });

  describe('Content', () => {
    let contentComponent;
    before(() => {
      renderedOutput = shallowRenderTooltip(null, true);
      contentComponent = renderedOutput.props.children[1];
    });

    it('should be a div with assigned ref', () => {
      expect(contentComponent.ref).to.equal('tooltipContent');
      expect(contentComponent.type).to.equal('div');
    });

    it('should have assigned props', () => {
      expect(contentComponent.props.className).to.equal('qfa1-tool-tip__content qfa1-tool-tip__hidden');
      expect(contentComponent.props.style).to.deep.equal({ marginLeft: 0, marginTop: 0 });
    });

    it('should enclose a content and a arrow element', () => {
      expect(contentComponent.props.children[0]).to.equal(baseProps.children);
      expect(contentComponent.props.children[1].type).to.equal('div');
    });

    it('should set the align prop to the arrow container className', () => {
      expect(contentComponent.props.children[1].props.className).to.equal('qfa1-tool-tip__arrow qfa1-tool-tip__arrow-left');
    });

    it('should have the arrow icon', () => {
      expect(contentComponent.props.children[1].props.children.type).to.equal(BorderTriangleStub);
    });
  });

  describe('Tooltip Events', () => {
    let tooltipComponent;
    let container;
    let getDimensionStub;

    before(() => {
      tooltipComponent = renderTooltip();
      container = TestUtils.findRenderedDOMComponentWithClass(tooltipComponent, 'qfa1-tool-tip');
      getDimensionStub = sinon.stub(tooltipComponent.refs.tooltipContent, 'getBoundingClientRect').returns({
        height: 5,
        width: 20
      });
    });

    after(() => {
      getDimensionStub.restore();
    });

    context('on click of the tooltip', () => {
      before(() => {
        TestUtils.Simulate.click(container);
      });

      it('should set isVisible to true, set posTop and posLeft', () => {
        expect(tooltipComponent.state.isVisible).to.equal(true);
        expect(tooltipComponent.state.posTop).to.equal('-50px');
        expect(tooltipComponent.state.posLeft).to.equal('0px');
      });
    });

    context('on clicking out of the tooltip', () => {
      before(() => {
        TestUtils.Simulate.blur(container);
      });

      it('should set isVisible to false', () => {
        expect(tooltipComponent.state.isVisible).to.equal(false);
      });
    });

    context('on focus of the tooltip icon', () => {
      before(() => {
        container = TestUtils.findRenderedDOMComponentWithClass(tooltipComponent, 'qfa1-tool-tip__icon');
        TestUtils.Simulate.focus(container);
      });

      it('should set isVisible to true, set posTop and posLeft', () => {
        expect(tooltipComponent.state.isVisible).to.equal(true);
        expect(tooltipComponent.state.posTop).to.equal('-50px');
        expect(tooltipComponent.state.posLeft).to.equal('0px');
      });
    });

    context('on focus when tooltip is aligned center', () => {
      before(() => {
        tooltipComponent = renderTooltip({ align: 'center' });
        container = TestUtils.findRenderedDOMComponentWithClass(tooltipComponent, 'qfa1-tool-tip__icon');
        getDimensionStub = sinon.stub(tooltipComponent.refs.tooltipContent, 'getBoundingClientRect').returns({
          height: 5,
          width: 30
        });
        TestUtils.Simulate.focus(container);
      });

      it('should set appropriate posLeft', () => {
        expect(tooltipComponent.state.posLeft).to.equal('-15px');
      });
    });

    context('on focus when tooltip is aligned right', () => {
      before(() => {
        tooltipComponent = renderTooltip({ align: 'right' });
        container = TestUtils.findRenderedDOMComponentWithClass(tooltipComponent, 'qfa1-tool-tip__icon');
        getDimensionStub = sinon.stub(tooltipComponent.refs.tooltipContent, 'getBoundingClientRect').returns({
          height: 5,
          width: 30
        });
        TestUtils.Simulate.focus(container);
      });

      it('should set appropriate posLeft', () => {
        expect(tooltipComponent.state.posLeft).to.equal('-10px');
      });
    });
  });
});


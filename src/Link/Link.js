import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinkIcon from '../Icons/LinkIcon';

export default class Link extends Component {
  static propTypes = {
    href: PropTypes.string.isRequired,
    className: PropTypes.string,
    ariaMessage: PropTypes.string,
    title: PropTypes.string,
    isExternal: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),
    withArrow: PropTypes.bool
  };

  static defaultProps = {
    className: 'qfa1-link',
    withArrow: false,
    isExternal: false,
    ariaMessage: 'Opens external site'
  };

  renderArrow() {
    return (this.props.withArrow) ? <LinkIcon /> : null;
  }

  renderScreenReaderMessage() {
    return (this.props.isExternal) ? <span className="widget-form__element--aria-hidden">{this.props.ariaMessage}</span> : null;
  }

  render() {
    const { href, className, title, children, ...otherProps } = this.props;
    return <a href={href} className={className} title={title} {...otherProps}>
      {children}
      {this.renderArrow()}
      {this.renderScreenReaderMessage()}
    </a>;
  }
}

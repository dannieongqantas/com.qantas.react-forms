import React from 'react';
import { shallow } from 'enzyme';
import Link from './Link';

describe('Link', () => {
  it('should render a link', () => {
    const wrapper = shallow(<Link href="url" withArrow="false" title="my link">Content</Link>);
    expect(wrapper.prop('href')).to.equal('url');
    expect(wrapper.prop('title')).to.equal('my link');
    expect(wrapper.text()).to.include('Content');
  });

  it('should render an arrow when withArrow is true', () => {
    const wrapper = shallow(<Link href="url" withArrow title="my link">Content</Link>);
    expect(wrapper.find('.svg__link-icon')).to.exist;
  });

  it('should render an aria message when isExternal is true', () => {
    const wrapper = shallow(<Link href="url" isExternal title="my link" ariaMessage="my message">Content</Link>);
    const ariaEl = wrapper.find('.widget-form__element--aria-hidden');
    expect(ariaEl).to.exist;
    expect(ariaEl.text()).to.equal('my message');
  });

  it('should bind given onClick event', () => {
    const onClickSpy = sinon.spy();
    const wrapper = shallow(<Link href="url" onClick={onClickSpy}>Content</Link>);
    wrapper.simulate('click');
    expect(onClickSpy).to.have.been.called;
  });
});

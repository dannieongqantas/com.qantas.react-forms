import React from 'react';
import TestUtils from 'react-dom/test-utils';
import FormInfoMessage from './FormInfoMessage';

const getMessageOutput = (props) => {
  const shallowRenderer = TestUtils.createRenderer();
  shallowRenderer.render(
    <FormInfoMessage {...props}><i /></FormInfoMessage>);
  return shallowRenderer.getRenderOutput();
};

describe('Form Messages', () => {
  let messageOutput;
  it('should render a message', () => {
    messageOutput = getMessageOutput({
      className: 'grid-class',
      content: 'Message',
      isVisible: true
    });
    expect(messageOutput.type).to.equal('div');
    expect(messageOutput.props.className).to.equal('qfa1-form-msg__container grid-class');
    expect(messageOutput.props.children).to.deep.equal([<i />, 'Message']);
    expect(messageOutput.props.tabIndex).to.deep.equal('0');
    expect(messageOutput.props.role).to.deep.equal('alert');
  });

  it('should not render message when isVisible is false', () => {
    messageOutput = getMessageOutput({
      className: 'grid-class',
      content: 'Message',
      isVisible: false
    });
    expect(messageOutput).to.be.equal(null);
  });

  it('should not render message when the content is an empty string', () => {
    messageOutput = getMessageOutput({
      className: 'grid-class',
      content: '',
      isVisible: true
    });
    expect(messageOutput).to.be.equal(null);
  });

  it('should not render message when the content is undefined', () => {
    messageOutput = getMessageOutput({
      className: 'grid-class',
      content: undefined,
      isVisible: true
    });
    expect(messageOutput).to.be.equal(null);
  });
});

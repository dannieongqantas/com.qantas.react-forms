import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function FormInfoMessage({ isVisible, content, className, children }) {
  const showMessage = (isVisible && content);
  const messageClass = classNames('qfa1-form-msg__container', className);
  return (showMessage ?
    <div role="alert" tabIndex="0" className={messageClass}>{children}{content}</div> :
    null);
}

FormInfoMessage.propTypes = {
  className: PropTypes.string,
  content: PropTypes.string.isRequired,
  isVisible: PropTypes.bool.isRequired,
  children: PropTypes.node
};

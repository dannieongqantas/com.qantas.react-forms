# React Form Elements

## Install
```npm install com.qantas.react-forms --save```

## Typeahead

[Typeahead](./src/Typeahead/Typeahead.md)

## Checkbox

[Checkbox](./src/Checkbox/Checkbox.md)

## Development

### Start up dev server
```npm run dev```
Open browser in http://localhost:9001/<ComponentName>

e.g http://localhost:9001/use/button

### Coding
Put a component reference in
- Use
  - Components
    - Button.js
    - DatePicker.js
    - YourComponent.js
    - YourComponent.sass

### Isomorphic
Check if your code is isomorphic (renderable on the server)
http://locahost:9001/use/button?server=true



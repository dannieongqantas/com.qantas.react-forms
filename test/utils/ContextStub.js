import { Component, PropTypes, Children } from 'react';
import each from 'lodash/each';

export default function contextStub(context) {
  class ContextStub extends Component {
    static propTypes = {
      children: PropTypes.element
    };

    getChildContext() {
      return context;
    }

    render() {
      return Children.only(this.props.children);
    }
  }

  ContextStub.childContextTypes = {};

  each(context, (n, key) => {
    ContextStub.childContextTypes[key] = PropTypes.any;
  });

  return ContextStub;
}

import React, { Component } from 'react';

export default function createComponentStub(element = 'div', props = {}) {
  class ComponentStub extends Component {
    static createElement(elm, newProps) {
      return React.createElement(elm, newProps);
    }

    render() {
      const newProps = {
        ...this.props,
        ...props
      };
      return ComponentStub.createElement(element, newProps);
    }
  }

  return ComponentStub;
}

import TestUtils from 'react-dom/test-utils';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import sinon from 'sinon';
import { jsdom } from 'jsdom';
import chaiAsPromised from 'chai-as-promised';
import chaiEnzyme from 'chai-enzyme';
import React from 'react';

const path = require('path');
require('app-module-path').addPath(path.join(__dirname, '..', 'src'));

const proxyquire = require('proxyquire').noCallThru();

// Delete require.cached files
// The --include-all-sources saves all files in the cache before mocha runs which can cause problems
Object.keys(require.cache)
  .filter(filePath => filePath.indexOf('node_modules') === -1)
  .forEach(filePath => delete require.cache[filePath]);

global.chai = chai;
global.expect = expect;
global.sinon = sinon;
global.TestUtils = TestUtils;
global.React = React;

global.proxyquire = (request, stubs, returnDefault = true) => {
  const result = proxyquire(request, stubs);
  return returnDefault ? (result.default || result) : result;
};

chai.use(sinonChai);
chai.use(chaiAsPromised);
chai.use(chaiEnzyme());

// setup the simplest document possible
const doc = jsdom('<!doctype html><html><body></body></html>');

// get the window object out of the document
const win = doc.defaultView;

// set globals for mocha that make access to document and window feel
// natural in the test environment
global.document = doc;
global.window = win;

global.window.localStorage = { getItem: () => '{}', setItem: () => '{}', removeItem() {} };

// take all properties of the window object and also attach it to the
// mocha global object
// from mocha-jsdom https://github.com/rstacruz/mocha-jsdom/blob/master/index.js#L80
(window => {
  Object.keys(window).forEach(key => {
    if (!global[key] && window.hasOwnProperty(key)) global[key] = window[key];
  });
})(win);

// Ignore scss modules for testing
require.extensions['.scss'] = (module, filename) => module._compile('', filename); //eslint-disable-line

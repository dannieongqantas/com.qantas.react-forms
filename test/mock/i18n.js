/*eslint-disable */

export default {
    language: 'English',
    shared: {
        typeahead: {
            noResultsText: 'We can\'t find a matching location for {value}'
        },
        errors: {
            generic: {
                header: 'Sorry, it looks like there\'s been an issue with your search',
                message: 'Please try again later.'
            }
        },
        amadeusResponseError: {
            message: 'An error has occurred',
            subtext: 'Please modify your search and try again.'
        },
        aria: {
            clearButton: 'Clear Button. Click on this to clear all text',
            toggleNextMonth: 'Click this to go to the next month',
            togglePreviousMonth: 'Click this to go to the previous month',
            typeaheadSuggestions: '{numResults} suggestions, use up and down arrow keys to navigate',
            selection: 'You have selected {selection}',
            dateLabel: 'Current date selected is {dateSelected}. Select a pick up date from the calendar by using up and down arrow keys to navigate consecutive weeks. Use left and right keys to navigate consecutive days. You can only select a pick up date up to {maxDays} days from today inclusive. Text field is disabled',
            dateLong: '{weekDay}, the {dayOfMonth} of {month} {year}'
        },
        months: {
            1: {
                short: 'jan',
                long: 'january'
            },
            2: {
                short: 'feb',
                long: 'february'
            },
            3: {
                short: 'mar',
                long: 'march'
            },
            4: {
                short: 'apr',
                long: 'april'
            },
            5: {
                short: 'may',
                long: 'may'
            },
            6: {
                short: 'jun',
                long: 'june'
            },
            7: {
                short: 'jul',
                long: 'july'
            },
            8: {
                short: 'aug',
                long: 'august'
            },
            9: {
                short: 'sep',
                long: 'september'
            },
            10: {
                short: 'oct',
                long: 'october'
            },
            11: {
                short: 'nov',
                long: 'november'
            },
            12: {
                short: 'dec',
                long: 'december'
            }
        },
        weekdays: {
            1: {
                short: 'mon',
                long: 'monday'
            },
            2: {
                short: 'tue',
                long: 'tuesday'
            },
            3: {
                short: 'wed',
                long: 'wednesday'
            },
            4: {
                short: 'thu',
                long: 'thursday'
            },
            5: {
                short: 'fri',
                long: 'friday'
            },
            6: {
                short: 'sat',
                long: 'saturday'
            },
            7: {
                short: 'sun',
                long: 'sunday'
            }
        },
        dayOfMonth: {
            1: 'first',
            2: 'second',
            3: 'third',
            4: 'fourth',
            5: 'fifth',
            6: 'sixth',
            7: 'seventh',
            8: 'eighth',
            9: 'ninth',
            10: 'tenth',
            11: 'eleventh',
            12: 'twelfth',
            13: 'thirteenth',
            14: 'fourteenth',
            15: 'fifteenth',
            16: 'sixteenth',
            17: 'seventeenth',
            18: 'eighteenth',
            19: 'nineteenth',
            20: 'twentieth',
            21: 'twenty first',
            22: 'twenty second',
            23: 'twenty third',
            24: 'twenty fourth',
            25: 'twenty fifth',
            26: 'twenty sixth',
            27: 'twenty seventh',
            28: 'twenty eighth',
            29: 'twenty ninth',
            30: 'thirtieth',
            31: 'thirty first'
        },
        number: {
            1: 1, //could be one
            2: 2,
            3: 3,
            4: 4,
            5: 5,
            6: 6,
            7: 7,
            8: 8,
            9: 9
        }
    },
    flight: {
        flexibleDates: {
            formLabel: 'Flexible with dates',
            ariaLabel: 'Flexible with dates checkbox',
            ariaLiveChecked: 'You have selected flexible dates',
            ariaLiveUnchecked: 'You have unselected flexible dates'
        },
        routeType : {
          legend : 'Booking options',
            nonClassic: {
              formLabel: 'Use money, points or both',
              ariaLabel: 'Use money, points or both',
              ariaLiveChecked: 'You have selected Use money, points or both',
              ariaLiveUnchecked: 'You have unselected Use money, points or both'
            },
            classic: {
              formLabel: 'Use points - Classic Flight Rewards only',
              ariaLabel: 'Use points - Classic Flight Rewards only',
              ariaLiveChecked: 'You have selected Use points - Classic Flight Rewards only',
              ariaLiveUnchecked: 'You have unselected Use points - Classic Flight Rewards only'
            }
        },
        travelClass: {
            formLabel: 'Travel Class',
            ariaLabel: 'pick a travel class, you can use the up and down arrows to select a travel class. Text field is disabled',
            initialItem: 0
        },
        travelClassItems: [
            {text: 'Economy', ariaLabel: 'Economy', backendCode: 'ECO'},
            {text: 'Premium Economy', ariaLabel: 'Premium Economy', backendCode: 'PRM'},
            {text: 'Business', ariaLabel: 'Business', backendCode: 'BUS'},
            {text: 'First', ariaLabel: 'First', backendCode: 'FIR'},
            {text: 'Business/First', ariaLabel: 'Business First', backendCode: 'BUS'}
        ],
        departureDate: {
            formLabel: 'Departure Date',
            ariaLabel: 'Current date selected is {dateSelected}. Select a departure date from the calendar by using up and down arrow keys to navigate consecutive weeks. Use left and right keys to navigate consecutive days. You can only select a departure date up to {maxDays} days from today inclusive. Text field is disabled'
        },
        returnDate: {
            formLabel: 'Return date',
            ariaLabel: 'Current date selected is {dateSelected}. Select a return date from the calendar by using up and down arrow keys to navigate consecutive weeks. Use left and right keys to navigate consecutive days. You can only select a return date up to {maxDays} days from the departure date inclusive. Text field is disabled'
        },
        fromLocation: {
            formLabel: 'From Airport',
            ariaLabel: 'Please type at least 3 characters'
        },
        toLocation: {
            formLabel: 'To Airport',
            ariaLabel: 'Type at least 3 characters'
        },
        passengers: {
            adults: {
              singular: 'Adult',
              plural: 'Adults',
              label: 'Adults',
              ariaLabel: 'Adults (sixteen years old and above). Select the number of adults between one to nine. Press up and down arrow keys to increase and decrease the number of adults selected',
              ariaMinLimitMessage:'A minimum of one adult is required',
              ariaMaxLimitMessage:'You cannot select more than nine adults'
            },
            youths: {
              singular: 'Youth',
              plural: 'Youths',
              label: 'Youths (12 - 15yrs)',
              ariaLabel: 'Youths (twelve to fifteen years old inclusive). Select the number of youths between one to nine. Press up and down arrow keys to increase and decrease the number of youths selected',
              ariaMinLimitMessage:'You currently have zero youths selected',
              ariaMaxLimitMessage:'You cannot select more than nine youths'
            },
            children: {
              singular: 'Child',
              plural: 'Children',
              ariaLabel: 'Children (three to twelve years old inclusive). Select the number of children between zero to nine. Press up and down arrow keys to increase and decrease the number of children selected',
              label: 'Children (3 - 12yrs)',
              ariaMinLimitMessage:'You currently have zero children selected',
              ariaMaxLimitMessage:'You cannot select more than nine children'
            },
            infants: {
              singular: 'Infant',
              plural: 'Infants',
              ariaLabel: 'Infants (zero to two years old inclusive). Select the number of infants between zero to nine. Press up and down arrow keys to increase and decrease the number of infants selected',
              label: 'Infants (0 - 2yrs)',
              ariaMinLimitMessage:'You currently have zero infants selected',
              ariaMaxLimitMessage:'You cannot select more than nine infants'
            },
            buttonText: 'SELECT',
            formLabel: 'Passengers',
            ariaLabel: 'Please tell us how many passengers there are',
            ariaSelectButton: 'select button aria'
        },
        flightSubmitButton: 'SEARCH FLIGHTS'
    },
    car: {
        pickupDate: {
            formLabel: 'Pick-up date',
            ariaLabel: 'Current date selected is {dateSelected}. Select a pick up date from the calendar by using up and down arrow keys to navigate consecutive weeks. Use left and right keys to navigate consecutive days. Use the n key to navigate to the next month. Use the p key to navigate to the previous month. You can only select a pick up date up to {maxDays} days from today inclusive. Text field is disabled',
            minDateMessage: 'You can not select a date earlier than today',
            maxDateMessage: 'The next date is outside of the booking period',
            minWeekMessage: 'You can not select a date in the past',
            maxWeekMessage: 'The next week is outside of the booking period',
            minMonthMessage: 'You can not select a date in the past',
            maxMonthMessage: 'The next month is outside of the booking period'
        },
        dropoffDate: {
            formLabel: 'Drop-off date',
            ariaLabel: 'Current date selected is {dateSelected}. Select a drop off date from the calendar by using up and down arrow keys to navigate consecutive weeks. Use left and right keys to navigate consecutive days. Use the n key to navigate to the next month. Use the p key to navigate to the previous month. You can only select a drop off date up to {maxDays} days from the pick-up date inclusive. Text field is disabled',
            minDateMessage: 'You can not select a dropoff date earlier than the pickup date',
            maxDateMessage: 'The next date is outside of the booking period',
            minWeekMessage: 'You can not select a dropoff date earlier than the pickup date',
            maxWeekMessage: 'The next date is outside of the booking period',
            minMonthMessage: 'You can not select a dropoff date earlier than the pickup date',
            maxMonthMessage: 'The next date is outside of the booking period'
        },
        pickupTime: {
            formLabel: 'Pick-up time',
            ariaLabel: 'pick a pick up time using thirty minute intervals in twenty four hour format, you can use the up and down arrows to select a time. Text field is disabled',
            initialItem: 20
        },
        dropoffTime: {
            formLabel: 'Drop-off time',
            ariaLabel: 'pick a drop off time using thirty minute intervals in twenty four hour format, you can use the up and down arrows to select a time. Text field is disabled',
            initialItem: 20
        },
        pickupLocation: {
            formLabel: 'Pick-up location',
            ariaLabel: 'Please type at least 3 characters. To clear all text tab onto the clear button'
        },
        dropoffLocation: {
            formLabel: 'Drop-off location',
            ariaLabel: 'Type at least 3 characters. To clear all text tab onto the clear button',
            checkbox: {
                formLabel: 'Drop off at a different location',
                ariaLabel: 'Checking this checkbox will display the Drop Off Location field as the next form element',
                ariaLiveChecked: 'You have selected to use a different Drop Off Location, the Drop Off Location field is now showing as the next form element',
                ariaLiveUnchecked: 'You have not selected to use a different Drop Off Location, the Drop Off Location field is now hidden'
            }
        },
        spinner: {
            topText: 'We\'re finding the best rates for your car hire, not long now.',
            bottomText: 'Get great deals on hotels, insurance, activities and more when you book with us.'
        },
        carSubmitButton: 'FIND YOUR CAR',
        errors: {
            locations: {
                header: 'It looks like something went wrong there',
                message: 'You can [try again](retryLink) now or come back later.'
            },
            search: {
                generic: {
                    header: 'We\'re having a problem with our car search',
                    message: 'We\'re working on fixing this. You can [try again](retryLink) now or come back later.'
                },
                'error.retrieving.frequent.flyer.data':{
                    header:'It looks like something went wrong retrieving your Frequent Flyer details',
                    message:'You can [try again](retryLink) now or come back later.'
                }
            }
        },
        validationMessages: {
            summaryMessage: 'We need the following information to continue:',
            pickupLocationRequired: 'Add a pick-up location',
            dropoffLocationRequired: 'Add a drop-off location',
            pickupTimeTooLate: 'The time you have entered is in the past, select a new time',
            pickupAfterDropoffTime: 'The pick-up time needs to be before the drop-off time'
        },
        time: [
            {text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00'},
            {text: '00:30', ariaLabel: 'zero thirty', backendCode: '00:30'},
            {text: '01:00', ariaLabel: 'zero one hundred', backendCode: '01:00'},
            {text: '01:30', ariaLabel: 'zero one thirty', backendCode: '01:30'},
            {text: '02:00', ariaLabel: 'zero two hundred', backendCode: '02:00'},
            {text: '02:30', ariaLabel: 'zero two thirty', backendCode: '02:30'},
            {text: '03:00', ariaLabel: 'zero three hundred', backendCode: '03:00'},
            {text: '03:30', ariaLabel: 'zero three thirty', backendCode: '03:30'},
            {text: '04:00', ariaLabel: 'zero four hundred', backendCode: '04:00'},
            {text: '04:30', ariaLabel: 'zero four thirty', backendCode: '04:30'},
            {text: '05:00', ariaLabel: 'zero five hundred', backendCode: '05:00'},
            {text: '05:30', ariaLabel: 'zero five thirty', backendCode: '05:30'},
            {text: '06:00', ariaLabel: 'zero six hundred', backendCode: '06:00'},
            {text: '06:30', ariaLabel: 'zero six thirty', backendCode: '06:30'},
            {text: '07:00', ariaLabel: 'zero seven hundred', backendCode: '07:00'},
            {text: '07:30', ariaLabel: 'zero seven thirty', backendCode: '07:30'},
            {text: '08:00', ariaLabel: 'zero eight hundred', backendCode: '08:00'},
            {text: '08:30', ariaLabel: 'zero eight thirty', backendCode: '08:30'},
            {text: '09:00', ariaLabel: 'zero nine hundred', backendCode: '09:00'},
            {text: '09:30', ariaLabel: 'zero nine thirty', backendCode: '09:30'},
            {text: '10:00', ariaLabel: 'ten hundred', backendCode: '10:00'},
            {text: '10:30', ariaLabel: 'ten thirty', backendCode: '10:30'},
            {text: '11:00', ariaLabel: 'eleven hundred', backendCode: '11:00'},
            {text: '11:30', ariaLabel: 'eleven thirty', backendCode: '11:30'},
            {text: '12:00', ariaLabel: 'twelve hundred', backendCode: '12:00'},
            {text: '12:30', ariaLabel: 'twelve thirty', backendCode: '12:30'},
            {text: '13:00', ariaLabel: 'thirteen hundred', backendCode: '13:00'},
            {text: '13:30', ariaLabel: 'thirteen thirty', backendCode: '13:30'},
            {text: '14:00', ariaLabel: 'fourteen hundred', backendCode: '14:00'},
            {text: '14:30', ariaLabel: 'fourteen thirty', backendCode: '14:30'},
            {text: '15:00', ariaLabel: 'fifteen hundred', backendCode: '15:00'},
            {text: '15:30', ariaLabel: 'fifteen thirty', backendCode: '15:30'},
            {text: '16:00', ariaLabel: 'sixteen hundred', backendCode: '16:00'},
            {text: '16:30', ariaLabel: 'sixteen thirty', backendCode: '16:30'},
            {text: '17:00', ariaLabel: 'seventeen hundred', backendCode: '17:00'},
            {text: '17:30', ariaLabel: 'seventeen thirty', backendCode: '17:30'},
            {text: '18:00', ariaLabel: 'eighteen hundred', backendCode: '18:00'},
            {text: '18:30', ariaLabel: 'eighteen thirty', backendCode: '18:30'},
            {text: '19:00', ariaLabel: 'nineteen hundred', backendCode: '19:00'},
            {text: '19:30', ariaLabel: 'nineteen thirty', backendCode: '19:30'},
            {text: '20:00', ariaLabel: 'twenty hundred', backendCode: '20:00'},
            {text: '20:30', ariaLabel: 'twenty thirty', backendCode: '20:30'},
            {text: '21:00', ariaLabel: 'twenty one hundred', backendCode: '21:00'},
            {text: '21:30', ariaLabel: 'twenty one thirty', backendCode: '21:30'},
            {text: '22:00', ariaLabel: 'twenty two hundred', backendCode: '22:00'},
            {text: '22:30', ariaLabel: 'twenty two thirty', backendCode: '22:30'},
            {text: '23:00', ariaLabel: 'twenty three hundred', backendCode: '23:00'},
            {text: '23:30', ariaLabel: 'twenty three thirty', backendCode: '23:30'}]
    },
    hotel: {
        guests: {
            adults: {
                singular: 'Adult',
                plural: 'Adults',
                label: 'Adults',
                ariaLabel: 'Adults (thirteen years old and above). Select the number of adults between one to nine. Press up and down arrow keys to increase and decrease the number of adults selected',
                ariaMinLimitMessage:'A minimum of one adult is required',
                ariaMaxLimitMessage:'You cannot select more than nine adults'
            },
            children: {
                singular: 'Child',
                plural: 'Children',
                ariaLabel: 'Children (three to twelve years old inclusive). Select the number of children between zero to nine. Press up and down arrow keys to increase and decrease the number of children selected',
                label: 'Children (3 - 12yrs)',
                ariaMinLimitMessage:'You currently have zero children selected',
                ariaMaxLimitMessage:'You cannot select more than nine children'
            },
            infants: {
                singular: 'Infant',
                plural: 'Infants',
                ariaLabel: 'Infants (zero to two years old inclusive). Select the number of infants between zero to nine. Press up and down arrow keys to increase and decrease the number of infants selected',
                label: 'Infants (0 - 2yrs)',
                ariaMinLimitMessage:'You currently have zero infants selected',
                ariaMaxLimitMessage:'You cannot select more than nine infants'
            },
            buttonText: 'SELECT',
            formLabel: 'Guests',
            ariaLabel: 'Select the number of guests. You can navigate to the adults, children and infants fields by using the tab and shift tab keys when the guest selector is expanded. The guest selector can be expanded or collapsed by using the space key. Text field is disabled',
            ariaSelectButton: 'Select button. Click this button to confirm you have selected {value}',
            ariaSelection: 'You have chosen {selection}'
        }
    }
}


export default {
  car: [
    {
      id: '1',
      code: 'ABX',
      name: 'Albury (Wodonga) City',
      country: {
        code: 'AU',
        name: 'Australia'
      },
      providers: {
        ZI: 'APT'
      }
    }, {
      id: '2',
      code: 'SYD',
      name: 'Sydney City',
      country: {
        code: 'AU',
        name: 'Australia'
      },
      providers: {
        ZI: 'APT'
      }
    },
    {
      id: '3',
      code: 'SYD',
      name: 'Austria',
      country: {
        code: 'AU',
        name: 'Australia'
      },
      providers: {
        ZI: 'APT'
      }
    }
  ],

  flight: [{
    name: 'Sydney',
    code: 'SYD',
    country: { name: 'Australia' }
  }, {
    name: 'Melbourne',
    code: 'MEL',
    country: { name: 'Australia' }
  }, {
    name: 'London (Heathrow)',
    code: 'LHR',
    country: { name: 'United Kingdom' }
  }, {
    name: 'Dubai',
    code: 'DXB',
    country: { name: 'United Arab Emirates ' }
  }]
};

# Changelogs

# 6.19.0
* Add `disabled` state to RadioButton component
* Add unit test to RadiobuttonIcon component

# 5.5.0
* Copy in date utils from the flight search widget to make refactoring them easier in future
* Remove some reliance on lodash to improve bundle sizes

# 5.2.0
* Upgrade React to 15.5.3

# 5.0.0
* Update WindowResize to use the new way of setting refs

# 4.0.0
* Update Componentblur and related components to use the new way of setting refs

# 3.15.0
* modified checkIfBlurred in the ComponentBlue component to check that
* the referenced component is defined before executing (in case referenced
* component has un-mounted or no longer exists)

# 3.14.0
* Added 'SELECT DEFAULT SELECTED ITEM' to SelectReducerCreator

# 3.6.1
* Added LinkArrow Icons to the React forms
* Moved Link Component

# 3.5.2
* Added Tooltips to the React forms

# 3.4.2
* add missing records to changelog

# 3.4.0
* fix inline label

# 3.3.0
* add event handling to Form and Input

# 3.2.0
* add tab key handling to Typeahead

# 3.1.0
* add dark theme

# 3.0.0
* changes for new Qantas font

#2.9.0
  Add Frequent Flyer Login Component and Form Message Component

# 2.8.1
* Add i18n reducer

# 2.6.0
* Added the children prop to the typeahead and also included classic rewards icon

# 2.3.1
* Added validationMessage as prop to the PeopleSelector

# 2.2.0
* Added disabled state for the date pickers

# 2.1.1
* Added an action to set the range of passenger selector

# 2.1.0
* Added the children prop to the calendar so that we can drop in child nodes to the calendar

# 2.0.1
* Add LocalStore to utils
* Remove unneccessary props from Svg and InputArea component
* Add greaterThan prototype to date
* Add analytics
* Add cookies to utils

# 1.7.1
* Added an action for repopulating the select box

# 1.6.0
* Added Input Component, Form Component and made minor changes to other components

# 1.4.3
* Fix button default class and input box sizing

# 1.4.2
* Fix datepicker style for non AEM integration

# 1.4.0
* change all gridClass prop to className

# 1.3.0

* Components use random ids rather than requiring an id prop
* PeopleSelector component now links up label with inputs properly
* Added CheckBox component

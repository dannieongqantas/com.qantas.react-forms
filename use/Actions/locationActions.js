import { RECEIVE_LOCATIONS, TYPEAHEAD_INPUT_CHANGE, TYPEAHEAD_CLEAR_FIELD, TYPEAHEAD_SELECT_ITEM } from '../ActionTypes/locationActionTypes';
import { locations } from '../data/locationData';

export function locationClearField(id) {
  return (dispatch) => {
    dispatch({
      type: TYPEAHEAD_CLEAR_FIELD,
      id
    });
  };
}

export function locationHandleChange(id, value) {
  return (dispatch, getState) => {
    const currentState = getState();

    if (value.length >= currentState.minLength) {
      // Reduced example - would normally fetch new results from API using current value
      dispatch({
        type: RECEIVE_LOCATIONS,
        locations,
        id
      });
    }

    dispatch({
      type: TYPEAHEAD_INPUT_CHANGE,
      value,
      id
    });
  };
}

export function locationSelectItem(id, value) {
  return (dispatch) => {
    const item = value.text;

    if (item) {
      // Reduced example - would normally fetch new results from API using current value
      dispatch({
        type: RECEIVE_LOCATIONS,
        locations,
        id
      });
    }

    dispatch({
      type: TYPEAHEAD_SELECT_ITEM,
      id,
      value
    });
  };
}

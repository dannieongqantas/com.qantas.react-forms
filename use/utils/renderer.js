import React, { Component, PropTypes } from 'react';
import Highlight from './Highlight';
import { render } from 'react-dom';
import canUseDOM from '../../src/utils/canUseDOM';
import debounce from 'lodash/debounce';

class RenderComponent extends Component {
  static propTypes = {
    initialProps: PropTypes.object,
    Component: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      props: props.initialProps,
      edit: true,
      mount: true
    };
    this.handleChange = debounce(this.handleChange.bind(this), 200);
    this.handleClick = this.handleClick.bind(this);
    this.handleRemount = this.handleRemount.bind(this);
  }

  handleChange(value) {
    let newData = ''; // eslint-disable-line
    eval(value.replace('var data', 'newData')); // eslint-disable-line
    this.setState({ props: newData });
  }

  handleClick() {
    this.setState({ edit: !this.state.edit });
  }

  handleRemount() {
    this.setState({ mount: false });
    setTimeout(() => {
      this.setState({ mount: true });
    });
  }

  renderEditor() {
    if (!this.state.edit) return null;
    return <div className="RenderComponent__Highlight">
      <Highlight onChange={this.handleChange} />
    </div>;
  }

  render() {
    const MyComponent = this.props.Component;

    return <div className="RenderComponent">
      <div className="RenderComponent__Component">
        {this.state.mount ? <MyComponent {...this.state.props} /> : null}
      </div>
      <button className="RenderComponent__Button" onClick={this.handleRemount}>Remount</button>
      <button className="RenderComponent__Button" onClick={this.handleClick}>{this.state.edit ? 'Edit' : 'Close'}</button>
      {this.renderEditor()}
    </div>;
  }
}

export default function renderer(MyComponent, initialProps) {
  if (canUseDOM) {
    render(<RenderComponent initialProps={initialProps} Component={MyComponent} />, document.getElementById('render'));
  }

  return () => <RenderComponent initialProps={initialProps} Component={MyComponent} />;
}

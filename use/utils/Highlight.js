import React, { Component, PropTypes } from 'react';
import AceEditor from 'react-ace';

import 'brace/theme/github';

const html = document.getElementById('data').textContent;

export default class Highlight extends Component {
  static propTypes = {
    onChange: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      value: html
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(newValue) {
    this.setState({ value: newValue });
    this.props.onChange(newValue);
  }

  render() {
    if (!this.state.value) return null;

    return <AceEditor
      mode="javascript"
      theme="github"
      onChange={this.onChange}
      name="UNIQUE_ID_OF_DIV"
      editorProps={{ $blockScrolling: true }}
      value={this.state.value}
      highlightActiveLine
      width="100%"
      showPrintMargin={false}
      enableBasicAutocompletion
    />;
  }
}

export const TYPEAHEAD_ID = 'TYPEAHEAD-123';

export const RECEIVE_LOCATIONS = 'RECEIVE_LOCATIONS';
export const TYPEAHEAD_CLEAR_FIELD = 'TYPEAHEAD_CLEAR_FIELD';
export const TYPEAHEAD_INPUT_CHANGE = 'TYPEAHEAD_INPUT_CHANGE';
export const TYPEAHEAD_SELECT_ITEM = 'TYPEAHEAD_SELECT_ITEM';

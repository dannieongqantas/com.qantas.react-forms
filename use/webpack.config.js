require('babel-core/register');
const webpack     = require('webpack');
const fs = require('fs');
const autoprefixer = require('autoprefixer');
const precss       = require('precss');
const host = (process.env.HOST || 'localhost');
const port = require('./Constants').PORT + 1;
const path = require('path');

function entryPath (name) {
  return [name ,'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://' + host + ':' + port]
}

const context = path.resolve(process.cwd(), "use/Components");
const entry = {};

fs.readdirSync(context)
  .filter((file) => /^\w*.js$/.test(file))
  .forEach((file) => {
    const name = file.substr(0, file.length - 3);
    const filePath = './' + file;
    entry[name] = entryPath(filePath);
  });

module.exports = {
  context,
  entry,
  cache: true,
  debug: true,
  devtool: "eval-source-map",

  output: {
    filename: '[name].js',
    path: path.resolve(process.cwd(), 'dist'),
    publicPath: 'http://' + host + ':' + port + '/dist'
  },

  devServer: {
    host: host,
    port: port,
    disableHostCheck: true,
    headers: { 'Access-Control-Allow-Origin': '*' }
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    preLoaders: [
      {test: /\.jsx?$/, loaders: ['eslint-loader']}
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.html$/,
        loader: "file?name=[name].[ext]"
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'postcss', 'sass']
      }
    ],
    postLoaders: [
      { test: /\.jsx?$/, loaders: ['react-hot', 'babel-loader'], exclude: /node_modules/}
    ],
    noParse: [
      /\/sinon.js/
    ]
  },
  postcss: function () {
    return [autoprefixer, precss];
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    })
  ]
};

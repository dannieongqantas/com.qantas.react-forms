export const locations = [
  {
    ariaLabel: 'Albury (Wodonga), Australia (ABX)',
    text: 'Albury (Wodonga), Australia (ABX)'
  },
  {
    ariaLabel: 'Brisbane, Australia (BNE)',
    text: 'Brisbane, Australia (BNE)'
  },
  {
    ariaLabel: 'Melbourne, Australia (MEL)',
    text: 'Melbourne, Australia (MEL)'
  },
  {
    ariaLabel: 'Perth, Australia (PER)',
    text: 'Perth, Australia (PER)'
  },
  {
    ariaLabel: 'Sydney, Australia (SYD)',
    text: 'Sydney, Australia (SYD)'
  }
];

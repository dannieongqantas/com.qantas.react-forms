export default {
  initialValue: {
    adults: 2,
    youths: 0,
    children: 0,
    infants: 0
  },
  onChange: value => console.log('passengers', value), // eslint-disable-line
  closeAndFocus: value => console.log('closed'), // eslint-disable-line
  copy: {
    adults: {
      singular: 'Adult',
      plural: 'Adults',
      label: 'Adults',
      ariaLabel: 'Choose between 1 to 9 adults, press up and down keys to increment or decrement'
    },
    youths: {
      singular: 'Youth',
      plural: 'Youths',
      label: 'Youths (12 - 15yrs)',
      ariaLabel: 'Choose between 1 to 9 youths, press up and down keys to increment or decrement'
    },
    children: {
      singular: 'Child',
      plural: 'Children',
      label: 'Children (2 - 12yrs)',
      ariaLabel: 'Choose between 1 to 9 children, press up and down keys to increment or decrement'
    },
    infants: {
      singular: 'Infant',
      plural: 'Infants',
      label: 'Infants (< 2yrs)',
      ariaLabel: 'Choose between 1 to 9 infants, press up and down keys to increment or decrement'
    },
    buttonText: 'SELECT',
    formLabel: 'Passengers',
    ariaLabel: 'Please tell us how many passengers there are',
    ariaSelectButton: 'Click this button to confirm you have selected {value}'
  }
};

import { RECEIVE_LOCATIONS, TYPEAHEAD_INPUT_CHANGE, TYPEAHEAD_CLEAR_FIELD, TYPEAHEAD_SELECT_ITEM } from '../ActionTypes/locationActionTypes';

export const initialState = {
  id: 'id',
  value: '',
  locations: [],
  selectedLocation: '',
  screenRead: '',
  customNoResults: '',
  minLength: 3
};

function locationReducer(state, action = {}) {
  if (action.id !== state.id) return state;

  switch (action.type) {
    case RECEIVE_LOCATIONS: {
      const newState = {
        ...state,
        locations: action.locations,
        customNoResults: ''
      };

      return newState;
    }

    case TYPEAHEAD_INPUT_CHANGE: {
      const newState = {
        ...state,
        value: action.value
      };

      if (action.value.length < 3) newState.locations = [];

      return newState;
    }

    case TYPEAHEAD_CLEAR_FIELD: {
      return { ...state, value: '' };
    }

    case TYPEAHEAD_SELECT_ITEM: {
      const selectedValue = action.value;

      if (!selectedValue) return state;

      return {
        ...state,
        selectedLocation: selectedValue,
        value: selectedValue.text
      };
    }

    default:
      return state;
  }
}

export default function (initialStateVal = {}) {
  return (state = { ...initialState, ...initialStateVal }, action) => locationReducer(state, action);
}
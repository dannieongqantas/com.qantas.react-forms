export default {
  medium: 768,
  large: 1280,
  xLarge: 1440,
  xxLarge: 1920
};

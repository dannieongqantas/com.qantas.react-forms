import renderer from '../utils/renderer';
import NumberPicker from '../../src/NumberPicker/NumberPicker';
import './NumberPicker.scss';

renderer(NumberPicker, {
  initialValue: 10,
  label: 'Pick a number',
  rangeStart: 0,
  rangeEnd: 20
});

import renderer from '../utils/renderer';
import { Select } from '../../src/Select/Select';
import './Select.scss';

export const props = {
  id: 'pickupTime',
  selectedItem: 20,
  opened: false,
  navigatedItem: 20,
  ariaNavigatedItem: -1,
  screenRead: '',
  backendCode: '10:00',
  formLabel: 'Pick-up time',
  ariaLabel: 'pick a pick up time using thirty minute intervals in twenty four hour format, you can use the up and down arrows to select a time. Text field is disabled',
  initialItem: 20,
  validationError: false,
  validationMessage: '',
  ariaSelection: 'You have selected {selection}',
  items: [{ text: '00:00', ariaLabel: 'zero hundred', backendCode: '00:00' }, {
    text: '00:30',
    ariaLabel: 'zero thirty',
    backendCode: '00:30'
  }, { text: '01:00', ariaLabel: 'zero one hundred', backendCode: '01:00' }, {
    text: '01:30',
    ariaLabel: 'zero one thirty',
    backendCode: '01:30'
  }, { text: '02:00', ariaLabel: 'zero two hundred', backendCode: '02:00' }, {
    text: '02:30',
    ariaLabel: 'zero two thirty',
    backendCode: '02:30'
  }, { text: '03:00', ariaLabel: 'zero three hundred', backendCode: '03:00' }, {
    text: '03:30',
    ariaLabel: 'zero three thirty',
    backendCode: '03:30'
  }, { text: '04:00', ariaLabel: 'zero four hundred', backendCode: '04:00' }, {
    text: '04:30',
    ariaLabel: 'zero four thirty',
    backendCode: '04:30'
  }, { text: '05:00', ariaLabel: 'zero five hundred', backendCode: '05:00' }, {
    text: '05:30',
    ariaLabel: 'zero five thirty',
    backendCode: '05:30'
  }, { text: '06:00', ariaLabel: 'zero six hundred', backendCode: '06:00' }, {
    text: '06:30',
    ariaLabel: 'zero six thirty',
    backendCode: '06:30'
  }, { text: '07:00', ariaLabel: 'zero seven hundred', backendCode: '07:00' }, {
    text: '07:30',
    ariaLabel: 'zero seven thirty',
    backendCode: '07:30'
  }, { text: '08:00', ariaLabel: 'zero eight hundred', backendCode: '08:00' }, {
    text: '08:30',
    ariaLabel: 'zero eight thirty',
    backendCode: '08:30'
  }, { text: '09:00', ariaLabel: 'zero nine hundred', backendCode: '09:00' }, {
    text: '09:30',
    ariaLabel: 'zero nine thirty',
    backendCode: '09:30'
  }, { text: '10:00', ariaLabel: 'ten hundred', backendCode: '10:00' }, {
    text: '10:30',
    ariaLabel: 'ten thirty',
    backendCode: '10:30'
  }, { text: '11:00', ariaLabel: 'eleven hundred', backendCode: '11:00' }, {
    text: '11:30',
    ariaLabel: 'eleven thirty',
    backendCode: '11:30'
  }, { text: '12:00', ariaLabel: 'twelve hundred', backendCode: '12:00' }, {
    text: '12:30',
    ariaLabel: 'twelve thirty',
    backendCode: '12:30'
  }, { text: '13:00', ariaLabel: 'thirteen hundred', backendCode: '13:00' }, {
    text: '13:30',
    ariaLabel: 'thirteen thirty',
    backendCode: '13:30'
  }, { text: '14:00', ariaLabel: 'fourteen hundred', backendCode: '14:00' }, {
    text: '14:30',
    ariaLabel: 'fourteen thirty',
    backendCode: '14:30'
  }, { text: '15:00', ariaLabel: 'fifteen hundred', backendCode: '15:00' }, {
    text: '15:30',
    ariaLabel: 'fifteen thirty',
    backendCode: '15:30'
  }, { text: '16:00', ariaLabel: 'sixteen hundred', backendCode: '16:00' }, {
    text: '16:30',
    ariaLabel: 'sixteen thirty',
    backendCode: '16:30'
  }, { text: '17:00', ariaLabel: 'seventeen hundred', backendCode: '17:00' }, {
    text: '17:30',
    ariaLabel: 'seventeen thirty',
    backendCode: '17:30'
  }, { text: '18:00', ariaLabel: 'eighteen hundred', backendCode: '18:00' }, {
    text: '18:30',
    ariaLabel: 'eighteen thirty',
    backendCode: '18:30'
  }, { text: '19:00', ariaLabel: 'nineteen hundred', backendCode: '19:00' }, {
    text: '19:30',
    ariaLabel: 'nineteen thirty',
    backendCode: '19:30'
  }, { text: '20:00', ariaLabel: 'twenty hundred', backendCode: '20:00' }, {
    text: '20:30',
    ariaLabel: 'twenty thirty',
    backendCode: '20:30'
  }, { text: '21:00', ariaLabel: 'twenty one hundred', backendCode: '21:00' }, {
    text: '21:30',
    ariaLabel: 'twenty one thirty',
    backendCode: '21:30'
  }, { text: '22:00', ariaLabel: 'twenty two hundred', backendCode: '22:00' }, {
    text: '22:30',
    ariaLabel: 'twenty two thirty',
    backendCode: '22:30'
  }, { text: '23:00', ariaLabel: 'twenty three hundred', backendCode: '23:00' }, {
    text: '23:30',
    ariaLabel: 'twenty three thirty',
    backendCode: '23:30'
  }]
};

renderer(Select, props);

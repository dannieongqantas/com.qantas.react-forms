import renderer from '../utils/renderer';
import Button from '../../src/Button/Button';
import './Button.scss';

renderer(Button, {
  className: 'button',
  text: 'Submit',
  ariaLabel: 'Hello',
  href: 's'
});

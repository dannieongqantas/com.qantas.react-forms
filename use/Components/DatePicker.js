import renderer from '../utils/renderer';
import DatePicker from '../../src/DatePicker/DatePicker';
import './DatePicker.scss';
import { currentDate, plusDays } from '../../src/utils/date';
import i18nData from '../../test/mock/i18n';

const i18n = { ...i18nData.shared, ...i18nData.car };

renderer(DatePicker, {
  i18n,
  ...i18n.pickupDate,
  children: 'Help',
  initialValue: plusDays(currentDate(), 9),
  onChange: value => console.log('date selected', value), // eslint-disable-line
  daysLimit: 90,
  isRangeEnabled: true
});

import renderer from '../utils/renderer';
import PeopleSelector from '../../src/PeopleSelector/PeopleSelector';
import props from '../data/PeopleSelector';
import './PeopleSelector.scss';

renderer(PeopleSelector, props);

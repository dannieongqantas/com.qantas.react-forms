/**
 *  Typeahead With Redux
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TYPEAHEAD_ID } from '../ActionTypes/locationActionTypes';
import { locationClearField, locationHandleChange, locationSelectItem } from '../Actions/locationActions';
import Typeahead from '../../src/Typeahead/Typeahead';

export class TypeaheadWithRedux extends Component { // eslint-disable-line
  static propTypes = {
    actionLocationClearField: PropTypes.func.isRequired,
    actionLocationHandleChange: PropTypes.func.isRequired,
    actionLocationSelectItem: PropTypes.func.isRequired,
    items: PropTypes.array
  };

  constructor(props) { // eslint-disable-line
    super(props);
  }

  render() {
    const {
      actionLocationClearField,
      actionLocationHandleChange,
      actionLocationSelectItem,
      items
    } = this.props;

    return (
      <Typeahead
        items={items}
        formLabel="Select a location"
        ariaLabel="Select a location"
        noResultsText="No results"
        onChange={value => actionLocationHandleChange(TYPEAHEAD_ID, value)}
        onSelectItem={value => actionLocationSelectItem(TYPEAHEAD_ID, value)}
        onClearField={() => actionLocationClearField(TYPEAHEAD_ID)}
        {...this.props} />
    );
  }
}

const mapStateToProps = (state) => ({
  checkinDatePicker: state.checkinDatePicker,
  checkoutDatePicker: state.checkoutDatePicker,
  guestSelector: state.guestSelector,
  i18n: state.i18n,
  value: state.value,
  items: state.locations,
  locations: state.locations,
  scripts: state.scripts,
  searchFormParams: state.searchFormParams,
  validation: state.validation
});

const mapDispatchToProps = {
  actionLocationClearField: locationClearField,
  actionLocationHandleChange: locationHandleChange,
  actionLocationSelectItem: locationSelectItem
};

export default connect(mapStateToProps, mapDispatchToProps)(TypeaheadWithRedux);

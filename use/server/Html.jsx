import React, { Component, PropTypes } from 'react';
import { PORT } from '../Constants';

export default class Html extends Component {
  static propTypes = {
    component: PropTypes.string.isRequired,
    ip: PropTypes.string.isRequired,
    html: PropTypes.string,
    props: PropTypes.string
  };

  render() {
    const { component, ip, html } = this.props;
    const fileName = component.split('.')[0];
    const src = `http://${ip}:${PORT + 1}/dist/${fileName}.js`;
    return <html >
      <head>
        <link rel="stylesheet" href={`/cssCache.css?cachebuser=${+new Date()}`} />
        <title>{component}</title>
        <script dangerouslySetInnerHTML={{__html: this.props.props}} id="data" />
        <script src={src} async />
      </head>
      <body>
      <div id="render" dangerouslySetInnerHTML={{__html: html}}></div>
      </body>
    </html>;
  }
}

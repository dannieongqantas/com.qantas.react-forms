/**
 * For dev only
 *
 * Retrieves the static.ss from aem-publisher of the dev branch. Every time this succeeds it stores the css in\
 * ${project}/tmp/static.css.
 *
 * If ${project}/tmp/static.css exists we return this directly but still do the request to aem-publisher to get the most
 * recent file for the next request...
 */

import request from 'superagent';
import fs from 'fs';

//Add multiple links so we get a rotation of the instances
const links = [
  'http://aem-publish-dev-a243-05-develop.qcpaws.qantas.com.au',
  'http://aem-publish-dev-a243-05-master.qcpaws.qantas.com.au',
  'http://aem-publish-dev-a243-05-callisto.qcpaws.qantas.com.au',
  'http://aem-publish-dev-a243-05-pricing.qcpaws.qantas.com.au'
];
let position = 0;
const rotateArray = function() {
  if (position === links.length) position = 0;
  return links[position++];
};

export default function (req, res) {

  const path = process.cwd() + '/tmp/';
  const cssFile = path + 'static.css';
  let alreadySent = false;

  res.setHeader('Content-Type', 'text/css');

  if (fs.existsSync(cssFile)) {
    res.send(fs.readFileSync(cssFile).toString());
    alreadySent = true;
  }

  request
    .get(rotateArray() + '/etc/designs/qantas/global/static.css')
    .end((err, data) => {
      if (err) {
        console.error(err);
        if (!alreadySent) res.send('* {}');
        return;
      }

      if (!fs.existsSync(path)) fs.mkdirSync(path);

      fs.writeFileSync(cssFile, data.text);

      if (!alreadySent) res.send(data.text);
    });

};

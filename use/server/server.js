import express from 'express';
import React from 'react';
import fs from 'fs';
import { renderToStaticMarkup } from 'react-dom/server';
import { PORT } from '../Constants';
import { renderToString } from 'react-dom/server';
import cssCache from './cssCache';
import Html from './Html';

const app = express();

require.extensions['.scss'] = (module, filename) => module._compile('', filename); //eslint-disable-line

app.get('/cssCache.css', cssCache);

app.get('/use/:component', (req, res) => {
  const { component } = req.params;
  const { server } = req.query;

  const host = req.get('host');
  const ip = host.split(':')[0];

  fs.readFile(`${process.cwd()}/use/data/${component}.js`, (err, data) => {
    const props = err ? '' : data.toString().replace('export default', 'var data =');

    let html = '';
    if (server) {
      const Component = require(`../Components/${component}`).default;
      html = renderToString(<Component />);
    }

    const markup = renderToStaticMarkup(<Html component={component} props={props} ip={ip} html={html} />);

    res
      .set({
        'content-type': 'text/html'
      })
      .send(`<!DOCTYPE HTML>${markup}`);
  });
});

app.use((req, res) => {
  res.send('');
});

app.listen(PORT, () => console.log(`Server started at locahost:${PORT}`));
